Web Application EVolution analysis tool 2
=========================================

This is the analysis tool developed to analyze web applications written in PHP. Check also our [wiki](https://bitbucket.org/pbkyriak/waev2/wiki/Home) for more detailed documentation. 

The Bundles
-----------

- BlogBundle
- CodeSimilarityBundle
- DeadCodeDetectorBundle
- DocBlockBundle
- FuncCallsBundle
- GitMinerBundle
- GlobalCodeBundle
- GraphRptBundle
- HomepageBundle
- MathBundle
- MediaBundle
- MetricsBundle
- MetronicBundle
- ParserBundle
- PDependBundle
- PhpdcdBundle
- RabbitMqBundle
- Reflection2Bundle
- ReflectionBundle
- SurvivalBundle
- TaskAggrBundle
- TaskConfigBundle
- TaskCoreBundle
- TaskTagBundle
- TypeAnalysisBundle
- UserBundle

Dependencies
------------

- Symfony full stack.
- knplabs/github-api
- jms/job-queue-bundle (deprecated)
- knplabs/knp-menu-bundle
- knplabs/knp-paginator-bundle
- nikic/php-parser
- apy/breadcrumbtrail-bundle

PHP installation notes:
-----------------------

Need to be updated, we are now php7 compatible and we prefer php7 because its really fast!

install latest php in ubuntu 
if  add-apt-repository is missing then

```
sudo apt-get install python-software-properties
```

after that run the commands: 

```
	sudo add-apt-repository ppa:ondrej/php5
	sudo apt-get update
	sudo apt-get upgrade
	sudo apt-get dist-upgrade
```

install php modules:

```
sudo apt-get install php-pear php5-dev make libpcre3-dev
sudo apt-get install php5-intl
sudo apt-get install php5-curl
sudo apt-get install php5-mysql
```

install apc
```
sudo apt-get install php-apc
```

```
sudo nano /etc/php5/apache2/php.ini
```

add to the end the extension:

```
extention=apc.so
```

restart apache
```
sudo service apache2 restart
```

Enable apache mod rewrite
```
sudo a2enmod rewrite
```

Waev required tools installation commands

```
sudo wget https://phar.phpunit.de/phpdcd.phar (deprecated)
sudo wget http://static.pdepend.org/php/1.1.1/pdepend.phar (deprecated)
sudo chmod +x phpdcd.phar (deprecated)
sudo chmod +x pdepend.phar (deprecated)
sudo mv phpdcd.phar /usr/local/bin/phpdcd (deprecated)
sudo mv pdepend.phar /usr/local/bin/pdepend (deprecated)
sudo apt-get install supervisor
```

mount host disk on my vbox

```
sudo mount -t vboxsf projects ./project-data/
```