<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            
            new JMS\JobQueueBundle\JMSJobQueueBundle(),
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),
            new APY\BreadcrumbTrailBundle\APYBreadcrumbTrailBundle(),
            
            new Slx\UserBundle\SlxUserBundle(),
            new Slx\MetronicBundle\SlxMetronicBundle(),
            new Slx\GitMinerBundle\SlxGitMinerBundle(),
            new Slx\GraphRptBundle\SlxGraphRptBundle(),
            new Slx\HomepageBundle\SlxHomepageBundle(),
            new Slx\MediaBundle\SlxMediaBundle(),
            new Slx\BlogBundle\SlxBlogBundle(),
            new Slx\TaskCoreBundle\SlxTaskCoreBundle(),
            new Slx\TaskConfigBundle\SlxTaskConfigBundle(),
            new Slx\TaskTagBundle\SlxTaskTagBundle(),
            new Slx\TaskAggrBundle\SlxTaskAggrBundle(),
            new Slx\ReflectionBundle\SlxReflectionBundle(),
            new Slx\PDependBundle\SlxPDependBundle(),
            new Slx\PhpdcdBundle\SlxPhpdcdBundle(),
            new Slx\SurvivalBundle\SlxSurvivalBundle(),
            new Slx\MathBundle\SlxMathBundle(),
            new Slx\FuncCallsBundle\SlxFuncCallsBundle(),
            new Slx\GlobalCodeBundle\SlxGlobalCodeBundle(),
            new Slx\MetricsBundle\SlxMetricsBundle(),
            new Slx\DeadCodeDetectorBundle\SlxDeadCodeDetectorBundle(),
            new Slx\RabbitMqBundle\SlxRabbitMqBundle(),
            new Slx\Reflection2Bundle\SlxReflection2Bundle(),
            new Slx\DocBlockBundle\SlxDocBlockBundle(),
            new Slx\ParserBundle\SlxParserBundle(),
            new Slx\TypeAnalysisBundle\SlxTypeAnalysisBundle(),
            new Slx\CodeSimilarityBundle\SlxCodeSimilarityBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
