*.txt
.git
deps
README.md
*.*~
- app/cache/*
- app/logs/*
- web/.htaccess
- app/config/parameters.yml
- web/bundles/slxmetronic/img/logo-big.png
- web/bundles/slxmetronic/img/logo.png
- web/uploads/project-data/*
