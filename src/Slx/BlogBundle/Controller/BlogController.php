<?php

namespace Slx\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Slx\BlogBundle\Entity\BlogPost;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Blog",route="slx_blog_posts")
 * @CurrentMenuItem("slx_blog_posts")
 */
class BlogController extends Controller
{
    
    public function recentPostsAction() {
        $posts = $this->getDoctrine()->getManager()->getRepository("SlxBlogBundle:BlogPost")->getRecentPosts(4);
        return $this->render('SlxBlogBundle:Blog:recentPosts.html.twig', array('posts'=>$posts));
    }
    
    public function postsAction() {
        $request = $this->get('request');
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT p FROM SlxBlogBundle:BlogPost p ORDER BY p.created_at DESC";
        $query = $em->createQuery($dql);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 5)
        );

        return $this->render('SlxBlogBundle:Blog:posts.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => null
        ));

    }
    
    public function postAction($id) {
        $post = $this->getDoctrine()->getManager()->getRepository('SlxBlogBundle:BlogPost')->find($id);
        if(!$post) {
            throw $this->createNotFoundException('Unable to find SlxBlogBundle:BlogPost entity.');
        }
        $this->get("apy_breadcrumb_trail")->add($post->getTitle());
        return $this->render('SlxBlogBundle:Blog:post.html.twig',
                array(
                'post' => $post
        ));
        
    }
}
