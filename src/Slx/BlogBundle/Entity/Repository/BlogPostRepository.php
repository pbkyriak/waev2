<?php

namespace Slx\BlogBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\BlogBundle\Entity\BlogPost;

/**
 * Description of BlogPostRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class BlogPostRepository  extends EntityRepository
{
    public function getRecentPosts($limit) {
        $posts = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('SlxBlogBundle:BlogPost', 'p')
            ->orderBy('p.created_at', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
        return $posts;
    }
 
}
