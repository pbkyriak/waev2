<?php

namespace Slx\BlogBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BlogPostType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $mrepo = new \Slx\MediaBundle\Lib\MediaRepository();
        $images = $mrepo->getImageList();
        $choices = array();
        foreach($images as $image) {
            $choices[$image['name']] = $image['name'];
        }
        
        $builder
            ->add('title', null, array('label'=> 'blogpost.edit.title'))
            ->add('content', null, array('label'=> 'blogpost.edit.content', 'attr'=>array('rows'=>15)))
            ->add('image', 'choice', array('choices'=>$choices, 'label'=>'blogpost.edit.image'))
            ->add('author', 'slx_userbundle_username', array('label'=> 'blogpost.edit.author'))
            ->add('tags', null, array('label'=> 'blogpost.edit.tags'))
            ->add('excerpt', null, array('label'=> 'blogpost.edit.excerpt', 'attr'=>array('rows'=>3)))
            ->add('save', 'submit', array('label'=>'blogpost.edit.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'blogpost.edit.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\BlogBundle\Entity\BlogPost'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_blogbundle_blogpost';
    }
}
