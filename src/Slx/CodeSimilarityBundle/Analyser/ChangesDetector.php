<?php

namespace Slx\CodeSimilarityBundle\Analyser;

use Slx\CodeSimilarityBundle\PrettyPrint\FuzzyPrettyPrinter;
use SebastianBergmann\Diff\Differ;
use SebastianBergmann\Diff\Parser;
use Slx\MathBundle\Util\StopWatch;

/**
 * 
 * @deprecated due perfomance issue. Use TagCodeSimilarityTask instead.
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 24, 2016
 */
class ChangesDetector {

    private $em;
    private $fhandle;
    private $metricGroupId = 251;
    private $stopwatch;
    private $rootDir;
    private $builder;
    
    public function __construct($em, $rootDir, $metricGroupId) {
        $this->em = $em;
        $this->rootDir = $rootDir;
        $this->metricGroupId = $metricGroupId;
    }

    public function setMethodDataQueryBuilder( $builder ) {
        $this->builder = $builder;
    }
    
    public function analyse($projectId) {
        $this->stopwatch = new StopWatch();
        $datafile = tempnam($this->rootDir."/web/uploads/tmp/", 'metrics');
        printf("output file:%s\n", $datafile);
        $this->fhandle = fopen($datafile, "w");
        if ($this->fhandle) {
            $this->doAnalyse4($projectId);
            fclose($this->fhandle);
            $this->importData($datafile);
        }
        print_R($this->stopwatch->getTimers());
    }

    public function doAnalyse3($projectId) {
        $projectTags = $this->em->getRepository("SlxGitMinerBundle:Project")->find($projectId)->getProjectTags();
        $currTag = null;
        $nextTag = null;
        $metrics = $this->getMetricNames();
        $gmquery = $this->builder->getQuery();
        print_R($metrics);
        printf("\n\n-----------------\n");
        $this->stopwatch->reset();
        $tagCnt = 0;
        foreach ($projectTags as $projectTag) {
            $nextTag = $projectTag;
            if ($currTag) {
                $this->stopwatch->start('tag');
                $this->cleanExistingData($currTag->getId());
                $sums = array_fill_keys($metrics, 0);
                $counts = array_fill_keys($metrics, 0);
                $countM = array_fill_keys($metrics, 0);
                $count = 0;
                $methods = $this->getMethodData($gmquery, $currTag->getId());
                foreach ($methods as $method => $mdata) {
                    $cfId = $mdata['file_id'];
                    $nfId = $this->getFileIdOfMethod($nextTag->getId(), $method);
                    if ($cfId && $nfId ) {
                        $distance = $this->getMethodDistance($currTag->getId(), $cfId, $nextTag->getId(), $nfId, $method);
                        // calculate metrics
                        foreach($metrics as $fld=>$metric) {
                            if($mdata[$fld]!=0) {
                                $sums[$metric] += $distance;
                                $counts[$metric] += $mdata[$fld];
                                $countM[$metric]++;
                                $this->storeMyMetric($currTag->getId(), $metric, $distance, 'method', $method);
                            }
                        }
                        $count++;
                    } 
                }
                // total tag metrics
                $this->doTagMetricsTotals($metrics, $sums, $currTag, $count, $counts, $countM);
                $this->stopwatch->stop('tag');
                
                $this->dumpUserInfo($currTag, $tagCnt, $methods);
                $tagCnt++;
                $this->stopwatch->reset();
                //break;
            }
            $currTag = $projectTag;
        }
    }
       
    public function doAnalyse4($projectId) {
        $projectTags = $this->em->getRepository("SlxGitMinerBundle:Project")->find($projectId)->getProjectTags();
        $currTag = null;
        $nextTag = null;
        $metrics = $this->getMetricNames();
        $gmquery = $this->builder->getQuery();
        print_R($metrics);
        printf("\n\n-----------------\n");
        $this->stopwatch->reset();
        $tagCnt = 0;
        foreach ($projectTags as $projectTag) {
            $nextTag = $projectTag;
            if ($currTag) {
                $this->stopwatch->start('tag');
                $this->cleanExistingData($currTag->getId());
                $sums = array_fill_keys($metrics, 0);
                $counts = array_fill_keys($metrics, 0);
                $countM = array_fill_keys($metrics, 0); // number of methods included to metric
                $count = 0; // total number of methods
                $methods = $this->getMethodData($gmquery, $currTag->getId());
                foreach ($methods as $method => $mdata) {
                    $cfId = $mdata['file_id'];
                    $nfId = $this->getFileIdOfMethod($nextTag->getId(), $method);
                    if ($cfId && $nfId ) {
                        $distance = $this->getMethodDistance($currTag->getId(), $cfId, $nextTag->getId(), $nfId, $method);
                        // calculate metrics
                        foreach($metrics as $fld=>$metric) {
                            $inc = false;
                            if( strpos($metric,'irregular')===false ) { // regular
                                $inc = ($mdata[$fld]!=0 && $this->getOthersSum($mdata, $metrics, $fld)==0);
                            }
                            else {  // irregular
                                $inc = ($mdata[$fld]!=0);
                            }
                            if($inc) {
                                $sums[$metric] += $distance;
                                $counts[$metric] += $mdata[$fld];
                                $countM[$metric]++;
                                $this->storeMyMetric($currTag->getId(), $metric, $distance, 'method', $method);
                            }
                        }
                        $count++;
                    } 
                }
                // total tag metrics
                $this->doTagMetricsTotals($metrics, $sums, $currTag, $count, $counts, $countM);
                $this->stopwatch->stop('tag');
                
                $this->dumpUserInfo($currTag, $tagCnt, $methods);
                $tagCnt++;
                $this->stopwatch->reset();
                //break;
            }
            $currTag = $projectTag;
        }
    }
       
    private function getOthersSum($mdata, $metrics, $exclFld) {
        $sum =0 ;
        foreach($metrics as $fld => $m) {
            if( $fld!=$exclFld) {
                $sum+= $mdata[$fld];
            }
        }
        return $sum;
    }
    /**
     * Retrieves pretty printed code of the two versions of the method and 
     * calculates the distance from first to second version.
     * 
     * @param integer $cTagId   first tag id
     * @param integer $cfId     first file id
     * @param integer $nTagId   second tag id
     * @param integer $nfId     second file id
     * @param string $method    method name to compare
     * @return float            the distance from first to second version
     */
    private function getMethodDistance($cTagId, $cfId, $nTagId, $nfId, $method) {
        // get method's pretty printed code
        $this->stopwatch->start('print');
        $currCode = $this->getMethodFuzzyCodeFromDB($cTagId, $cfId, $method);
        $nextCode = $this->getMethodFuzzyCodeFromDB($nTagId, $nfId, $method);
        $this->stopwatch->stop('print');
        // calculate distance
        $this->stopwatch->start('diff');
        $distance = $this->calculateDistance($currCode, $nextCode);
        $this->stopwatch->stop('diff');
        return $distance;
    }
    
    /**
     * Calculate and store tag metrics totals
     * 
     * @param array $metrics
     * @param array $sums
     * @param ProjectTag $currTag
     * @param integer $count
     */
    private function doTagMetricsTotals($metrics, $sums, $currTag, $count, $counts, $countM) {
        // total tag metrics
        $distances = array_fill_keys(array_values($metrics), -1);
        if ($count != 0) {
            foreach($metrics as $metric) {
                $distances[$metric] = 0;
                if( $countM[$metric]!=0 ) {
                    $distances[$metric] = ($sums[$metric] / $countM[$metric]);
                }
            }
        }
        foreach($metrics as $metric) {
            if( isset($distances[$metric]) && $distances[$metric]!=-1 ) {
                $this->storeMyMetric($currTag->getId(), $metric, $distances[$metric], 'tag', 'tag');
            }
        }        
        foreach($countM as $metric => $cnt) {
            $this->storeMyMetric($currTag->getId(), 'count_'.$metric, $cnt, 'tag', 'tag');
        }
    }
    
    /**
     * Display some info to the user about the progress of the calculations
     * 
     * @param type $currTag
     * @param integer $tagCnt
     */
    private function dumpUserInfo($currTag, $tagCnt, $methods) {
        $tim = '';
        foreach ($this->stopwatch->getTimers() as $k => $v) {
            $tim .= sprintf(" [%s=%s] ", $k, $v);
        }
        printf("%s. %s, method count=%s timers=%s\n", $tagCnt, $currTag->getId(), count($methods), $tim);
        
    }
    
    /**
     * Construct metric names from builder's fields names
     * 
     * @return array
     */
    private function getMetricNames() {
        $flds = $this->builder->getColNames();
        $metrics = array();
        foreach($flds as $fld) {
            $metrics[$fld] = str_replace('count', 'similarity', $fld);
        }
        return $metrics;
    }
    
    /**
     * Return method data
     * 
     * @param integer $tagId
     * @return array
     */
    private function getMethodData($gmquery, $tagId) {
        $stmt = $this->em
                ->getConnection()
                ->executeQuery($gmquery, ['tid' => $tagId]);
        $m1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $out = array();
        if ($m1) {
            foreach ($m1 as $m1r) {
                $out[$m1r['cname']] = $m1r;
            }
        }

        return $out;
    }

    /**
     * Get the file_id where the method lives
     * 
     * @param integer $tagId
     * @param string $method
     * @return integer
     */
    private function getFileIdOfMethod($tagId, $method) {
        $out = 0;
        $q = "SELECT project_file_id FROM project_func_signature WHERE project_tag_id=:tid AND namespaced=:meth";
        $stmt = $this->em
                ->getConnection()
                ->executeQuery($q, ['tid' => $tagId, 'meth' => $method]);
        $result = $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
        if ($result) {
            $out = $result[0];
        }
        return $out;
    }

    /**
     * Get pretty printed code of the method that lives in the file_id
     * @param integer $tagId
     * @param string $fileId
     * @param string $method
     * @return string
     */
    private function getMethodFuzzyCodeFromDB($tagId, $fileId, $method) {
        $out = '';
        $q = "SELECT pretty_code FROM project_method_pretty_code WHERE project_tag_id=:tid and project_file_id=:fid and method=:m";
        $stmt = $this->em->getConnection()->executeQuery($q, ['tid' => $tagId, 'fid' => $fileId, 'm' => $method]);
        $result = $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
        if ($result) {
            $out = gzdecode($result[0]);
        }
        return $out;
    }

    /**
     * Employs Bergmann's Diff to calculate the distance from str1 to str2
     * 
     * @param string $str1
     * @param string $str2
     * @return int
     */
    private function calculateDistance($str1, $str2) {
        $out = 0;
        if (sha1($str1) == sha1($str2)) {
            return 1;
        }
        $codeLines1 = count(preg_split('(\r\n|\r|\n)', $str1));
        $codeLines2 = count(preg_split('(\r\n|\r|\n)', $str2));
        $differ = new Differ(''); //'',true);
        $diff = $differ->diff($str1, $str2);
        $lines = preg_split('(\r\n|\r|\n)', $diff);
        $marks = ['+' => 0, '-' => 0];
        foreach ($lines as $line) {
            $mark = substr($line, 0, 1);
            if ($mark == '+' || $mark == '-') {
                $marks[$mark] ++;
            }
        }
        //$out = round((($marks['+'] / $codeLines2 + $marks['-'] / $codeLines1) / 2), 3);
        $out = round( ($marks['+'] + $marks['-']) / ($codeLines1+$codeLines2), 3);
        return $out;
    }

    /**
     * Stores metric data 
     * 
     * @param integer $tagId
     * @param string $metricName
     * @param float $metric
     * @param string $ctype
     * @param string $cname
     */
    private function storeMyMetric($tagId, $metricName, $metric, $ctype, $cname) {
        $data = array(
            'project_tag_id' => $tagId,
            'ctype' => $ctype,
            'cname' => $this->em->getConnection()->quote($cname, \PDO::PARAM_STR),
            'metric_name' => $metricName,
            'metric' => $metric,
            'metric_group_id' => $this->metricGroupId,
        );
        $this->writeToDatafile($data);
    }

    /**
     * Writes data to tmp csv file that holds all metric data and then it will be loaded to the db using LOAD DATA infile command
     * @param array $data
     */
    private function writeToDatafile($data) {
        $data['cname'] = substr($data['cname'], 1, strlen($data['cname']) - 2);
        fputcsv($this->fhandle, $data, "\t");
    }

    /**
     * Imports data to DB using LOAD DATA infile statement
     * @param string $datafile
     */
    private function importData($datafile) {
        printf("loading data from file to database...\n");
        $q = "load data local infile '%s' ignore into table project_metric fields terminated by '\\t' optionally enclosed by '\"' (project_tag_id, ctype, cname,metric_name, metric,metric_group_id);";
        $q = sprintf($q, $datafile);
        $this->stopwatch->start('db');
        for ($tt = 0; $tt < 3; $tt++) {
            try {
                $this->em->getConnection()->executeQuery($q);
                break;
            } catch (\PDOException $ex) {
                printf("pdo error %s\n Will retry (%s) in 10 seconds.\n", $tt + 1, $ex->getMessage());
                sleep(10);
            }
        }
        unlink($datafile);
        $this->stopwatch->stop('db');
    }

    /**
     * Deletes records from project_metric table for a project_metric_group_id
     * 
     * @param integer $tagId
     */
    private function cleanExistingData($tagId) {
        $this->em->createQuery("DELETE FROM SlxMetricsBundle:ProjectMetric a WHERE a.projectTag=:tagid and a.metric_group_id=:mgid ")
                ->setParameter('tagid', $tagId)
                ->setParameter('mgid', $this->metricGroupId)
                ->execute();
        $this->em->flush();
    }

}
