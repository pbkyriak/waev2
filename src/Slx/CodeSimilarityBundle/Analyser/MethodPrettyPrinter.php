<?php

namespace Slx\CodeSimilarityBundle\Analyser;


use Slx\CodeSimilarityBundle\PrettyPrint\FuzzyPrettyPrinter;
use SebastianBergmann\Diff\Differ;
use SebastianBergmann\Diff\Parser;
use Slx\MathBundle\Util\StopWatch;

/**
 * Description of MethodPrettyPrinter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 6, 2016
 */
class MethodPrettyPrinter {

    private $em;
    private $extractors;
    private $fhandle;
    private $stopwatch;
    private $rowsBuffer = array();
    private $ctype = 'method';
    public function __construct($em, $ctype) {
        $this->em = $em;
        $this->ctype = $ctype;
    }

    public function addExtractor($extractor) {
        $this->extractors = $extractor;
    }

    public function analyse($projectId) {
        $this->stopwatch = new StopWatch();
        /*
        $datafile = tempnam("/var/www/waev2/web/uploads/tmp/", 'metrics');
        printf("output file:%s\n", $datafile);
        $this->fhandle = fopen($datafile, "w");
        if ($this->fhandle) {
            $this->doAnalyse($projectId);
            fclose($this->fhandle);
            $this->importData($datafile);
        }
         * 
         */
        $this->doPrettyPrint($projectId);
        print_R($this->stopwatch->getTimers());
    }

    public function doPrettyPrint($projectId) {
        $projectTags = $this->em->getRepository("SlxGitMinerBundle:Project")->find($projectId)->getProjectTags();
        $currTag = null;
        printf("\n\n-----------------\n");
        $this->stopwatch->reset();
        $tagIdx = 0;
        printf("Total tags = %s\n", count($projectTags));
        foreach ($projectTags as $projectTag) {
            //if( $projectTag->getId()<4646 ) {   // temporary for testing
            //    continue;
            //}
            $currTag = $projectTag;
            $this->stopwatch->start('tag');
            
            $count=0;
            $exoticMethods = $this->getTagAllMethods($currTag->getId(), $this->ctype);
            printf("currTag=%s total=%s \n", $currTag->getId(), count($exoticMethods));
            foreach ($exoticMethods as $method => $cfId) {
                if ($cfId) {
                    $currCode = $this->getMethodFuzzyCode($currTag, $cfId, $method);
                    $count++;
                }
            }
            $this->processRowBuffer();
            $this->stopwatch->stop('tag');
            $tim='';
            foreach($this->stopwatch->getTimers() as $k => $v) {
                $tim .= sprintf(" %s=%s ", $k,$v);
            }
            printf("%s. %s, %s, %s\n", 
                    $tagIdx,
                    $currTag->getId(), 
                    $count, 
                    $tim
                    );
            $tagIdx++;
            $this->stopwatch->reset();
            //break;
        }
    }
    private function getTagAllMethods($tagId, $ctype) {
        //$q0= "update project_func_signature set namespaced=CONCAT(project_func_signature.namespace,'\\',project_func_signature.fname) where project_tag_id=:tid";
        //$this->em->getConnection()->executeQuery($q0, ['tid' => $tagId]);
        $q1 = "SELECT  
                cname , pfs.project_file_id as file_id
              FROM
                project_metric pm 
                LEFT JOIN project_func_signature pfs
                ON (pfs.project_tag_id=pm.project_tag_id AND pm.cname=pfs.namespaced)
              WHERE ctype = '$ctype' 
                AND metric_group_id IN (2, 3) 
                AND pm.`project_tag_id` = :tid
                AND NOT ISNULL(pfs.project_file_id)
              GROUP BY cname
              ORDER BY pfs.project_file_id
        ";
        $stmt = $this->em
                ->getConnection()
                ->executeQuery($q1, ['tid' => $tagId]);
        $m1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $out = array();
        if( $m1 ) {
            foreach($m1 as $m1r) {
                $out[$m1r['cname']] = $m1r['file_id'];
            }
        }
        return $out;
    }

    private function getMethodFuzzyCode($tag, $fileId, $method) {
        $this->stopwatch->start('dbq1');
        $isInDD = $this->hasMethodFuzzyCodeInDB($tag->getId(), $fileId, $method);
        $this->stopwatch->stop('dbq1');
        $code = '';
        if (!$isInDD) {
            $this->stopwatch->start('ast');
            $ast = $this->extractors->getMethodAST($tag, $fileId, $method);
            $this->stopwatch->stop('ast');
            if ($ast) {
                $this->stopwatch->start('print');
                $printer = new FuzzyPrettyPrinter();
                $code = $printer->prettyPrint($ast);
                $this->stopwatch->stop('print');
                $this->stopwatch->start('dbq2');
                $this->setMethodFuzzyCodeToDB($tag->getId(), $fileId, $method, $code);
                $this->stopwatch->stop('dbq2');
            }
        }
        return $code;
    }

    private function hasMethodFuzzyCodeInDB($tagId, $fileId, $method) {
        $out = false;
        $q = "SELECT count(*) as cnt FROM project_method_pretty_code WHERE project_tag_id=:tid and project_file_id=:fid and method=:m";
        $stmt = $this->em->getConnection()->executeQuery($q, ['tid' => $tagId, 'fid' => $fileId, 'm' => $method]);
        $result = $stmt->fetchAll(\PDO::FETCH_COLUMN, 0);
        if ($result) {
            $out = ($result[0]>0);
        }
        return $out;
    }

    private function setMethodFuzzyCodeToDB($tagId, $fileId, $method, $code) {
        $this->rowsBuffer[] = [
                        'project_tag_id' => $tagId,
                        'project_file_id' => $fileId,
                        'method' => $method,
                        'pretty_code' => gzencode($code),
                    ];
        if( count($this->rowsBuffer)> 20 ) {
            $this->processRowBuffer();
        }
    }

    private function processRowBuffer() {        
        $conn = $this->em->getConnection();
        $fields = 'project_tag_id, project_file_id, method, pretty_code';
        $values = [];
        foreach($this->rowsBuffer as $row) {
            $values[] = sprintf(" (%s, %s, %s, %s) ",
                    $row['project_tag_id'],
                    $row['project_file_id'],
                    $conn->quote($row['method']),
                    $conn->quote($row['pretty_code'])
                    );
        }
        if( count($values) ) {
            $q = "INSERT INTO project_method_pretty_code ({$fields}) VALUES ". implode(',', $values);
            unset($values);
            $this->rowsBuffer = [];
            try {
                $conn->executeQuery($q);
            }
            catch(\PDOException $ex) {
                if( $ex->getCode()=='23000' ) {
                    printf("Duplicate entry in pretty_code insert\n");
                }
            }
        }
    }

    private function writeToDatafile($data) {
        $data['cname'] = substr($data['cname'], 1, strlen($data['cname']) - 2);
        fputcsv($this->fhandle, $data, "\t");
    }
    
    private function importData($datafile) {
        printf("loading data from file to database...\n");
        $q = "load data local infile '%s' ignore into table project_method_pretty_code fields terminated by '\\t' optionally enclosed by '\"' (project_tag_id, project_file_id, method, pretty_code);";
        $q = sprintf($q, $datafile);
        $this->stopwatch->start('db');
        for ($tt = 0; $tt < 3; $tt++) {
            try {
                $this->em->getConnection()->executeQuery($q);
                break;
            } catch (\PDOException $ex) {
                printf("pdo error %s\n Will retry (%s) in 10 seconds.\n", $tt + 1, $ex->getMessage());
                sleep(10);
            }
        }
        unlink($datafile);
        $this->stopwatch->stop('db');
    }

}
