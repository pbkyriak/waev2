<?php

namespace Slx\CodeSimilarityBundle\Analyser;

/**
 * Description of QueryBuilderMeth
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 15, 2016
 */
class QueryBuilderMeth {
    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    private $objPatterns;
    private $methPatterns;
    private $ambiguity;
    
    public function __construct($em, $mgi) {
        $this->em = $em;
        $this->objPatterns = $mgi->getObjPatterns();
        $this->methPatterns = $mgi->getMethPatterns();
        $this->ambiguity = $mgi->getAmbiguity();
        
    }
     
    private function getLiveMethPatterns() {
        $q1 = "SELECT SUBSTRING_INDEX(pm.metric_name,':',2) AS mg,pm.metric_name AS mm, SUM(pm.metric) AS cnt, COUNT(DISTINCT pt.project_id) AS pcnt
                FROM project_metric pm
                LEFT JOIN project_tag pt ON (pm.project_tag_id=pt.id) 
                WHERE pm.metric_group_id=2 
                AND pm.ctype='tag' 
                GROUP BY mm
                ORDER BY mg,cnt DESC, mm ;";
        $stmt = $this->em
                ->getConnection()
                ->executeQuery($q1);
        $m1 = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $m1;
    }
    
    private function getGroupedPatterns() {
        $grouped = array();
        $patternRows = $this->getLiveMethPatterns();
        foreach($patternRows as $patternRow) {
            $parts = explode(":", $patternRow['mm']);
            if( isset($this->objPatterns[$parts[2]]) && isset($this->methPatterns[$parts[3]]) ) {
                $pattern = sprintf("%s.%s", $this->objPatterns[$parts[2]],$this->methPatterns[$parts[3]]);
                $ambiguity = sprintf("%s%s", $this->ambiguity[$this->objPatterns[$parts[2]]],$this->ambiguity[$this->methPatterns[$parts[3]]]);
                if( !isset($grouped[$ambiguity])) {
                    $grouped[$ambiguity] = array();
                }
                if( !isset($grouped[$ambiguity][$pattern])) {
                    $grouped[$ambiguity][$pattern] = $patternRow['mm'];
                }
            }
        }
        return $grouped;
        
    }
    
    private function getFields() {
        $grouped = $this->getGroupedPatterns();
        $fields = array();
        foreach($grouped as $amb => $patterns) {
            $in = implode("','",$patterns);
            $colName = sprintf("count%s", $amb);
            if( $in ) {
                $in = sprintf("SUM(IF( metric_name IN ('%s'), 1,0)) AS %s", $in, $colName);
            }
            else {
                $in = sprintf("0 AS %s", $colName);
            }
            $fields[$colName] = $in;
        }
        return $fields;
    }
    
    public function getQuery() {
        $fields = $this->getFields();
        $fieldStr = implode(",\n", $fields);
        
        $q1 = "SELECT DISTINCT 
                cname,
                COUNT(*) AS ctotal,
                pfs.project_file_id AS file_id,
                $fieldStr
              FROM
                project_metric pm 
                LEFT JOIN project_tag pt 
                  ON (pm.project_tag_id = pt.id) 
                LEFT JOIN project_func_signature pfs 
                  ON (
                    pfs.project_tag_id = pm.project_tag_id 
                    AND pm.cname = pfs.namespaced
                  ) 
              WHERE ctype = 'method'   
                AND pm.metric_group_id=2
                AND pm.project_tag_id = :tid
                AND NOT ISNULL(pfs.project_file_id) 
              GROUP BY cname 
              ORDER BY cname ;";
        return $q1;
    }

    public function getColNames() {
        $fields = $this->getFields();
        return array_keys($fields);
    }
}
