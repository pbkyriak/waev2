<?php

namespace Slx\CodeSimilarityBundle\AnalysisTask\AggrTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;
/**
 * Used only for object patterns. For method patterns use TagCodeSimilarity task
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 2, 2016
 */
class CodeSimilarityTask extends AbstractTask {

    /** @var Project */
    protected $project;
    protected $projectId;
    private $analyser;
    
    final public function configure($options) {
        $this->projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        return true;
    }

    public function setDetector($analyser) {
        $this->analyser = $analyser;
    }
    
    public function execute() {
        $this->analyser->analyse($this->project->getId());
        return true;
    }

    public function getTaskName() {
        return 'Code similarity calculator';
    }

    public function onFail() {
        return true;
    }

}
