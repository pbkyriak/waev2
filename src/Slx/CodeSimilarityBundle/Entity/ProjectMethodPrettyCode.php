<?php
namespace Slx\CodeSimilarityBundle\Entity;
/**
 * Description of ProjectMethodPrettyCode
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 2, 2016
 */
class ProjectMethodPrettyCode {

    private $id;
    private $project_tag_id;
    private $project_file_id;
    private $method;
    private $pretty_code;
    
    public function getId() {
        return $this->id;
    }
    
    public function setId($v) {
        $this->id=$v;
    }
    
    
    
    public function setProjectTagId($v) {
        $this->project_tag_id=$v;
    }
    public function getProjectTagId() {
        return $this->project_tag_id;
    }
    
    public function setProjectFileId($v) {
        $this->project_file_id=$v;
    }
    public function getProjectFileId() {
        return $this->project_file_id;
    }
    
    public function setMethod($v) {
        $this->method=$v;
    }
    public function getMethod() {
        return $this->method;
    }
    
    public function setPrettyCode($v) {
        $this->pretty_code=$v;
    }
    public function getPrettyCode() {
        return $this->pretty_code;
    }

}
