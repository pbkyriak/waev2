<?php
namespace Slx\CodeSimilarityBundle\PrettyPrint;

use PHPParser_Node_Expr;
use PHPParser_Node_Expr_ArrayDimFetch;
use PHPParser_Node_Expr_BitwiseAnd;
use PHPParser_Node_Expr_BitwiseNot;
use PHPParser_Node_Expr_BitwiseOr;
use PHPParser_Node_Expr_BitwiseXor;
use PHPParser_Node_Expr_BooleanAnd;
use PHPParser_Node_Expr_BooleanNot;
use PHPParser_Node_Expr_BooleanOr;
use PHPParser_Node_Expr_Cast_Array;
use PHPParser_Node_Expr_Cast_Bool;
use PHPParser_Node_Expr_Cast_Double;
use PHPParser_Node_Expr_Cast_Int;
use PHPParser_Node_Expr_Cast_Object;
use PHPParser_Node_Expr_Cast_String;
use PHPParser_Node_Expr_Cast_Unset;
use PHPParser_Node_Expr_ClassConstFetch;
use PHPParser_Node_Expr_Concat;
use PHPParser_Node_Expr_ConstFetch;
use PHPParser_Node_Expr_Div;
use PHPParser_Node_Expr_Equal;
use PHPParser_Node_Expr_ErrorSuppress;
use PHPParser_Node_Expr_Greater;
use PHPParser_Node_Expr_GreaterOrEqual;
use PHPParser_Node_Expr_Identical;
use PHPParser_Node_Expr_Instanceof;
use PHPParser_Node_Expr_LogicalAnd;
use PHPParser_Node_Expr_LogicalOr;
use PHPParser_Node_Expr_LogicalXor;
use PHPParser_Node_Expr_Minus;
use PHPParser_Node_Expr_Mod;
use PHPParser_Node_Expr_Mul;
use PHPParser_Node_Expr_NotEqual;
use PHPParser_Node_Expr_NotIdentical;
use PHPParser_Node_Expr_Plus;
use PHPParser_Node_Expr_PostDec;
use PHPParser_Node_Expr_PostInc;
use PHPParser_Node_Expr_PreDec;
use PHPParser_Node_Expr_PreInc;
use PHPParser_Node_Expr_PropertyFetch;
use PHPParser_Node_Expr_ShiftLeft;
use PHPParser_Node_Expr_ShiftRight;
use PHPParser_Node_Expr_Smaller;
use PHPParser_Node_Expr_SmallerOrEqual;
use PHPParser_Node_Expr_StaticPropertyFetch;
use PHPParser_Node_Expr_UnaryMinus;
use PHPParser_Node_Expr_UnaryPlus;
use PHPParser_Node_Expr_Variable;
use PHPParser_Node_Scalar_DNumber;
use PHPParser_Node_Scalar_Encapsed;
use PHPParser_Node_Scalar_LNumber;
use PHPParser_Node_Scalar_String;
use PHPParser_PrettyPrinter_Default;

/**
 * Description of FuzzyPrettyPrinter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 20, 2016
 */
class FuzzyPrettyPrinter extends PHPParser_PrettyPrinter_Default {
    
    const VARIABLE = 'VARIABLE';
    const BINARY_OPERATOR = 'OPER';
    const UNARY_OPERATOR = 'UOPER';
    const LITERAL = 'LITERAL';
    
    // variables
    public function pExpr_Variable(PHPParser_Node_Expr_Variable $node) {
        if ($node->name instanceof PHPParser_Node_Expr) {
            return '${' . $this->p($node->name) . '}';
        } else {
            return self::VARIABLE;
        }
    }

    // Scalars

    public function pScalar_String(PHPParser_Node_Scalar_String $node) {
        return self::LITERAL;
    }

    public function pScalar_Encapsed(PHPParser_Node_Scalar_Encapsed $node) {
        return self::LITERAL;
    }

    public function pScalar_LNumber(PHPParser_Node_Scalar_LNumber $node) {
        return self::LITERAL;
    }

    public function pScalar_DNumber(PHPParser_Node_Scalar_DNumber $node) {
        return self::LITERAL;
    }

    // Binary expressions

    public function pExpr_Plus(PHPParser_Node_Expr_Plus $node) {
        return $this->pInfixOp('Expr_Plus', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Minus(PHPParser_Node_Expr_Minus $node) {
        return $this->pInfixOp('Expr_Minus', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Mul(PHPParser_Node_Expr_Mul $node) {
        return $this->pInfixOp('Expr_Mul', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Div(PHPParser_Node_Expr_Div $node) {
        return $this->pInfixOp('Expr_Div', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Concat(PHPParser_Node_Expr_Concat $node) {
        return $this->pInfixOp('Expr_Concat', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Mod(PHPParser_Node_Expr_Mod $node) {
        return $this->pInfixOp('Expr_Mod', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_BooleanAnd(PHPParser_Node_Expr_BooleanAnd $node) {
        return $this->pInfixOp('Expr_BooleanAnd', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_BooleanOr(PHPParser_Node_Expr_BooleanOr $node) {
        return $this->pInfixOp('Expr_BooleanOr', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_BitwiseAnd(PHPParser_Node_Expr_BitwiseAnd $node) {
        return $this->pInfixOp('Expr_BitwiseAnd', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_BitwiseOr(PHPParser_Node_Expr_BitwiseOr $node) {
        return $this->pInfixOp('Expr_BitwiseOr', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_BitwiseXor(PHPParser_Node_Expr_BitwiseXor $node) {
        return $this->pInfixOp('Expr_BitwiseXor', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_ShiftLeft(PHPParser_Node_Expr_ShiftLeft $node) {
        return $this->pInfixOp('Expr_ShiftLeft', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_ShiftRight(PHPParser_Node_Expr_ShiftRight $node) {
        return $this->pInfixOp('Expr_ShiftRight', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_LogicalAnd(PHPParser_Node_Expr_LogicalAnd $node) {
        return $this->pInfixOp('Expr_LogicalAnd', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_LogicalOr(PHPParser_Node_Expr_LogicalOr $node) {
        return $this->pInfixOp('Expr_LogicalOr', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_LogicalXor(PHPParser_Node_Expr_LogicalXor $node) {
        return $this->pInfixOp('Expr_LogicalXor', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Equal(PHPParser_Node_Expr_Equal $node) {
        return $this->pInfixOp('Expr_Equal', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_NotEqual(PHPParser_Node_Expr_NotEqual $node) {
        return $this->pInfixOp('Expr_NotEqual', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Identical(PHPParser_Node_Expr_Identical $node) {
        return $this->pInfixOp('Expr_Identical', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_NotIdentical(PHPParser_Node_Expr_NotIdentical $node) {
        return $this->pInfixOp('Expr_NotIdentical', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Greater(PHPParser_Node_Expr_Greater $node) {
        return $this->pInfixOp('Expr_Greater', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_GreaterOrEqual(PHPParser_Node_Expr_GreaterOrEqual $node) {
        return $this->pInfixOp('Expr_GreaterOrEqual', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Smaller(PHPParser_Node_Expr_Smaller $node) {
        return $this->pInfixOp('Expr_Smaller', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_SmallerOrEqual(PHPParser_Node_Expr_SmallerOrEqual $node) {
        return $this->pInfixOp('Expr_SmallerOrEqual', $node->left, ' '.self::BINARY_OPERATOR.' ', $node->right);
    }

    public function pExpr_Instanceof(PHPParser_Node_Expr_Instanceof $node) {
        return $this->pInfixOp('Expr_Instanceof', $node->expr, ' '.self::BINARY_OPERATOR.' ', $node->class);
    }
    
    // Unary expressions

    public function pExpr_BooleanNot(PHPParser_Node_Expr_BooleanNot $node) {
        return $this->pPrefixOp('Expr_BooleanNot', ' '.self::UNARY_OPERATOR.' ', $node->expr);
    }

    public function pExpr_BitwiseNot(PHPParser_Node_Expr_BitwiseNot $node) {
        return $this->pPrefixOp('Expr_BitwiseNot', ' '.self::UNARY_OPERATOR.' ', $node->expr);
    }

    public function pExpr_UnaryMinus(PHPParser_Node_Expr_UnaryMinus $node) {
        return $this->pPrefixOp('Expr_UnaryMinus', ' '.self::UNARY_OPERATOR.' ', $node->expr);
    }

    public function pExpr_UnaryPlus(PHPParser_Node_Expr_UnaryPlus $node) {
        return $this->pPrefixOp('Expr_UnaryPlus', ' '.self::UNARY_OPERATOR.' ', $node->expr);
    }

    public function pExpr_PreInc(PHPParser_Node_Expr_PreInc $node) {
        return $this->pPrefixOp('Expr_PreInc', ' '.self::UNARY_OPERATOR.' ', $node->var);
    }

    public function pExpr_PreDec(PHPParser_Node_Expr_PreDec $node) {
        return $this->pPrefixOp('Expr_PreDec', ' '.self::UNARY_OPERATOR.' ', $node->var);
    }

    public function pExpr_PostInc(PHPParser_Node_Expr_PostInc $node) {
        return $this->pPostfixOp('Expr_PostInc', $node->var, ' '.self::UNARY_OPERATOR.' ');
    }

    public function pExpr_PostDec(PHPParser_Node_Expr_PostDec $node) {
        return $this->pPostfixOp('Expr_PostDec', $node->var, ' '.self::UNARY_OPERATOR.' ');
    }

    public function pExpr_ErrorSuppress(PHPParser_Node_Expr_ErrorSuppress $node) {
        return $this->pPrefixOp('Expr_ErrorSuppress', ' '.self::UNARY_OPERATOR.' ', $node->expr);
    }
    
    // array, const, class const, property, static property fetch
    
    public function pExpr_ArrayDimFetch(PHPParser_Node_Expr_ArrayDimFetch $node) {
        return $this->pVarOrNewExpr($node->var)
             . '[' . (null !== $node->dim ? $this->p($node->dim) : '') . ']';
    }

    public function pExpr_ConstFetch(PHPParser_Node_Expr_ConstFetch $node) {
        return self::LITERAL;
    }

    public function pExpr_ClassConstFetch(PHPParser_Node_Expr_ClassConstFetch $node) {
        return self::LITERAL;
    }

    public function pExpr_PropertyFetch(PHPParser_Node_Expr_PropertyFetch $node) {
        return $this->pVarOrNewExpr($node->var) . '->' . $this->pObjectProperty($node->name);
    }

    public function pExpr_StaticPropertyFetch(PHPParser_Node_Expr_StaticPropertyFetch $node) {
        return $this->p($node->class) . '::$' . $this->pObjectProperty($node->name);
    }

    // Casts
    public function pExpr_Cast_Int(PHPParser_Node_Expr_Cast_Int $node) {
        return $this->pPrefixOp('Expr_Cast_Int', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_Double(PHPParser_Node_Expr_Cast_Double $node) {
        return $this->pPrefixOp('Expr_Cast_Double', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_String(PHPParser_Node_Expr_Cast_String $node) {
        return $this->pPrefixOp('Expr_Cast_String', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_Array(PHPParser_Node_Expr_Cast_Array $node) {
        return $this->pPrefixOp('Expr_Cast_Array', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_Object(PHPParser_Node_Expr_Cast_Object $node) {
        return $this->pPrefixOp('Expr_Cast_Object', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_Bool(PHPParser_Node_Expr_Cast_Bool $node) {
        return $this->pPrefixOp('Expr_Cast_Bool', '(TYPE) ', $node->expr);
    }

    public function pExpr_Cast_Unset(PHPParser_Node_Expr_Cast_Unset $node) {
        return $this->pPrefixOp('Expr_Cast_Unset', '(TYPE) ', $node->expr);
    }

    
    // comments
    protected function pComments(array $comments) {
        return '';
    }
}
