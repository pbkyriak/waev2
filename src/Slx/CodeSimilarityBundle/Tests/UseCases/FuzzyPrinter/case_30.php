<?php
		$query = '
			REPLACE INTO`'._DB_PREFIX_.'accounting_zone_shop`
			(id_zone, id_shop, account_number)
			VALUES %s';
		
		$values = '';
		
		// Build the query for the update 
		foreach ($asso_zone_shop_list as $asso)
			if (array_key_exists('id_zone', $asso) &&
					array_key_exists('id_shop', $asso) &&
					array_key_exists('num', $asso))
				$values .= '('.(int)$asso['id_zone'].','.(int)$asso['id_shop'].', \''.pSQL($asso['num']).'\'), ';
		$query = sprintf($query, rtrim($values, ', '));
		
		if (!empty($values))
			return Db::getInstance()->execute($query);
		return false;
