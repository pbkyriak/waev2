<?php

namespace Slx\CodeSimilarityBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\CodeSimilarityBundle\PrettyPrint\FuzzyPrettyPrinter;

/**
 * Description of fuzzyPrinterTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class fuzzyPrinterTest extends WebTestCase {

    private $container;
    /** @var use Doctrine\ORM\EntityManager */
    //private $em;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        //$this->em = $client->getContainer()->get('Doctrine');
    }
        //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';

    public function NO_testCase01() {
        $fn = __DIR__.'/UseCases/FuzzyPrinter/case_01.php';
        $expected = file_get_contents(__DIR__.'/UseCases/FuzzyPrinter/case_01.exp');
        $this->fuzzyPrint($fn, $expected);
    }

    public function NO_testCase02() {
        $fn = __DIR__.'/UseCases/FuzzyPrinter/case_02.php';
        $expected = trim(file_get_contents(__DIR__.'/UseCases/FuzzyPrinter/case_02.exp'));
        $this->fuzzyPrint($fn, $expected);
    }
    
    public function NO_testCase30() {
        $fn = __DIR__.'/UseCases/FuzzyPrinter/case_30.php';
        $expected = trim(file_get_contents(__DIR__.'/UseCases/FuzzyPrinter/case_02.exp'));
        $this->fuzzyPrint($fn, $expected);
    }
    
    private function fuzzyPrint($fn, $expected) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        $printer = new FuzzyPrettyPrinter();
        $code = $printer->prettyPrint($ast);
        printf("\n\n-----------------\n");
        echo $code;
        printf("\n\n-----------------\n");
        $this->assertEquals($expected, $code);
    }
    
    public function NO_testFully() {
        $projectTagId = 4900;
        $fileId = 2195069;
        // $projectTagId = 4901;
        // $fileId = 2197654;
        // $projectTagId = 4917;
        // $fileId = 2226476;
        $method = '\JInstallerAdapterPlugin::install';
        $extractor = $this->container->get('waev.MethodASTExtractor');
        $em = $this->container->get('doctrine.orm.entity_manager');
        $projectTag = $em->getRepository("SlxGitMinerBundle:ProjectTag")->find($projectTagId);
        $ast = $extractor->getMethodAST($projectTag, $fileId, $method);
        if( $ast ) {
            $printer = new FuzzyPrettyPrinter();
//            $printer=new \PHPParser_PrettyPrinter_Default();
            $code = $printer->prettyPrint($ast);
            printf("\n\n-----------------\n");
            echo $code;
            printf("\n\n-----------------\n");
        }
        else {
            printf("\n\n-----------------\n");
            echo 'NO CODE';
            printf("\n\n-----------------\n");
        }
        
    }
    
    public function NO_testProject() {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $analyser = $this->container->get('waev.MethodChangesDetector');
        $analyser->analyse(61);
    }

}