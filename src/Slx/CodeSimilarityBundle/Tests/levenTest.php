<?php

namespace Slx\CodeSimilarityBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use SebastianBergmann\Diff\Differ;

/**
 * Description of levenTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 21, 2016
 */
class levenTest extends WebTestCase {

    private $container;

    /** @var use Doctrine\ORM\EntityManager */
    //private $em;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        //$this->em = $client->getContainer()->get('Doctrine');
    }

    public function test01() {
        $text1 = file_get_contents(__DIR__ . '/UseCases/distance/c01-a.txt');
        $text2 = file_get_contents(__DIR__ . '/UseCases/distance/c01-b.txt');
        $t1 = time();
        printf("performing diff...\n");
        printf("distance=%s\n", $this->calculateDistance($text1, $text2));
        printf("duration =%s\n", time() - $t1);
        printf("------------------\n");
    }

    public function NO_test02() {
        $text1 = file_get_contents(__DIR__ . '/UseCases/distance/c01-a.txt');
        $text2 = file_get_contents(__DIR__ . '/UseCases/distance/c01-b.txt');
        $t1 = time();
        printf("performing diff...\n");
        printf("distance=%s\n", $this->leven($text1, $text2));
        printf("duration =%s\n", time() - $t1);
        printf("------------------\n");
    }
    public function test03() {
        $text1 = file_get_contents(__DIR__ . '/UseCases/distance/c01-a.txt');
        $text2 = file_get_contents(__DIR__ . '/UseCases/distance/c01-b.txt');
        $t1 = time();
        printf("performing diff...\n");
        printf("distance=%s\n", $this->sift3Plus($text1, $text2,5));
        printf("duration =%s\n", time() - $t1);
        printf("------------------\n");
    }
    function leven($s1, $s2) {
        $l1 = strlen($s1);                    // Länge des $s1 Strings
        $l2 = strlen($s2);                    // Länge des $s2 Strings
        $dis = range(0, $l2);                  // Erste Zeile mit (0,1,2,...,n) erzeugen 
        // $dis stellt die vorrangeganene Zeile da. 
        for ($x = 1; $x <= $l1; $x++) {
            $dis_new[0] = $x;               // Das erste element der darauffolgenden Zeile ist $x, $dis_new ist damit die aktuelle Zeile mit der gearbeitet wird
            for ($y = 1; $y <= $l2; $y++) {
                $c = ($s1[$x - 1] == $s2[$y - 1]) ? 0 : 1;
                $dis_new[$y] = min($dis[$y] + 1, $dis_new[$y - 1] + 1, $dis[$y - 1] + $c);
            }
            $dis = $dis_new;
        }

        return $dis[$l2];
    }

    private function calculateDistance($str1, $str2) {
        $out = 0;
        if (sha1($str1) == sha1($str2)) {
            return 1;
        }
        $codeLines1 = count(preg_split('(\r\n|\r|\n)', $str1));
        $codeLines2 = count(preg_split('(\r\n|\r|\n)', $str2));
        $differ = new Differ(''); //'',true);
        $diff = $differ->diff($str1, $str2);
        $lines = preg_split('(\r\n|\r|\n)', $diff);
        $marks = ['+' => 0, '-' => 0];
        foreach ($lines as $line) {
            $mark = substr($line, 0, 1);
            if ($mark == '+' || $mark == '-') {
                $marks[$mark] ++;
            }
        }
        $out = 1 - round((($marks['+'] / $codeLines2 + $marks['-'] / $codeLines1) / 2), 3);
        return $out;
    }

    function sift3Plus($s1, $s2, $maxOffset) {
        $s1Length = strlen($s1);
        $s2Length = strlen($s2);
        if (empty($s1)) {
            return (empty($s2) ? 0 : $s2Length);
        }
        if (empty($s2)) {
            return $s1Length;
        }
        $c1 = $c2 = $lcs = 0;

        while (($c1 < $s1Length) && ($c2 < $s2Length)) {
            if (($d = $s1{$c1}) == $s2{$c2}) {
                $lcs++;
            } else {
                for ($i = 1; $i < $maxOffset; $i++) {
                    if (($c1 + $i < $s1Length) && (($d = $s1{$c1 + $i}) == $s2{$c2})) {
                        $c1 += $i;
                        break;
                    }
                    if (($c2 + $i < $s2Length) && (($d = $s1{$c1}) == $s2{$c2 + $i})) {
                        $c2 += $i;
                        break;
                    }
                }
            }
            $c1++;
            $c2++;
        }
        return (($s1Length + $s2Length) / 2 - $lcs);
    }

}
