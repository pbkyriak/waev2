<?php

namespace Slx\DeadCodeDetectorBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\DeadCodeDetectorBundle\Analyser\Detector;

/**
 * Description of 
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DeadCodeDetectorTask extends AbstractTagTask {
    
    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        //$this->em->getConnection()->query("SET autocommit=0;"); 
        try {
            $this->resetDeadFlag();
            $fileList = $this->getFileList();
            printf("files to analyse: %s \n", count($fileList));
            $detector = new Detector();
            $results = $detector->detectDeadCode($fileList);
            $cnt=count($results);
            printf("storing %s results\n", $cnt);
            foreach($results as $name => $source) {
                $this->updateFunction($name);
            }
            //$this->em->getConnection()->query("COMMIT;");
        }
        catch(\Exception $ex) {
            printf("DCD Error: %s\n", $ex->getMessage());
            //$this->em->getConnection()->query("ROLLBACK;");
        }
        //$this->em->getConnection()->query("SET autocommit=1;"); 
        $mem2 = (memory_get_usage() / 1024);
        echo "DCD Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;
        return true;
    }

    public function getTaskName() {
        return 'Dead Code Detector';
    }

    public function onFail() {
        return true;
    }

    private function getFileList() {
        $params = array('tid'=>$this->tagId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery("SELECT id, fname FROM project_file WHERE project_tag_id=:tid ", $params);   // and parser='good'
        $list = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $out = array();
        foreach($list as $item) {
            $out[$item['id']] = $this->tag->getSourceCodePath() . $item['fname'];
        }
        return $out;
    }

    private function resetDeadFlag() {
        $this->em->getConnection()->update(
            'project_code_construct', 
            array('dead'=>0), 
            array('project_tag_id'=>$this->tagId)
        );
    }
    
    private function updateFunction($cname) {
        printf("%s %s \n", $this->tagId, $cname);
        $this->em->getConnection()->update(
            'project_code_construct', 
            array('dead'=>1), 
            array('project_tag_id'=>$this->tag->getId(), 'cname'=>$cname)
        );

    }
    
    
} 
