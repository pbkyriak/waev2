<?php

namespace Slx\DocBlockBundle\DocBlock;

use Slx\DocBlockBundle\DocBlock\DocBlocks\DocBlockTag;
/**
 * Process docblock comments.
 * 
 * Based on zend 1.2 doc block parser
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DocBlock 
{
    
    protected $comment;
    private $tags;
    private $shortDescription;
    private $longDescription;
    
    public function __construct($comment)
    {
        $this->comment = $comment;
        $this->parse();
    }
    
    /**
     * Parses docComment block. Based on zendFramework doc block parser
     */
    private function parse() {
        $docComment = $this->comment;
        // First remove doc block line starters
        $docComment = preg_replace('#[ \t]*(?:\/\*\*|\*\/|\*)?[ ]{0,1}(.*)?#', '$1', $docComment);
        $docComment = ltrim($docComment, "\r\n"); // @todo should be changed to remove first and last empty line
        $docComment = preg_replace('/\*\/$/', "\n", $docComment);
        // Next parse out the tags and descriptions
        $parsedDocComment = $docComment;
        $lineNumber = $firstBlandLineEncountered = 0;
        while (($newlinePos = strpos($parsedDocComment, "\n")) !== false) {
            $lineNumber++;
            $line = substr($parsedDocComment, 0, $newlinePos);
            $matches = array();

            if ((strpos($line, '@') === 0) && (preg_match('#^(@\w+.*?)(\n)(?:@|\r?\n|$)#s', $parsedDocComment, $matches))) {
                $this->tags[] = DocBlockTag::factory($matches[1]);
                $parsedDocComment = str_replace($matches[1] . $matches[2], '', $parsedDocComment);
            } else {
                if ($lineNumber < 3 && !$firstBlandLineEncountered) {
                    $this->shortDescription .= $line . "\n";
                } else {
                    $this->longDescription .= $line . "\n";
                }

                if ($line == '') {
                    $firstBlandLineEncountered = true;
                }

                $parsedDocComment = substr($parsedDocComment, $newlinePos + 1);
            }

        }

        $this->shortDescription = rtrim($this->shortDescription);
        $this->longDescription  = rtrim($this->longDescription);
    }
    
    /**
     * Get docBlock tags
     * 
     * @return array
     */
    public function getTags() {
        return $this->tags;
    }
    
    /**
     * Get if any, variable type.
     * 
     * @param string $variableName
     * @return string
     */
    public function getVariableParam($variableName) {
        $out = '';
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockParam ) {
                    if( $tag->getVariableName()==$variableName ) {
                        $out = $tag->getType();
                    }
                }
            }
        }
        return $this->clearIDEDefType($out);
    }

    /**
     * Get if any, variable type.
     * 
     * @param string $variableName
     * @return string
     */
    public function getVariableVariable($variableName) {
        $out = '';
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockVar ) {
                    if( $tag->getVariableName()==$variableName ) {
                        $out = $tag->getType();
                    }
                }
            }
        }
        return $this->clearIDEDefType($out);
    }

    private function clearIDEDefType( $type ) {
        if(in_array($type, array('type', '[type]'))) {
            $type = '';
        }
        return $type;        
    }
    
    /**
     * Get if docBlock defined, return type.
     * 
     * @return string
     */
    public function getReturnType() {
        $out = 'mixed';
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockReturn ) {
                    $out = $tag->getType();
                }
            }
        }
        return $out;        
    }
    
    /**
     * if docBlock has return tag
     * 
     * @return boolean
     */
    public function hasReturnTag() {
        $out = false;
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockReturn ) {
                    $out = true;
                }
            }
        }
        return $out;                
    }
    
    /**
     * Get if docBlock defined access type
     * 
     * @return string
     */
    public function getAccessType() {
        $out = 'public';
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockAccess ) {
                    $out = $tag->getType();
                    break;
                }
            }
        }        
        return $out;
    }
    
    /**
     * Returns true if in docblock there is access tag
     * 
     * @return boolean
     */
    public function hasAccessTag() {
        $out = false;
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockAccess ) {
                    $out = true;
                    break;
                }
            }
        }        
        return $out;        
    }
    
    /**
     * If in docBlock there is deprecated tag
     * 
     * @return true
     */
    public function isDeprecated() {
        $out = false;
        if( $this->getTags() ) {
            foreach($this->getTags() as $tag) {
                if( $tag instanceof DocBlocks\DocBlockDeprecated ) {
                    $out = $tag->getType();
                    break;
                }
            }
        }        
        return $out;
    }
}
