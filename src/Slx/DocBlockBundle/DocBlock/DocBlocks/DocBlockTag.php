<?php

namespace Slx\DocBlockBundle\DocBlock\DocBlocks;
use Slx\ReflectionBundle\Reflection\ReflectionException;

/**
 * DocBlockTag reflection.
 * 
 * Based on zend framework 1.2 reflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DocBlockTag
{
    protected $name;
    protected $description;
    
    protected static $tagClasses = array( 
        'param' =>      __NAMESPACE__.'\DocBlockParam',
        'return' =>     __NAMESPACE__.'\DocBlockReturn',
        'access' =>     __NAMESPACE__.'\DocBlockAccess',
        'deprecated' => __NAMESPACE__.'\DocBlockDeprecated',
        'var' =>        __NAMESPACE__.'\DocBlockVar',
    );
    
    public static function factory($tagDocblockLine)
    {
        $matches = array();

        if (!preg_match('#^@(\w+)(\s|$)#', $tagDocblockLine, $matches)) {
            throw new ReflectionException('No valid tag name found within provided docblock line.');
        }

        $tagName = $matches[1];
        if (array_key_exists($tagName, self::$tagClasses)) {
            $tagClass = self::$tagClasses[$tagName];
            try {
                return new $tagClass($tagDocblockLine);
            }
            catch(ReflectionException $ex) {
                 
            }
        }
        return new self($tagDocblockLine);
    }
    
    /**
     * Constructor
     *
     * @param  string $tagDocblockLine
     * @return void
     */
    public function __construct($tagDocblockLine)
    {
        $matches = array();

        // find the line
        if (!preg_match('#^@(\w+)(?:\s+([^\s].*)|$)?#', $tagDocblockLine, $matches)) {
            throw new ReflectionException('Provided docblock line does not contain a valid tag');
        }

        $this->name = $matches[1];
        if (isset($matches[2]) && $matches[2]) {
            $this->description = $matches[2];
        }
    }

    public function getName() 
    {
        return $this->name;
    }
    
    public function getDescription() 
    {
        return $this->description;
    }
        
}
