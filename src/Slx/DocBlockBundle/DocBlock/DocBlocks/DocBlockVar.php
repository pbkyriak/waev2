<?php

namespace Slx\DocBlockBundle\DocBlock\DocBlocks;

use Slx\ReflectionBundle\Reflection\ReflectionException;

/**
 * DocBlockParam reflection.
 * 
 * Based on zend framework 1.2 reflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DocBlockVar extends DocBlockTag
{
    private $variableName;
    private $type;
    
    public function __construct($tagDocblockLine)
    {
        $matches = array();
        $tagDocblockLine = preg_replace('/\*\/$/', '', $tagDocblockLine);
        if (!preg_match('#^@(\w+)\s+([^\s]+)(?:\s+(\$\S+))?(?:\s+(.*))?#s', $tagDocblockLine, $matches)) {
            throw new ReflectionException('Provided docblock line is does not contain a valid tag');
        }

        if ($matches[1] != 'var') {
            throw new ReflectionException('Provided docblock line is does not contain a valid @var tag');
        }

        $this->name = 'var';
        $this->type = $matches[2];

        if (isset($matches[3])) {
            $this->variableName = $matches[3];
        }

        if (isset($matches[4])) {
            $this->description = preg_replace('#\s+#', ' ', $matches[4]);
        }
    }
    
    public function getVariableName() {
        return $this->variableName;
    }
    
    public function getType() {
        return $this->type;
    }
}
