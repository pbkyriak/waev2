<?php

namespace Slx\FuncCallsBundle\Analyser\FunctionCalls;
/**
 * Description of FuncCall
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncCall
{
    private $tagId;
    private $fileId;
    private $contextFunc;
    private $calledFuncId;
    private $line;
    
    public function __construct($tagId, $fileId, $contextClass, $contextFunc, $line, $calledFuncId)
    {
        $this->tagId = $tagId;
        $this->fileId=$fileId;
        if( $contextClass ) {
            $this->contextFunc = sprintf("%s:%s", $contextClass, $contextFunc);
        }
        elseif( $contextFunc ) {
            $this->contextFunc = $contextFunc;
        }
        else {
            $this->contextFunc = '';
        }
        $this->calledFuncId = $calledFuncId;
        $this->line = $line;
    }
    
    public function __toString()
    {
        return sprintf("fid=%s cf=%s cfid=%s ln=%s", $this->fileId, $this->contextFunc, $this->calledFuncId, $this->line);
    }
    
    public function persist($em) {
        $data = array(
            'project_tag_id' => $this->tagId,
            'project_file_id' => $this->fileId,
            'function_id' => $this->calledFuncId,
            'context' => $this->contextFunc,
            'line' => $this->line,
        );
        $em->getConnection()->insert('project_tag_func_call', $data);
    }
}
