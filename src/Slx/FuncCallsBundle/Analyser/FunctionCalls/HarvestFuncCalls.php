<?php

namespace Slx\FuncCallsBundle\Analyser\FunctionCalls;

use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;
use PHPParser_Node;
use PHPParser_NodeVisitor;
use Slx\FuncCallsBundle\Analyser\FunctionCalls\FuncCall;

/**
 * Description of FuncCalls
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class HarvestFuncCalls implements PHPParser_NodeVisitor
{

    /** @var EntityManager $em */
    private $em;
    /** @var ProjectTag */
    private $tag;
    private $tagFunctions;
    private $context;
    private $calls;
    private $fileId;
    private $phpFuncs;
    private $phpCalls;
    
    public function __construct(EntityManager $em, ProjectTag $tag)
    {
        $this->em = $em;
        $this->tag = $tag;
        $this->getTagFunctions();
        $this->phpFuncs = $this->loadPHPFunctions();
        $this->content = array('file'=>'', 'class'=>'', 'function'=>'');
        $this->calls = array();
        $this->phpCalls = array();
    }
    
    private function getTagFunctions() {
        $this->tagFunctions=array();
        $funcs = $this->em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getTagFunctionNames($this->tag);
        foreach($funcs as $func) {
            $this->tagFunctions[$func['id']]=$func['cname'];
        }
    }
    
    private function loadPHPFunctions() {
        $out = array();
        $handle = fopen(__DIR__.'/../../Resources/data/phpdefs.csv', "r");
        if($handle) {
            while($row = fgetcsv($handle)) {
                if( strpos($row[0],'::')===false) { // only functions, not methods
                    $out[]=$row[0];
                }
            }
            fclose($handle);
        }
        return $out;
    }
    
    public function setFile($filename) {
        $fn = str_replace($this->tag->getSourceCodePath(),'', $filename);
        $this->context['file']=$fn;
        $this->context['class']='';
        $this->context['function']='';
        $file = $this->em->getRepository('SlxGitMinerBundle:ProjectFile')->findOneBy(array('fname'=>$fn));
        if( $file ) {
            $this->fileId = $file->getId();
        }
    }
    
    public function persistCalls() {
        foreach($this->calls as $key => $call) {
            $call->persist($this->em);
            unset($this->calls[$key]);
            unset($call);
        }
        unset($this->calls);
        $this->calls = array();
    }
    
    public function persistPHPCalls() {
        foreach($this->phpCalls as $fname => $cnt) {
            $data = array('fname'=>$fname, 'call_count'=>$cnt, 'project_tag_id'=>$this->tag->getId());
            $this->em->getConnection()->insert('project_tag_php_func_call', $data);
        }
    }
    
    public function afterTraverse(array $nodes)
    {
        
    }

    public function beforeTraverse(array $nodes)
    {
        
    }

    public function enterNode(PHPParser_Node $node)
    {
        switch (get_class($node)) {
            case 'PHPParser_Node_Stmt_Class':
                $this->context['class']=$node->name;
                break;
            case 'PHPParser_Node_Stmt_Function':
                $this->context['class']='';
                $this->context['function']=$node->name;
                break;
            case 'PHPParser_Node_Stmt_ClassMethod':
                $this->context['function']=$node->name;
                break;
        }        
    }

    public function leaveNode(PHPParser_Node $node)
    {
        switch (get_class($node)) {
            case 'PHPParser_Node_Stmt_Class':
                $this->context['class']='';
                $this->context['function']='';
                break;
            case 'PHPParser_Node_Stmt_Function':
                $this->context['class']='';
                $this->context['function']='';
                break;
            case 'PHPParser_Node_Stmt_ClassMethod':
                $this->context['function']='';
                break;
            case 'PHPParser_Node_Expr_FuncCall':
                if( get_class($node->name)=='PHPParser_Node_Name' )
                    $fname = $node->name->getLast();
                else
                    break;
                if( in_array($fname, $this->phpFuncs) ) {
                    
                    if( !isset($this->phpCalls[$fname]) ) {
                        $this->phpCalls[$fname] = 0;
                        printf("%s new System func:%s\n",count($this->phpCalls), $fname);
                    }
                    $this->phpCalls[$fname]++;
                }
                else {
                    $id = array_search($fname, $this->tagFunctions);
                    if(!($id===false)) {
                        $this->calls[] = new FuncCall(
                            $this->tag->getId(),
                            $this->fileId, 
                            $this->context['class'], 
                            $this->context['function'],
                            $node->getLine(),
                            $id
                            );
                    }
                }
                break;
        }
        
    }

}
