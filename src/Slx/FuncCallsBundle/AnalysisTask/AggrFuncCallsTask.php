<?php

namespace Slx\FuncCallsBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Aggregate number of function calls foreach Tag's function
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class AggrFuncCallsTask extends AbstractTagTask
{

    public function getTaskName()
    {
        return 'Aggregate nb function calls';
    }
    
    public function execute()
    {
        $out = true;
        try {
            //$this->em->getConnection()->query("SET autocommit=0;"); 
            $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
            $this->aggregate($this->tag);
            $this->aggregateNamed($this->tag);
            $this->cleanFuncCalls($this->tag);
            //$this->em->getConnection()->query("COMMIT;");
            $this->em->clear();
        } catch (\Exception $ex) {
            // some logging here
            $this->isCriticalFail = false;
            printf("exception %s\n", $ex->getMessage());
            //$this->em->getConnection()->query("ROOLBACK;");
            $out = false;
        }
        //$this->em->getConnection()->query("SET autocommit=1;"); 

        return true;
    }

    public function onFail()
    {
        return true;
    }

    private function aggregate(ProjectTag $tag) {
        $query = "update project_code_construct set nb_calls=0, fanout=0 where project_tag_id in(select id from project_tag where project_id=:pid)";
        $this->em->getConnection()->executeQuery($query, array('pid'=>$tag->getId()));
        $dql = "select cf.id, cf.cname, count(fc.id) as cnt from SlxGitMinerBundle:ProjectTagFuncCall fc left join fc.calledFunction cf where fc.projectTag = :tag group by cf.id";
        $rows = $this->em->createQuery($dql)->setParameter('tag', $tag)->getArrayResult();
        printf("cnt=%s\n", count($rows));
        //$this->em->getConnection()->query("SET autocommit=0;"); 
        foreach($rows as $row) {
            $query = "update project_code_construct set nb_calls=:nb_calls, fanout=waev_get_func_fanout(:pid, :func) where id=:fid";
            $params = array('nb_calls'=> $row['cnt'], 'func'=>$row['cname'], 'pid'=>$tag->getId(), 'fid'=>$row['id']);
            $this->em->getConnection()->executeQuery($query, $params);
        }
        
    }

    private function aggregateNamed(ProjectTag $tag) {
        $this->em->createQuery('delete FROM SlxGitMinerBundle:ProjectTagFuncCallAggr fc where fc.projectTag=:tag')->setParameter('tag', $tag)->execute();
        $dql = "SELECT cf.cname, count(fc.id) as cnt from SlxGitMinerBundle:ProjectCodeConstruct cf LEFT JOIN SlxGitMinerBundle:ProjectTagFuncCall fc With cf.id=fc.calledFunction WHERE cf.projectTag=:tag and cf.ctype='function' group by cf.cname";
        $rows = $this->em->createQuery($dql)->setParameter('tag', $tag)->getArrayResult();
        printf("cnt named=%s\n", count($rows));
        //$this->em->getConnection()->query("SET autocommit=0;"); 
        foreach($rows as $row) {
            $query = "insert into project_tag_func_call_aggr (func, nb_calls, fanout, project_tag_id) values (:func, :nb_calls, waev_get_func_fanout(:pid, :func), :pid)";
            $params = array('func'=>$row['cname'],'nb_calls'=> $row['cnt'], 'pid'=>$tag->getId());
            $this->em->getConnection()->executeQuery($query, $params);
        }
    }
    
    private function cleanFuncCalls(ProjectTag $tag) {
        $this->em->getConnection()->executeQuery("DELETE FROM project_tag_func_call where project_tag_id=:tid", array('tid'=>$tag->getId()));
    }
}
