<?php

namespace Slx\FuncCallsBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\ReflectionBundle\Analyser\SourceCodeHelper;
use Slx\ReflectionBundle\Reflection\Traverser;
use Slx\FuncCallsBundle\Analyser\FunctionCalls\HarvestFuncCalls;

/**
 * Description of FuncCallsTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 18 Αυγ 2014
 */
class FuncCallsTask extends AbstractTagTask
{
    public function onFail() {
        return true;
    }

    public function getTaskName()
    {
        return 'Collects function calls.';
    }

    public function execute() 
    {
        $mem1 = (memory_get_usage() / 1024);
        $this->badFiles = array();
        
        $this->scHelper = new SourceCodeHelper($this->tag);
        $this->fcalls = new HarvestFuncCalls($this->em, $this->tag);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        //$this->em->getConnection()->query("SET autocommit=0;"); 
        // cleanup prev data import for this tag
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectTagFuncCall a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $this->tag->getId())
            ->execute();
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectTagPhpFuncCall a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $this->tag->getId())
            ->execute();

        $files = $this->scHelper->getTagSourceFilesDB($this->em);
        foreach ($files as $file) {
            $this->processFile($file);
        }
        $this->fcalls->persistPHPCalls();
        $this->setTagStatus(ProjectTag::STATUS_RFGC_DONE);
//        $this->em->getConnection()->query("COMMIT;");
        //$this->em->getConnection()->query("SET autocommit=1;"); 
        if( count($this->badFiles)) {
            $i=1;
            printf("Failed to read ast for the files\n");
            foreach($this->badFiles as $bf) {
                printf("  %s. %s\n", $i, $bf);
                $i++;
            }
        }
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;

        return true;
    }   
    
    private function processFile($file) {
        try {
            $ast = $this->scHelper->getFileAst($file);
        } catch (\Exception $e) {
            printf("%s Exception: %s\n", __CLASS__, $e->getMessage());
            $ast = null;
            gc_collect_cycles();
        }
            
        if( $ast ) {
            try {
                $traverser = new Traverser();
                $this->fcalls->setFile($file);
                $traverser->addVisitor($this->fcalls);
                $traverser->traverseAst($ast);
                $this->em->getConnection()->query("SET autocommit=0;"); 
                $this->fcalls->persistCalls();
                $this->em->getConnection()->query("COMMIT;");
                $this->em->clear();
                $this->tag = $this->getTagById($this->tagId);

                $traverser->free();
                $traverser=null;
                gc_collect_cycles();
            } catch(\InvalidArgumentException $e) {
                $this->em->getConnection()->query("ROLLBACK;");
                printf('%s file: %s InvalidArgumentException: %s\n',__CLASS__, $file, $e->getMessage());
            } catch (\Exception $e) {
                $this->em->getConnection()->query("ROLLBACK;");
                printf('%s file: %s Exception: %s\n',__CLASS__,$file, $e->getMessage());
            }
            $this->em->getConnection()->query("SET autocommit=1;"); 
        }                
        else {
            $this->badFiles[] = $file;
        }
        time_nanosleep(0, 10000000);
    }
}
