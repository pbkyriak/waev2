<?php

namespace Slx\GitMinerBundle\Analyser;

/**
 * Calculates Gini index for an array of data
 * Usage example:
 *       $data = array(1000,2000,3000,4000,5000);
 *       $gini = new GiniCalculator();
 *       $g = $gini->setData($data)->getGini();
 *       printf("gini=%s\n", $g);
 *
 * Added 11/11/13
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class GiniCalculator
{

    private $inData;
    private $gini;
    private $numberOfParticipants=0;
    private $totalIncome=0;
    
    public function setData($inData) {
        $this->inData=$inData;
        $this->numberOfParticipants=count($this->inData);
        $this->totalIncome=0;
        $this->gini=null;
        return $this;
    }
    
    public function getGini() {
        if( $this->gini==null ) {
            $this->calculate();
        }
        return $this->gini;
    }
    
    private function calculate() {
        sort($this->inData);
        $this->gini =
            $this->calcGini(
                $this->calcTotalParticipantsArea(
                    $this->processIncome(
                        $this->processParticipants(
                            $this->inData
                        )
                    )
                )
            );
    }
    
    private function processParticipants($data) {
        $out = array();
        $i=1;
        foreach($data as $income) {
            $out[] = array(
                'i'=>$i, 
                'yi'=>$income, 
                'pi'=> $i/$this->numberOfParticipants, 
                'qi'=>0
                );
            $this->totalIncome += $income;
            $i++;
        }
        return $out;
    }
    
    private function processIncome($data) {
        $prev = 0;
        foreach($data as $idx => $row) {
            $data[$idx]['qi'] = $prev + $row['yi']/$this->totalIncome;
            $prev = $data[$idx]['qi'];
        }
        return $data;
    }
    
    private function calcTotalParticipantsArea($data) {
        $total = 0;
        foreach($data as $idx => $row) {
            $piPrev = $idx==0 ? 0 : $data[$idx-1]['pi'];
            $qiPrev = $idx==0 ? 0 : $data[$idx-1]['qi'];
            $pi = $data[$idx]['pi'];
            $qi = $data[$idx]['qi'];
            $zi = 0.5 * ($qi+$qiPrev) * ($pi-$piPrev);            
            $total += $zi;
        }
        return $total;
    }
    
    private function calcGini($totalPArea) {
        return (0.5-$totalPArea)/0.5;
    }
    
}
