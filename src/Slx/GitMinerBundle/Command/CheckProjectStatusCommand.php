<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
use JMS\JobQueueBundle\Entity\Job;

class CheckProjectStatusCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    
    protected function configure()
    {
        $this
            ->setName('gitminer:checkProjectStatus')
            ->setDescription("Checks project status")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->output = $output;
        $this->checkProjects($this->em);
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }

    private function checkProjects(EntityManager $em)
    {
        $projects = $em
            ->getRepository('SlxGitMinerBundle:Project')
            ->getNotFinishedProjects();
        foreach ($projects as $project) {
            $this->updateTagJobState($project);
            $this->setNewProjectStatus($project);
            $em->persist($project);
            $this->addNextProjectJobs($project);
        }
        $em->flush();
    }
    
    /**
     * checks if project jobs finished running and updates tag table with worse job status
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    private function updateTagJobState(Project $project)
    {
        foreach ($project->getProjectTags() as $tag) {
            $jobId = $tag->getJobId();
            $job = $this->em->getRepository('JMSJobQueueBundle:Job')->find($jobId);
            if ($job) {
                $jobState = $this->getJobFinalState($job, 10);
                $tag->setJobState($jobState);
                $this->em->persist($tag);
            }
        }
        $this->em->flush();
    }

    /**
     * drills down job's dependancy tree to get the worse job status
     * 
     * @param \JMS\JobQueueBundle\Entity\Job $job
     * @param integer $state
     * @return integer
     */
    private function getJobFinalState(Job $job, $state)
    {
        if ($job) {
            $tmpState = $this->jobStateToMyState($job->getState());
            if ($state > $tmpState) {
                $state = $tmpState;
            }
            foreach ($job->getDependencies() as $djob) {
                $tmpState = $this->jobStateToMyState($djob->getState());
                if ($state > $tmpState) {
                    $state = $tmpState;
                }
                $state = $this->getJobFinalState($djob, $state);
            }
        }
        else {
            $state=-1;
        }
        return $state;
    }

    /**
     * converts JMS job status to integer shortned cases
     * 
     * @param string $jobState
     * @return int
     */
    private function jobStateToMyState($jobState)
    {
        $state = 0;
        if ($jobState == Job::STATE_PENDING || $jobState == Job::STATE_NEW) {
            $state = 1;
        } elseif ($jobState == Job::STATE_RUNNING) {
            $state = 2;
        } elseif ($jobState == Job::STATE_FINISHED) {
            $state = 3;
        } else {    // failed or canceled or something else bad
            $state = 4;
        }
        return $state;
    }

    /**
     * Gets the worse status for project's tags and sets project status
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    private function setNewProjectStatus(Project $project)
    {
        $newStatus = Project::STATUS_INIT;
        $minTagStatus = ProjectTag::STATUS_DP_IMPORT;
        $projectTags = $project->getProjectTags();
        if (count($projectTags)) {
            foreach ($project->getProjectTags() as $tag) {
                if ($tag->getStatus() < $minTagStatus)
                    $minTagStatus = $tag->getStatus();
            }
        }
        else {
            $this->addProjectTags($project);
            $minTagStatus = ProjectTag::STATUS_NEW;
        }
        if ($minTagStatus == ProjectTag::STATUS_DP_IMPORT)
            $newStatus = Project::STATUS_WAITING_FOR_2;
        elseif ($minTagStatus == ProjectTag::STATUS_NEW) {
            $newStatus = Project::STATUS_INIT;
        } elseif ($minTagStatus < ProjectTag::STATUS_DP_IMPORT) {
            $newStatus = Project::STATUS_ANALYSIS_1;
        }
        $project->setStatus($newStatus);
    }

    /**
     * Adds jobs for the next analysis step of the project
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    private function addNextProjectJobs(Project $project)
    {
        if ($project->getStatus() == Project::STATUS_INIT) {
            $this->addInitJobs($project);
        }
        if ($project->getStatus() == Project::STATUS_WAITING_FOR_2) {
            $this->AddAnalysis2Jobs($project);
        }
    }

    /**
     * Initial jobs: get source from gitHub, pdepend, import pdepent output, remove source code
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    private function addInitJobs(Project $project)
    {
        $em = $this->em;
        $tags = $project->getProjectTags();
        $prevJob = null;
        foreach ($tags as $tag) {
            if( $tag->getJobId()!=0 ) 
                continue;
            $job1 = new Job('gitminer:git:getZip', array($tag->getId()));
            if ($prevJob) {
                $job1->addDependency($prevJob);
            }
            $job2 = new Job('gitminer:pd:tag', array($tag->getId()));
            $job2->addDependency($job1);
            $job3 = new Job('gitminer:import:pdxml', array($tag->getId()));
            $job3->addDependency($job2);
            $job4 = new Job('gitminer:cleanTagFiles', array($tag->getId()));
            $job4->addDependency($job3);

            if ($prevJob) {
                $em->persist($prevJob);
            }
            $em->persist($job1);
            $em->persist($job2);
            $em->persist($job3);
            $em->persist($job4);
            $prevJob = $job4;
            $em->flush();
            $tag->setJobId($job1->getId());
            $em->persist($tag);
            $em->flush();
        }
    }

    /**
     * Adds project tags to project (gets them from github)
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    private function addProjectTags(Project $project)
    {
        $em = $this->em;
        $client = new \Github\Client();
        $tagsInfo = $client->api('repo')->tags($project->getOwner(),
            $project->getPname());
        if ($tagsInfo) {
            $this->output->writeln(sprintf("found in gitHub %s tags for project %s/%s",
                    count($tagsInfo), $project->getOwner(), $project->getPname()));
            $cnt = 0;
            $tagsInfo = array_reverse($tagsInfo);
            foreach ($tagsInfo as $tagInfo) {
                // check if tag exists
                if (!$this->projectTagExists($em, $project, $tagInfo['name'])) {
                    $cnt++;
                    $tag = new ProjectTag();
                    $tag->setProject($project)
                        ->setTag($tagInfo['name'])
                        ->setZipballUrl($tagInfo['zipball_url'])
                        ->setUrl($tagInfo['commit']['url'])
                    ;
                    $em->persist($tag);
                    $em->flush();
                    //
                    if( $cnt==4 )
                        break;
                    //
                }
            }
        } 
    }
    
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $tag
     * @return boolean
     */
    private function projectTagExists(EntityManager $em, $project, $tag)
    {
        $out = false;
        $o = $em->getRepository('SlxGitMinerBundle:ProjectTag')->findOneBy(array('project' => $project, 'tag' => $tag));
        if ($o)
            $out = true;
        return $out;
    }

    private function AddAnalysis2Jobs(Project $project)
    {
        printf("project %s ready for analysis2\n", $project->getPname());
    }

}

