<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * @todo Move functionality to AnalysisTask
 * 
 */
class CleanProjectSourceFilesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('gitminer:cleanProjectSourceFiles')
            ->setDescription("Remove project's source code")
            ->addArgument('projectid', InputArgument::REQUIRED,
                'Project Id to remove source code?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $projectId = $input->getArgument('projectid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw new Exception(spinrtf('Project with id %s not found!',
                $projectId));
        }
        foreach($project->getProjectTags() as $tag) {
            $command = sprintf("rm -rf %s", $tag->getSourceCodePath());
            exec($command);
        }
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }

}

