<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * @todo Move functionality to AnalysisTask
 * 
 */
class CleanTagFilesCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('gitminer:cleanTagFiles')
            ->setDescription("Remove tag's source code")
            ->addArgument('tagid', InputArgument::REQUIRED,
                'Project Tag Id to remove source code?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$tag) {
            throw new Exception(spinrtf('ProjectTag with id %s not found!',
                $tagId));
        }
        $command = sprintf("rm -rf %s", $tag->getTagWorkingFolder());
        exec($command);
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }

}

