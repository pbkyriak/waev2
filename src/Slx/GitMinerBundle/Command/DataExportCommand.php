<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of DataExportCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 8 Ιουλ 2014
 */
class DataExportCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('gitminer:data:export')
            ->setDescription("Reads single parametered (pid) query from file and output results to tab delimeted file.")
            ->addArgument('projectid',
                InputArgument::REQUIRED,
                'Project Id to get data?')
            ->addArgument('query',
                InputArgument::REQUIRED,
                'File that contains the query?')
            ->addArgument('outfn',
                InputArgument::REQUIRED,
                'file to write data?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $projectId = $input->getArgument('projectid');
        $fn = $input->getArgument('outfn');
        $queryFn = $input->getArgument('query');
        $query = $this->loadQuery($queryFn);
        $params = array('pid'=>$projectId);
        if( $query ) {
            $results = $this->fetch($query, $params);
            $this->writeOutput($fn, $results);
        }
        else {
            $output->writeln('Couldnt read query from file');
        }
        $output->writeln(sprintf("execution time: %s sec", time()-$t1));
    }

    private function loadQuery($fn) {
        $q = file_get_contents($fn);
        return $q;
    }
     
    private function fetch($sql, $params)
    {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    private function writeOutput($fn, $data) {
        $handle = fopen($fn, "w");
        if( $handle ) {
            $header = null;
            reset($data);
            $i=1;
            while( ($row=current($data)) ) {
                if( !$header ) {
                    $header = array_keys($row);
                    array_unshift($header, '#');
                    fputcsv($handle,$header,"\t");
                }
                array_unshift($row, $i);
                fputcsv($handle,$row,"\t");
                next($data);
                $i++;
            }
            fclose($handle);
        }

    }

}
