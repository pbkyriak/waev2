<?php
namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of DeleteProjectCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class DeleteProjectCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('waev:project:delete')
            ->setDescription("Delete a project")
            ->addArgument('projectId', InputArgument::REQUIRED,
                'Project Id to delete?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $projectId = $input->getArgument('projectId');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if(!$project) {
            $output->writeln(sprintf('No project found with id=%s', $projectId));
            return;
        }
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $em->getConnection()->query("SET autocommit=0;"); 
        $em->getConnection()->query(sprintf("delete from project where id=%s;", $projectId));
        $em->getConnection()->query("COMMIT;");
        $output->writeln(sprintf("Project id=%s deleted.", $projectId));
        $output->writeln(sprintf("execution time: %s sec", time()-$iTime));
        return true;

    }

}
