<?php
namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of DeleteProjectCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class DeleteTagCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('waev:tag:delete')
            ->setDescription("Delete a tag")
            ->addArgument('tagId', InputArgument::REQUIRED,
                'Tag Id to delete?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagId');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if(!$tag) {
            $output->writeln(sprintf('No tag found with id=%s', $tagId));
            return;
        }
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $em->getConnection()->query("SET autocommit=0;"); 
        $em->getConnection()->query(sprintf("delete from project_tag where id=%s;", $tagId));
        $em->getConnection()->query("COMMIT;");
        $output->writeln(sprintf("Tag id=%s deleted.", $tagId));
        $output->writeln(sprintf("execution time: %s sec", time()-$iTime));
        return true;

    }

}
