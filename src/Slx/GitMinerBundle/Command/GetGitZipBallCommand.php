<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * @todo drop this file
 */

class GetGitZipBallCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('gitminer:git:getZip')
            ->setDescription("Gets from github project's tag zip")
            ->addArgument('tagid', InputArgument::REQUIRED,
                'Project Tag Id to get source code?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$tag) {
            throw new \Exception(sprintf('ProjectTag with id %s not found!',
                $tagId));
        }
        if( $tag->downloadZipBall() ) {
            $tag->setStatus(ProjectTag::STATUS_SRC_DOWNLOADED);
            $em->persist($tag);
            $em->flush();
        }
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
        return true;
    }

}

