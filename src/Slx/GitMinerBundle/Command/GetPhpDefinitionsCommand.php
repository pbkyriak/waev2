<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of GetPhpDefinitionsCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class GetPhpDefinitionsCommand extends ContainerAwareCommand {

    protected function configure()
    {
        $this
            ->setName('waev:getPhpFuncs')
            ->setDescription("Scrapping from php.net function definitions")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $handle = fopen(__DIR__.'/../../../../phpdefs.csv', "w+");
        $list = $this->getList();
        $lnCnt = count($list);
        $progressBar = $this->getHelper('progress');
        $progressBar->start($output, $lnCnt);
        foreach($list as $mname => $uri) {
            $def = $this->getMethodDefinition($mname, $uri);
            fputcsv($handle, array($mname, $def));
            $progressBar->advance();
        }
        fclose($handle);
        $progressBar->finish();
    }
    
    private function getList() {
        $out = array();
        $url = 'http://php.net/manual/en/indexes.functions.php';
        $content = file_get_contents($url);
        $html = new \DOMDocument();
        if( @$html->loadHTML('<?xml encoding="UTF-8">' .$content) ) {
            $xpath = new \DOMXPath( $html ); 
            $items = $xpath->query(".//a[@class='index']");
            foreach ($items as $item) {
                $out[$item->nodeValue] = $item->getAttribute('href');
            }
            
        }

        return $out;
    }
    
    private function getMethodDefinition($mname, $uri) {
        $out = '';
        $url = 'http://php.net/manual/en/'.$uri;
        $content = file_get_contents($url);
        $html = new \DOMDocument();
        if( @$html->loadHTML('<?xml encoding="UTF-8">' .$content) ) {
            $xpath = new \DOMXPath( $html ); 
            $items = $xpath->query(".//div[@class='methodsynopsis dc-description']");
            foreach ($items as $item) {
                $out = trim(preg_replace('!\s+!', ' ',str_replace("\n", "", $item->nodeValue)));
            }
        }
        return $out;
    }
    
    private function setupProgressBar($output, $lnCnt) {
        $progress = null;
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, $lnCnt);
        $progress->setRedrawFrequency(100);
        $progress->advance();
        return $progress;
    }

}
