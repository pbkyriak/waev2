<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
use JMS\JobQueueBundle\Entity\Job;

class GetProjectTagsCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('gitminer:git:getTags')
            ->setDescription("Gets from github project's tags")
            ->addArgument('prgid', InputArgument::REQUIRED,
                'Project Id to get tags?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $projectId = $input->getArgument('prgid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw new Exception(spinrtf('Project with id %s not found!',
                $projectId));
        }
        $client = new \Github\Client();
        $tagsInfo = $client->api('repo')->tags($project->getOwner(),
            $project->getPname());
        if ($tagsInfo) {
            $output->writeln(sprintf("found in gitHub %s tags for project %s/%s", count($tagsInfo), $project->getOwner(), $project->getPname()));
            $cnt = 0;
            $tagsInfo = array_reverse($tagsInfo);
            foreach ($tagsInfo as $tagInfo) {
                // check if tag exists
                if (!$this->projectTagExists($em, $project, $tagInfo['name'])) {
                    $cnt++;
                    $tag = new ProjectTag();
                    $tag->setProject($project)
                        ->setTag($tagInfo['name'])
                        ->setZipballUrl($tagInfo['zipball_url'])
                        ->setUrl($tagInfo['commit']['url'])
                    ;
                    $em->persist($tag);
                    $em->flush();
                }
            }
            $output->writeln(sprintf("Added %s tags for project %s/%s", $cnt, $project->getOwner(), $project->getPname()));
            $this->addJobs($em, $project);
        } else {
            $output->writeln(sprintf("No tags for project %s/%s", $project->getOwner(), $project->getPname()));
        }
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }

    private function projectTagExists(EntityManager $em, $project, $tag)
    {
        $out = false;
        $o = $em->getRepository('SlxGitMinerBundle:ProjectTag')->findOneBy(array('project' => $project, 'tag' => $tag));
        if ($o)
            $out = true;
        return $out;
    }

    private function addJobs(EntityManager $em, Project $project) {
        $tags = $project->getProjectTags();
        $prevJob = null;
        foreach($tags as $tag) {
            $job1 = new Job('gitminer:git:getZip', array($tag->getId()));
            if( $prevJob ) {
                $job1->addDependency($prevJob);
            }
            $job2 = new Job('gitminer:pd:tag', array($tag->getId()));
            $job2->addDependency($job1);
            $job3 = new Job('gitminer:import:pdxml', array($tag->getId()));
            $job3->addDependency($job2);
            $job4 = new Job('gitminer:cleanTagFiles', array($tag->getId()));
            $job4->addDependency($job3);
            
            if( $prevJob ) {
                $em->persist($prevJob);
            }
            $em->persist($job1);
            $em->persist($job2);
            $em->persist($job3);
            $em->persist($job4);
            $prevJob = $job4;
            break;
        }
        $em->flush();
    }
}