<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Symfony\Component\Process\ProcessBuilder;
use Slx\TaskConfigBundle\AnalysisTask\InitAnalysisLv1Task;

use Slx\GitMinerBundle\AnalysisTask\DiffTask\DiffTagFilesTask;
use Slx\GitMinerBundle\Reflection\FileReflector;
use Slx\ReflectionBundle\Analyser\GlobalCode\GlobalCodeExtractor;
use Slx\ReflectionBundle\Analyser\GlobalCode\GlobalCodeMetrics;
use Slx\ReflectionBundle\AnalysisTask\TagTasks\GlobalCodeTask;
use Slx\ReflectionBundle\Reflection\Traverser;

use Slx\SurvivalBundle\Survival\KMSurvivalFuncs;
use Slx\GitMinerBundle\Analyser\GiniCalculator;

use Slx\TaskAggrBundle\AnalysisTask\FuncRegistryTask;
use Slx\TaskAggrBundle\AnalysisTask\ProjectGenericDiffsTask;

class TestCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('gitminer:test')
            ->setDescription("Gets from github project's tags")
            ->addArgument('tagid',
                InputArgument::REQUIRED,
                'Project Id to get tags?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        //$this->executeDeleteProject($input, $output);
        //$this->execute16($input, $output);
        //$this->execute21($input, $output);
        //$this->execute9($input, $output);
        //$this->interfaceUsage($input, $output);
        //$this->inheritanceTest($input, $output);
        //$this->testReflection($input, $output);
        $this->findNewUDiff($input, $output);
        $output->writeln(sprintf("execution time: %s sec", time()-$t1));
    }

    private function execute1(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$tag) {
            throw new Exception(spinrtf('ProjectTag with id %s not found!',
                $tagId));
        }
        $builder = new ProcessBuilder(array('/var/www/waev/app/console', 'gitminer:git:getZip',
            $tagId));
        $process = $builder->getProcess();
        $process->run();

        if (!($builder->getProcess()->isSuccessful())) {
            $output->writeln('shitt');
            $output->writeln($builder->getProcess()->getErrorOutput());
        }
        $output->writeln("process output: ");
        print $builder->getProcess()->getOutput();
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec',
                $min,
                $sec));
    }


    private function addJobs(EntityManager $em, Project $project)
    {
        $tags = $project->getProjectTags();
        $prevJob = null;
        /* @var $tag ProjectTag */
        foreach ($tags as $tag) {
            $job1 = new Job('gitminer:git:getZip',
                array($tag->getId()));
            if ($prevJob) {
                $job1->addDependency($prevJob);
            }
            $job2 = new Job('gitminer:pd:tag',
                array($tag->getId()));
            $job2->addDependency($job1);
            $job3 = new Job('gitminer:import:pdxml',
                array($tag->getId()));
            $job3->addDependency($job2);
            if ($prevJob) {
                $em->persist($prevJob);
            }
            $em->persist($job1);
            $em->persist($job2);
            $em->persist($job3);

            $prevJob = $job3;
            break;
        }
        $em->flush();
    }

    private function execute9(InputInterface $input, OutputInterface $output)
    {
        $fn = '/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces1c.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        //var_dump($stmts);die(1);
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        
        printf("GLOBAL CONSTANTS \n");
        foreach ($fr->getConstants() as $const) {
            printf("const name=%s values=%s\n", $const->getName(), $const->getValue());
        }

        printf("FUNCTIONS \n");
        foreach ($fr->getFunctions() as $func) {
            printf("func name= %s sl=%s el=%s signature=%s hash=%s\n",
                $func->getName(),
                $func->getCodeBlockStartLine(),
                $func->getCodeBlockEndLine(),
                $func->getSignature(),
                $func->getCodeBlockHash()
            );
        }

        printf("CLASSES \n");
        foreach ($fr->getClasses() as $class) {
            printf("class %s extends=%s\n", $class->getFQName(), $class->getParentClass());
            foreach ($class->getMethods() as $meth) {
                printf("method name= %s sl=%s el=%s signature=%s hash=%s\n",
                    $meth->getName(),
                    $meth->getStartLine(),
                    $meth->getEndLine(),
                    $meth->getSignature(),
                    $meth->getHash()
                );
            }
            foreach ($class->getConstants() as $const) {
                printf("const name=%s values=%s\n", $const->getName(), $const->getValue());
            }
            foreach ($class->getProperties() as $prop) {
                printf("prop name=%s default=%s\n", $prop->getName(), $prop->getDefault());
            }
        }
        printf("INTERFACES \n");
        foreach($fr->getInterfaces() as $inter) {
            printf("interface %s\n", $inter->getName());
            foreach ($inter->getMethods() as $meth) {
                printf("method name= %s sl=%s el=%s signature=%s hash=%s\n",
                    $meth->getName(),
                    $meth->getStartLine(),
                    $meth->getEndLine(),
                    $meth->getSignature(),
                    $meth->getHash()
                );
            }
        }
        printf("TRAITS \n");
        foreach ($fr->getTraits() as $class) {
            printf("traits %s\n", $class->getName());
            foreach ($class->getMethods() as $meth) {
                printf("method name= %s sl=%s el=%s signature=%s hash=%s\n",
                    $meth->getName(),
                    $meth->getStartLine(),
                    $meth->getEndLine(),
                    $meth->getSignature(),
                    $meth->getHash()
                );
            }
            foreach ($class->getConstants() as $const) {
                printf("const name=%s values=%s\n", $const->getName(), $const->getValue());
            }
            foreach ($class->getProperties() as $prop) {
                printf("prop name=%s default=%s\n", $prop->getName(), $prop->getDefault());
            }
        }

    }

    private function execute8(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        for($tagId=3004; $tagId<=3060; $tagId++) {
            printf("tagid=%s\n", $tagId);
            $task = new DiffTagFilesTask($em);
            $task->run(array('tag_id' => $tagId));
        }
    }

    private function execute11(InputInterface $input, OutputInterface $output)
    {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $kms = new KMSurvivalFuncs($em);
        $kms->setParameters($projectId,0);
        $results = $kms->calculate();
        for($i=0; $i<count($results); $i++) {
            printf("%s;%s;%s\n", $results[$i]['tag'], $i, $results[$i]['estimator']);
        }
    }
    
    private function execute12() {
        $data = array(1000,2000,3000,4000,5000);
        $gini = new GiniCalculator();
        $g = $gini->setData($data)->getGini();
        printf("gini=%s\n", $g);
    }
    
    private function execute13(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default'); 
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        echo "Memory usage before: " . (memory_get_usage() / 1024) . " KB" . PHP_EOL;
        foreach($project->getProjectTags() as $tag) {
            //if( $tag->getId()==2986 ) {
                $output->writeln(sprintf("analysing tag_id %s", $tag->getId()));
                $task = new GlobalCodeTask($em);
                $task->configure(array('tag_id'=>$tag->getId()));
                $task->execute();
                $task=null;
                time_nanosleep(0, 10000000);
                gc_collect_cycles();
            //}
        }
    }

    private function execute14(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default'); 
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        $startTag = $project->getProjectTags()->first();
        $droped = $em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getTagDroppedFunctions($startTag);
        printf("count=%s\n", count($droped));
        foreach($droped as $func) {
            $q = $em->createQuery("SELECT sum(a.nb_calls) as nbCalls FROM SlxGitMinerBundle:ProjectTagFuncCallAggr a WHERE a.func=:func and a.projectTag>=:tag")
                ->setParameters(array('func'=>$func['cname'], 'tag'=>$func['droped_at_tag_id']));
            $nbCallsAfter = $q->getSingleScalarResult();
            $q = $em->createQuery("SELECT t.id, a.nb_calls FROM SlxGitMinerBundle:ProjectTagFuncCallAggr a LEFT JOIN a.projectTag t WHERE a.func=:func and a.projectTag<:tag")
                ->setParameters(array('func'=>$func['cname'], 'tag'=>$func['droped_at_tag_id']));
            $nbCallsBefore = $q->getArrayResult();
            $output->writeln(sprintf("func=%s dropTag=%s after calls=%s", $func['cname'], $func['droped_at_tag_id'], $nbCallsAfter));
            foreach($nbCallsBefore as $row) {
                $output->writeln(sprintf("\ttag=%s calls=%s", $row['id'], $row['nb_calls']));
            }
        }
        
    }
    
    /**
     * Project level analysis. Must be executed after tag level analysis (Lv1)
     * Must be moved to an AnalysisTask
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    private function execute15(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        
    /*
        foreach($project->getProjectTags() as $tag) {
            $output->writeln(sprintf("analysing tag_id %s", $tag->getId()));
            $task = new \Slx\GitMinerBundle\AnalysisTask\TagTasks\AggrFuncCallsTask($em);
            $task->configure(array('tag_id' => $tag->getId()));
            $task->execute();
            $task=null;
            time_nanosleep(0, 10000000);
            gc_collect_cycles();
        }
       */
        $output->writeln('Executing FuncRegistryTask');
        $task = new \Slx\GitMinerBundle\AnalysisTask\AggrTasks\FuncRegistryTask($em);
        $task->configure(array('project_id' => $project->getId()));
        $task->execute();
        
        $output->writeln('Executing ProjectGenericDiffsTask');
        $task = new \Slx\GitMinerBundle\AnalysisTask\AggrTasks\ProjectGenericDiffsTask($em);
        $task->configure(array('project_id' => $project->getId()));
        $task->execute();

        $output->writeln('Executing FuncSurvivalTask');
        $task = new \Slx\SurvivalBundle\AnalysisTask\AggrTasks\FuncSurvivalTask($em);
        $task->configure(array('project_id' => $project->getId()));
        $task->execute();

        $output->writeln('Executing DiffFuncSignaturesTask');
        $task = new \Slx\GitMinerBundle\AnalysisTask\AggrTasks\DiffFuncSignaturesTask($em);
        $task->configure(array('project_id' => $project->getId()));
        $task->execute();

    }
    
    private function execute16(InputInterface $input, OutputInterface $output) {
        $id = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        $task = new \Slx\ReflectionBundle\AnalysisTask\FuncSignaturesTask($em);
        $task->configure(array('tag_id' => $id));
        $task->execute();

        //$an = new \Slx\GitMinerBundle\Analyser\FuncSurvival($em);
        //$an->analyse($projectId);
    }
    
    private function execute17(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        $data = array();
        $handle = fopen("/var/www/waev/wordpress-functions-lifetime.csv", "r");
        if( $handle ) {
            while( $row = fgetcsv($handle, 1000, ',')) {
                $data[] = $row;
            }
        }
        print_R($data);
        
        $an = new \Slx\GitMinerBundle\Analyser\Survival\DataKaplanMeier($data, 0, 1, 2);
        $results = $an->getResults();
        foreach($results as $result) {
            printf("day=%s inRisk=%s slaughtered=%s estimator=%s\n", $result['day'], $result['inRisk'], $result['slaughtered'], $result['estimator']);
        }
        
    }
    
    private function execute18(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');

        $settings = new \Slx\SurvivalBundle\Analyser\Survival\FuncCheckPointsSetttingsWC();
        $settings->setProjectId($projectId);
        $an = new \Slx\SurvivalBundle\Analyser\Survival\FuncCheckPointsSurvival($em);
        $an->setParameters($settings);
        $an->analyse();
        
        $settings = new \Slx\SurvivalBundle\Analyser\Survival\FuncCheckPointsSetttingsNC();
        $settings->setProjectId($projectId);
        $an = new \Slx\SurvivalBundle\Analyser\Survival\FuncCheckPointsSurvival($em);
        $an->setParameters($settings);
        $an->analyse();

    }
    
    private function execute19(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);

        foreach($project->getProjectTags() as $tag) {
            $fanouts = $em->getRepository('SlxGitMinerBundle:ProjectTagFuncCallAggr')->getTagFanouts($tag->getId());
            $gca = new \Slx\GitMinerBundle\Analyser\GiniCalculator();
            $gca->setData($fanouts);
            $output->writeln(sprintf("tag=%s gini=%s", $tag->getId(), $gca->getGini()));
        }
    }

    private function executeDeleteProject(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        $output->writeln("Deleting project ".$project->getId());
        $em->remove($project);
        $em->flush();
        $output->writeln('Deleted.');
    }

    /**
     * Test task. Testing phpDCD and phpDCD import
     * 
     * @param \Symfony\Component\Console\Input\InputInterface $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     */
    private function execute20(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default'); 
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        echo "Memory usage before: " . (memory_get_usage() / 1024) . " KB" . PHP_EOL;
        foreach($project->getProjectTags() as $tag) {
                $output->writeln(sprintf("analysing tag_id %s", $tag->getId()));
                $task = new \Slx\GitMinerBundle\AnalysisTask\TagTasks\GetGitZipTask($em);
                $task->configure(array('tag_id'=>$tag->getId()));
                $task->execute();
                $task=null;
                $task = new \Slx\GitMinerBundle\AnalysisTask\TagTasks\PhpDCDTask($em);
                $task->configure(array('tag_id'=>$tag->getId()));
                $task->execute();
                $task=null;
                $task = new \Slx\GitMinerBundle\AnalysisTask\TagTasks\ImportPhpDCDTask($em);
                $task->configure(array('tag_id'=>$tag->getId()));
                $task->execute();
                $task=null;
                $task = new \Slx\GitMinerBundle\AnalysisTask\TagTasks\DropSourceTask($em);
                $task->configure(array('tag_id'=>$tag->getId()));
                $task->execute();
                $task=null;
                time_nanosleep(0, 10000000);
                gc_collect_cycles();
        }
        $task = new \Slx\GitMinerBundle\AnalysisTask\AggrTasks\FuncRegistryTask($em);
        $task->configure(array('project_id'=>$tag->getId()));
        $task->execute();
        $task=null;
    }
    
    private function execute21(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $dwu = new \Slx\SurvivalBundle\DataWriter\Survival\FuncStaggeredExport($this->em,$projectId);
        $dwu->export();
    }
    
    private function execute22(InputInterface $input, OutputInterface $output) {
        //$factory = $this->getContainer()->get('waev.task.factory');
        //$factory->dumpDefs();
        //$task = $factory->getObject("InitAnalysisLv1");
        echo 'hello';
    }
    
    private function interfaceUsage(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        $an = new \Slx\TaskTagBundle\Analyser\InterfaceUsage($this->em);
        foreach($project->getProjectTags() as $tag) {
            $an->countTag($tag->getId());
        }
    }
    private function testReflection(InputInterface $input, OutputInterface $output) {
        $tagId = $input->getArgument('tagid');
        $tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        $scHelper = new \Slx\ReflectionBundle\Analyser\SourceCodeHelper($tag);
        $file = ['id'=>'4223321', 'fname'=>'/typo3/sysext/cms/layout/class.tx_cms_layout.php'];
        $ast = $scHelper->getFileAst($file);
        $reflector = new \Slx\ReflectionBundle\Reflection\FileReflector($file);
        $traverser = new \Slx\ReflectionBundle\Reflection\Traverser();
        $traverser->addVisitor($reflector);
        $traverser->traverseAst($ast);
        printf("%s\n", count($reflector->getClasses()));
        foreach( $reflector->getClasses() as $clazz ) {
            printf("%s\n", $clazz->getSignature());
        }
    }
    
    private function updateProjectClassRelations(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $sql = "update project_class a, project_class b set a.parent_id=b.id
                where a.project_tag_id=:tid and a.extends!='' and b.project_tag_id=a.project_tag_id and a.extends=b.fqname and not isnull(b.id);";
        $stmt = $this->em->getConnection()->executeQuery("select id from project_tag where project_id=:pid", ['pid'=>$projectId]);
        while( $row =$stmt->fetch() ) {
            $rAff = $this->em->getConnection()->executeUpdate($sql, ['tid'=>$row['id']]);
            printf("tagID=%s affected=%s\n", $row['id'], $rAff);
        }
    }    
    
    private function findNewUDiff(InputInterface $input, OutputInterface $output) {
        $projectId = $input->getArgument('tagid');
        $fnames = $this->em->getConnection()->executeQuery("select id from project_tag where project_id=:pid", ['pid'=>$projectId])->fetchAll(\PDO::FETCH_ASSOC);
        $q1= "select a.cname, a.cnt c1, b.cnt c2
                from
                (
                select cname, sum(pm.metric) as cnt
                from project_metric as pm
                left join project_tag as pt on (pm.project_tag_id=pt.id)
                where pm.metric_group_id=6 and ctype='file' 
                and pm.project_tag_id =:tid
                group by cname
                ) a
                left join (
                select cname, sum(pm.metric) as cnt
                from project_metric as pm
                where pm.metric_group_id=3 and ctype='file' 
                and pm.project_tag_id =:tid
                and pm.metric_name like 'OI:NEW:%'
                group by cname) b
                on (a.cname=b.cname)
                having a.cnt- b.cnt!=0
                ";
        
        $q1 = "select if( isnull(a.cname), b.cname, a.cname) as fname, if(isnull(a.cnt), 0, a.cnt) c1, if(isnull(b.cnt), 0, b.cnt) c2
from
(
	select cname, sum(pm.metric) as cnt
	from project_metric as pm
	left join project_tag as pt on (pm.project_tag_id=pt.id)
	where pm.metric_group_id=6 and ctype='file' 
	and pm.project_tag_id =:tid
	group by cname
) a
 left outer join (
	select cname, sum(pm.metric) as cnt
	from project_metric as pm
	where pm.metric_group_id=3 and ctype='file' 
	and pm.project_tag_id =:tid
	and pm.metric_name like 'OI:NEW:%'
    group by cname
) b
on (a.cname=b.cname)
having c1-c2!=0

union 

select if( isnull(a.cname), b.cname, a.cname) as fname, if(isnull(a.cnt), 0, a.cnt) c1, if(isnull(b.cnt), 0, b.cnt) c2
from
(
	select cname, sum(pm.metric) as cnt
	from project_metric as pm
	left join project_tag as pt on (pm.project_tag_id=pt.id)
	where pm.metric_group_id=6 and ctype='file' 
	and pm.project_tag_id =:tid
	group by cname
) a
 right outer join (
	select cname, sum(pm.metric) as cnt
	from project_metric as pm
	where pm.metric_group_id=3 and ctype='file' 
	and pm.project_tag_id =:tid
	and pm.metric_name like 'OI:NEW:%'
    group by cname
) b
on (a.cname=b.cname)
having c1-c2!=0";
        $dCnt = 0;
        
        $fnames = array(4955,4956,5019,5097,5096,5098,5099,5100,5101,5105,5106,5107,5108,5112);
        if( $fnames ) {
            //foreach($fnames as $fnamer) {
            //    $fname = $fnamer['id'];
            foreach($fnames as $fname) {
                $results = $this->em->getConnection()->executeQuery($q1, ['tid'=>$fname])->fetchAll(\PDO::FETCH_ASSOC);
                if( $results ) {
                    //printf("Tag: %s\n", $fname);
                    foreach($results as $result) {
                        printf("%s\t%s\t%s\t%s\n", $fname,$result['fname'], $result['c1'], $result['c2']);
                        $dCnt+= abs($result['c1']-$result['c2']);
                    }
                }
            }
        }
        if($dCnt) {
            printf("%s diffs!! :)\n", $dCnt);            
        }
        else {
            printf("no diff!! :)\n", $projectId);
        }
    }
    /// -----------------------------------------------------------------------------------------------------
    private $tagClasses;
    
    private function inheritanceTest(InputInterface $input, OutputInterface $output) {
        $this->tagClasses = array();
        $stmt = $this->em->getConnection()->executeQuery("SELECT fname,extends from project_class where project_tag_id=4825");
        while($row=$stmt->fetch()) {
            $cname = $row['fname'];
            $tc = new TClass(1, $cname, substr($row['extends'],1));
            $this->addClassMethods($cname, $tc);
            $this->tagClasses[$cname] = $tc;
        }
        
        foreach($this->tagClasses as $cname => $tc) {
            //$tc = $this->tagClasses['tx_cms_webinfo_page'];

            $this->getClassExtend($tc, $tc->ename,1);
            $this->dumpClass($tc);
        }
    }
    private function addClassMethods($cname, $tc) {
        $stmt2 = $this->em->getConnection()->executeQuery(
                "SELECT fname from project_func_signature where project_tag_id=4825 and fname like :cn", 
                array('cn'=>sprintf("%s::%%",$cname))
            );
        while( $row=$stmt2->fetch()) {
            $mname = str_replace($cname.'::', '', $row['fname']);
            $tc->addMethod($mname, $cname);
        }
    }
    
    private function getClassExtend($tc, $ename, $level) {
        //printf("%s EXTENDS %s level %s\n", $tc->cname, $ename, $level);
        if($level>5) die(1);
        if( $ename!='' ) {
            if( isset($this->tagClasses[$ename])) {
                foreach($this->tagClasses[$ename]->getMethods() as $fname) {
                    $mname = str_replace($ename.'::', '', $fname);
                    $this->tagClasses[$tc->cname]->addMethod($mname, $ename);
                }
                if( $this->tagClasses[$ename]->ename!='') {
                    $this->getClassExtend($tc,$this->tagClasses[$ename]->ename, $level+1);
                }
            }
            else {
                printf("EXTEND NOT FOUND %s \n", $ename);
            }
        }
    }
    
    private function dumpClass($tc) {
        printf("%s \n", $tc->cname);
        foreach($tc->getMethods() as $m) {
            $mdef = $tc->getMDefiner($m);
            printf("\t%s %s\n", $m, $mdef ? '@'.$mdef:'');
        }
    }
}

class TClass {
    
    private $id;
    public $cname;
    public $ename;
    private $methods = array();
    private $mdefiners = array();
    
    public function __construct($id, $cname, $ename) {
        $this->id = $id;
        $this->cname = $cname;
        $this->ename = $ename;
    }
    
    public function addMethod($cname, $definer) {
        if( !in_array($cname, $this->methods)) {
            $this->methods[] = $cname;
            $this->mdefiners[$cname] = $definer;
        }
    }
    
    public function getMethods() {
        return $this->methods;
    }
    
    public function getMDefiner($mname) {
        return isset($this->mdefiners[$mname]) ? ($this->mdefiners[$mname]!=$this->cname ? $this->mdefiners[$mname] : '') : '';
    }
}