<?php

namespace Slx\GitMinerBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DOMDocument;
use DOMXPath;
/**
 * Description of WpFuncReference
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class WpFuncReferenceCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('gitminer:wpfuncref:update')
            ->setDescription("Gets wordpress.org function reference")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $url = "http://codex.wordpress.org/Function_Reference";
        $html = $this->getHtml($url);
        $funcList = $this->parseHtml($html);
        print_r($funcList);
    }

    private function getHtml($url)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_COOKIEJAR, 'cookie.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($ch);
        curl_close($ch);
        return $content;
    }

    private function parseHtml($html) {
        $out = array();
        $dom = new DOMDocument(); 
        $dom->loadHTML($html);
		$xpath = new DOMXPath( $dom ); 
		$links = $xpath->query( "//td//tt//a" ); 
        foreach($links as $link) {
            $out[] = $link->nodeValue;
        }
        sort($out);
        return $out;
    }
}
