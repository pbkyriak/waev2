<?php

namespace Slx\GitMinerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SlxGitMinerBundle:Default:index.html.twig');
    }
}
