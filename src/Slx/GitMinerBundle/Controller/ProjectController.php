<?php

namespace Slx\GitMinerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Form\ProjectType;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\AnalysisTask;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use JMS\JobQueueBundle\Entity\Job;

/**
 * Project controller.
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")

 */
class ProjectController extends Controller
{

    /**
     * Lists all Project entities.
     * @Breadcrumb("Λίστα")
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return $this->render('SlxGitMinerBundle:Project:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxGitMinerBundle:Project a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['pname']) {
                $where[] = " a.pname like :pname ";
                $filterQryParams['pname'] = str_replace('*', '%',
                    $filterData['pname']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'pname', 'text',
                array('required' => false, 'label' => 'gitminer.project.pname')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function createAction(Request $request)
    {
        $entity = new Project();
        $form = $this->createForm(new ProjectType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('git_project')->addProjectTagsFromGit($entity);
            $this->get('session')->getFlashBag()->add('info','gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('project_show', array('id' => $entity->getId())));
        } else {
            $this->get('session')->getFlashBag()->add('notice','gsprod.general.record_errors');
        }

        return $this->render('SlxGitMinerBundle:Project:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new Project();
        $form = $this->createForm(new ProjectType(), $entity);

        return $this->render('SlxGitMinerBundle:Project:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Project entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $entity = $this->getProject($id);
        $editForm = $this->createEditForm($entity);

        return $this->render('SlxGitMinerBundle:Project:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Creates a form to edit a Project entity.
     *
     * @param Project $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Project $entity)
    {
        $form = $this->createForm(new ProjectType(), $entity,
            array(
            'action' => $this->generateUrl('project_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));
        return $form;
    }

    /**
     * Edits an existing Project entity.
     * @Breadcrumb("Προσθήκη")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getProject($id);
        $editForm = $this->createForm(new ProjectType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_saved');
            return $this->redirect($this->generateUrl('project_show', array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'gsprod.general.record_errors');
        }

        return $this->render('SlxGitMinerBundle:Project:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a Project entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        $job = new Job('waev:project:delete', array($project->getId()));
        $em->persist($job);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'gitminer.project.project_to_be_deleted');

        return $this->redirect($this->generateUrl('project'));
    }

    public function deleteTagAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($id);
        if (!$tag) {
            throw $this->createNotFoundException('Unable to find ProjectTag entity.');
        }
        $project = $tag->getProject();
        $tag->dropFiles();
        $em->remove($tag);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_deleted');
        return $this->redirect($this->generateUrl('project_show', array('id' => $project->getId())));
    }

    /**
     * adds project to analysis queue
     */
    public function addToAnalysisAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        $em->getRepository('SlxGitMinerBundle:Project')->addProjectToAnalysis($project);
        $this->get('session')->getFlashBag()->add('info','gitminer.project.project_added_to_analysis_queue');
        return $this->redirect($this->generateUrl('project_show', array('id' => $project->getId()) ) );
    }

    /**
     * adds project to analysis queue again (secondary analysis)
     */
    public function addToSupplAnalysisAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        $em->getRepository('SlxGitMinerBundle:Project')
            ->cleanAnalysisTasks($project)
            ->addProjectToAnalysis($project);
        $this->get('session')->getFlashBag()->add('info','gitminer.project.project_added_to_analysis_queue');
        return $this->redirect($this->generateUrl('project_show', array('id' => $project->getId()) ) );
    }

    /**
     * Finds and displays a Project entity.
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getProject($id);
        $this->get("apy_breadcrumb_trail")->add($entity->getPname(), 'project_show', array('id'=>$entity->getId()));
        
        $jobId = $entity->getJobId();
        $job=null;
        if( $jobId ) {
            $job = $em->getRepository('JMSJobQueueBundle:Job')->find($jobId);
        }
        return $this->render('SlxGitMinerBundle:Project:show.html.twig',
                array(
                'entity' => $entity,
                'job' => $job,
        ));
    }
    
    public function runInfoAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProject($id);
        $this->get("apy_breadcrumb_trail")->add($entity->getPname(), 'project_show', array('id'=>$entity->getId()));
        $this->get("apy_breadcrumb_trail")->add('Run information');
        
        $jobId = $entity->getJobId();
        $job=null;
        if( $jobId ) {
            $job = $em->getRepository('JMSJobQueueBundle:Job')->find($jobId);
        }
        
        $isResumable = $job ? $em->getRepository('SlxGitMinerBundle:Project')->isResumable($entity, $job) : false;
        $isCancelable = $em->getRepository('SlxGitMinerBundle:Project')->isCancelable($entity);
        return $this->render('SlxGitMinerBundle:Project:runInfo.html.twig',
                array(
                'entity' => $entity,
                'job' => $job,
                'isResumable' => $isResumable,
                'isCancelable' => $isCancelable,
        ));
        
    }
    
    public function batchAction(Request $request) {
        $action = $request->get('batch_action');
        $finalAction = sprintf("batch%s", ucfirst($action));
        if( !method_exists($this, $finalAction) ) {
            throw new \RuntimeException(sprintf('A `%s::%s` method must be created', get_class($this), $finalAction));
        }
        return call_user_func(array($this, $finalAction), $request);
    }
    
    /**
     * called from batchAction
     * 
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    private function batchDelete(Request $request) {
        $tagIds = $request->get('idx');
        $tagDescrs = array();
        $em = $this->getDoctrine()->getManager();
        foreach($tagIds as $id) {
            $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($id);
            if ($tag) {
                $tagDescrs[] = $tag->getTag();
                $project = $tag->getProject();
                $tag->dropFiles();
                $em->remove($tag);
            }
        }
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'gsprod.general.records_deleted');

        return $this->redirect($this->generateUrl('project_show', array('id' => $project->getId())));

    }
    
    public function redoAnalysisAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        if( $em->getRepository('SlxGitMinerBundle:Project')->hasRunningJob($project) ) {
            $this->get('session')->getFlashBag()->add('notice', 'gitminer.project.analysis_is_running');
        }
        else {
            $em->getRepository('SlxGitMinerBundle:Project')->resetAnalysis($project);
            $this->get('session')->getFlashBag()->add('info','gitminer.project.analysis_will_redo');
        }
        return $this->redirect($this->generateUrl('project_run_info', array('id' => $project->getId())));                        
    }

    public function redoAnalysisNCAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        if( $em->getRepository('SlxGitMinerBundle:Project')->hasRunningJob($project) ) {
            $this->get('session')->getFlashBag()->add('notice', 'gitminer.project.analysis_is_running');
        }
        else {
            $em->getRepository('SlxGitMinerBundle:Project')->resetAnalysis($project, false);
            $this->get('session')->getFlashBag()->add('info','gitminer.project.analysis_will_redo');
        }
        return $this->redirect($this->generateUrl('project_run_info', array('id' => $project->getId())));                        
    }

    public function killRestTasksAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        $em->getRepository('SlxGitMinerBundle:Project')->killRestTasks($project);
        $this->get('session')->getFlashBag()->add('info','gitminer.project.kill_rest_tasks_info');
        return $this->redirect( $this->generateUrl('project_run_info',array('id' => $project->getId()) ));
    }
    
    public function cancelAnalysisAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        if( $em->getRepository('SlxGitMinerBundle:Project')->cancelProject($project) ) {
            $this->get('session')->getFlashBag()->add('info', 'gitminer.project.cancel_analysis_info');
        }
        return $this->redirect($this->generateUrl('project_run_info', array('id' => $project->getId())));        
    }
    
    public function resumeFailedAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($id);
        $em->getRepository('SlxGitMinerBundle:Project')->resumeProject($project);
        return $this->redirect($this->generateUrl('project_run_info', array('id' => $project->getId())));        
    }
    
    private function getProject($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($id);
        if(!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        return $project;
    }
} 
// https:\/\/github.com\/([a-z0-9_\-]+)\/([a-z0-9_\-]+)(.git)?