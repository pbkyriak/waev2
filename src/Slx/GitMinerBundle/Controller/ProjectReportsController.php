<?php

namespace Slx\GitMinerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * ProjectReports controller.
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")

 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectReportsController extends Controller
{
    private function getProjectEntity($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxGitMinerBundle:Project')->find($id);        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $this->get("apy_breadcrumb_trail")->add($entity->getPname(), 'project_show', array('id'=>$entity->getId()));
        return $entity;
    }
    
    public function showAddModRemAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('To explore...');

        $addModRem = $em->getRepository('SlxGitMinerBundle:ProjectTagDiff')->getAddModRem($entity);

        $reports = array();
        $reportIds = array(19,20,21,18,25,22,23,24);
        foreach( $reportIds as $rId) {
            $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
            $rep1->setRptEntityId($rId)
                ->setParameters(array('project_id'=>$entity->getId()))
                ->createResults();
            $reports[] = $rep1;
        }

        return $this->render('SlxGitMinerBundle:ProjectReports:showAddModRem.html.twig',
                array(
                'entity' => $entity,
                'addModRem' => $addModRem,
                'reports' => $reports,
        ));        
    }
    
    public function funcSurvivalAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Survival');

        $rep1 = new \Slx\SurvivalBundle\Reports\ReportFuncRemKM($em);
        $rep1->setParameters(array('project'=>$entity));
        $rep1->createResults();

        $rep4 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep4->setRptEntityId(31)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep5 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep5->setRptEntityId(15)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
                
        $rep6 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep6->setRptEntityId(35)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        
        return $this->render('SlxGitMinerBundle:ProjectReports:funcSurvival.html.twig',
                array(
                'entity' => $entity,
                'descrId' => 'all',
                'rep1' => $rep1,
                'rep4' => $rep4,
                'rep5' => $rep5,
                'rep6' => $rep6,
        ));    
    }
    
    public function funcSurvivalWCAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Function slaughter only with calls');

        $rep1 = new \Slx\SurvivalBundle\Reports\ReportFuncRemKM($em);
        $rep1->setParameters(array('project'=>$entity, 'atype'=>1));
        $rep1->createResults();
        
        $rep5 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep5->setRptEntityId(17)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        /*
        $rep6 = new \Slx\GitMinerBundle\Reports\ReportAllWCFuncRemKM($em);
        $rep6->setParameters(array('project'=>$entity));
        $rep6->createResults();
        */
        $rep6 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep6->setRptEntityId(37)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        return $this->render('SlxGitMinerBundle:ProjectReports:funcSurvival.html.twig',
                array(
                'entity' => $entity,
                'descrId' => 'wc',
                'rep1' => $rep1,
                'rep5' => $rep5,
                'rep6' => $rep6,
        ));  
        
        
    }
    
    public function funcSurvivalNCAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Function slaughter only without calls');

        $rep1 = new \Slx\SurvivalBundle\Reports\ReportFuncRemKM($em);
        $rep1->setParameters(array('project'=>$entity, 'atype'=>2));
        $rep1->createResults();
        
        $rep5 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep5->setRptEntityId(16)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        /*
        $rep6 = new \Slx\GitMinerBundle\Reports\ReportAllNCFuncRemKM($em);
        $rep6->setParameters(array('project'=>$entity));
        $rep6->createResults();
        */
        $rep6 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep6->setRptEntityId(36)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        return $this->render('SlxGitMinerBundle:ProjectReports:funcSurvival.html.twig',
                array(
                'entity' => $entity,
                'descrId' => 'nc',
                'rep1' => $rep1,
                'rep5' => $rep5,
                'rep6' => $rep6,
        ));  
                
    }
    
    public function funcNCAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Functions without calls');

        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(14)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        
        return $this->render('SlxGitMinerBundle:ProjectReports:funcNC.html.twig',
                array(
                'entity' => $entity,
                'descrId' => 'nc',
                'rep1' => $rep1,
        )); 
    }
    
    public function ccnAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('CCN');

        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(10)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep2->setRptEntityId(9)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep3 = new \Slx\GitMinerBundle\Reports\ReportFuncCcnGini($em);
        $rep3->setParameters(array('project'=>$entity));
        $rep3->createResults();

        return $this->render('SlxGitMinerBundle:ProjectReports:showCCN.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rep1,
                'rep2' => $rep2,
                'rep3' => $rep3,
        )); 
        
    }

    public function clocAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('CLOC');

        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(11)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep2->setRptEntityId(6)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        
        $rep3 = new \Slx\GitMinerBundle\Reports\ReportFuncClocGini($em);
        $rep3->setParameters(array('project'=>$entity));
        $rep3->createResults();
        
        return $this->render('SlxGitMinerBundle:ProjectReports:showCLOC.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rep1,
                'rep2' => $rep2,
                'rep3' => $rep3,
        )); 

    }
    
    public function sizeGrowAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Size growth');

        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(12)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep2->setRptEntityId(13)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();
        
        return $this->render('SlxGitMinerBundle:ProjectReports:sizeGrow.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rep1,
                'rep2' => $rep2,
        )); 

    }
    
    public function fanoutAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Fan-Out');

        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(8)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep2->setRptEntityId(7)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep3 = new \Slx\GitMinerBundle\Reports\ReportFuncFanoutGini($em);
        $rep3->setParameters(array('project'=>$entity));
        $rep3->createResults();

        return $this->render('SlxGitMinerBundle:ProjectReports:fanout.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rep1,
                'rep2' => $rep2,
                'rep3' => $rep3,
        )); 

    }
    
    public function locAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('LOC');
        
        $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep1->setRptEntityId(4)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rep2->setRptEntityId(5)
            ->setParameters(array('project_id'=>$entity->getId()))
            ->createResults();

        $rep3 = new \Slx\GitMinerBundle\Reports\ReportFuncLocGini($em);
        $rep3->setParameters(array('project'=>$entity));
        $rep3->createResults();

        return $this->render('SlxGitMinerBundle:ProjectReports:funcLoc.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rep1,
                'rep2' => $rep2,
                'rep3' => $rep3,
        )); 

    }
    
    public function funcSignatureAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Function signatures');
        
        $reports = array();
        $reportIds = array(26,27,28,29,30);
        foreach( $reportIds as $rId) {
            $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
            $rep1->setRptEntityId($rId)
                ->setParameters(array('project_id'=>$entity->getId()))
                ->createResults();
            $reports[] = $rep1;
        }

        return $this->render('SlxGitMinerBundle:ProjectReports:funcSignature.html.twig',
                array(
                'entity' => $entity,
                'reports' => $reports,
        ));      
    }
    
    public function runSimpleRptAction($id, $rptId) {
        $em = $this->getDoctrine()->getManager();
        $reports = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->findAll();
        $r = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->getCategorizedReports();

        $entity = $this->getProjectEntity( $id);        
        $this->get("apy_breadcrumb_trail")->add('Εκτέλεση αναφορών');

        $rpt1 = null;
        if( $rptId!=0 ) {
            try {
                $rpt1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
                $rpt1->setRptEntityId($rptId);
                $rpt1->setParameters(array('project_id'=>$entity->getId()))->createResults();            
            } 
            catch(\Slx\GraphRptBundle\Exception\ReportNotFoundException $ex) {
                $this->get('session')->getFlashBag()->add('notice',$ex->getMessage());
                $rpt1 = null;
            }
        }
        
        return $this->render('SlxGitMinerBundle:ProjectReports:runSimpleRpt.html.twig',
                array(
                'entity' => $entity,
                'reports' => $r,
                'rep1' => $rpt1,
                )
        ); 
        
    }
    
    public function testAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $this->getProjectEntity( $id);        
        
        $rptEntity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find(2);
        $rpt1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $rpt1->setRptEntity($rptEntity)->setParameters(array('project_id'=>$entity->getId()))->createResults();
        
        return $this->render('SlxGitMinerBundle:ProjectReports:test.html.twig',
                array(
                'entity' => $entity,
                'rep1' => $rpt1,
        )); 
    }
    
    public function deadFuncKMAction($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getProjectEntity( $id);        
        $rpt1 = new \Slx\SurvivalBundle\Reports\ReportDeadFuncKM($em, $id);
        return $this->render('SlxGitMinerBundle:ProjectReports:deadFuncKM.html.twig',
                array(
                'entity' => $entity,
                'rpt1' => $rpt1,
        )); 
        
    }
}
