<?php

namespace Slx\GitMinerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Form\ProjectType;
use Slx\GitMinerBundle\Entity\ProjectTag;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use \PDO;
/**
 * Description of ProjectTagController
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectTagController extends Controller
{

    public function indexAction($projectId, $tagId)
    {
        $em = $this->getDoctrine()->getManager();
        list( $project, $projectTag, $dir, $file, $folders) = $this->getParameterEntities($projectId, $tagId, null, null);
                
        $files = $em->getRepository('SlxGitMinerBundle:ProjectFile')->findBy(array('projectTag' => $projectTag, 'dir_depth' => 1),
            array('fname' => 'asc'));
        $dirs = $em->getRepository('SlxGitMinerBundle:ProjectDir')->findBy(array('projectTag' => $projectTag, 'dir_depth' => 1),
            array('fname' => 'asc'));
        return $this->render('SlxGitMinerBundle:ProjectTag:index.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'dir' => null,
                'dirList' => $dirs,
                'fileList' => $files,
                'folders' => null,
        ));
    }

    public function dirAction($projectId, $tagId, $dirId)
    {
        $em = $this->getDoctrine()->getManager();
        list( $project, $projectTag, $dir, $file, $folders) = $this->getParameterEntities($projectId, $tagId, $dirId, null);
        $files = $em->getRepository('SlxGitMinerBundle:ProjectFile')->getDirFiles($projectTag,
            $dir);
        $dirs = $em->getRepository('SlxGitMinerBundle:ProjectDir')->getDirSubDirs($projectTag,
            $dir);

        return $this->render('SlxGitMinerBundle:ProjectTag:index.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'dir' => $dir,
                'dirList' => $dirs,
                'fileList' => $files,
                'folders' => $folders,
        ));
    }

    public function fileAction($projectId, $tagId, $fileId)
    {
        $em = $this->getDoctrine()->getManager();
        list( $project, $projectTag, $dir, $file, $folders) = $this->getParameterEntities($projectId, $tagId, null, $fileId);
        $fContents = $em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getFileContents($file);
        return $this->render('SlxGitMinerBundle:ProjectTag:file.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'file' => $file,
                'folders' => $folders,
                'contents' => $fContents,
        ));
    }

    public function classAction($projectId, $tagId, $fileId, $classname)
    {
        $em = $this->getDoctrine()->getManager();
        list( $project, $projectTag, $dir, $file, $folders) = $this->getParameterEntities($projectId, $tagId, null, $fileId);

        $class = $em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')
            ->findOneBy(array('projectFile' => $file, 'ctype' => 'class', 'cname' => $classname));
        $fContents = $em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')
            ->getClassMethods($file,$classname);
        $this->get("apy_breadcrumb_trail")->add($classname);
        return $this->render('SlxGitMinerBundle:ProjectTag:file.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'file' => $file,
                'class' => $class,
                'classname' => $classname,
                'folders' => $folders,
                'contents' => $fContents,
        ));
    }

    /**
     * returns entities in array($project, $projectTag, $dir, $file) 
     * $dirId and $fileId can be null, corresponding entities will be null
     * 
     * @param integer $projectId
     * @param integer $tagId
     * @param integer $dirId
     * @param integer $fileId
     */
    private function getParameterEntities($projectId, $tagId, $dirId, $fileId)
    {
        $em = $this->getDoctrine()->getManager();
        $folders = null;
        
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        
        $projectTag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$projectTag) {
            throw $this->createNotFoundException('Unable to find ProjectTag entity.');
        }
        
        if ($dirId) {
            $dir = $em->getRepository('SlxGitMinerBundle:ProjectDir')->find($dirId);
            if (!$dir) {
                throw $this->createNotFoundException('Unable to find ProjectDir entity.');
            }
            $folders = $em->getRepository('SlxGitMinerBundle:ProjectDir')
                ->getPathDirs($projectTag,dirname($dir->getFname()));
        }
        else {
            $dir = null;
        }
        if ($fileId) {
            $file = $em->getRepository('SlxGitMinerBundle:ProjectFile')->find($fileId);
            if (!$file) {
                throw $this->createNotFoundException('Unable to find ProjectFile entity.');
            }
            $folders = $em->getRepository('SlxGitMinerBundle:ProjectDir')
                ->getPathDirs($projectTag,dirname($file->getFname()));
        } else {
            $file = null;
        }
        $this->addBreadcrumbs($project, $projectTag, $dir, $file, $folders);
        return array($project, $projectTag, $dir, $file, $folders);
    }
    
    private function addBreadcrumbs($project, $projectTag, $dir, $file, $folders) {
        $this->get("apy_breadcrumb_trail")->add($project->getPname(), 'project_show', array('id'=>$project->getId()));
        $this->get("apy_breadcrumb_trail")->add($projectTag->getTag(), 'projectTag', array('projectId'=>$project->getId(),'tagId'=>$projectTag->getId()));
        $clr = '/';
        if( $folders ) {
            foreach($folders as $folder) {
                $name = str_replace($clr, '', $folder->getFname());
                if( $name ) {
                    $this->get("apy_breadcrumb_trail")
                        ->add($name, 
                            'projectTag_dir', 
                            array('projectId'=>$project->getId(),'tagId'=>$projectTag->getId(), 'dirId'=>$folder->getId())
                            );
                    $clr = $folder->getFname() . '/';
                }
            }
        }
        if( $dir ) {
            $name = str_replace($clr, '', $dir->getFname());
            $this->get("apy_breadcrumb_trail")
                ->add($name, 
                    'projectTag_dir', 
                    array('projectId'=>$project->getId(),'tagId'=>$projectTag->getId(), 'dirId'=>$dir->getId())
                    );
        }
        if( $file ) {
            $name = str_replace($clr, '', $file->getFname());
            $this->get("apy_breadcrumb_trail")
                ->add($name, 
                    'projectTag_file', 
                    array('projectId'=>$project->getId(),'tagId'=>$projectTag->getId(), 'fileId'=>$file->getId())
                    );
        }
    }
    
    public function classesListAction($projectId, $tagId ) {
        $this->get("apy_breadcrumb_trail")->add("Classes");        
        $em = $this->getDoctrine()->getManager();
        
        list($project, $projectTag) = $this->getApiParameterEntities($projectId, $tagId);
        $form = $this->getFilterForm();
        
        $query = $this->getClassesListQuery($projectTag, $form, 'class');
        
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );
        return $this->render('SlxGitMinerBundle:ProjectTag:classes.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'pagination' => $pagination,
                'filterForm' => $form->createView(),
        ));
    }

    private function getApiParameterEntities($projectId, $tagId) {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $projectTag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$projectTag) {
            throw $this->createNotFoundException('Unable to find ProjectTag entity.');
        }
        $this->addBreadcrumbs($project, $projectTag, null, null, null);
        return array($project, $projectTag);
    }
    
   /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'gitminer.general.title')
            )
            ->getForm();
        return $form;
    }
    
    private function getClassesListQuery(ProjectTag $projectTag, $form, $ctype) {
        /** @var Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        
        $form->handleRequest($this->get('request'));
        $dql = "SELECT a FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectTag=:tag and a.ctype=:ctype";
        $filterQryParams = array(
            'tag' => $projectTag, 
            'ctype' => $ctype,
        );
        $where = array();

        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = 'a.cname like :cname';
                $filterQryParams['cname']= str_replace('*', '%', $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' and '.implode(' and ', $where);
        }
        $dql .= ' ORDER BY a.cname';
        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }
}

