<?php

namespace Slx\GitMinerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use PDO;

/**
 * Description of ProjectTagFunctionsController
 * 
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectTagFunctionsController extends Controller
{

    public function functionsListAction($projectId, $tagId) {
        $this->get("apy_breadcrumb_trail")->add("Functions");
        list($project, $projectTag) = $this->getApiParameterEntities($projectId,
            $tagId);
        $form = $this->getFilterForm();
        $pagination = $this->getFunctionListPagination($projectTag, $form);

        return $this->render('SlxGitMinerBundle:ProjectTag:functions.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'pagination' => $pagination,
                'filterForm' => $form->createView(),
        ));
        
    }
    
    private function getFunctionListPagination(ProjectTag $projectTag, $form) {
        /** @var Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $params = $this->collectFilterParams($projectTag, $form);
        $query = $em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getTagFunctionListQuery($params);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15),
            array('distinct' => false)
        );

        return $pagination;
    }
    
    private function getApiParameterEntities($projectId, $tagId, $tagRequired=true)
    {
        $em = $this->getDoctrine()->getManager();

        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $this->get("apy_breadcrumb_trail")->add($project->getPname(),
            'project_show', array('id' => $project->getId()));
        if( $tagRequired ) {
            $projectTag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
            if (!$projectTag) {
                throw $this->createNotFoundException('Unable to find ProjectTag entity.');
            }
            $this->get("apy_breadcrumb_trail")->add($projectTag->getTag(),
                'projectTag',
                array('projectId' => $project->getId(), 'tagId' => $projectTag->getId()));
        }
        else {
            $projectTag=null;
        }
        return array($project, $projectTag);
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                    array('method' => 'GET', 'csrf_protection' => false))
                ->add(
                    'title', 'text',
                    array('required' => false, 'label' => 'gitminer.general.title')
                )
                ->add(
                    'loc_l', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.loc_l')
                )
                ->add(
                    'loc_u', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.loc_u')
                )
                ->add(
                    'ccn_l', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.ccn_l')
                )
                ->add(
                    'ccn_u', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.ccn_u')
                )
                ->add(
                    'fanin_l', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.fanin_l')
                )
                ->add(
                    'fanin_u', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.fanin_u')
                )
                ->add(
                    'fanout_l', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.fanout_l')
                )
                ->add(
                    'fanout_u', 'text',
                    array('required' => false, 'label' => 'gitminer.projectTagFunctions.fanout_u')
                )->getForm();
        return $form;
    }
    /**
     * Gets parameters from filter form and creates query parameter array and if any more where clause dql 
     * 
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @param type $form
     * @return array
     */
    private function collectFilterParams(ProjectTag $projectTag, $form)
    {
        $dql = '';
        $where = array();
        $filterQryParams = array(
            'tid' => $projectTag->getId(),
        );

        $form->handleRequest($this->get('request'));
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = 'a.cname like :cname';
                $filterQryParams['cname'] = str_replace('*', '%',
                    $filterData['title']);
            }
            if (is_numeric($filterData['loc_l'])) {
                $where[] = 'a.loc>=:loc_l';
                $filterQryParams['loc_l'] = (int) $filterData['loc_l'];
            }
            if (is_numeric($filterData['loc_u'])) {
                $where[] = 'a.loc<=:loc_u';
                $filterQryParams['loc_u'] = (int) $filterData['loc_u'];
            }
            if (is_numeric($filterData['ccn_l'])) {
                $where[] = 'a.ccn>=:ccn_l';
                $filterQryParams['ccn_l'] = (int) $filterData['ccn_l'];
            }
            if (is_numeric($filterData['ccn_u'])) {
                $where[] = 'a.ccn<=:ccn_u';
                $filterQryParams['ccn_u'] = (int) $filterData['ccn_u'];
            }
            if (is_numeric($filterData['fanin_l'])) {
                $where[] = 'b.nb_calls>=:fanin_l';
                $filterQryParams['fanin_l'] = (int) $filterData['fanin_l'];
            }
            if (is_numeric($filterData['fanin_u'])) {
                $where[] = 'b.nb_calls<=:fanin_u';
                $filterQryParams['fanin_u'] = (int) $filterData['fanin_u'];
            }
            if (is_numeric($filterData['fanout_l'])) {
                $where[] = 'b.fanout>=:fanout_l';
                $filterQryParams['fanout_l'] = (int) $filterData['fanout_l'];
            }
            if (is_numeric($filterData['fanout_u'])) {
                $where[] = 'b.fanout<=:fanout_u';
                $filterQryParams['fanout_u'] = (int) $filterData['fanout_u'];
            }
        }
        if ($where) {
            $dql = implode(' and ', $where);
        }
        return array('params'=>$filterQryParams, 'dql'=>$dql);
    }
    
    public function functionHistoryAction($projectId)
    {
        list($project, $projectTag) = $this->getApiParameterEntities($projectId,
            0, false);
        $form = $this->getHistoryForm();
        $classes = $this->getFunctionHistory($project->getId(), $form);
        $this->get("apy_breadcrumb_trail")->add("Function History");

        return $this->render('SlxGitMinerBundle:ProjectTagFunctions:functionHistory.html.twig',
                array(
                'project' => $project,
                'tag' => $projectTag,
                'classes' => $classes,
                'filterForm' => $form->createView(),
        ));
        
        
    }

    private function getHistoryForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'gitminer.general.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * 
     * @param int $projectId
     * @param Symfony\Component\Form\Form $form
     * @return array
     */
    private function getFunctionHistory($projectId, $form)
    {
        // For future this is the dql statement to get function history
        // SELECT a.cname, c.id,b.nb_calls,pf.signature FROM SlxGitMinerBundle:ProjectCodeConstruct a LEFT JOIN a.projectFile c LEFT JOIN SlxGitMinerBundle:ProjectTagFuncCallAggr b with a.cname=b.func and a.projectTag=b.projectTag LEFT JOIN a.projectTag pt LEFT JOIN SlxGitMinerBundle:ProjectFuncSignature pf with a.projectTag=pf.projectTag and a.cname=pf.fname and a.ctype=pf.ftype WHERE pt.project=31 and a.ctype='function' and a.cname='access_row'

        /** @var Doctrine\ORM\EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $sql = "SELECT pcc.cname, pcc.ccn, pcc.loc, pcc.cloc, ptfa.nb_calls as fanin, ptfa.fanout as fanout, pcc.project_file_id, pt.tag, pf.fname, pcc.project_tag_id, pfs.signature
                FROM waev.project_code_construct pcc
                left join project_tag_func_call_aggr as ptfa on ( ptfa.func=pcc.cname and ptfa.project_tag_id=pcc.project_tag_id)
                left join project_file as pf on (pf.id=pcc.project_file_id)
                left join project_tag as pt on (pt.id=pcc.project_tag_id)
                left join project_func_signature as pfs on ( pt.id=pfs.project_tag_id and pcc.cname=pfs.fname and pcc.ctype=pfs.ftype)
                where pt.project_id=:pid and pcc.ctype='function' and pcc.cname=:fname";

        $filterQryParams = array(
            'pid' => $projectId,
        );
        $data = array();
        $form->handleRequest($this->get('request'));
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if( $filterData['title']) {
                $filterQryParams['fname']=trim($filterData['title']);
                $stmt = $em
                    ->getConnection()
                    ->executeQuery($sql, $filterQryParams);
                $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        }
        return $data;
    }

}
