<?php

namespace Slx\GitMinerBundle\DataWriter;

/**
 * Description of DataWriterFileUtils
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DataWriterFileUtils
{
    
    public static function getInstance() {
        return new DataWriterFileUtils();
    }
    
    public function addProjectPath($projectId, $fn) {
        return sprintf("%s/%s/%s",
            realpath(__DIR__ . '/../../../../web/uploads/projects'),
            $projectId,
            $fn
            );
    }    
}
