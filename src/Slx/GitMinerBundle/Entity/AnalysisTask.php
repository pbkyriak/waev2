<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AnalysisTask
 */
class AnalysisTask
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $params;

    /**
     * @var integer
     */
    private $status=0;

    /**
     * @var integer
     */
    private $state=0;

    /**
     * @var integer
     */
    private $duration=0;
    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return AnalysisTask
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return AnalysisTask
     */
    public function setClass($class)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set params
     *
     * @param string|array $params
     * @return AnalysisTask
     */
    public function setParams($params)
    {
        if(is_array($params)) {
            $params = serialize($params);
        }
        $this->params = $params;
    
        return $this;
    }

    /**
     * Get params
     *
     * @return string 
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return AnalysisTask
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return AnalysisTask
     */
    public function setState($state)
    {
        $this->state = $state;
    
        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return AnalysisTask
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    public function setDuration($v) {
        $this->duration = $v;
        return $this;
    }
    
    public function getDuration() {
        return $this->duration;
    }
}