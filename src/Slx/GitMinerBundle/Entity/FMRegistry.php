<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FMRegistry
 */
class FMRegistry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ftype;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var integer
     */
    private $age;

    /**
     *
     * @var string
     */
    private $section;
    
    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $toa;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $toe;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $tod;

    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ftype
     *
     * @param string $ftype
     * @return FMRegistry
     */
    public function setFtype($ftype)
    {
        $this->ftype = $ftype;
    
        return $this;
    }

    /**
     * Get ftype
     *
     * @return string 
     */
    public function getFtype()
    {
        return $this->ftype;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return FMRegistry
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set age
     *
     * @param integer $age
     * @return FMRegistry
     */
    public function setAge($age)
    {
        $this->age = $age;
    
        return $this;
    }

    /**
     * Get age
     *
     * @return integer 
     */
    public function getAge()
    {
        return $this->age;
    }

    public function setSection($v) {
        $this->section=$v;
        return $this;
    }
    
    public function getSection() {
        return $this->section;
    }
    /**
     * Set toa
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $toa
     * @return FMRegistry
     */
    public function setToa(\Slx\GitMinerBundle\Entity\ProjectTag $toa = null)
    {
        $this->toa = $toa;
    
        return $this;
    }

    /**
     * Get toa
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getToa()
    {
        return $this->toa;
    }

    /**
     * Set toe
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $toe
     * @return FMRegistry
     */
    public function setToe(\Slx\GitMinerBundle\Entity\ProjectTag $toe = null)
    {
        $this->toe = $toe;
    
        return $this;
    }

    /**
     * Get toe
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getToe()
    {
        return $this->toe;
    }

    /**
     * Set tod
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $tod
     * @return FMRegistry
     */
    public function setTod(\Slx\GitMinerBundle\Entity\ProjectTag $tod = null)
    {
        $this->tod = $tod;
    
        return $this;
    }

    /**
     * Get tod
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getTod()
    {
        return $this->tod;
    }

    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return FMRegistry
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}