<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FuncRegistry
 */
class FuncRegistry
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var integer
     */
    private $nb_calls;

    /**
     * @var integer
     */
    private $call_trend;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $toa;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $toe;

    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    private $age;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return FuncRegistry
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set nb_calls
     *
     * @param integer $nbCalls
     * @return FuncRegistry
     */
    public function setNbCalls($nbCalls)
    {
        $this->nb_calls = $nbCalls;
    
        return $this;
    }

    /**
     * Get nb_calls
     *
     * @return integer 
     */
    public function getNbCalls()
    {
        return $this->nb_calls;
    }

    /**
     * Set call_trend
     *
     * @param integer $callTrend
     * @return FuncRegistry
     */
    public function setCallTrend($callTrend)
    {
        $this->call_trend = $callTrend;
    
        return $this;
    }

    /**
     * Get call_trend
     *
     * @return integer 
     */
    public function getCallTrend()
    {
        return $this->call_trend;
    }

    /**
     * Set toa
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $toa
     * @return FuncRegistry
     */
    public function setToa(\Slx\GitMinerBundle\Entity\ProjectTag $toa = null)
    {
        $this->toa = $toa;
    
        return $this;
    }

    /**
     * Get toa
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getToa()
    {
        return $this->toa;
    }

    /**
     * Set toe
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $toe
     * @return FuncRegistry
     */
    public function setToe(\Slx\GitMinerBundle\Entity\ProjectTag $toe = null)
    {
        $this->toe = $toe;
    
        return $this;
    }

    /**
     * Get toe
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getToe()
    {
        return $this->toe;
    }

    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return FuncRegistry
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    public function getAge() {
        return $this->age;
    }
    
    public function setAge($v) {
        $this->age = $v;
        return $this;
    }
}