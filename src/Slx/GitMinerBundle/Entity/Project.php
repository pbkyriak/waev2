<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\ExecutionContextInterface;

/**
 * Project
 */
class Project
{

    /**
     * initial project status, user is making settings and thinking if ready to run
     */
    const STATUS_INIT = 0;

    /**
     * pending for getting analyzed. ProjectControl command will see it and start process
     * 
     */
    const STATUS_PENDING = 1;

    /**
     * AnalysisTaskRunner is running this project
     */
    const STATUS_RUNNING = 2;
    const STATUS_CANCELING = 3;

    /**
     * Critical error occured. Cant be recovered analysis is aborted.
     */
    const STATUS_CRITICAL = 7;

    /**
     * Maybe couldnt download source for a tag, or pdepend it. IMHO not critical.
     * Tag will be dropped and analysis will continue with the rest of the tags
     */
    const STATUS_RECOVERABLE = 8;

    /**
     * Current analysis finished successfully
     */
    const STATUS_FINSHED = 9;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $owner;

    /**
     * @var string
     */
    private $pname;

    /**
     * @var string
     */
    private $git_url = '';

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $projectTags;

    /**
     *
     * @var integer
     */
    private $status = 0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $analysisTasks;
    private $job_id = 0;
    private $file_extensions = 'php';
    private $libraries = '';
    private $tests = '';
    private $lib_excl_string = '';
    private $published=false;
    private $analysis;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projectTags = new \Doctrine\Common\Collections\ArrayCollection();
        $this->analysisTasks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->analysis = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return Project
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set pname
     *
     * @param string $pname
     * @return Project
     */
    public function setPname($pname)
    {
        $this->pname = $pname;

        return $this;
    }

    /**
     * Get pname
     *
     * @return string 
     */
    public function getPname()
    {
        return $this->pname;
    }

    /**
     * Set git_url
     *
     * @param string $gitUrl
     * @return Project
     */
    public function setGitUrl($gitUrl)
    {
        $this->git_url = $gitUrl;

        return $this;
    }

    /**
     * Get git_url
     *
     * @return string 
     */
    public function getGitUrl()
    {
        return $this->git_url;
    }

    /**
     * Add projectTags
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTags
     * @return Project
     */
    public function addProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTags)
    {
        $this->projectTags[] = $projectTags;

        return $this;
    }

    /**
     * Remove projectTags
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTags
     */
    public function removeProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTags)
    {
        $this->projectTags->removeElement($projectTags);
    }

    /**
     * Get projectTags
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjectTags()
    {
        return $this->projectTags;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Add analysisTasks
     *
     * @param \Slx\GitMinerBundle\Entity\AnalysisTask $analysisTasks
     * @return Project
     */
    public function addAnalysisTask(\Slx\GitMinerBundle\Entity\AnalysisTask $analysisTasks)
    {
        $analysisTasks->setProject($this);
        $this->analysisTasks[] = $analysisTasks;

        return $this;
    }

    /**
     * Remove analysisTasks
     *
     * @param \Slx\GitMinerBundle\Entity\AnalysisTask $analysisTasks
     */
    public function removeAnalysisTask(\Slx\GitMinerBundle\Entity\AnalysisTask $analysisTasks)
    {
        $this->analysisTasks->removeElement($analysisTasks);
    }

    /**
     * Get analysisTasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnalysisTasks()
    {
        return $this->analysisTasks;
    }

    public function getJobId()
    {

        return $this->job_id;
    }

    public function setJobId($job_id)
    {
        $this->job_id = $job_id;
        return $this;
    }

    public function getFileExtensions()
    {
        return $this->file_extensions;
    }

    public function setFileExtensions($ext)
    {
        $this->file_extensions = $ext;
        return $this;
    }

    /**
     * Validates project if exists in gitHub
     * @param ExecutionContextInterface $context
     */
    public function isGitProjectValid(ExecutionContextInterface $context)
    {
        $client = new \Github\Client();
        try {
            $info = $client->api('repo')->show($this->getOwner(),
                $this->getPname());
        } catch (\Exception $ex) {
            $context->addViolationAt('pname', 'gitminer.project.not_found',
                array(), null);
        }
    }

    public function __toString()
    {
        return sprintf("id=%s project=%s", $this->getId(), $this->getPname());
    }

    public function getLibraries()
    {
        return $this->libraries;
    }

    public function setLibraries($v)
    {
        if( !isset($v) ) {
            $v = '';
        }
        $this->libraries = $v;
        return $this;
    }

    public function getTests()
    {
        return $this->tests;
    }

    public function setTests($v)
    {
        if( !isset($v) ) {
            $v = '';
        }
        $this->tests = $v;
        return $this;
    }

    public function getLibNamesFilter($fldName)
    {
        return $this->getNamesFilter($fldName, trim($this->getLibraries()));
    }

    public function getTestNamesFilter($fldName)
    {
        return $this->getNamesFilter($fldName, trim($this->getTests()));
    }
    
    private function getNamesFilter($fldName, $txtNames) {
        $filters = array();
        if( $txtNames ) {
            $names = explode("\n", str_replace("\r", "", $txtNames));
            foreach ($names as $name) {
                $filters[] = sprintf(" %s like '%s' ", $fldName, $name);
            }
        }
        $out = implode(' or ', $filters);
        return $out;        
    }
    
    public function getPublished() {
        return $this->published;
    }
    
    public function setPublished($v) {
        $this->published=$v;
        return $this;
    }
    
    /**
     * Add projectAnalysis
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectTag $pa
     * @return Project
     */
    public function addAnalysis(\Slx\TaskCoreBundle\Entity\ProjectAnalysis $pa)
    {
        $this->analysis[] = $pa;

        return $this;
    }

    /**
     * Remove project analysis
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $pa
     */
    public function removeAnalysis(\Slx\TaskCoreBundle\Entity\ProjectAnalysis $pa)
    {
        $this->analysis->removeElement($pa);
    }

    /**
     * Get project analyseis
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnalyseis()
    {
        return $this->analysis;
    }
    
    /**
     * If project in state that is not in run states
     * 
     */
    public function isInNutralState() {
        $out = false;
        if( in_array( 
                $this->getStatus(), 
                array(self::STATUS_INIT, self::STATUS_FINSHED) 
            )) {
            $out = true;
        }
        return $out;
    }
    
    public function isRunning() {
        $out = false;
        $states = array(self::STATUS_PENDING, self::STATUS_RUNNING, self::STATUS_CANCELING);
        if( in_array($this->getStatus(), $states) ) {
            $out= true;
        }
        return false;
    }
    
    public function isStopped() {
        $out = false;
        $states = array(self::STATUS_CRITICAL, self::STATUS_RECOVERABLE, self::STATUS_CRITICAL);
        if( in_array($this->getStatus(), $states) ) {
            $out= true;
        }
        return false;        
    }

    /**
     * Add analysis
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis
     * @return Project
     */
    public function addAnalysi(\Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis)
    {
        $this->analysis[] = $analysis;
    
        return $this;
    }

    /**
     * Remove analysis
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis
     */
    public function removeAnalysi(\Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis)
    {
        $this->analysis->removeElement($analysis);
    }

    /**
     * Get analysis
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }
    
    public function getLibExclString() {
        return $this->lib_excl_string;
    }
    
    public function setLibExclString($v) {
        $this->lib_excl_string = $v;
        return $this;
    }
}