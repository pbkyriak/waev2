<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectClass
 */
class ProjectClass
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var string
     */
    private $modifiers;

    /**
     * @var string
     */
    private $namespace;

    /**
     * @var string
     */
    private $extends;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $intefaces;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $traits;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    private $project_file_id;
    private $fqname;
    private $parent_id;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->intefaces = new \Doctrine\Common\Collections\ArrayCollection();
        $this->traits = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectClass
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set modifiers
     *
     * @param string $modifiers
     * @return ProjectClass
     */
    public function setModifiers($modifiers)
    {
        $this->modifiers = $modifiers;
    
        return $this;
    }

    /**
     * Get modifiers
     *
     * @return string 
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @return ProjectClass
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    
        return $this;
    }

    /**
     * Get namespace
     *
     * @return string 
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set extends
     *
     * @param string $extends
     * @return ProjectClass
     */
    public function setExtends($extends)
    {
        $this->extends = $extends;
    
        return $this;
    }

    /**
     * Get extends
     *
     * @return string 
     */
    public function getExtends()
    {
        return $this->extends;
    }

    /**
     * Add intefaces
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectClassInterface $intefaces
     * @return ProjectClass
     */
    public function addInteface(\Slx\GitMinerBundle\Entity\ProjectClassInterface $intefaces)
    {
        $this->intefaces[] = $intefaces;
    
        return $this;
    }

    /**
     * Remove intefaces
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectClassInterface $intefaces
     */
    public function removeInteface(\Slx\GitMinerBundle\Entity\ProjectClassInterface $intefaces)
    {
        $this->intefaces->removeElement($intefaces);
    }

    /**
     * Get intefaces
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIntefaces()
    {
        return $this->intefaces;
    }

    /**
     * Add traits
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectClassTrait $traits
     * @return ProjectClass
     */
    public function addTrait(\Slx\GitMinerBundle\Entity\ProjectClassTrait $traits)
    {
        $this->traits[] = $traits;
    
        return $this;
    }

    /**
     * Remove traits
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectClassTrait $traits
     */
    public function removeTrait(\Slx\GitMinerBundle\Entity\ProjectClassTrait $traits)
    {
        $this->traits->removeElement($traits);
    }

    /**
     * Get traits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTraits()
    {
        return $this->traits;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectClass
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    
    public function getProjectFileId() {
        return $this->project_file_id;
    }
    
    public function setProjectFileId($v) {
        $this->project_file_id = $v;
        return $this;
    }
    
    public function getFQName() {
        return $this->fqname;
    }
    
    public function setFQName($v) {
        $this->fqname = $v;
        return $this;
    }
    
    public function getParentId() {
        return $this->parent_id;
    }
    
    public function setParentId($v) {
        $this->parent_id = $v;
        return $this;
    }
}