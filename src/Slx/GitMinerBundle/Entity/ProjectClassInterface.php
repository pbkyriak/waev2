<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectClassInterface
 */
class ProjectClassInterface
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectClass
     */
    private $pclass;

    private $namespace;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectClassInterface
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set pclass
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectClass $pclass
     * @return ProjectClassInterface
     */
    public function setPclass(\Slx\GitMinerBundle\Entity\ProjectClass $pclass = null)
    {
        $this->pclass = $pclass;
    
        return $this;
    }

    /**
     * Get pclass
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectClass 
     */
    public function getPclass()
    {
        return $this->pclass;
    }
    
    public function getNamespace() {
        return $this->namespace;
    }
    
    public function setNamespace($v) {
        $this->namespace = $v;
        return $this;
    }
}