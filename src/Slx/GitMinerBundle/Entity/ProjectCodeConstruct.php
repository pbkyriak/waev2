<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectCodeConstruct
 */
class ProjectCodeConstruct
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ctype;

    /**
     * @var string
     */
    private $cname;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFile
     */
    private $projectFile;
    
    private $droped_at_tag_id=0;
    
    private $nb_calls=0;
    
    private $fanout=0;

    private $dead=0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ctype
     *
     * @param string $ctype
     * @return ProjectCodeConstruct
     */
    public function setCtype($ctype)
    {
        $this->ctype = $ctype;
    
        return $this;
    }

    /**
     * Get ctype
     *
     * @return string 
     */
    public function getCtype()
    {
        return $this->ctype;
    }

    /**
     * Set cname
     *
     * @param string $cname
     * @return ProjectCodeConstruct
     */
    public function setCname($cname)
    {
        $this->cname = $cname;
    
        return $this;
    }

    /**
     * Get cname
     *
     * @return string 
     */
    public function getCname()
    {
        return $this->cname;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectCodeConstruct
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }

    /**
     * Set projectFile
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFile
     * @return ProjectCodeConstruct
     */
    public function setProjectFile(\Slx\GitMinerBundle\Entity\ProjectFile $projectFile = null)
    {
        $this->projectFile = $projectFile;
    
        return $this;
    }

    /**
     * Get projectFile
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFile 
     */
    public function getProjectFile()
    {
        return $this->projectFile;
    }
    /**
     * @var integer
     */
    private $ca=0;

    /**
     * @var integer
     */
    private $cbo=0;

    /**
     * @var integer
     */
    private $ce=0;

    /**
     * @var integer
     */
    private $cis=0;

    /**
     * @var integer
     */
    private $cloc=0;

    /**
     * @var float
     */
    private $cr=0;

    /**
     * @var integer
     */
    private $csz=0;

    /**
     * @var integer
     */
    private $dit=0;

    /**
     * @var integer
     */
    private $eloc=0;

    /**
     * @var integer
     */
    private $impl=0;

    /**
     * @var integer
     */
    private $lloc=0;

    /**
     * @var integer
     */
    private $loc=0;

    /**
     * @var integer
     */
    private $ncloc=0;

    /**
     * @var integer
     */
    private $noam=0;

    /**
     * @var integer
     */
    private $nocc=0;

    /**
     * @var integer
     */
    private $nom=0;

    /**
     * @var integer
     */
    private $noom=0;

    /**
     * @var integer
     */
    private $npm=0;

    /**
     * @var float
     */
    private $rcr=0;

    /**
     * @var integer
     */
    private $vars=0;

    /**
     * @var integer
     */
    private $varsi=0;

    /**
     * @var integer
     */
    private $varsnp=0;

    /**
     * @var integer
     */
    private $wmc=0;

    /**
     * @var integer
     */
    private $wmci=0;

    /**
     * @var integer
     */
    private $wmcnp=0;

    /**
     * @var integer
     */
    private $ccn=0;

    /**
     * @var integer
     */
    private $ccn2=0;

    /**
     * @var integer
     */
    private $npath=0;


    /**
     * Set ca
     *
     * @param integer $ca
     * @return ProjectCodeConstruct
     */
    public function setCa($ca)
    {
        $this->ca = $ca;
    
        return $this;
    }

    /**
     * Get ca
     *
     * @return integer 
     */
    public function getCa()
    {
        return $this->ca;
    }

    /**
     * Set cbo
     *
     * @param integer $cbo
     * @return ProjectCodeConstruct
     */
    public function setCbo($cbo)
    {
        $this->cbo = $cbo;
    
        return $this;
    }

    /**
     * Get cbo
     *
     * @return integer 
     */
    public function getCbo()
    {
        return $this->cbo;
    }

    /**
     * Set ce
     *
     * @param integer $ce
     * @return ProjectCodeConstruct
     */
    public function setCe($ce)
    {
        $this->ce = $ce;
    
        return $this;
    }

    /**
     * Get ce
     *
     * @return integer 
     */
    public function getCe()
    {
        return $this->ce;
    }

    /**
     * Set cis
     *
     * @param integer $cis
     * @return ProjectCodeConstruct
     */
    public function setCis($cis)
    {
        $this->cis = $cis;
    
        return $this;
    }

    /**
     * Get cis
     *
     * @return integer 
     */
    public function getCis()
    {
        return $this->cis;
    }

    /**
     * Set cloc
     *
     * @param integer $cloc
     * @return ProjectCodeConstruct
     */
    public function setCloc($cloc)
    {
        $this->cloc = $cloc;
    
        return $this;
    }

    /**
     * Get cloc
     *
     * @return integer 
     */
    public function getCloc()
    {
        return $this->cloc;
    }

    /**
     * Set cr
     *
     * @param float $cr
     * @return ProjectCodeConstruct
     */
    public function setCr($cr)
    {
        $this->cr = $cr;
    
        return $this;
    }

    /**
     * Get cr
     *
     * @return float 
     */
    public function getCr()
    {
        return $this->cr;
    }

    /**
     * Set csz
     *
     * @param integer $csz
     * @return ProjectCodeConstruct
     */
    public function setCsz($csz)
    {
        $this->csz = $csz;
    
        return $this;
    }

    /**
     * Get csz
     *
     * @return integer 
     */
    public function getCsz()
    {
        return $this->csz;
    }

    /**
     * Set dit
     *
     * @param integer $dit
     * @return ProjectCodeConstruct
     */
    public function setDit($dit)
    {
        $this->dit = $dit;
    
        return $this;
    }

    /**
     * Get dit
     *
     * @return integer 
     */
    public function getDit()
    {
        return $this->dit;
    }

    /**
     * Set eloc
     *
     * @param integer $eloc
     * @return ProjectCodeConstruct
     */
    public function setEloc($eloc)
    {
        $this->eloc = $eloc;
    
        return $this;
    }

    /**
     * Get eloc
     *
     * @return integer 
     */
    public function getEloc()
    {
        return $this->eloc;
    }

    /**
     * Set impl
     *
     * @param integer $impl
     * @return ProjectCodeConstruct
     */
    public function setImpl($impl)
    {
        $this->impl = $impl;
    
        return $this;
    }

    /**
     * Get impl
     *
     * @return integer 
     */
    public function getImpl()
    {
        return $this->impl;
    }

    /**
     * Set lloc
     *
     * @param integer $lloc
     * @return ProjectCodeConstruct
     */
    public function setLloc($lloc)
    {
        $this->lloc = $lloc;
    
        return $this;
    }

    /**
     * Get lloc
     *
     * @return integer 
     */
    public function getLloc()
    {
        return $this->lloc;
    }

    /**
     * Set loc
     *
     * @param integer $loc
     * @return ProjectCodeConstruct
     */
    public function setLoc($loc)
    {
        $this->loc = $loc;
    
        return $this;
    }

    /**
     * Get loc
     *
     * @return integer 
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set ncloc
     *
     * @param integer $ncloc
     * @return ProjectCodeConstruct
     */
    public function setNcloc($ncloc)
    {
        $this->ncloc = $ncloc;
    
        return $this;
    }

    /**
     * Get ncloc
     *
     * @return integer 
     */
    public function getNcloc()
    {
        return $this->ncloc;
    }

    /**
     * Set noam
     *
     * @param integer $noam
     * @return ProjectCodeConstruct
     */
    public function setNoam($noam)
    {
        $this->noam = $noam;
    
        return $this;
    }

    /**
     * Get noam
     *
     * @return integer 
     */
    public function getNoam()
    {
        return $this->noam;
    }

    /**
     * Set nocc
     *
     * @param integer $nocc
     * @return ProjectCodeConstruct
     */
    public function setNocc($nocc)
    {
        $this->nocc = $nocc;
    
        return $this;
    }

    /**
     * Get nocc
     *
     * @return integer 
     */
    public function getNocc()
    {
        return $this->nocc;
    }

    /**
     * Set nom
     *
     * @param integer $nom
     * @return ProjectCodeConstruct
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    
        return $this;
    }

    /**
     * Get nom
     *
     * @return integer 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set noom
     *
     * @param integer $noom
     * @return ProjectCodeConstruct
     */
    public function setNoom($noom)
    {
        $this->noom = $noom;
    
        return $this;
    }

    /**
     * Get noom
     *
     * @return integer 
     */
    public function getNoom()
    {
        return $this->noom;
    }

    /**
     * Set npm
     *
     * @param integer $npm
     * @return ProjectCodeConstruct
     */
    public function setNpm($npm)
    {
        $this->npm = $npm;
    
        return $this;
    }

    /**
     * Get npm
     *
     * @return integer 
     */
    public function getNpm()
    {
        return $this->npm;
    }

    /**
     * Set rcr
     *
     * @param float $rcr
     * @return ProjectCodeConstruct
     */
    public function setRcr($rcr)
    {
        $this->rcr = $rcr;
    
        return $this;
    }

    /**
     * Get rcr
     *
     * @return float 
     */
    public function getRcr()
    {
        return $this->rcr;
    }

    /**
     * Set vars
     *
     * @param integer $vars
     * @return ProjectCodeConstruct
     */
    public function setVars($vars)
    {
        $this->vars = $vars;
    
        return $this;
    }

    /**
     * Get vars
     *
     * @return integer 
     */
    public function getVars()
    {
        return $this->vars;
    }

    /**
     * Set varsi
     *
     * @param integer $varsi
     * @return ProjectCodeConstruct
     */
    public function setVarsi($varsi)
    {
        $this->varsi = $varsi;
    
        return $this;
    }

    /**
     * Get varsi
     *
     * @return integer 
     */
    public function getVarsi()
    {
        return $this->varsi;
    }

    /**
     * Set varsnp
     *
     * @param integer $varsnp
     * @return ProjectCodeConstruct
     */
    public function setVarsnp($varsnp)
    {
        $this->varsnp = $varsnp;
    
        return $this;
    }

    /**
     * Get varsnp
     *
     * @return integer 
     */
    public function getVarsnp()
    {
        return $this->varsnp;
    }

    /**
     * Set wmc
     *
     * @param integer $wmc
     * @return ProjectCodeConstruct
     */
    public function setWmc($wmc)
    {
        $this->wmc = $wmc;
    
        return $this;
    }

    /**
     * Get wmc
     *
     * @return integer 
     */
    public function getWmc()
    {
        return $this->wmc;
    }

    /**
     * Set wmci
     *
     * @param integer $wmci
     * @return ProjectCodeConstruct
     */
    public function setWmci($wmci)
    {
        $this->wmci = $wmci;
    
        return $this;
    }

    /**
     * Get wmci
     *
     * @return integer 
     */
    public function getWmci()
    {
        return $this->wmci;
    }

    /**
     * Set wmcnp
     *
     * @param integer $wmcnp
     * @return ProjectCodeConstruct
     */
    public function setWmcnp($wmcnp)
    {
        $this->wmcnp = $wmcnp;
    
        return $this;
    }

    /**
     * Get wmcnp
     *
     * @return integer 
     */
    public function getWmcnp()
    {
        return $this->wmcnp;
    }

    /**
     * Set ccn
     *
     * @param integer $ccn
     * @return ProjectCodeConstruct
     */
    public function setCcn($ccn)
    {
        $this->ccn = $ccn;
    
        return $this;
    }

    /**
     * Get ccn
     *
     * @return integer 
     */
    public function getCcn()
    {
        return $this->ccn;
    }

    /**
     * Set ccn2
     *
     * @param integer $ccn2
     * @return ProjectCodeConstruct
     */
    public function setCcn2($ccn2)
    {
        $this->ccn2 = $ccn2;
    
        return $this;
    }

    /**
     * Get ccn2
     *
     * @return integer 
     */
    public function getCcn2()
    {
        return $this->ccn2;
    }

    /**
     * Set npath
     *
     * @param integer $npath
     * @return ProjectCodeConstruct
     */
    public function setNpath($npath)
    {
        $this->npath = $npath;
    
        return $this;
    }

    /**
     * Get npath
     *
     * @return integer 
     */
    public function getNpath()
    {
        return $this->npath;
    }
    
    public function getDropedAtTagId() {
        return $this->droped_at_tag_id;
    }

    public function setDropedAtTagId($id) {
        $this->droped_at_tag_id = $id;
        return $this;
    }
    
    public function getNbCalls() {
        return $this->nb_calls;
    }
    
    public function setNbCalls($v) {
        $this->nb_calls=$v;
        return $this;
    }
    
    public function getFanout() {
        return $this->fanout;
    }
    
    public function setFanout($v) {
        $this->fanout=$v;
        return $this;
    }

    public function getDead() {
        return $this->dead;
    }
    
    public function setDead($v) {
        $this->dead=$v;
        return $this;
    }
}