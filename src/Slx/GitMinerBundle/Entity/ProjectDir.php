<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectDir
 */
class ProjectDir
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var integer
     */
    private $dir_depth;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectDir
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set dir_depth
     *
     * @param integer $dirDepth
     * @return ProjectDir
     */
    public function setDirDepth($dirDepth)
    {
        $this->dir_depth = $dirDepth;
    
        return $this;
    }

    /**
     * Get dir_depth
     *
     * @return integer 
     */
    public function getDirDepth()
    {
        return $this->dir_depth;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectDir
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
}