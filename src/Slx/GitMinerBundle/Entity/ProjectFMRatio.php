<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of ProjectFMRatio
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 2 Ιουν 2014
 */
class ProjectFMRatio
{

    private $id;
    private $idx;
    private $mratio;
    private $fratio;
    
    public function getId() {
        return $this->id;
    }
    
    public function setId($v) {
        $this->id=$v;
        return $this;
    }
    
    public function getIdx() {
        return $this->idx;
    }
    
    public function setIdx($v) {
        $this->idx=$v;
        return $this;
    }
    
    public function getFRatio() {
        return $this->fratio;
    }
    
    public function setFRatio($v) {
        $this->fratio=$v;
        return $this;
    }
    public function getMRatio() {
        return $this->mratio;
    }
    
    public function setMRatio($v) {
        $this->mratio=$v;
        return $this;
    }
    
    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;

    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectLibUsage
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

}