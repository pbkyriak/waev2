<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFile
 */
class ProjectFile
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var \Slx\GitMinerBundle\Entity\projectTag
     */
    private $projectTag;


    private $sha1_hash;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectFile
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
        $this->setDirDepth(substr_count($fname,'/'));
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\projectTag $projectTag
     * @return ProjectFile
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\projectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\projectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $codeConstructs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->codeConstructs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add codeConstructs
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectCodeConstruct $codeConstructs
     * @return ProjectFile
     */
    public function addCodeConstruct(\Slx\GitMinerBundle\Entity\ProjectCodeConstruct $codeConstructs)
    {
        $this->codeConstructs[] = $codeConstructs;
    
        return $this;
    }

    /**
     * Remove codeConstructs
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectCodeConstruct $codeConstructs
     */
    public function removeCodeConstruct(\Slx\GitMinerBundle\Entity\ProjectCodeConstruct $codeConstructs)
    {
        $this->codeConstructs->removeElement($codeConstructs);
    }

    /**
     * Get codeConstructs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCodeConstructs()
    {
        return $this->codeConstructs;
    }
    /**
     * @var integer
     */
    private $cloc=0;

    /**
     * @var integer
     */
    private $eloc=0;

    /**
     * @var integer
     */
    private $lloc=0;

    /**
     * @var integer
     */
    private $loc=0;

    /**
     * @var integer
     */
    private $ncloc=0;

    private $dir_depth=0;
    
    /**
     *
     * @var integer
     */
    private $llocGlobal=0;
    
    /**
     *
     * @var integer
     */
    private $ccn2Global=0;

    /**
     *
     * @var integer
     */
    private $ccnGlobal=0;

    /**
     *
     * @var integer
     */
    private $clocGlobal=0;

    private $parser = 'good';
    private $section = 'system';
    
    /**
     * Set cloc
     *
     * @param integer $cloc
     * @return ProjectFile
     */
    public function setCloc($cloc)
    {
        $this->cloc = $cloc;
    
        return $this;
    }

    /**
     * Get cloc
     *
     * @return integer 
     */
    public function getCloc()
    {
        return $this->cloc;
    }

    /**
     * Set eloc
     *
     * @param integer $eloc
     * @return ProjectFile
     */
    public function setEloc($eloc)
    {
        $this->eloc = $eloc;
    
        return $this;
    }

    /**
     * Get eloc
     *
     * @return integer 
     */
    public function getEloc()
    {
        return $this->eloc;
    }

    /**
     * Set lloc
     *
     * @param integer $lloc
     * @return ProjectFile
     */
    public function setLloc($lloc)
    {
        $this->lloc = $lloc;
    
        return $this;
    }

    /**
     * Get lloc
     *
     * @return integer 
     */
    public function getLloc()
    {
        return $this->lloc;
    }

    /**
     * Set loc
     *
     * @param integer $loc
     * @return ProjectFile
     */
    public function setLoc($loc)
    {
        $this->loc = $loc;
    
        return $this;
    }

    /**
     * Get loc
     *
     * @return integer 
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set ncloc
     *
     * @param integer $ncloc
     * @return ProjectFile
     */
    public function setNcloc($ncloc)
    {
        $this->ncloc = $ncloc;
    
        return $this;
    }

    /**
     * Get ncloc
     *
     * @return integer 
     */
    public function getNcloc()
    {
        return $this->ncloc;
    }
    
    public function getDirDepth() {
        return $this->dir_depth;
    }
    
    public function setDirDepth($v) {
        $this->dir_depth=$v;
        return $this;
    }
    
   /**
     * Set llocGlobal
     *
     * @param integer $v
     * @return ProjectFile
     */
    public function setLlocGlobal($v)
    {
        $this->llocGlobal = $v;
    
        return $this;
    }

    /**
     * Get llocGlobal
     *
     * @return integer 
     */
    public function getLlocGlobal()
    {
        return $this->llocGlobal;
    }
 
   /**
     * Set Ccn2Global
     *
     * @param integer $v
     * @return ProjectFile
     */
    public function setCcn2Global($v)
    {
        $this->ccn2Global = $v;
    
        return $this;
    }

    /**
     * Get Ccn2Global
     *
     * @return integer 
     */
    public function getCcn2Global()
    {
        return $this->ccn2Global;
    }
 
    public function getSha1_hash()
    {
        return $this->sha1_hash;
    }

    public function setSha1_hash($sha1_hash)
    {
        $this->sha1_hash = $sha1_hash;
        return $this;
    }

    public function getCcnGlobal() {
        return $this->ccnGlobal;
    }
    
    public function setCcnGlobal($v) {
        $this->ccnGlobal=$v;
        return $this;
    }
    
    public function getClocGlobal() {
        return $this->clocGlobal;
    }

    public function setClocGlobal($clocGlobal) {
        $this->clocGlobal = $clocGlobal;
        return $this;
    }



    /**
     * Set sha1_hash
     *
     * @param string $sha1Hash
     * @return ProjectFile
     */
    public function setSha1Hash($sha1Hash)
    {
        $this->sha1_hash = $sha1Hash;
    
        return $this;
    }

    /**
     * Get sha1_hash
     *
     * @return string 
     */
    public function getSha1Hash()
    {
        return $this->sha1_hash;
    }
    
    public function getParser() {
        return $this->parser;
    }
    
    public function setParser($v) {
        $this->parser = $v;
        return $this;
    }
    
    public function getSection() {
        return $this->section;
    }
    
    public function setSection($v) {
        $this->section = $v;
        return $this;
    }
}