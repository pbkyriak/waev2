<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFileDiff
 */
class ProjectFileDiff
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var string
     */
    private $action;

    /**
     * @var integer
     */
    private $lines_add;

    /**
     * @var integer
     */
    private $lines_del;

    /**
     * @var integer
     */
    private $lines_mod;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFile
     */
    private $projectFileFrom;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFile
     */
    private $projectFileTo;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectFileDiff
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set action
     *
     * @param string $action
     * @return ProjectFileDiff
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set lines_add
     *
     * @param integer $linesAdd
     * @return ProjectFileDiff
     */
    public function setLinesAdd($linesAdd)
    {
        $this->lines_add = $linesAdd;
    
        return $this;
    }

    /**
     * Get lines_add
     *
     * @return integer 
     */
    public function getLinesAdd()
    {
        return $this->lines_add;
    }

    /**
     * Set lines_del
     *
     * @param integer $linesDel
     * @return ProjectFileDiff
     */
    public function setLinesDel($linesDel)
    {
        $this->lines_del = $linesDel;
    
        return $this;
    }

    /**
     * Get lines_del
     *
     * @return integer 
     */
    public function getLinesDel()
    {
        return $this->lines_del;
    }

    /**
     * Set lines_mod
     *
     * @param integer $linesMod
     * @return ProjectFileDiff
     */
    public function setLinesMod($linesMod)
    {
        $this->lines_mod = $linesMod;
    
        return $this;
    }

    /**
     * Get lines_mod
     *
     * @return integer 
     */
    public function getLinesMod()
    {
        return $this->lines_mod;
    }

    /**
     * Set projectFileFrom
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFileFrom
     * @return ProjectFileDiff
     */
    public function setProjectFileFrom(\Slx\GitMinerBundle\Entity\ProjectFile $projectFileFrom = null)
    {
        $this->projectFileFrom = $projectFileFrom;
    
        return $this;
    }

    /**
     * Get projectFileFrom
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFile 
     */
    public function getProjectFileFrom()
    {
        return $this->projectFileFrom;
    }

    /**
     * Set projectFileTo
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFileTo
     * @return ProjectFileDiff
     */
    public function setProjectFileTo(\Slx\GitMinerBundle\Entity\ProjectFile $projectFileTo = null)
    {
        $this->projectFileTo = $projectFileTo;
    
        return $this;
    }

    /**
     * Get projectFileTo
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFile 
     */
    public function getProjectFileTo()
    {
        return $this->projectFileTo;
    }
}