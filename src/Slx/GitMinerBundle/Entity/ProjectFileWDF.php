<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFileWDF
 */
class ProjectFileWDF
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $deletion_nb;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFile
     */
    private $projectFile;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set deletion_nb
     *
     * @param integer $deletionNb
     * @return ProjectFileWDF
     */
    public function setDeletionNb($deletionNb)
    {
        $this->deletion_nb = $deletionNb;
    
        return $this;
    }

    /**
     * Get deletion_nb
     *
     * @return integer 
     */
    public function getDeletionNb()
    {
        return $this->deletion_nb;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectFileWDF
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }

    /**
     * Set projectFile
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFile
     * @return ProjectFileWDF
     */
    public function setProjectFile(\Slx\GitMinerBundle\Entity\ProjectFile $projectFile = null)
    {
        $this->projectFile = $projectFile;
    
        return $this;
    }

    /**
     * Get projectFile
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFile 
     */
    public function getProjectFile()
    {
        return $this->projectFile;
    }
}