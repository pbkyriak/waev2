<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFuncSignature
 */
class ProjectFuncSignature
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ftype;

    /**
     * @var string
     */
    private $modifiers;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var string
     */
    private $signature;

    /**
     * @var string
     */
    private $hash;

    /**
     * @var string
     */
    private $return_type;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $arguments;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    private $project_file_id;
    
    private $is_deprecated;
    private $has_access_tag;
    /**
     *
     * @var string
     */
    private $namespace;
    
    private $project_class_id;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->arguments = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modifiers
     *
     * @param string $modifiers
     * @return ProjectFuncSignature
     */
    public function setModifiers($pccId)
    {
        $this->modifiers = $pccId;
    
        return $this;
    }

    /**
     * Get pcc_id
     *
     * @return string 
     */
    public function getModifiers()
    {
        return $this->modifiers;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectFuncSignature
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set signature
     *
     * @param string $signature
     * @return ProjectFuncSignature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    
        return $this;
    }

    /**
     * Get signature
     *
     * @return string 
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Set hash
     *
     * @param string $hash
     * @return ProjectFuncSignature
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string 
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set return_type
     *
     * @param string $returnType
     * @return ProjectFuncSignature
     */
    public function setReturnType($returnType)
    {
        $this->return_type = $returnType;
    
        return $this;
    }

    /**
     * Get return_type
     *
     * @return string 
     */
    public function getReturnType()
    {
        return $this->return_type;
    }

    /**
     * Add arguments
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFuncSignatureArgument $arguments
     * @return ProjectFuncSignature
     */
    public function addArgument(\Slx\GitMinerBundle\Entity\ProjectFuncSignatureArgument $arguments)
    {
        $this->arguments[] = $arguments;
    
        return $this;
    }

    /**
     * Remove arguments
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFuncSignatureArgument $arguments
     */
    public function removeArgument(\Slx\GitMinerBundle\Entity\ProjectFuncSignatureArgument $arguments)
    {
        $this->arguments->removeElement($arguments);
    }

    /**
     * Get arguments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectFuncSignature
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    
    public function setFtype($ftype) {
        $this->ftype=$ftype;
        return $this;
    }
    
    public function getFtype() {
        return $this->ftype;
    }
    
    public function getProjectFileId()
    {
        return $this->project_file_id;
    }

    public function setProjectFileId($projectFileId)
    {
        $this->project_file_id = $projectFileId;
        return $this;
    }

    public function getIsDeprecated() {
        return $this->is_deprecated;
    }

    public function setIsDeprecated($v) {
        $this->is_deprecated=$v;
        return $this;
    }
    
    public function getHasAccessTag() {
        return $this->has_access_tag;
    }
    
    public function setHasAccessTag($v) {
        $this->has_access_tag = $v;
        return $this;
    }
    
    public function getNamespace() {
        return $this->namespace;
    }
    
    public function setNamespace($v) {
        $this->namespace = $v;
        return $this;
    }
    
    public function getProjectClassId() {
        return $this->project_class_id;
    }
    
    public function setProjectClassId($v) {
        $this->project_class_id = $v;
        return $this;
    }
}
