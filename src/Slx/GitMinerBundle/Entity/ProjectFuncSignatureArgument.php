<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFuncSignatureArgument
 */
class ProjectFuncSignatureArgument
{
    /**
     * @var integer
     */
    private $id;

    private $by_ref;
    
    /**
     * @var string
     */
    private $arg_type;

    /**
     * @var boolean
     */
    private $is_optional;

    /**
     * @var string
     */
    private $default_value;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFuncSignature
     */
    private $funcSignature;

    private $arg_idx;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set arg_type
     *
     * @param string $argType
     * @return ProjectFuncSignatureArgument
     */
    public function setArgType($argType)
    {
        $this->arg_type = $argType;
    
        return $this;
    }

    /**
     * Get arg_type
     *
     * @return string 
     */
    public function getArgType()
    {
        return $this->arg_type;
    }

    /**
     * Set is_optional
     *
     * @param boolean $isOptional
     * @return ProjectFuncSignatureArgument
     */
    public function setIsOptional($isOptional)
    {
        $this->is_optional = $isOptional;
    
        return $this;
    }

    /**
     * Get is_optional
     *
     * @return boolean 
     */
    public function getIsOptional()
    {
        return $this->is_optional;
    }

    /**
     * Set default_value
     *
     * @param string $defaultValue
     * @return ProjectFuncSignatureArgument
     */
    public function setDefaultValue($defaultValue)
    {
        $this->default_value = $defaultValue;
    
        return $this;
    }

    /**
     * Get default_value
     *
     * @return string 
     */
    public function getDefaultValue()
    {
        return $this->default_value;
    }

    /**
     * Set funcSignature
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFuncSignature $funcSignature
     * @return ProjectFuncSignatureArgument
     */
    public function setFuncSignature(\Slx\GitMinerBundle\Entity\ProjectFuncSignature $funcSignature = null)
    {
        $this->funcSignature = $funcSignature;
    
        return $this;
    }

    /**
     * Get funcSignature
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFuncSignature 
     */
    public function getFuncSignature()
    {
        return $this->funcSignature;
    }
    
    public function getBy_ref()
    {
        return $this->by_ref;
    }

    public function setBy_ref($by_ref)
    {
        $this->by_ref = $by_ref;
        return $this;
    }

    public function getArgIdx() {
        return $this->arg_idx;
    }

    public function setArgIdx($argIdx) {
        $this->arg_idx = $argIdx;
        return $this;
    }

    /**
     * Set by_ref
     *
     * @param boolean $byRef
     * @return ProjectFuncSignatureArgument
     */
    public function setByRef($byRef)
    {
        $this->by_ref = $byRef;
    
        return $this;
    }

    /**
     * Get by_ref
     *
     * @return boolean 
     */
    public function getByRef()
    {
        return $this->by_ref;
    }
}