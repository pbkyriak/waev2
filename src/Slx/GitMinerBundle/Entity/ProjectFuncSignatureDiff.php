<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status: 
 *      0=stable, 
 *      1=αλλαγή πλήθους υποχρεωτικών ορισμάτων,
 *      2=προσθήκη προαιρετικών ορισμάτων,
 *      3=αφαίρεση προαιρετικών ορισμάτων,
 *      4=αλλαγή default τιμών σε προαιρετικά ορίσματα
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectFuncSignatureDiff
{
 
    /**
     * @var integer
     */
    private $id;

    private $fname;
    
    private $status;
    
    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagFrom;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagTo;

   /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectTagDiff
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFname()
    {
        return $this->fname;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setFname($fname)
    {
        $this->fname = $fname;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }



    /**
     * Set projectTagFrom
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom
     * @return ProjectFuncSignatureDiff
     */
    public function setProjectTagFrom(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom = null)
    {
        $this->projectTagFrom = $projectTagFrom;
    
        return $this;
    }

    /**
     * Get projectTagFrom
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagFrom()
    {
        return $this->projectTagFrom;
    }

    /**
     * Set projectTagTo
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagTo
     * @return ProjectFuncSignatureDiff
     */
    public function setProjectTagTo(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagTo = null)
    {
        $this->projectTagTo = $projectTagTo;
    
        return $this;
    }

    /**
     * Get projectTagTo
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagTo()
    {
        return $this->projectTagTo;
    }
}