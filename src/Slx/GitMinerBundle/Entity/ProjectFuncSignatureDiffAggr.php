<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Status: 
 *      0=stable, 
 *      1=αλλαγή πλήθους υποχρεωτικών ορισμάτων,
 *      2=προσθήκη προαιρετικών ορισμάτων,
 *      3=αφαίρεση προαιρετικών ορισμάτων,
 *      4=αλλαγή default τιμών σε προαιρετικά ορίσματα
 *      5=η υπογραφή ίδια αλλά άλλαξε το σώμα της συνάρτησης
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectFuncSignatureDiffAggr
{
 
    /**
     * @var integer
     */
    private $id;

    private $func_count;
    
    private $total;
    
    private $status;
    
    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagFrom;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagTo;

   /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectTagDiff
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getFuncCount()
    {
        return $this->func_count;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setFuncCount($funcCount)
    {
        $this->func_count = $funcCount;
        return $this;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    public function getTotal() {
        return $this->total;
    }
    
    public function setTotal($total) {
        $this->total=$total;
        return $this;
    }

    /**
     * Set projectTagFrom
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom
     * @return ProjectFuncSignatureDiffAggr
     */
    public function setProjectTagFrom(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom = null)
    {
        $this->projectTagFrom = $projectTagFrom;
    
        return $this;
    }

    /**
     * Get projectTagFrom
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagFrom()
    {
        return $this->projectTagFrom;
    }

    /**
     * Set projectTagTo
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagTo
     * @return ProjectFuncSignatureDiffAggr
     */
    public function setProjectTagTo(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagTo = null)
    {
        $this->projectTagTo = $projectTagTo;
    
        return $this;
    }

    /**
     * Get projectTagTo
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagTo()
    {
        return $this->projectTagTo;
    }
}