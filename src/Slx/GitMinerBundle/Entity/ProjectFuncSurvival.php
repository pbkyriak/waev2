<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectFuncSurvival
 */
class ProjectFuncSurvival
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $atype;

    /**
     * @var string
     */
    private $label;

    /**
     * @var float
     */
    private $estimator;

    private $surv_func;
    /**
     * @var integer
     */
    private $in_risk;

    /**
     * @var integer
     */
    private $slaughtered;

    /**
     * @var integer
     */
    private $idx;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagFrom;

    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set atype
     *
     * @param integer $atype
     * @return ProjectFuncSurvival
     */
    public function setAtype($atype)
    {
        $this->atype = $atype;
    
        return $this;
    }

    /**
     * Get atype
     *
     * @return integer 
     */
    public function getAtype()
    {
        return $this->atype;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return ProjectFuncSurvival
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set estimator
     *
     * @param float $estimator
     * @return ProjectFuncSurvival
     */
    public function setEstimator($estimator)
    {
        $this->estimator = $estimator;
    
        return $this;
    }

    /**
     * Get estimator
     *
     * @return float 
     */
    public function getEstimator()
    {
        return $this->estimator;
    }

    /**
     * Set in_risk
     *
     * @param integer $inRisk
     * @return ProjectFuncSurvival
     */
    public function setInRisk($inRisk)
    {
        $this->in_risk = $inRisk;
    
        return $this;
    }

    /**
     * Get in_risk
     *
     * @return integer 
     */
    public function getInRisk()
    {
        return $this->in_risk;
    }

    /**
     * Set slaughtered
     *
     * @param integer $slaughtered
     * @return ProjectFuncSurvival
     */
    public function setSlaughtered($slaughtered)
    {
        $this->slaughtered = $slaughtered;
    
        return $this;
    }

    /**
     * Get slaughtered
     *
     * @return integer 
     */
    public function getSlaughtered()
    {
        return $this->slaughtered;
    }

    /**
     * Set idx
     *
     * @param integer $idx
     * @return ProjectFuncSurvival
     */
    public function setIdx($idx)
    {
        $this->idx = $idx;
    
        return $this;
    }

    /**
     * Get idx
     *
     * @return integer 
     */
    public function getIdx()
    {
        return $this->idx;
    }

    /**
     * Set projectTagFrom
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom
     * @return ProjectFuncSurvival
     */
    public function setProjectTagFrom(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagFrom = null)
    {
        $this->projectTagFrom = $projectTagFrom;
    
        return $this;
    }

    /**
     * Get projectTagFrom
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagFrom()
    {
        return $this->projectTagFrom;
    }

    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectFuncSurvival
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    public function getSurvFunc() {
        return $this->surv_func;
    }
    
    public function setSurvFunc($v) {
        $this->surv_func = $v;
        return $this;
    }
}