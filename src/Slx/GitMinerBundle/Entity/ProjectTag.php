<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Finder\Finder;

/**
 * ProjectTag
 */
class ProjectTag
{

    const STATUS_NEW=0;
    const STATUS_SRC_DOWNLOADED=1;
    const STATUS_DP_DONE=2;
    const STATUS_DP_IMPORT=3;
    const STATUS_RFGC_DONE=4;
    const STATUS_DIRTREE_DONE=5;
    const STATUS_DCD_DONE=6;
    const STATUS_DCD_IMPORT=7;
    const STATUS_FC_DONE=8;
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $tag;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $projectFiles;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $projectDirs;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $projectMetrics;
    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;

    /**
     *
     * @var integer
     */
    private $status = ProjectTag::STATUS_NEW;
    
    /**
     *
     * @var integer
     */
    private $job_id = 0;
    
    /**
     *
     * @var string
     */
    private $job_state;
    
    /**
     * @var float
     */
    private $ahh = 0;

    /**
     * @var float
     */
    private $andc = 0;

    /**
     * @var integer
     */
    private $calls = 0;

    /**
     * @var integer
     */
    private $ccn = 0;

    /**
     * @var integer
     */
    private $ccn2 = 0;

    /**
     * @var integer
     */
    private $cloc = 0;

    /**
     * @var integer
     */
    private $clsa = 0;

    /**
     * @var integer
     */
    private $clsc = 0;

    /**
     * @var integer
     */
    private $eloc = 0;

    /**
     * @var integer
     */
    private $fanout = 0;

    /**
     * @var integer
     */
    private $leafs = 0;

    /**
     * @var integer
     */
    private $lloc = 0;

    /**
     * @var integer
     */
    private $loc = 0;

    /**
     * @var integer
     */
    private $maxDIT = 0;

    /**
     * @var integer
     */
    private $ncloc = 0;

    /**
     * @var integer
     */
    private $noc = 0;

    /**
     * @var integer
     */
    private $nof = 0;

    /**
     * @var integer
     */
    private $noi = 0;

    /**
     * @var integer
     */
    private $nom = 0;

    /**
     * @var integer
     */
    private $nop = 0;

    /**
     * @var integer
     */
    private $roots = 0;

    /**
     *
     * @var integer
     */
    private $llocGlobal = 0;
    
    /**
     *
     * @var integer
     */
    private $ccn2Global = 0;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projectFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->projectDirs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     * @return ProjectTag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string 
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Add projectFiles
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFiles
     * @return ProjectTag
     */
    public function addProjectFile(\Slx\GitMinerBundle\Entity\ProjectFile $projectFiles)
    {
        $this->projectFiles[] = $projectFiles;

        return $this;
    }

    /**
     * Remove projectFiles
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFiles
     */
    public function removeProjectFile(\Slx\GitMinerBundle\Entity\ProjectFile $projectFiles)
    {
        $this->projectFiles->removeElement($projectFiles);
    }

    /**
     * Get projectFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjectFiles()
    {
        return $this->projectFiles;
    }

    /**
     * Add projectDir
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectDir
     * @return ProjectTag
     */
    public function addProjectDir(\Slx\GitMinerBundle\Entity\ProjectFile $projectDir)
    {
        $this->projectDirs[] = $projectDir;

        return $this;
    }

    /**
     * Remove projectDirs
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectDirs
     */
    public function removeProjectDir(\Slx\GitMinerBundle\Entity\ProjectFile $projectDir)
    {
        $this->projectDirs->removeElement($projectDir);
    }

    /**
     * Get projectFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjectDirs()
    {
        return $this->projectDirs;
    }
    
    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectTag
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @var string
     */
    private $url;

    /**
     * Set url
     *
     * @param string $url
     * @return ProjectTag
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string 
     */
    public function getUrl()
    {
        return $this->url;
    }


    
    /**
     * Set ahh
     *
     * @param float $ahh
     * @return ProjectTag
     */
    public function setAhh($ahh)
    {
        $this->ahh = $ahh;

        return $this;
    }

    /**
     * Get ahh
     *
     * @return float 
     */
    public function getAhh()
    {
        return $this->ahh;
    }

    /**
     * Set andc
     *
     * @param float $andc
     * @return ProjectTag
     */
    public function setAndc($andc)
    {
        $this->andc = $andc;

        return $this;
    }

    /**
     * Get andc
     *
     * @return float 
     */
    public function getAndc()
    {
        return $this->andc;
    }

    /**
     * Set calls
     *
     * @param integer $calls
     * @return ProjectTag
     */
    public function setCalls($calls)
    {
        $this->calls = $calls;

        return $this;
    }

    /**
     * Get calls
     *
     * @return integer 
     */
    public function getCalls()
    {
        return $this->calls;
    }

    /**
     * Set ccn
     *
     * @param integer $ccn
     * @return ProjectTag
     */
    public function setCcn($ccn)
    {
        $this->ccn = $ccn;

        return $this;
    }

    /**
     * Get ccn
     *
     * @return integer 
     */
    public function getCcn()
    {
        return $this->ccn;
    }

    /**
     * Set ccn2
     *
     * @param integer $ccn2
     * @return ProjectTag
     */
    public function setCcn2($ccn2)
    {
        $this->ccn2 = $ccn2;

        return $this;
    }

    /**
     * Get ccn2
     *
     * @return integer 
     */
    public function getCcn2()
    {
        return $this->ccn2;
    }

    /**
     * Set cloc
     *
     * @param integer $cloc
     * @return ProjectTag
     */
    public function setCloc($cloc)
    {
        $this->cloc = $cloc;

        return $this;
    }

    /**
     * Get cloc
     *
     * @return integer 
     */
    public function getCloc()
    {
        return $this->cloc;
    }

    /**
     * Set clsa
     *
     * @param integer $clsa
     * @return ProjectTag
     */
    public function setClsa($clsa)
    {
        $this->clsa = $clsa;

        return $this;
    }

    /**
     * Get clsa
     *
     * @return integer 
     */
    public function getClsa()
    {
        return $this->clsa;
    }

    /**
     * Set clsc
     *
     * @param integer $clsc
     * @return ProjectTag
     */
    public function setClsc($clsc)
    {
        $this->clsc = $clsc;

        return $this;
    }

    /**
     * Get clsc
     *
     * @return integer 
     */
    public function getClsc()
    {
        return $this->clsc;
    }

    /**
     * Set eloc
     *
     * @param integer $eloc
     * @return ProjectTag
     */
    public function setEloc($eloc)
    {
        $this->eloc = $eloc;

        return $this;
    }

    /**
     * Get eloc
     *
     * @return integer 
     */
    public function getEloc()
    {
        return $this->eloc;
    }

    /**
     * Set fanout
     *
     * @param integer $fanout
     * @return ProjectTag
     */
    public function setFanout($fanout)
    {
        $this->fanout = $fanout;

        return $this;
    }

    /**
     * Get fanout
     *
     * @return integer 
     */
    public function getFanout()
    {
        return $this->fanout;
    }

    /**
     * Set leafs
     *
     * @param integer $leafs
     * @return ProjectTag
     */
    public function setLeafs($leafs)
    {
        $this->leafs = $leafs;

        return $this;
    }

    /**
     * Get leafs
     *
     * @return integer 
     */
    public function getLeafs()
    {
        return $this->leafs;
    }

    /**
     * Set lloc
     *
     * @param integer $lloc
     * @return ProjectTag
     */
    public function setLloc($lloc)
    {
        $this->lloc = $lloc;

        return $this;
    }

    /**
     * Get lloc
     *
     * @return integer 
     */
    public function getLloc()
    {
        return $this->lloc;
    }

    /**
     * Set loc
     *
     * @param integer $loc
     * @return ProjectTag
     */
    public function setLoc($loc)
    {
        $this->loc = $loc;

        return $this;
    }

    /**
     * Get loc
     *
     * @return integer 
     */
    public function getLoc()
    {
        return $this->loc;
    }

    /**
     * Set maxDIT
     *
     * @param integer $maxDIT
     * @return ProjectTag
     */
    public function setMaxDIT($maxDIT)
    {
        $this->maxDIT = $maxDIT;

        return $this;
    }

    /**
     * Get maxDIT
     *
     * @return integer 
     */
    public function getMaxDIT()
    {
        return $this->maxDIT;
    }

    /**
     * Set ncloc
     *
     * @param integer $ncloc
     * @return ProjectTag
     */
    public function setNcloc($ncloc)
    {
        $this->ncloc = $ncloc;

        return $this;
    }

    /**
     * Get ncloc
     *
     * @return integer 
     */
    public function getNcloc()
    {
        return $this->ncloc;
    }

    /**
     * Set noc
     *
     * @param integer $noc
     * @return ProjectTag
     */
    public function setNoc($noc)
    {
        $this->noc = $noc;

        return $this;
    }

    /**
     * Get noc
     *
     * @return integer 
     */
    public function getNoc()
    {
        return $this->noc;
    }

    /**
     * Set nof
     *
     * @param integer $nof
     * @return ProjectTag
     */
    public function setNof($nof)
    {
        $this->nof = $nof;

        return $this;
    }

    /**
     * Get nof
     *
     * @return integer 
     */
    public function getNof()
    {
        return $this->nof;
    }

    /**
     * Set noi
     *
     * @param integer $noi
     * @return ProjectTag
     */
    public function setNoi($noi)
    {
        $this->noi = $noi;

        return $this;
    }

    /**
     * Get noi
     *
     * @return integer 
     */
    public function getNoi()
    {
        return $this->noi;
    }

    /**
     * Set nom
     *
     * @param integer $nom
     * @return ProjectTag
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return integer 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set nop
     *
     * @param integer $nop
     * @return ProjectTag
     */
    public function setNop($nop)
    {
        $this->nop = $nop;

        return $this;
    }

    /**
     * Get nop
     *
     * @return integer 
     */
    public function getNop()
    {
        return $this->nop;
    }

    /**
     * Get llocGlobal
     * 
     * @param integer $v
     * @return \Slx\GitMinerBundle\Entity\ProjectTag
     */
    public function setLlocGlobal($v) {
        $this->llocGlobal = $v;
        return $this;
    }
    
    /**
     * Get llocGlobal
     * @return integer
     */
    public function getLlocGlobal() {
        return $this->llocGlobal;
    }

    /**
     * Set ccn2Global
     * 
     * @param integer $v
     * @return \Slx\GitMinerBundle\Entity\ProjectTag
     */
    public function setCcn2Global($v) {
        $this->ccn2Global = $v;
        return $this;
    }
    
    /**
     * Get ccn2Global
     * 
     * @return integer
     */
    public function getCcn2Global() {
        return $this->ccn2Global;
    }

    /**
     * Set roots
     *
     * @param integer $roots
     * @return ProjectTag
     */
    public function setRoots($roots)
    {
        $this->roots = $roots;

        return $this;
    }

    /**
     * Get roots
     *
     * @return integer 
     */
    public function getRoots()
    {
        return $this->roots;
    }

    /**
     * @var string
     */
    private $zipball_url;

    /**
     * Set zipball_url
     *
     * @param string $zipballUrl
     * @return ProjectTag
     */
    public function setZipballUrl($zipballUrl)
    {
        $this->zipball_url = $zipballUrl;

        return $this;
    }

    /**
     * Get zipball_url
     *
     * @return string 
     */
    public function getZipballUrl()
    {
        return $this->zipball_url;
    }

    protected function getUploadRootDir()
    {
        return realpath(__DIR__ . '/../../../../web/' . $this->getUploadDir());
        //return realpath('/media/waev-data/' . $this->getUploadDir());
    }

    protected function getUploadDir()
    {
        return 'uploads/project-data';
        //return 'uploads/projects';
    }

    public function getTagWorkingFolder() {
        $folder = sprintf("%s/%s/%s", $this->getUploadRootDir(),
            $this->getProject()->getId(), $this->getId());
        return $folder;
    }
    
    public function getGlobalCodeFolder() {
        return sprintf("%s/%s", $this->getTagWorkingFolder(), 'global-code/');
    }
    
    public function downloadZipBall()
    {
        $out = false;
        $folder = $this->getTagWorkingFolder();
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
        $zipFile = sprintf("%s/%s.zip", $folder, $this->getTag());
        //$url = str_replace(" ", "%20", $this->getZipballUrl());
        $url = sprintf("https://github.com/%s/%s/archive/%s.zip", $this->getProject()->getOwner(), $this->getProject()->getPname(), $this->getTag());
        printf("url = %s\n", $url);
        if ($this->wgetFile($url, $zipFile)) {
            $scode = $this->unzipZipBall($folder, $zipFile);
            if( $scode ) {
                @unlink($zipFile);
                $this->setSourceCodePath($scode);
                $out = true;
            }
        }
        return $out;
    }
    
    private function wgetFile($url, $filename) {
        $command = sprintf("wget -q -O %s %s", $filename, $url);
        $output = array();
        try {
            exec($command, $output);
            return true;
        }
        catch(\Exception $ex) {
            return false;
        }
    }
    
    private function downloadFile($url, $filename) {
        $out = true;
        set_time_limit(0);
        $fp = fopen($filename, 'w+'); //This is the file where we save the    information
        $ch = curl_init($url); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); // write curl response to file
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_exec($ch); // get curl response
        
        if (curl_error($ch)) {
            printf("err_no: %s error: %s\n", curl_errno($ch), curl_error($ch));
            $out = false;
        }
        curl_close($ch);
        fclose($fp);     
        return $out;
    }
    
    private function unzipZipBall($folder, $zipFile) {
        $out = '';
        $zip = new \ZipArchive();
        if ($zip->open($zipFile) === TRUE) {
            $zip->extractTo($folder);
            $zip->close();
            $out = '';
            $finder = new Finder();
            $finder->directories()->in($folder);
            foreach($finder as $file) {
                $out = $file->getRealpath();
                break;
            }
        } else {
            $out = false;
        }        
        return $out;
    }

    
    public function getPdependXmlFilename() {
        $folder = $this->getTagWorkingFolder();
        return sprintf("%s/%s.xml", $folder, $this->getId() );
    }
    
    public function getPhpLocCsvFilename() {
        $folder = $this->getTagWorkingFolder();
        return sprintf("%s/phploc-%s.csv", $folder, $this->getId() );
    }

    public function getPhpDCDFilename() {
        $folder = $this->getTagWorkingFolder();
        return sprintf("%s/%s.phpdcd", $folder, $this->getId() );
    }

    /**
     * @var string
     */
    private $source_code_path;


    /**
     * Set source_code_path
     *
     * @param string $sourceCodePath
     * @return ProjectTag
     */
    public function setSourceCodePath($sourceCodePath)
    {
        $this->source_code_path = $sourceCodePath;
    
        return $this;
    }

    /**
     * Get source_code_path
     *
     * @return string 
     */
    public function getSourceCodePath()
    {
        return $this->source_code_path;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    public function setStatus($status) {
        $this->status = $status;
        return $this;
    }
    
    public function getJobId() {
        return $this->job_id;
    }
    
    public function setJobId($jobId) {
        $this->job_id = $jobId;
        return $this;
    }
    
    public function getJobState() {
        return $this->job_state;
    }
    
    public function setJobState($jobState) {
        $this->job_state = $jobState;
        return $this;
    }

    public function dropFiles() {
        $command = sprintf("rm -rf %s", $this->getTagWorkingFolder());
        exec($command);
    }
 
    public function resetData() {
        $this->ahh=0;
        $this->andc=0;
        $this->calls=0;
        $this->ccn=0;
        $this->ccn2Global=0;
        $this->cloc=0;
        $this->clsa=0;
        $this->clsc=0;
        $this->eloc=0;
        $this->fanout=0;
        $this->leafs=0;
        $this->lloc=0;
        $this->llocGlobal=0;
        $this->loc=0;
        $this->maxDIT=0;
        $this->ncloc=0;
        $this->noc=0;
        $this->nof=0;
        $this->noi=0;
        $this->nom=0;
        $this->nop=0;
        $this->roots=0;
    }
    
    /**
     * Add addProjectMetric
     *
     * @param \Slx\MetricsBundle\Entity\ProjectMetric $projectMetric
     * @return ProjectTag
     */
    public function addProjectMetric(\Slx\MetricsBundle\Entity\ProjectMetric $projectMetric)
    {
        $this->projectMetrics[] = $projectMetric;

        return $this;
    }

    /**
     * Remove projectFiles
     *
     * @param \Slx\MetricsBundle\Entity\ProjectMetric $projectMetric
     */
    public function removeProjectMetric(\Slx\MetricsBundle\Entity\ProjectMetric $projectMetric)
    {
        $this->projectMetrics->removeElement($projectMetric);
    }

    /**
     * Get projectFiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProjectMetrics()
    {
        return $this->projectMetrics;
    }

}