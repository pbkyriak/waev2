<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTagDiff
 */
class ProjectTagDiff
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $fadd;

    /**
     * @var integer
     */
    private $fmod;

    /**
     * @var integer
     */
    private $frem;

    /**
     * @var integer
     */
    private $fnadd;

    /**
     * @var integer
     */
    private $fnmod;

    /**
     * @var integer
     */
    private $fnrem;

    private $fnrem_nc;
    /**
     * @var integer
     */
    private $madd;

    /**
     * @var integer
     */
    private $mmod;

    /**
     * @var integer
     */
    private $mrem;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagFrom;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTagTo;

    /**
     *
     * @var integer
     */
    private $fnrem_files_nb;
    
    /**
     *
     * @var integer
     */
    private $fnrem_private_nb;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fadd
     *
     * @param integer $fadd
     * @return ProjectTagDiff
     */
    public function setFadd($fadd)
    {
        $this->fadd = $fadd;
    
        return $this;
    }

    /**
     * Get fadd
     *
     * @return integer 
     */
    public function getFadd()
    {
        return $this->fadd;
    }

    /**
     * Set fmod
     *
     * @param integer $fmod
     * @return ProjectTagDiff
     */
    public function setFmod($fmod)
    {
        $this->fmod = $fmod;
    
        return $this;
    }

    /**
     * Get fmod
     *
     * @return integer 
     */
    public function getFmod()
    {
        return $this->fmod;
    }

    /**
     * Set frem
     *
     * @param integer $frem
     * @return ProjectTagDiff
     */
    public function setFrem($frem)
    {
        $this->frem = $frem;
    
        return $this;
    }

    /**
     * Get frem
     *
     * @return integer 
     */
    public function getFrem()
    {
        return $this->frem;
    }

    /**
     * Set fnadd
     *
     * @param integer $fnadd
     * @return ProjectTagDiff
     */
    public function setFnadd($fnadd)
    {
        $this->fnadd = $fnadd;
    
        return $this;
    }

    /**
     * Get fnadd
     *
     * @return integer 
     */
    public function getFnadd()
    {
        return $this->fnadd;
    }

    /**
     * Set fnmod
     *
     * @param integer $fnmod
     * @return ProjectTagDiff
     */
    public function setFnmod($fnmod)
    {
        $this->fnmod = $fnmod;
    
        return $this;
    }

    /**
     * Get fnmod
     *
     * @return integer 
     */
    public function getFnmod()
    {
        return $this->fnmod;
    }

    /**
     * Set fnrem
     *
     * @param integer $fnrem
     * @return ProjectTagDiff
     */
    public function setFnrem($fnrem)
    {
        $this->fnrem = $fnrem;
    
        return $this;
    }

    /**
     * Get fnrem
     *
     * @return integer 
     */
    public function getFnrem()
    {
        return $this->fnrem;
    }

    /**
     * Set fnrem
     *
     * @param integer $fnrem_nc
     * @return ProjectTagDiff
     */
    public function setFnremNc($fnrem)
    {
        $this->fnrem_nc = $fnrem;
    
        return $this;
    }

    /**
     * Get fnrem_nc
     *
     * @return integer 
     */
    public function getFnremNc()
    {
        return $this->fnrem_nc;
    }
    
    /**
     * Set madd
     *
     * @param integer $madd
     * @return ProjectTagDiff
     */
    public function setMadd($madd)
    {
        $this->madd = $madd;
    
        return $this;
    }

    /**
     * Get madd
     *
     * @return integer 
     */
    public function getMadd()
    {
        return $this->madd;
    }

    /**
     * Set mmod
     *
     * @param integer $mmod
     * @return ProjectTagDiff
     */
    public function setMmod($mmod)
    {
        $this->mmod = $mmod;
    
        return $this;
    }

    /**
     * Get mmod
     *
     * @return integer 
     */
    public function getMmod()
    {
        return $this->mmod;
    }

    /**
     * Set mrem
     *
     * @param integer $mrem
     * @return ProjectTagDiff
     */
    public function setMrem($mrem)
    {
        $this->mrem = $mrem;
    
        return $this;
    }

    /**
     * Get mrem
     *
     * @return integer 
     */
    public function getMrem()
    {
        return $this->mrem;
    }

    /**
     * Set projectTagSrc
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagSrc
     * @return ProjectTagDiff
     */
    public function setProjectTagFrom(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagSrc = null)
    {
        $this->projectTagFrom = $projectTagSrc;
    
        return $this;
    }

    /**
     * Get projectTagSrc
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagFrom()
    {
        return $this->projectTagFrom;
    }

    /**
     * Set projectTagTrg
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTagTrg
     * @return ProjectTagDiff
     */
    public function setProjectTagTo(\Slx\GitMinerBundle\Entity\ProjectTag $projectTagTrg = null)
    {
        $this->projectTagTo = $projectTagTrg;
    
        return $this;
    }

    /**
     * Get projectTagTrg
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTagTo()
    {
        return $this->projectTagTo;
    }
    /**
     * @var integer
     */
    private $cadd;

    /**
     * @var integer
     */
    private $cmod;

    /**
     * @var integer
     */
    private $crem;


    /**
     * Set cadd
     *
     * @param integer $cadd
     * @return ProjectTagDiff
     */
    public function setCadd($cadd)
    {
        $this->cadd = $cadd;
    
        return $this;
    }

    /**
     * Get cadd
     *
     * @return integer 
     */
    public function getCadd()
    {
        return $this->cadd;
    }

    /**
     * Set cmod
     *
     * @param integer $cmod
     * @return ProjectTagDiff
     */
    public function setCmod($cmod)
    {
        $this->cmod = $cmod;
    
        return $this;
    }

    /**
     * Get cmod
     *
     * @return integer 
     */
    public function getCmod()
    {
        return $this->cmod;
    }

    /**
     * Set crem
     *
     * @param integer $crem
     * @return ProjectTagDiff
     */
    public function setCrem($crem)
    {
        $this->crem = $crem;
    
        return $this;
    }

    /**
     * Get crem
     *
     * @return integer 
     */
    public function getCrem()
    {
        return $this->crem;
    }
    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;


    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectTagDiff
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\GitMinerBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
    
    /**
     * Get number of files where functions were removed
     * 
     * @return integer
     */
    public function getFnremFilesNb() {
        return $this->fnrem_files_nb;
    }
    
    public function setFnremFilesNb($nb) {
        $this->fnrem_files_nb = $nb;
        return $this;
    }
    
    /**
     * Get number for removed private functions
     * 
     * @return integer
     */
    public function getFnremPrivateNb() {
        return $this->fnrem_private_nb;
    }
    
    public function setFnremPrivateNb($nb) {
        $this->fnrem_private_nb = $nb;
        return $this;
    }
}