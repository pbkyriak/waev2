<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTagFuncCall
 */
class ProjectTagFuncCall
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $context;

    /**
     * @var integer
     */
    private $line;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectFile
     */
    private $projectFile;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectCodeConstruct
     */
    private $calledFunction;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set context
     *
     * @param string $context
     * @return ProjectTagFuncCall
     */
    public function setContext($context)
    {
        $this->context = $context;
    
        return $this;
    }

    /**
     * Get context
     *
     * @return string 
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set line
     *
     * @param integer $line
     * @return ProjectTagFuncCall
     */
    public function setLine($line)
    {
        $this->line = $line;
    
        return $this;
    }

    /**
     * Get line
     *
     * @return integer 
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectTagFuncCall
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }

    /**
     * Set projectFile
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectFile $projectFile
     * @return ProjectTagFuncCall
     */
    public function setProjectFile(\Slx\GitMinerBundle\Entity\ProjectFile $projectFile = null)
    {
        $this->projectFile = $projectFile;
    
        return $this;
    }

    /**
     * Get projectFile
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectFile 
     */
    public function getProjectFile()
    {
        return $this->projectFile;
    }

    /**
     * Set function
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectCodeConstruct $function
     * @return ProjectTagFuncCall
     */
    public function setCalledFunction(\Slx\GitMinerBundle\Entity\ProjectCodeConstruct $function = null)
    {
        $this->calledFunction = $function;
    
        return $this;
    }

    /**
     * Get function
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectCodeConstruct 
     */
    public function getCalledFunction()
    {
        return $this->calledFunction;
    }
}