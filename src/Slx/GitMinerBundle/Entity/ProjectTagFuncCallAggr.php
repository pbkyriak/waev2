<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTagFuncCallAggr
 */
class ProjectTagFuncCallAggr
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $func;

    /**
     * @var integer
     */
    private $nb_calls;

    private $fanout=0;
    
    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set func
     *
     * @param string $func
     * @return ProjectTagFuncCallAggr
     */
    public function setFunc($func)
    {
        $this->func = $func;
    
        return $this;
    }

    /**
     * Get func
     *
     * @return string 
     */
    public function getFunc()
    {
        return $this->func;
    }

    /**
     * Set nb_calls
     *
     * @param integer $nbCalls
     * @return ProjectTagFuncCallAggr
     */
    public function setNbCalls($nbCalls)
    {
        $this->nb_calls = $nbCalls;
    
        return $this;
    }

    /**
     * Get nb_calls
     *
     * @return integer 
     */
    public function getNbCalls()
    {
        return $this->nb_calls;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectTagFuncCallAggr
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    
    public function getFanout() {
        return $this->fanout;
    }
    
    public function setFanout($v) {
        $this->fanout = $v;
        return $this;
    }
}