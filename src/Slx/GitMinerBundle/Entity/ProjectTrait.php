<?php

namespace Slx\GitMinerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectTrait
 */
class ProjectTrait
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $fname;

    /**
     * @var string
     */
    private $namespace;

    /**
     * @var \Slx\GitMinerBundle\Entity\ProjectTag
     */
    private $projectTag;

    private $project_file_id;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fname
     *
     * @param string $fname
     * @return ProjectTrait
     */
    public function setFname($fname)
    {
        $this->fname = $fname;
    
        return $this;
    }

    /**
     * Get fname
     *
     * @return string 
     */
    public function getFname()
    {
        return $this->fname;
    }

    /**
     * Set namespace
     *
     * @param string $namespace
     * @return ProjectTrait
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;
    
        return $this;
    }

    /**
     * Get namespace
     *
     * @return string 
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $projectTag
     * @return ProjectTrait
     */
    public function setProjectTag(\Slx\GitMinerBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\GitMinerBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    
    public function getProjectFileId() {
        return $this->project_file_id;
    }
    
    public function setProjectFileId($v) {
        $this->project_file_id = $v;
        return $this;
    }

}