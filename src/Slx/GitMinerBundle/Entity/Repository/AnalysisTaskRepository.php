<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\AnalysisTask;

/**
 * Description of AnalysisTaskRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class AnalysisTaskRepository  extends EntityRepository
{

    public function getLastProjectTasksInRunningState(Project $project) {
        return $this->createQueryBuilder('a')
            ->where('a.project=:project and a.status=2')
            ->setParameter('project', $project)
            ->getQuery()
            ->getResult();
    }
}
