<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectDir;
use Slx\GitMinerBundle\Entity\ProjectTag;
use PDO;

/**
 * Description of ProjectCodeConstructRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectCodeConstructRepository extends EntityRepository
{
    public function getFileContents(ProjectFile $file) {
        return $this->getEntityManager()
            ->createQuery("SELECT a FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectFile=:file AND a.ctype IN ('function', 'class')")
            ->setParameter('file', $file)
            ->getResult();
    }
    
    public function getClassMethods(ProjectFile $file, $className) {
        return $this->getEntityManager()
            ->createQuery("SELECT a FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectFile=:file AND a.ctype='method' AND a.cname LIKE :class")
            ->setParameters(array('file'=>$file, 'class'=> sprintf("%s%%", $className)))
            ->getResult();
        
    }
    
    public function getTagFunctionsForSC(ProjectTag $tag) {
        return $this->getEntityManager()
            ->createQuery("SELECT a.id, a.cname, a.signature FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectTag=:tag AND a.ctype='function' ORDER BY a.cname ")
            ->setParameter('tag', $tag)
            ->getArrayResult();
    }
    
    public function getTagFunctionNames(ProjectTag $tag) {
        return $this->getEntityManager()
            ->createQuery("SELECT a.id, a.cname FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectTag=:tag AND a.ctype='function' ORDER BY a.cname ")
            ->setParameter('tag', $tag)
            ->getArrayResult();
    }
    
    public function getTagDroppedFunctions(ProjectTag $tag) {
        return $this->getEntityManager()
            ->createQuery("SELECT a.cname, a.droped_at_tag_id FROM SlxGitMinerBundle:ProjectCodeConstruct a WHERE a.projectTag=:tag AND a.ctype='function' and a.droped_at_tag_id!=0")
            ->setParameter('tag', $tag)
            ->getArrayResult();
    }
    
    /**
     * Returns two column array with tag and percentage of functions without calls on total project functions.
     * 
     * @param integer $projectId
     * @return array
     */
    public function getFunctionsWithoutCallsOnTotalPerTag($projectId) {
        $sql = "select pt.tag, round(sum(if(fr.nb_calls=0, 1,0))/count(*)*100,1) as perc
                from project_code_construct as pc
                left join func_registry as fr on (fr.fname=pc.cname)
                left join project_tag as pt on (pc.project_tag_id=pt.id)
                where pc.ctype='function' and pt.project_id=:pid
                group by pc.project_tag_id";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    /**
     * Returns two column array with tag and percentage of functions with fanOut=0 on total project functions.
     * 
     * @param integer $projectId
     * @return array
     */
    public function getFunctionsWithFanoutZero($projectId) {
        $sql = "select pt.tag, round(sum(if(pcc.fanout=0, 1, 0))/count(*)*100,1) as perc
                from project_code_construct as pcc
                left join project_tag as pt on (pcc.project_tag_id=pt.id)
                where pt.project_id=:pid and pcc.ctype='function'
                group by pcc.project_tag_id";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    /**
     * Returns two column array with tag and percentage of functions with fanOut=0 on total project functions.
     * 
     * @param integer $projectId
     * @return array
     */
    public function getFunctionsAvgFanout($projectId) {
        $sql = "select pt.tag, round(sum(pcc.fanout)/count(*),1) as perc
                from project_code_construct as pcc
                left join project_tag as pt on (pcc.project_tag_id=pt.id)
                where pt.project_id=:pid and pcc.ctype='function'
                group by pcc.project_tag_id";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getTagFunctionsCCNWealth($tagId) {
        $sql = "select ccn from project_code_construct where project_tag_id=:tid";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('tid'=>$tagId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $out = array();
        foreach($data as $row) {
            $out[] = $row['ccn'];
        }
        return $out; 
    }

    public function getTagLOCWealth($tagId) {
        $sql = "select loc from project_code_construct where project_tag_id=:tid";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('tid'=>$tagId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $out = array();
        foreach($data as $row) {
            $out[] = $row['loc'];
        }
        return $out; 
    }
    
    public function getTagCLOCPerLOCWealth($tagId) {
        $sql = "select round(cloc/loc*100,1) as avg from project_code_construct where project_tag_id=:tid";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('tid'=>$tagId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $out = array();
        foreach($data as $row) {
            $out[] = $row['avg'];
        }
        return $out; 
    }
    
    public function getTagFunctionListQuery($filterParams) {

        $params = $filterParams['params'];
        $qb = $this->createQueryBuilder('a')
            ->select('a.cname, a.loc, a.cloc, a.ccn, b.nb_calls fanin, b.fanout, c.id file_id')
            ->join('a.projectFile', 'c')
            ->join('SlxGitMinerBundle:ProjectTagFuncCallAggr', 'b', 'WITH', 'a.cname=b.func and a.projectTag=b.projectTag')
            ->where("a.projectTag=:tid and a.ctype='function'")
            ->orderBy('a.cname');
        if($filterParams['dql']) {
            $qb->andWhere($filterParams['dql']);
        }
        $q = $qb->getQuery();
        $count = $qb->select('count(a)')->getQuery()->setParameters($params)->getSingleScalarResult();
        $q->setParameters($params);
        $q->setHint('knp_paginator.count', $count);
        return $q;
    }
}

