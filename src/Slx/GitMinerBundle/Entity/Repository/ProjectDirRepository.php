<?php
namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\ProjectDir;
use Slx\GitMinerBundle\Entity\ProjectTag;
/**
 * Description of ProjectDirRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectDirRepository extends EntityRepository
{
    public function getDirSubDirs(ProjectTag $tag, ProjectDir $dir) {
        
        return $this->getEntityManager()
            ->createQuery("SELECT a FROM SlxGitMinerBundle:ProjectDir a WHERE a.projectTag=:tag AND a.fname like :root AND a.dir_depth=:dd ORDER BY a.fname ASC")
            ->setParameters(array(
                'tag' => $tag,
                'root'=>$dir->getFname().'%', 
                'dd'=>$dir->getDirDepth()+1
            ))
            ->getResult();
    }
    
    public function getPathDirs($tag, $path) {
        $folders = $this->getDirs($path);
        $dirs = null;
        if( $folders ) {
            $dirs = $this->createQueryBuilder('d')
                ->where('d.projectTag=:tag')
                ->andWhere('d.fname in (:dirs)')
                ->setParameters(array(
                    'tag'=>$tag,
                    'dirs'=>$folders,
                ))
                ->getQuery()
                ->getResult();
        }
        return $dirs;
    }
    
    private function getDirs($fname) {
        $out = array();
        $dd = explode('/', $fname);
        $d = array_shift($dd);
        if( $d )
            $out[] = $d;
        foreach($dd as $t) {
            $d = $d. '/'.$t;
            if( $d!='/' )
                $out[] = $d;
        }
        return $out;
    }

}

