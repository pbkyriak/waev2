<?php
namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\ProjectDir;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of ProjectFileRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectFileRepository extends EntityRepository
{
    public function getDirFiles(ProjectTag $tag, ProjectDir $dir) {
        return $this->getEntityManager()
            ->createQuery("SELECT a FROM SlxGitMinerBundle:ProjectFile a WHERE a.projectTag=:tag AND a.fname LIKE :fn AND a.dir_depth=:dd ORDER BY a.fname")
            ->setParameters(array(
                'tag' => $tag,
                'fn'=>$dir->getFname().'%', 
                'dd'=>$dir->getDirDepth()+1
            ))
            ->getResult();
    }
    
    public function getTagTotalFiles($projectTag) {
        $em = $this->getEntityManager();
        $dql = "SELECT count(a.id) as cnt FROM SlxGitMinerBundle:ProjectFile a WHERE a.projectTag=:tag";
        $query = $em->createQuery($dql);
        $query->setParameters(array('tag'=>$projectTag));
        return $query->getSingleScalarResult();        
    }
}

