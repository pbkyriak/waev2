<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
use JMS\JobQueueBundle\Entity\Job;
use PDO;
use Slx\GitMinerBundle\Entity\AnalysisTask;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProjectRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectRepository extends EntityRepository
{

    /**
     *
     * @return array
     */
    public function getNotFinishedProjects()
    {
        return $this
                ->getEntityManager()
                ->createQuery("SELECT p FROM SlxGitMinerBundle:Project p WHERE p.status<:status")
                ->setParameter('status', Project::STATUS_FINSHED)
                ->getResult();
    }

    public function killRestTasks($project)
    {
        $em = $this->getEntityManager();
        $em->createQuery("DELETE FROM SlxGitMinerBundle:AnalysisTask pd WHERE pd.project=:project and pd.status=0")
            ->setParameter('project', $project)
            ->execute();
        return $this;
    }

    /**
     * Changes project status to cancel, at first chance engine will stop analysis.
     *
     * @param Project $project
     * @return boolean
     */
    public function cancelProject($project)
    {
        if ($this->isCancelable($project)) {
            $em = $this->getEntityManager();
            $project->setStatus(Project::STATUS_CANCELING);
            $em->persist($project);
            $em->flush();
            return true;
        }
        return false;
    }

    /**
     * Resume project run. Useful after failures (bug, crash of something).
     * Turns project status to pending and last task to ready.
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     */
    public function resumeProject($project)
    {
        $em = $this->getEntityManager();
        $tasks = $em->getRepository('SlxGitMinerBundle:AnalysisTask')->getLastProjectTasksInRunningState($project);
        foreach ($tasks as $task) {
            $task->setStatus(0);
            $em->persist($task);
        }
        // set project to pending, so projectControl will start a new job for it
        $project->setStatus(Project::STATUS_PENDING);
        $em->persist($project);
        $em->flush();
        return $this;
    }

    /**
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @param \JMS\JobQueueBundle\Entity\Job $job
     * @return boolean
     */
    public function isResumable($project, Job $job)
    {
        $em = $this->getEntityManager();
        $out = false;
        if ($job) {
            if ($job->getState() != Job::STATE_RUNNING) {
                $out = $this->hasPendingTasks($project);
            }
        }
        return $out;
    }

    /**
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return boolean
     */
    public function hasPendingTasks(Project $project)
    {
        $tasks = $this
            ->getEntityManager()
            ->getRepository('SlxGitMinerBundle:AnalysisTask')
            ->findBy(array('project' => $project, 'status' => Project::STATUS_INIT));
        return count($tasks) != 0;
    }

    /**
     * Cancelable run is when status is pending or running
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return boolean
     */
    public function isCancelable(Project $project)
    {
        $out = false;
        if ($project->getStatus() == Project::STATUS_PENDING || $project->getStatus() == Project::STATUS_RUNNING) {
            $out = true;
        }
        return $out;
    }

    /**
     * Returns average CCN and CCN function divisions 0-5,5-10,10+ per tag.
     *
     * @param int $projectId
     * @return array
     */
    public function getFunctionsCCN($projectId)
    {
        $sql = "
                select
                    pt.tag,
                    round(sum(ptc.ccn)/count(*),1) as ccnAvg,
                    round(sum(if(ptc.ccn <= 5, 1, 0))/count(*)*100,1) as ccn05,
                    round(sum(if(ptc.ccn > 5 and ptc.ccn <= 10, 1, 0))/count(*)*100,1) as ccn510,
                    round(sum(if(ptc.ccn > 10, 1, 0))/count(*)*100,1) as ccn10
                from
                    project_code_construct as ptc
                left join
                    project_tag pt ON pt.id = ptc.project_tag_id
                where
                    pt.project_id = :pid and ptc.ctype='function'
                group by ptc.project_tag_id
                ";
        $stmt = $this
            ->getEntityManager()
            ->getConnection()
            ->executeQuery($sql, array('pid' => $projectId));
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Adds project to analysis engine
     *
     * @param Project $project
     */
    public function addProjectToAnalysis($project)
    {
        $em = $this->getEntityManager();
        $aTask = new AnalysisTask();
        $aTask->setProject($project);
        $aTask->setClass('InitAnalysisLv1');
        $aTask->setName('Initialize analysis level 1');
        $aTask->setParams(serialize(array('project_id' => $project->getId())));
        $em->persist($aTask);
        $project->setStatus(Project::STATUS_PENDING);
        $em->persist($project);
        $em->flush();
        return $this;
    }

    /**
     * Cleans project data.
     *
     * @param Project $project
     */
    public function cleanProjectData($project)
    {
        $em = $this->getEntityManager();

        $tags = $em
            ->getRepository('SlxGitMinerBundle:ProjectTag')
            ->findBy(array('project' => $project));
        foreach ($tags as $tag) {
            $em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectDir pd WHERE pd.projectTag=:tag")
                ->setParameter('tag', $tag)
                ->execute();
            $em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectFile pf WHERE pf.projectTag=:tag")
                ->setParameter('tag', $tag)
                ->execute();
            $tag->dropFiles();
            $tag->resetData();
            $tag->setStatus(ProjectTag::STATUS_NEW);
            $em->persist($tag);
            $em->flush();
        }
        return $this;
    }

    /**
     * Checks if project has a Job running
     *
     * @param Project $project
     * @return boolean
     */
    public function hasRunningJob($project)
    {
        $em = $this->getEntityManager();
        $out = false;
        $job = $em
            ->getRepository('JMSJobQueueBundle:Job')
            ->find($project->getJobId());
        if ($job) {
            if ($job->isRunning()) {
                $out = true;
            }
        }
        return $out;
    }

    /**
     * Reset project analysis, drops data!
     * @param Project $project
     */
    public function resetAnalysis($project, $cleanData=true)
    {
        $this->cleanAnalysisTasks($project);
        if( $cleanData ) {
            $this->cleanProjectData($project);
        }
        $this->addProjectToAnalysis($project);
        return $this;
    }

    /**
     * Removes all analysis tasks of the project
     *
     * @param Project $project
     */
    public function cleanAnalysisTasks($project)
    {
        $em = $this->getEntityManager();

        $atasks = $em
            ->getRepository('SlxGitMinerBundle:AnalysisTask')
            ->findBy(array('project' => $project));
        foreach ($atasks as $atask) {
            $em->remove($atask);
        }
        $em->flush();
        return $this;
    }

}
