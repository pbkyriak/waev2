<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use PDO;

/**
 * Description of ProjectTagDiffRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectTagDiffRepository extends EntityRepository
{
    
    public function getFunctionsRemovedPercOnTotal($projectId) {
        $sql = "
                select tt.tag, round(ptd.fnrem/count(pcc.id)*100,1) as perc
                from project_tag_diff as ptd
                left join project_tag as tf on (ptd.project_tag_id_from=tf.id)
                left join project_tag as tt on (ptd.project_tag_id_to=tt.id)
                left join project_code_construct pcc on (ptd.project_tag_id_from=pcc.project_tag_id and pcc.ctype='function')
                where ptd.project_id=:pid
                group by pcc.project_tag_id
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getFunctionsRemovedWithCallsPercOnTotal($projectId) {
        $sql = "
                select tt.tag, round((ptd.fnrem-ptd.fnrem_nc)/count(pcc.id)*100,1) as perc
                from project_tag_diff as ptd
                left join project_tag as tf on (ptd.project_tag_id_from=tf.id)
                left join project_tag as tt on (ptd.project_tag_id_to=tt.id)
                left join project_code_construct pcc on (ptd.project_tag_id_from=pcc.project_tag_id and pcc.ctype='function')
                where ptd.project_id=:pid
                group by pcc.project_tag_id
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getFunctionsRemovedWithoutCallsPercOnTotal($projectId) {
        $sql = "
                select tt.tag, round(ptd.fnrem_nc/count(pcc.id)*100,1) as perc
                from project_tag_diff as ptd
                left join project_tag as tf on (ptd.project_tag_id_from=tf.id)
                left join project_tag as tt on (ptd.project_tag_id_to=tt.id)
                left join project_code_construct pcc on (ptd.project_tag_id_from=pcc.project_tag_id and pcc.ctype='function')
                where ptd.project_id=:pid
                group by pcc.project_tag_id
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getFuncAndMethAddRemPercOnTotal($projectId) {
        $sql = "
                select 
                    tt.tag, 
                    round((ptd.fnadd+ptd.madd)/count(pcc.id)*100,1) as t1,
                    round((ptd.fnrem+ptd.mrem)/count(pcc.id)*100,1) as t2
                from project_tag_diff as ptd
                left join project_tag as tf on (ptd.project_tag_id_from=tf.id)
                left join project_tag as tt on (ptd.project_tag_id_to=tt.id)
                left join project_code_construct pcc on (ptd.project_tag_id_from=pcc.project_tag_id and pcc.ctype in ('function', 'method'))
                where ptd.project_id=:pid
                group by pcc.project_tag_id
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getAddModRem($project) {
        $em  = $this->getEntityManager();
        $dql = "SELECT a, b, c FROM SlxGitMinerBundle:ProjectTagDiff a JOIN a.projectTagFrom b JOIN a.projectTagTo c WHERE a.project=:project";
        $query = $em->createQuery($dql);
        $query->setParameters(array('project'=>$project));
        return $query->getResult();
    }
}
