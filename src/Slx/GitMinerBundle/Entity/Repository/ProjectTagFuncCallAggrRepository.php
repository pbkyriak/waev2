<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use PDO;

/**
 * Description of ProjectTagFuncCallAggrRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectTagFuncCallAggrRepository extends EntityRepository
{
    
    
    public function getTagFanouts($tagId) {
        $sql = "select fanout from project_tag_func_call_aggr where project_tag_id=:tid";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('tid'=>$tagId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $out = array();
        foreach($data as $row) {
            $out[] = $row['fanout'];
        }
        return $out; 
    }

}
