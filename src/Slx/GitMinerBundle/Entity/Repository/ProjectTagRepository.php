<?php

namespace Slx\GitMinerBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use PDO;

/**
 * Description of ProjectTagRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectTagRepository extends EntityRepository
{
    public function getAvgCLOC($projectId) {
        $sql = "
                select tag, round(cloc/loc*100,1) as avg
                from project_tag
                where project_id=:pid
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getLOCs($projectId) {
        $sql = "
                select tag, loc, lloc
                from project_tag
                where project_id=:pid
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
    public function getNumOfFCM($projectId) {
        $sql = "
                select tag, nof, noc, nom
                from project_tag
                where project_id=:pid
                ";
        $stmt = $this->getEntityManager()->getConnection()->executeQuery($sql,array('pid'=>$projectId));
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data; 
    }
    
}
