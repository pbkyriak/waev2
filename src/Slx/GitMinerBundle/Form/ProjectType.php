<?php

namespace Slx\GitMinerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProjectType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('owner', null, array('label'=>'gitminer.project.owner'))
            ->add('pname', null, array('label'=>'gitminer.project.pname'))
            ->add('file_extensions', null, array('label'=>'gitminer.project.file_extensions'))
            ->add('libraries', null, array('label'=>'gitminer.project.libraries', 'required'=>false))
            ->add('lib_excl_string', null, array('label'=>'gitminer.project.libexclstring', 'required'=>false))
            ->add('tests', null, array('label'=>'gitminer.project.tests', 'required'=>false))
            ->add('published', null, array('label'=>'gitminer.project.published', 'required'=>false))
            ->add('save', 'submit', array('label'=>'gsprod.general.save','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\GitMinerBundle\Entity\Project'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_gitminerbundle_project';
    }
}
