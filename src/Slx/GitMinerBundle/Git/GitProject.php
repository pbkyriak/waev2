<?php
namespace Slx\GitMinerBundle\Git;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;
/**
 * Description of GitProject
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 13 Αυγ 2014
 */
class GitProject
{
    /* @var $em EntityManager */
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    /**
     * Gets projects tags from GitHub and adds them to the project
     * 
     * @param Project $project
     */
    public function addProjectTagsFromGit($project) {
        //$tagsInfo = $this->getGitRepoTags($project->getOwner(),$project->getPname());
        $tagsInfo = $this->getGitDataTagsAll($project->getOwner(),$project->getPname());
        if ($tagsInfo) {
            $cnt = 0;
            foreach ($tagsInfo as $tagInfo) {
                // check if tag exists
                if (!$this->projectTagExists($project, $tagInfo['name'])) {
                    $cnt++;
                    $tag = new ProjectTag();
                    $tag->setProject($project)
                        ->setTag($tagInfo['name'])
                        ->setZipballUrl($tagInfo['zip'])
                        ->setUrl($tagInfo['url'])
                    ;
                    $this->em->persist($tag);
                    $this->em->flush();
                }
            }
        }     
    }
    
    private function getGitRepoTags($owner, $pname) {
        $out = array();
        $client = new \Github\Client();
        $tagsInfo = $client->api('repo')->tags($owner, $pname);
        if( $tagsInfo ) {
            $tagsInfo = array_reverse($tagsInfo);
            foreach ($tagsInfo as $tagInfo) {
                $out[] = array(
                    'name' => $tagInfo['name'],
                    'url' => $tagInfo['zipball_url'],
                    'zip' => $tagInfo['commit']['url']
                );
            }
        }
        return $out;
    }
    
    private function getGitDataTagsAll($owner, $pname) {
        $out = array();
        $client = new \Github\Client();
        $tagsInfo = $client->api('git_data')->tags()->all($owner,$pname);
        $i=1;
        if( $tagsInfo ) {
            foreach($tagsInfo as $info) {
                $tag = str_replace('refs/tags/','',$info['ref']);
                $sortTag = preg_replace("/^([a-z]*)[0-9]/", '', $tag);
                $out[$sortTag] = array(
                    'name'=> $tag,
                    'url'=> sprintf("https://github.com/%s/%s/tree/%s", $owner, $pname, $tag),
                    'zip' => sprintf("https://github.com/%s/%s/archive/%s.zip", $owner, $pname, $tag)
                );
            }
            ksort($out);
        }
        return $out;
    }
    
    /**
     * Helper that queries for project tag.
     * 
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $tag
     * @return boolean
     */
    private function projectTagExists($project, $tag)
    {
        $out = false;
        if( $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->findOneBy(array('project' => $project, 'tag' => $tag)) ) {
            $out = true;
        }
        return $out;
    }

}
