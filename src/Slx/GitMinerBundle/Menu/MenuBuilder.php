<?php

// src/Acme/MainBundle/Menu/MenuBuilder.php

namespace Slx\GitMinerBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder
{

    /** @var ContainerInterface */
    private $container;

    /** @var Router */
    private $router;

    /**
     * @var SecurityContext      
     */
    private $securityContext;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->securityContext = $container->get('security.context');
        $this->currentItemRouteName = 'metronic_admin_homepage';
    }

    public function createMainMenu(FactoryInterface $factory)
    {
     
        $menu = $factory->createItem('root');
        $menu->addChild('Home', array('route' => 'metronic_admin_homepage'))->setAttribute('icon',
            'icon-home');
        
        $projects = $menu->addChild('Projects')->setAttribute('icon',
            'icon-user');
        $projects->addChild('Projects', array('route' => 'project'));
        $menu->addChild('Blog', array('route' => 'slx_blog_posts'))->setAttribute('icon',
            'icon-home');
        $settings = $menu->addChild('Οργάνωση')->setAttribute('icon',
            'icon-sitemap');
        $settings->addChild('Απλές αναφορές', array('route' => 'simpleChartRpt'));
        $settings->addChild('Report categories', array('route' => 'simpleChartRpt_reportCategories'));
        $settings->addChild('Blog', array('route' => 'slx_blogpost'));
        $settings->addChild('Media', array('route' => 'slx_media'));
        $users = $menu->addChild('Χρήστες')->setAttribute('icon',
            'icon-user');
        if( $this->securityContext->isGranted(array('ROLE_SUPER_ADMIN'))) {
            $users->addChild('Roles', array('route' => 'roles'));
        }
        if( $this->securityContext->isGranted(array('ROLE_SUPER_ADMIN', 'ROLE_ADMIN'))) {
            $users->addChild('Users', array('route' => 'user'));
        }
                
        if($this->currentItemRouteName ) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

}