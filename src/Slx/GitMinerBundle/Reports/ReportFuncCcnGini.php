<?php

namespace Slx\GitMinerBundle\Reports;

use Slx\GraphRptBundle\Report\AbstractGraphReport;

/**
 * Description of ReportFuncFanoutGini
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportFuncCcnGini extends AbstractGraphReport
{
    
    private $project;
    
    public function createResults()
    {
        $sIdx = $this->addSeries();
        foreach($this->project->getProjectTags() as $tag) {
            $ccns = $this->em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getTagFunctionsCCNWealth($tag->getId());
            $gca = new \Slx\GitMinerBundle\Analyser\GiniCalculator();
            $gca->setData($ccns);
            $this->addSeriesRow($sIdx,$tag->getTag(), $gca->getGini());
            $gca=null;
        }
        $this->setLabel($sIdx, 'CCN Gini');
        $this->setColumnLabels($sIdx, 'Tag', 'CCN Gini');
    }

    public function getReportTitle()
    {
        return 'CCN Gini';
    }

    public function setParameters($params)
    {
        $this->project = $params['project'];
    }

}
