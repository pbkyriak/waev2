<?php

namespace Slx\GitMinerBundle\Reports;

use Slx\GraphRptBundle\Report\AbstractGraphReport;

/**
 * Description of ReportFuncFanoutGini
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportFuncLocGini extends AbstractGraphReport
{
    
    private $project;
    
    public function createResults()
    {
        $sIdx = $this->addSeries();
        foreach($this->project->getProjectTags() as $tag) {
            $fanouts = $this->em->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')->getTagLOCWealth($tag->getId());
            $gca = new \Slx\GitMinerBundle\Analyser\GiniCalculator();
            $gca->setData($fanouts);
            $this->addSeriesRow($sIdx,$tag->getTag(), $gca->getGini());
            $gca=null;
        }
        $this->setLabel($sIdx, 'LOC Gini');
        $this->setColumnLabels($sIdx, 'Tag', 'LOC Gini');
    }

    public function getReportTitle()
    {
        return 'LOC Gini';
    }

    public function setParameters($params)
    {
        $this->project = $params['project'];
    }

}
