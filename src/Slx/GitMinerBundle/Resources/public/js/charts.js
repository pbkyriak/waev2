if (!window.slxCharts)
  var slxCharts = new Object();

slxCharts.Methods = {
  areaChart: function(containerId, dataSeries, stack) {
    $.plot($(containerId), dataSeries, {
      series: {
        stack: stack
        ,
        lines: {
          show: true,
          fill: true,
          steps: false
        }
      },
      colors: ["#EDC240", "#CB4B4B", "#AFD8F8"]
    });
  },
  lineChart: function(containerId, dataSeries) {
    $.plot($(containerId), dataSeries, {
      canvas: true,
      series: {
        lines: {
          show: true
        },
        points: {
          show: true
        }
      },
      xaxis: {
        mode: "categories"
      },
      yaxis: {
      },
      grid: {
        backgroundColor: {
          colors: ["#fff", "#eee"]
        }
      }
    });
  }
};

jQuery.extend(slxCharts, slxCharts.Methods);