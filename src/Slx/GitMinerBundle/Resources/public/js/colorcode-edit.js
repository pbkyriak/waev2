
jQuery(document).ready(function() {
  collectionHolder = $('#slx_gsprodbundle_colorcodetype_color_code_alts');
  collectionHolder.data('index', collectionHolder.find(':input').length);
  
  var addFunc = function add() {
    // Get the data-prototype explained earlier
    var prototype = collectionHolder.data('prototype');

    // get the new index
    var index = collectionHolder.data('index');

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    var newForm = prototype.replace(/__name__/g, index);

    // increase the index with one for the next item
    collectionHolder.data('index', index + 1);
    $('table tbody', collectionHolder).append(newForm);
    
  };
  
  $('a.remove', collectionHolder).live('click',function(event) {
      event.preventDefault();
      $(this).closest('tr').remove();
      if ($('table tbody tr', collectionHolder).length === 0) {
          addFunc();
      }
  });
  
  $('a.add').live('click', function(event) {
    event.preventDefault();
    addFunc();
  });
});