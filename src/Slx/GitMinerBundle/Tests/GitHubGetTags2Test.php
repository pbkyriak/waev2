<?php
namespace Slx\GitMinerBundle\Tests;

/**
 * Description of GitHubGetTags2Test
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 24 Νοε 2014
 */
class GitHubGetTags2Test extends \PHPUnit_Framework_TestCase
{
    
    /**
     * gets only annotated tags, not lightweight tags.
     */
    public function tesGetTags() {
        $owner = 'TYPO3';
        $project = 'TYPO3.CMS';
        $client = new \Github\Client();
        $tagsInfo = $client->api('repo')->tags($owner,$project);
        $tagsInfo = array_reverse($tagsInfo);
        $i=1;
        foreach($tagsInfo as $info) {
            printf("%s. %s \n", $i++, $info['name']);
        }
    }
    
    public function testGetAllTags() {
        $owner = 'TYPO3';
        $project = 'TYPO3.CMS';
        $client = new \Github\Client();
        $tagsInfo = $client->api('git_data')->tags()->all($owner,$project);
        $i=1;
        foreach($tagsInfo as $info) {
            $tag = str_replace('refs/tags/','',$info['ref']);
            $url = sprintf("https://github.com/%s/%s/tree/%s", $owner, $project, $tag);
            $zip = sprintf("https://github.com/%s/%s/archive/%s.zip", $owner, $project, $tag);
            printf("%s \n\t%s\n\t%s\n", $tag, $url, $zip);
        }
    }
}
