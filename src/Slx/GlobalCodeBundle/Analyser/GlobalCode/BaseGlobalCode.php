<?php

namespace Slx\GlobalCodeBundle\Analyser\GlobalCode;

/**
 * Description of GlobalCodeBase
 *
 * @author panos
 */
class BaseGlobalCode {

    protected function isNotGlobalNode($node, $includeHtml=true) {
        $notGlobal = array(
            'PHPParser_Node_Stmt_Interface',
            'PHPParser_Node_Stmt_Class',
            'PHPParser_Node_Stmt_Trait',
            'PHPParser_Node_Stmt_Function',
        );
        if( $includeHtml ) {
            $notGlobal[] = 'PHPParser_Node_Stmt_InlineHTML';
        }
        if (in_array(get_class($node), $notGlobal)) {
            return true;
        }
        return false;
    }
}