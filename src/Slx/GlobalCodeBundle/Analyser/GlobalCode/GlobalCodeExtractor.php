<?php

namespace Slx\GlobalCodeBundle\Analyser\GlobalCode;

use Slx\GlobalCodeBundle\Analyser\GlobalCode\PretyPrinterOnlyGlobal;
use Slx\GlobalCodeBundle\Analyser\GlobalCode\BaseGlobalCode;
use PHPParser_Parser;
use PHPParser_Lexer;

/**
 * Extracts global php code from AST.
 *
 * @author panos
 */
class GlobalCodeExtractor extends BaseGlobalCode {

    private $ast;
    private $globalAst;
    private $outFilename;

    public function __construct($outFilename, $ast) {
        $this->ast = $ast;
        $this->outFilename = $outFilename;
        $this->globalAst = null;
    }

    public function process($writeGlobalCodeFile = true) {
        try {
            $code = $this->extract();
        } catch (\Exception $e) {
            printf('Exception: %s\n',$e->getMessage());
            return false;
        }
        if ($code) {
            if ($writeGlobalCodeFile) {
                if (!file_exists(dirname($this->outFilename))) {
                    mkdir(dirname($this->outFilename), 0777, true);
                }
                file_put_contents($this->outFilename, $code);
            }
            return true;
        }
        return false;
    }

    /**
     * Returns global code.
     * 
     * @return string
     */
    private function extract() {
        $global = array();
        if( !$this->ast ) {
            return false;
        }
        foreach ($this->ast as $node) {
            if (!$this->isNotGlobalNode($node)) {
                if( in_array('PHPParser_Node', class_implements($node)) ) {
                    if( $node->type ) {
                        $global[] = $node;
                    }
                }
            }
        }
        
        $this->globalAst = $global;

        $code = null;
        $printer = new PretyPrinterOnlyGlobal();
        try {
            if( count($global) ) {
                $code = $printer->prettyPrint($global);
            }
        } catch (\Exception $e) {
            printf('Exception: %s\n',$e->getMessage());
            $code = false;
        }
        if( $code ) {
            // add <?php and remove blank lines
            $code = trim(preg_replace(
                            "/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $code
                    ));
            $code = sprintf(
                "<?php\n%s", $code
            );
            $parser = new PHPParser_Parser(new PHPParser_Lexer());
            try {
                $this->globalAst = $parser->parse($code);
            } catch(\Exception $e) {
                printf("Exception: %s\n", $e->getMessage());
                return false;
            }
        }
        return $code;
    }

    public function getGlobalAst() {
        return $this->globalAst;
    }

}

