<?php

namespace Slx\GlobalCodeBundle\Analyser\GlobalCode;

use \PHPParser_NodeVisitor;
use \PHPParser_Node;
use \PHPParser_NodeTraverser;
use Slx\GlobalCodeBundle\Analyser\GlobalCode\BaseGlobalCode;

/**
 * Description of GlobalCodeMetrics
 *
 * @author panos
 */
class GlobalCodeMetrics extends BaseGlobalCode implements PHPParser_NodeVisitor {

    protected $ast;
    private $fline=-1;
    private $lline=1;
    protected $loc;
    protected $lloc;
    protected $cloc=0;
    protected $ccn;
    protected $ccn2;
    protected $infunction = false;
    protected $commentStack = array();

    public function __construct( $ast) {
        $this->ast = $ast;
    }

    public function process() {
        $this->lloc = 0;
        $this->ccn = 0;
        $this->ccn2 = 0;

        $traverser = new PHPParser_NodeTraverser();
        $traverser->addVisitor($this);
        $traverser->traverse($this->ast);
    }

    public function getLLoc() {
        return $this->lline-$this->fline+1;
    }

    public function getCCN() {
        return $this->ccn;
    }
    
    public function getCCN2() {
        return $this->ccn2;
    }
    
    public function getCLoc() {
        return $this->cloc;
    }
    public function afterTraverse(array $nodes) {
        
    }

    public function beforeTraverse(array $nodes) {
        
    }

    public function enterNode(PHPParser_Node $node) {
        //printf("node=%s %s\n", $node->getType(), $node->getLine());
        if( $this->fline<0 ) {
            $this->fline = $node->getLine ();
        }
        if( $node->getLine()>$this->lline ) {
            $this->lline = $node->getLine ();
        }
        if( $node->getLine()<$this->fline ) {
            $this->fline = $node->getLine ();
        }
    }

    public function leaveNode(PHPParser_Node $node) {
        $this->calcCCN($node);
        $this->calcCLOC($node);
    }

    private function calcCCN($node) {
        // calculating ccn
        switch (get_class($node)) {
            case 'PHPParser_Node_Stmt_If':
            case 'PHPParser_Node_Stmt_ElseIf':
            case 'PHPParser_Node_Stmt_For':
            case 'PHPParser_Node_Stmt_Foreach':
            case 'PHPParser_Node_Stmt_Switch':
            case 'PHPParser_Node_Stmt_Case':
                $this->ccn++;
                $this->ccn2++;
                break;
            case 'PHPParser_Node_Expr_BooleanAnd':
            case 'PHPParser_Node_Expr_BooleanNot':
            case 'PHPParser_Node_Expr_BooleanOr':
                $this->ccn2++;
                break;
        }
    }
    
    private function calcCLOC($node) {
        if( $node->hasAttribute('comments') ) {
            /** @var PHPParser_Comment $comment */
            foreach($node->getAttribute('comments') as $comment) {
                if(!in_array($comment, $this->commentStack)) {
                    $str = $comment->getText();
                    $this->cloc += count(preg_split('/\n|\r/',$str));
                    $this->commentStack[] = $comment;
                }
            }
        }        
    }
}
