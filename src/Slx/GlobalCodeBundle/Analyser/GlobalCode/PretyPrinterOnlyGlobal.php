<?php

namespace Slx\GlobalCodeBundle\Analyser\GlobalCode;

use \PHPParser_PrettyPrinter_Default;
use \PHPParser_Node_Stmt_InlineHTML;
use \PHPParser_Node_Stmt_Class;
use \PHPParser_Node_Stmt_Trait;
use \PHPParser_Node_Stmt_Interface;
use \PHPParser_Node_Stmt_Function;
use \PHPParser_Node_Stmt_Use;
use \PHPParser_Node_Stmt_Namespace;

/**
 * Used to get code without inner html 
 *
 * @author panos
 */
class PretyPrinterOnlyGlobal extends PHPParser_PrettyPrinter_Default {
    
    public function pStmt_InlineHTML(PHPParser_Node_Stmt_InlineHTML $node) {
        return '';
    }
    
    public function pStmt_Class(PHPParser_Node_Stmt_Class $node) {
        return '';
    }
    
    public function pStmt_Trait(PHPParser_Node_Stmt_Trait $node) {
        return '';
    }
    
    public function pStmt_Interface(PHPParser_Node_Stmt_Interface $node) {
        return '';
    }
    
    public function pStmt_Function(PHPParser_Node_Stmt_Function $node) {
        return '';
    }
    
    public function pStmt_Use(PHPParser_Node_Stmt_Use $node) {
        return '';
    }
    
    public function pStmt_Namespace(PHPParser_Node_Stmt_Namespace $node) {
        return '';
    }
    
}
