<?php

namespace Slx\GlobalCodeBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GlobalCodeBundle\Analyser\GlobalCode\GlobalCodeExtractor;
use Slx\GlobalCodeBundle\Analyser\GlobalCode\GlobalCodeMetrics;
use Slx\ReflectionBundle\Analyser\SourceCodeHelper;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Symfony\Component\Finder\Finder;

/**
 * Description of GlobalCodeTask
 *
 * @author panos
 */
class GlobalCodeTask extends AbstractTagTask {

    public function onFail() {
        return true;
    }

    public function execute() 
    {
        $mem1 = (memory_get_usage() / 1024);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->scHelper = new SourceCodeHelper($this->tag);
        $files = $this->getTagSourceFiles();
        foreach ($files as $file) {
            //printf("file: %s %s", $file, PHP_EOL);
            $this->processFile($file);
        }
        $this->setTagStatus(ProjectTag::STATUS_RFGC_DONE);
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;

        return true;
    }

    private function getTagSourceFiles() {
        $codePath = $this->tag->getSourceCodePath();
        $exts = explode(',',$this->tag->getProject()->getFileExtensions());
        if( !$exts ) {
            $exts = array('php');
        }
        $finder = new Finder();
        $finder->in($codePath);
        foreach($exts as $ext) {
            $finder->name(sprintf('*.%s', $ext));
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRealpath();
        }

        return $files;
    }

    private function processFile($file) {
        $globalFile = str_replace(
            $this->tag->getSourceCodePath(), 
            $this->tag->getGlobalCodeFolder(), 
            $file
        );

        $ast = $this->scHelper->getFileAst($file);
        if( $ast) {
            $extractor = new GlobalCodeExtractor($globalFile, $ast);
            $extractor->process();
            $gast = $extractor->getGlobalAst();
            if( $gast ) {
                $metrics = new GlobalCodeMetrics($gast);
                $metrics->process();
                $this->tag = $this->getTagById($this->tagId);
                $this->storeGlobalMetrics($file, $metrics);
            }
            $this->tag = $this->getTagById($this->tagId);
            $this->em->clear();
        }
        gc_collect_cycles();
        time_nanosleep(0, 10000000);
        
    }
    
    /**
     * Updates projectFile with global metrics. 
     * 
     * If no projectFile record found, creates new.
     * 
     * @param string $file
     * @param GlobalCodeMetrics $metrics
     */
    private function storeGlobalMetrics($file, GlobalCodeMetrics $metrics) {
        $cleanFilename = $this->cleanFilename($file);
        $fileEntity = $this->em
                ->getRepository('SlxGitMinerBundle:ProjectFile')
                ->findOneBy(
                        array(
                            'projectTag'=>$this->tag, 
                            'fname'=> $cleanFilename,
                            )
                        );
        if( !$fileEntity ) {
            $fileEntity = new ProjectFile();
            $fileEntity->setProjectTag($this->tag);
            $fileEntity->setFname($cleanFilename);
            $fileEntity->setSha1_hash(sha1_file($file));
        }
        $fileEntity->setCcn2Global($metrics->getCCN2())
                ->setCcnGlobal($metrics->getCCN())
                ->setClocGlobal($metrics->getCLoc())
                ->setLlocGlobal($metrics->getLLoc());
        $this->em->persist($fileEntity);
        $this->em->flush();
    }
    
    private function cleanFilename($fname) {
        return str_replace($this->tag->getSourceCodePath(),'', $fname);
    }

    public function getTaskName()
    {
        return 'Global code analysis';
    }
}
