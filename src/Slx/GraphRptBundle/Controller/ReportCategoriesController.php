<?php

namespace Slx\GraphRptBundle\Controller;

namespace Slx\GraphRptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Slx\GraphRptBundle\Entity\ReportCategory;
use Slx\GraphRptBundle\Form\ReportCategoryType;

/**
 * Description of ReportCategoriesController
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * 
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Categories",route="simpleChartRpt_reportCategories")
 * @CurrentMenuItem("simpleChartRpt_reportCategories")
 */

class ReportCategoriesController extends Controller {
    
    public function indexAction() {
        $form = $this->getFilterForm();
        $request = $this->get('request');
        $query = $this->makeListQuery($form);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $this->get('request')->query->get('page', 1) /* page number */,
                $this->get('request')->query->get('limit', 15)
            );

        return $this->render('SlxGraphRptBundle:ReportCategories:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }
    
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxGraphRptBundle:ReportCategory a";

        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }
    
    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'graphrpt.edit.title')
            )
            ->getForm();
        return $form;
    }


    /**
     * Displays a form to create a new entity.
     * @Breadcrumb("Νέος")
     */
    public function newAction()
    {
        $entity = new ReportCategory();
        $form = $this->createForm(new ReportCategoryType(), $entity);

        return $this->render('SlxGraphRptBundle:ReportCategories:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new  entity.
     * @Breadcrumb("Νέος")
     */
    public function createAction(Request $request)
    {
        $entity = new ReportCategory();

        $form = $this->createForm(new ReportCategoryType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'graphrpt.edit.record_saved');
            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('simpleChartRpt_reportCategories_new'));
            }
            else {
                return $this->redirect($this->generateUrl('simpleChartRpt_reportCategories_show',
                            array('id' => $entity->getId())));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'graphrpt.edit.record_errors');
        }

        return $this->render('SlxGraphRptBundle:ReportCategories:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a entity.
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:ReportCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReportCategory entity.');
        }

        return $this->render('SlxGraphRptBundle:ReportCategories:show.html.twig',
                array(
                'entity' => $entity,
                ));
    }

    /**
     * Displays a form to edit an existing ColorCode entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:ReportCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Report Category entity.');
        }

        $editForm = $this->createForm(new ReportCategoryType(), $entity);

        return $this->render('SlxGraphRptBundle:ReportCategories:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing ColorCode entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:ReportCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReportCategory entity.');
        }

        $editForm = $this->createForm(new ReportCategoryType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')
                ->getFlashBag()
                ->add('info','graphrpt.edit.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('simpleChartRpt_reportCategories_new'));
            }
            else {
                return $this->redirect($this->generateUrl('simpleChartRpt_reportCategories_show',
                            array('id' => $id)));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'graphrpt.edit.record_errors');
        }


        return $this->render('SlxGraphRptBundle:ReportCategories:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a ColorCode entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxGraphRptBundle:ReportCategory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ReportCategory entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('simpleChartRpt_reportCategories'));
    }
    
}
