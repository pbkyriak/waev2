<?php

namespace Slx\GraphRptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Slx\GraphRptBundle\Entity\SimpleChartReport;
use Slx\GraphRptBundle\Entity\ReportQueryParamBinding;
use Slx\GraphRptBundle\Entity\SimpleChartReportSeries;
use Slx\GraphRptBundle\Form\SimpleChartReportType;
use Slx\GraphRptBundle\Form\ReportQueryParamBindingType;
use Slx\GraphRptBundle\Form\SimpleChartReportSeriesType;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Οργάνωση")
 * @Breadcrumb("Απλές αναφορές",route="simpleChartRpt")
 * @CurrentMenuItem("simpleChartRpt")
 */
class SimpleChartRptController extends Controller
{
    /**
     * Lists all ColorCode entities.
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $request = $this->get('request');
        $query = $this->makeListQuery($form);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $query,
                $this->get('request')->query->get('page', 1) /* page number */,
                $this->get('request')->query->get('limit', 15)
            );

        return $this->render('SlxGraphRptBundle:SimpleChartRpt:index.html.twig',
                array(
                'pagination' => $pagination,
                'filterForm' => $form->createView()
        ));
    }

    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxGraphRptBundle:SimpleChartReport a";

        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'graphrpt.edit.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new  entity.
     * @Breadcrumb("Νέος")
     */
    public function createAction(Request $request)
    {
        $entity = new SimpleChartReport();

        $form = $this->createForm(new SimpleChartReportType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'graphrpt.edit.record_saved');
            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('simpleChartRpt_new'));
            }
            else {
                return $this->redirect($this->generateUrl('simpleChartRpt_show',
                            array('id' => $entity->getId())));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'graphrpt.edit.record_errors');
        }

        return $this->render('SlxGraphRptBundle:SimpleChartRpt:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new ColorCode entity.
     * @Breadcrumb("Νέος")
     */
    public function newAction()
    {
        $entity = new SimpleChartReport();
        $entity->addQParam(new ReportQueryParamBinding());
        $entity->addSery(new SimpleChartReportSeries());
        $form = $this->createForm(new SimpleChartReportType(), $entity);

        return $this->render('SlxGraphRptBundle:SimpleChartRpt:new.html.twig',
                array(
                'entity' => $entity,
                'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a entity.
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SimpleChartReport entity.');
        }

        return $this->render('SlxGraphRptBundle:SimpleChartRpt:show.html.twig',
                array(
                'entity' => $entity,
                ));
    }

    /**
     * Displays a form to edit an existing ColorCode entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SimpleChartReport entity.');
        }

        $editForm = $this->createForm(new SimpleChartReportType(), $entity);

        return $this->render('SlxGraphRptBundle:SimpleChartRpt:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Edits an existing ColorCode entity.
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SimpleChartReport entity.');
        }

        $originalB = array();
        foreach ($entity->getQParams() as $tag) {
            $originalB[] = $tag;
        }
        $originalS = array();
        foreach ($entity->getSeries() as $tag) {
            $originalS[] = $tag;
        }

        $editForm = $this->createForm(new SimpleChartReportType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            // parameter bindings
            foreach ($entity->getQParams() as $tag) {
                foreach ($originalB as $key => $toDel) {
                    if ($toDel->getId() === $tag->getId()) {
                        unset($originalB[$key]);
                    }
                }
            }

            foreach ($originalB as $alt) {
                $alt->setQReport(null);
                $em->persist($alt);
                $em->remove($alt);
            }
            // series
            foreach ($entity->getSeries() as $tag) {
                foreach ($originalS as $key => $toDel) {
                    if ($toDel->getId() === $tag->getId()) {
                        unset($originalS[$key]);
                    }
                }
            }

            foreach ($originalS as $alt) {
                $alt->setQReport(null);
                $em->persist($alt);
                $em->remove($alt);
            }

            $em->persist($entity);
            $em->flush();
            $this->get('session')
                ->getFlashBag()
                ->add('info','graphrpt.edit.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('simpleChartRpt_new'));
            }
            else {
                return $this->redirect($this->generateUrl('simpleChartRpt_show',
                            array('id' => $id)));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'graphrpt.edit.record_errors');
        }


        return $this->render('SlxGraphRptBundle:SimpleChartRpt:edit.html.twig',
                array(
                'entity' => $entity,
                'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a ColorCode entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SimpleChartReport entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('simpleChartRpt'));
    }
    
    public function cloneAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($id);
        $clonedEntity = clone $entity;
        $clonedEntity->setTitle($clonedEntity->getTitle().' [cloned]');
        $em->persist($clonedEntity);
        $em->flush();
        return $this->redirect($this->generateUrl('simpleChartRpt_show',
                    array('id' => $clonedEntity->getId())));

    }
}
