<?php

namespace Slx\GraphRptBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of SimpleChartRptExportController
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 6 Ιουν 2014
 */
class SimpleChartRptExportController extends Controller
{

    public function exportAction($pId, $rId)
    {
        $request = $this->get('request');

        $em = $this->getDoctrine()->getManager();
        $report = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
        $report->setRptEntityId($rId)
            ->setParameters(array('project_id' => $pId))
            ->createResults();

        $results = $this->renderView(
            'SlxGraphRptBundle:SimpleChartRptExport:dataset.csv.twig',
            array('report'=>$report)
        );

        $response = $this->render('SlxGraphRptBundle:SimpleChartRptExport:export.csv.twig',
            array(
            'results' => iconv("UTF-8", "ISO-8859-7", $results)
        ));
        $response->headers->set('Content-Type', 'text/csv');
        $date = new \DateTime();
        $response->headers->set('Content-Disposition',
            'attachment; filename="' . $date->format("U") . '-dataset.csv"');
        return $response;
    }

}
