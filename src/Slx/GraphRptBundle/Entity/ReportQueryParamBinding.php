<?php

namespace Slx\GraphRptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="report_query_param_binding")
 * @ORM\Entity()
 * 
 * Description of ReportQueryParamBinding
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportQueryParamBinding
{

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="q_param", type="string", length=50)
     */
    private $qParam;

    /**
     * @ORM\Column(name="in_param", type="string", length=50)
     */
    private $inParam;

    /**
     *
     * @ORM\ManyToOne(targetEntity="SimpleChartReport", inversedBy="qparams")
     */
    private $qreport;
    
    public function getId()
    {
        return $this->id;
    }

    public function getQParam()
    {
        return $this->qParam;
    }

    public function getInParam()
    {
        return $this->inParam;
    }

    public function getQreport()
    {
        return $this->qreport;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setQParam($qParam)
    {
        $this->qParam = $qParam;
        return $this;
    }

    public function setInParam($inParam)
    {
        $this->inParam = $inParam;
        return $this;
    }

    public function setQreport($qreport)
    {
        $this->qreport = $qreport;
        return $this;
    }

    public function __clone() {
        if($this->id) {
            $this->setId(NULL);
        }
    }
}
