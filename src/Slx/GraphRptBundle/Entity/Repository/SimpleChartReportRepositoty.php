<?php
namespace Slx\GraphRptBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\AnalysisTask;

/**
 * Description of SimpleChartReportRepositoty
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SimpleChartReportRepositoty   extends EntityRepository {
    
    public function getCategorizedReports() {
        $out = array();
        $rpts = $this->createQueryBuilder('r')
                ->leftJoin('r.category', 'c')
                ->addSelect('c')
                ->getQuery()
                ->getResult();
        foreach($rpts as $rpt) {
            $cId = $rpt->getCategory()->getId();
            if( !isset($out[$cId])) {
                $out[$cId] = array('title'=>$rpt->getCategory()->getTitle(), 'items'=>array());
            }
            $out[$cId]['items'][$rpt->getId()] = array('title'=>$rpt->getTitle(), 'descr'=>$rpt->getDescription());
        }
        return $out;
    }
    
}
