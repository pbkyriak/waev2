<?php
namespace Slx\GraphRptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Ένα query που μπορεί να έχει πολλές παραμέτρους για τρέξει και
 * φέρνει 1..n σειρές δεδομένων
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * 
 * @ORM\Table(name="simple_chart_report")
 * @ORM\Entity(repositoryClass="Slx\GraphRptBundle\Entity\Repository\SimpleChartReportRepositoty")
 */
class SimpleChartReport
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(name="query", type="text")
     */
    private $query;
    
    /**
     * @ORM\Column(name="xname", type="string", length=50)
     */
    private $xname;
    
    /**
     * @ORM\Column(name="x_column_name", type="string", length=50)
     */    
    private $xColumnName;
        
    /**
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     *
     * @ORM\OneToMany(targetEntity="ReportQueryParamBinding", mappedBy="qreport", cascade={"persist", "remove"})
     */
    private $qparams;

    /**
     *
     * @ORM\OneToMany(targetEntity="SimpleChartReportSeries", mappedBy="qreport", cascade={"persist", "remove"})
     */
    private $series;

    /**
     *
     * @ORM\ManyToOne(targetEntity="ReportCategory")
     */
    private $category;
    
    public function __construct()
    {
        $this->qparams = new ArrayCollection();
        $this->series = new ArrayCollection();
        $this->category = null;
    }
    
    public function getId()
    {
        return $this->id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getQuery()
    {
        return $this->query;
    }

    public function getXname()
    {
        return $this->xname;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    public function setQuery($query)
    {
        $this->query = $query;
        return $this;
    }

    public function setXname($xname)
    {
        $this->xname = $xname;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }
    
    public function getXColumnName()
    {
        return $this->xColumnName;
    }

    public function setXColumnName($xColumnName)
    {
        $this->xColumnName = $xColumnName;
        return $this;
    }

    public function addQParam(\Slx\GraphRptBundle\Entity\ReportQueryParamBinding $param)
    {
        $param->setQreport($this);
        $this->qparams[] = $param;
        return $this;
    }

    public function removeQParam(\Slx\GraphRptBundle\Entity\ReportQueryParamBinding $param)
    {
        $this->qparams->removeElement($param);
    }

    public function getQParams()
    {
        return $this->qparams;
    }

    public function addSery(\Slx\GraphRptBundle\Entity\SimpleChartReportSeries $sery)
    {
        $sery->setQreport($this);
        $this->series[] = $sery;
        return $this;
    }

    public function removeSery(\Slx\GraphRptBundle\Entity\SimpleChartReportSeries $series)
    {
        $this->series->removeElement($series);
    }

    public function getSeries()
    {
        return $this->series;
    }

    public function setCategory(ReportCategory $category) {
        $this->category = $category;
        return $this;
    }
    
    public function getCategory() {
        return $this->category;
    }
    
    public function __clone()
    {
        if( $this->id ) {
            $this->setId(null);
            foreach($this->series as $k => $sery ) {
                $this->series[$k] = clone $sery;
                $this->series[$k]->setQreport($this);
            }
            foreach($this->qparams as $k => $qparam ) {
                $this->qparams[$k] = clone $qparam;
                $this->qparams[$k]->setQreport($this);
            }
        }
    }
}
