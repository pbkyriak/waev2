<?php

namespace Slx\GraphRptBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="simple_chart_report_series")
 * @ORM\Entity()
 * 
 * Description of SimpleChartReportSeries
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SimpleChartReportSeries
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\Column(name="y_column_name", type="string", length=50)
     */
    private $yColumnName;
    
    /**
     * @ORM\Column(name="yname", type="string", length=50)
     */
    private $yname;
    
    /**
     * @ORM\Column(name="label", type="string", length=60)
     */
    private $label;
    
    /**
     *
     * @ORM\ManyToOne(targetEntity="SimpleChartReport", inversedBy="series")
     */
    private $qreport;

    public function getId()
    {
        return $this->id;
    }

    public function getYColumnName()
    {
        return $this->yColumnName;
    }

    public function getYname()
    {
        return $this->yname;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getQreport()
    {
        return $this->qreport;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function setYColumnName($yColumnName)
    {
        $this->yColumnName = $yColumnName;
        return $this;
    }

    public function setYname($yname)
    {
        $this->yname = $yname;
        return $this;
    }

    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function setQreport($qreport)
    {
        $this->qreport = $qreport;
        return $this;
    }

    public function __clone() {
        if( $this->id ) {
            $this->setId(null);
        }
    }
}
