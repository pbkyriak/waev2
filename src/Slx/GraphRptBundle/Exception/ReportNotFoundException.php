<?php

namespace Slx\GraphRptBundle\Exception;

/**
 * Description of ReportNotFoundException
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportNotFoundException extends \RuntimeException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
