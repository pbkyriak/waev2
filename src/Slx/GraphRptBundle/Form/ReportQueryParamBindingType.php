<?php

namespace Slx\GraphRptBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ReportQueryParamBindingType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('qParam', null, array('label'=>'graphrpt.edit.qparam'))
            ->add('inParam', null, array('label'=>'graphrpt.edit.inparam'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\GraphRptBundle\Entity\ReportQueryParamBinding'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_graphrptbundle_reportqueryparambinding';
    }
}
