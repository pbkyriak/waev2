<?php

namespace Slx\GraphRptBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SimpleChartReportSeriesType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('yColumnName', null, array('label'=>'graphrpt.edit.yColumnName'))
            ->add('yname', null, array('label'=>'graphrpt.edit.yname'))
            ->add('label', null, array('label'=>'graphrpt.edit.label'))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\GraphRptBundle\Entity\SimpleChartReportSeries'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_graphrptbundle_simplechartreportseries';
    }
}
