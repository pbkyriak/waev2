<?php

namespace Slx\GraphRptBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SimpleChartReportType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, array('label'=>'graphrpt.edit.title'))
            ->add('category', null, array('label'=>'graphrpt.edit.category'))
            ->add('query', null, array('label'=>'graphrpt.edit.query', 'attr'=>array('class'=>'col-md-12', 'rows'=>10)))
            ->add('xname', null, array('label'=>'graphrpt.edit.xname'))
            ->add('xColumnName', null, array('label'=>'graphrpt.edit.xColumnName'))
            ->add('description', null, array('label'=>'graphrpt.edit.description', 'attr'=>array('class'=>'col-md-12', 'rows'=>5)))
            ->add('series', 'collection', array(
                'type' => new SimpleChartReportSeriesType(), 
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label'=> 'graphrpt.edit.series')
                )
            ->add('qparams', 'collection', array(
                'type' => new ReportQueryParamBindingType(), 
                'allow_add' => true,
                'by_reference' => false,
                'allow_delete' => true,
                'label'=> 'graphrpt.edit.bindings')
                )
            ->add('save', 'submit', array('label'=>'graphrpt.edit.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'graphrpt.edit.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\GraphRptBundle\Entity\SimpleChartReport'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_graphrptbundle_simplechartreport';
    }
}
