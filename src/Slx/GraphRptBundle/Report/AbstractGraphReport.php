<?php

namespace Slx\GraphRptBundle\Report;

/**
 * Description of AbstractGraphReport
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
abstract class AbstractGraphReport
{

    protected $labels;
    protected $series;
    protected $columnLabels;
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    private $tabledData=null;
    
    public function __construct(\Doctrine\ORM\EntityManager $em)
    {
        $this->em=$em;
        $this->series = array();
        $this->columnLabels = array();
        $this->labels = array();
    }
    
    /**
     * Returns series labels
     * 
     * @return array
     */
    final public function getLabels()
    {
        return $this->labels;
    }
    
    /**
     * Returns the number of series
     * @return int
     */
    final public function getSeriesCount()
    {
        return count($this->series);
    }

    /**
     * Returns the $idx series of the report
     * 
     * @param int $idx
     * @return array
     */
    final public function getSeries($idx) {
        if( isset($this->series[$idx]) ) {
            return $this->series[$idx];
        }
        else {
            return array();
        }
    }
    
    /**
     * Adds a new empty series to the report and returns its index
     * 
     * @return int
     */
    protected function addSeries() {
        $idx = count($this->series);
        $this->series[$idx] = array();
        $this->columnLabels[$idx] = array('x'=>'x', 'y'=>'y');
        return $idx;
    }

    /**
     * Sets the label of the $sIdx series of the report
     * 
     * @param int $sIdx
     * @param string $label
     */
    protected function setLabel($sIdx, $label) {
        $this->labels[$sIdx] = $label;
    }
    
    /**
     * Sets the column labels of the $sIdx series of the report.
     * Column labels are useful when shown tabled the data of the report.
     * @param int $sIdx
     * @param string $labelX
     * @param string $labelY
     */
    protected function setColumnLabels($sIdx, $labelX, $labelY) {
        $this->columnLabels[$sIdx]['x'] = $labelX;
        $this->columnLabels[$sIdx]['y'] = $labelY;
    }
    
    /**
     * Returns the X (first column) label of the $sIdx series
     * 
     * @param int $sIdx
     * @return string
     */
    final public function getColumnLabelX($sIdx) {
        $out = '';
        if( isset($this->columnLabels[$sIdx]) ) {
            if( isset($this->columnLabels[$sIdx]['x'])) {
                $out = $this->columnLabels[$sIdx]['x'];
            }
        }
        return $out;
    }
    
    /**
     * Returns the Y (second column) label of the $sIdx series
     * 
     * @param int $sIdx
     * @return string
     */
    final public function getColumnLabelY($sIdx) {
        $out = '';
        if( isset($this->columnLabels[$sIdx])) {
            if( isset($this->columnLabels[$sIdx]['y'])) {
                $out = $this->columnLabels[$sIdx]['y'];
            }
        }
        return $out;
    }
    
    /**
     * Adds a new row to $sIdx series
     * 
     * @param int $sIdx
     * @param array $row
     * @return int
     */
    protected function addSeriesRow($sIdx, $k, $v) {
        $idx = count($this->series[$sIdx]);
        $this->series[$sIdx][$idx] = array($k,$v);
        return $idx;
    }

    /**
     * Returns tabled the data of all series in the report. 
     * Useful when we want to show in a table all series data
     * 
     * @return array
     */
    final public function getTabledData() {
        if( !$this->tabledData ) {
            $this->tabledData = $this->buildTabledData();
        }
        return $this->tabledData;
    }
    
    /**
     * Returns column headers for tabled data.
     * Useful when we want to show in a table all series data
     * 
     * @return array
     */
    final public function getTabledDataLabels() {
        $tlabels = array();
        $tlabels[] = $this->getColumnLabelX(0);
        for($i=0; $i<count($this->series); $i++) {
            $tlabels[] = $this->getColumnLabelY($i);
        }
        return $tlabels;
    }
    
    /**
     * Builds a table with all series data
     * 
     * @return array;
     */
    private function buildTabledData() {
        $keys = $this->getDataKeys();
        $tabData = array();
        foreach($keys as $key) {
            $tabData[$key] = array();
            $tabData[$key][0] = $key;
            for($i=0; $i<count($this->series); $i++) {
                $tabData[$key][$i+1]=null;
            }
        }
        
        foreach($this->series as $sIdx => $ser) {
            foreach($ser as $row) {
                $tabData[$row[0]][$sIdx+1]=$row[1];
            }
        }
        return $tabData;
    }
    
    /**
     * Returns X column data aggregated from all series.
     * @todo Describe how it is done
     * 
     * @return array
     */
    private function getDataKeys() {
        $keys = array();
        $sIdx = 0;
        if( !count($this->series) ) {
            return $keys;
        }
        foreach($this->series as $idx => $ser) {
            if( count($this->series[$idx])>count($this->series[$sIdx]) ) {
                $sIdx=$idx;
            }
        }
        foreach($this->series[$sIdx] as $row) {
            $keys[] = $row[0];
        }
        
        foreach($this->series as $ser) {
            foreach($ser as $row) {
                if( !in_array($row[0], $keys) ) {
                    $keys[] = $row[0];
                }            
            }
        }
        return $keys;
    }
    
    /**
     * Sets reports parameters. After setting the parameters createResults() 
     * must be called, otherwise the report will not have any data.
     * 
     * @param array $params
     */
    abstract public function setParameters($params);    
    /**
     * Creates report results. Should be called in the controller after setParams()
     */
    abstract public function createResults();
    /**
     * Returns report's title. Just implement a return 'my report title';
     * The value could be set in setParameters or in the constructor.
     * Maybe it will change in future version.
     * 
     * @return string
     */
    abstract public function getReportTitle();
    
}
