<?php

namespace Slx\GraphRptBundle\Report;

use Slx\GraphRptBundle\Report\AbstractGraphReport;
use Slx\GraphRptBundle\Entity\SimpleChartReport;
use PDO;
use Slx\GraphRptBundle\Exception\ReportNotFoundException;

/**
 * Description of SimpleChartRpt
 * πρέπει να χτίσω ένα μηχανισμό που να παίρνει το SimpleChartReport entity και να το τρέχει 
 * και να στήνει το report 
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SimpleChartRpt extends AbstractGraphReport
{

    /**
     *
     * @var SimpleChartReport
     */
    private $rptEntity = null;
    private $inputParams;

    /**
     * runs the query and loads data to the report.
     * 
     */
    public function createResults()
    {
        $rows = $this->getData();
        $rptSeries = $this->rptEntity->getSeries();
        $serDef = array();
        foreach ($rptSeries as $se) {
            $sIdx = $this->addSeries();
            $this->setLabel($sIdx, $se->getLabel());
            $this->setColumnLabels($sIdx, $this->rptEntity->getXColumnName(),
                $se->getYColumnName());
            $serDef[] = array('sIdx' => $sIdx, 'xname' => $this->rptEntity->getXname(), 'yname' => $se->getYname());
        }
        foreach ($rows as $row) {
            foreach ($serDef as $se) {
                $this->addSeriesRow(
                    $se['sIdx'], $row[$se['xname']], $row[$se['yname']]);
            }
        }
    }

    /**
     * parameter binding??
     * 
     * @return array
     */
    private function getData()
    {
        $params = array();
        // @var Slx\GraphReportBundle\Entity\ReportQueryParamBinding $qparam
        foreach ($this->rptEntity->getQParams() as $qparam) {
            if (isset($this->inputParams[$qparam->getInParam()])) {
                $params[$qparam->getQParam()] = $this->inputParams[$qparam->getInParam()];
            }
        }
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($this->rptEntity->getQuery(), $params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getReportTitle()
    {
        return $this->rptEntity->getTitle();
    }

    public function getReportDescription() {
        return $this->rptEntity->getDescription();
    }
    /**
     * Sets the report entity to process. You can use setRptEntityId also.
     * 
     * @param  SimpleChartReport $rptEntity the report entity to process
     * @return \Slx\GraphRptBundle\Report\SimpleChartRpt
     */
    public function setRptEntity($rptEntity)
    {
        $this->rptEntity = $rptEntity;
        return $this;
    }

    /**
     * You can use this method instead of setRptEntity. It fetches the report entity it self.
     * 
     * @param int $rtpEntityId  SimpleChartReport entity id 
     * @throws \Slx\GraphRptBundle\Exception\ReportNotFoundException
     */
    public function setRptEntityId($rptEntityId)
    {
        $rptEntity = $this->em->getRepository('SlxGraphRptBundle:SimpleChartReport')->find($rptEntityId);
        if (!$rptEntity) {
            throw new ReportNotFoundException(sprintf('Report with id=%s was not found',$rptEntityId));
        }
        $this->rptEntity = $rptEntity;
        return $this;
    }

    public function getRptEntityId() {
        return $this->rptEntity->getId();
    }
    
    public function setParameters($params)
    {
        $this->inputParams = $params;
        return $this;
    }

}
