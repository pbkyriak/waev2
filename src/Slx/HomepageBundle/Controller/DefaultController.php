<?php

namespace Slx\HomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;

/**
 * Project controller.
 * @Breadcrumb("Home", route="metronic_public_homepage")
 * @Breadcrumb(template="SlxMetronicBundle::public-breadcrumbtrail.html.twig")
 */
class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('SlxHomepageBundle:Default:index.html.twig');
    }
    
    public function resultsAction($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getDoctrine()->getRepository('SlxGitMinerBundle:Project')->find($id);
        $this->get('metronic_home.menu_builder')
            ->setCurrentMenuItemUri(
                $this->generateUrl(
                    'public_project_results', 
                    array('id'=>$id)
                    )
                );

        
        $reports = array();
        $reportIds = array(40,41,42,43,39,38,9);
        foreach( $reportIds as $rId) {
            $rep1 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($em);
            $rep1->setRptEntityId($rId)
                ->setParameters(array('project_id'=>$project->getId()))
                ->createResults();
            $reports[] = $rep1;
        }

        $this->get("apy_breadcrumb_trail")->add($project->getPname());
        return $this->render('SlxHomepageBundle:Default:results.html.twig', 
            array(
                'project' => $project,
                'reports' => $reports,
            )
        );
    }
}
