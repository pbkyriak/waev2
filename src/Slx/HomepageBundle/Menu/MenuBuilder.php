<?php

// src/Acme/MainBundle/Menu/MenuBuilder.php

namespace Slx\HomepageBundle\Menu;

use Knp\Menu\FactoryInterface;
use Knp\Menu\ItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Slx\MetronicBundle\Menu\AbstractMenuBuilder;

class MenuBuilder extends AbstractMenuBuilder
{

    /** @var ContainerInterface */
    private $container;

    /** @var Router */
    private $router;

    /**
     * @var SecurityContext      
     */
    private $securityContext;
    /** @var EntityManager */
    private $em;
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->router = $container->get('router');
        $this->securityContext = $container->get('security.context');
        $this->em=$container->get('doctrine');
        $this->currentItemRouteName = 'metronic_public_homepage';
    }

    public function createMainMenu(FactoryInterface $factory)
    {
        
        $menu = $factory->createItem('root');
        $menu->addChild('Home', array('route' => 'metronic_public_homepage'))->setAttribute('icon',
            'icon-home');
        $this->addProjectsMenu($menu);
        $menu->addChild('Engine', array('route' => 'metronic_admin_homepage'));
                
        if($this->getCurrentMenuItemUri()) {
            $this->setCurrentMenuItemFromUri($menu,$this->getCurrentMenuItemUri());
        }
        elseif($this->currentItemRouteName ) {
            $this->setCurrentMenuItem($menu, $this->currentItemRouteName);
        }
        return $menu;
    }

    private function addProjectsMenu($menu) {
        $projects = $this->em->getRepository('SlxGitMinerBundle:Project')->findBy(array('published'=>1));
        if( $projects ) {
            $eMenu = $menu->addChild('Projects');
            foreach($projects as $project) {
                $eMenu->addChild(
                    $project->getPname(), 
                    array(
                        'route' => 'public_project_results',
                        'routeParameters' => array('id'=>$project->getId())
                        )
                    );
            }
        }
    }
}