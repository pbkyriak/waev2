<?php

namespace Slx\MathBundle\Analyser;

/**
 * Finds local minima of a dataset
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DataMaxima
{

    private $colname;
    
    public function getMaximaPoints($data, $colname, $maximaCount) {
        $this->colname = $colname;
        $tmaxima = $this->getBestPoints($this->findMaxima($data), $maximaCount);
        return $tmaxima;
    }
    
    private function smooth($data, $i) {
        $l = ( $i-1<0 ) ? $data[0][$this->colname] : $data[$i-1][$this->colname];
        $r = ($i+1>=count($data)) ? $data[$i][$this->colname] : $data[$i+1][$this->colname];
        $smoo = ($l+2*$data[$i][$this->colname]+$r)/4;
        return $smoo;
    }
    
    private function findMaxima($data) {
        $out = array();
        $goUp = 1;
        $prev = $data[0][$this->colname];
        for($i=0; $i<count($data); $i++ ) {
            $cur = $this->smooth($data, $i);
            if( $goUp ) {
                if( $prev>$cur && $i>0 ) {
                    $out[] = $data[$i-1];
                    $goUp = 0;
                }
            }
            else {
                if( $prev<$cur ) {
                    $goUp=1;
                }
            }
            $prev=$cur;
        }
        return $out;
    }
    
    private function getBestPoints($maxima, $maxCount) {
        // sort maxima in descending order
        usort( 
            $maxima, 
            function($a,$b) { 
                if($a[$this->colname]==$b[$this->colname]) 
                    return 0; 
                return $a[$this->colname]>$b[$this->colname] ? -1 : 1;
            }
        );
        // return $maxCount largest maxima
        return array_slice($maxima, 0, $maxCount);
    }
}
