<?php

namespace Slx\MathBundle\Analyser;

/**
 * RegressionLineCalculator calculates regression (Best Fit) line.
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RegressionLineCalculator
{

    private $data;
    private $slope=0;
    private $intercept=0;
    
    public function setData($inData) {
        $this->data = $inData;
        $this->slope=0;
        $this->intercept=0;
        $this->calculate();
    }
    
    private function calculate() {
        $sumX=0;
        $sumXY=0;
        $sumX2=0;
        $sumY=0;
        $n = count($this->data);
        foreach($this->data as $row) {
            $sumX += $row[0];
            $sumY += $row[1];
            $sumXY += $row[0]*$row[1];
            $sumX2 += $row[0]*$row[0];
        }
        $this->slope = ($n*$sumXY - $sumX*$sumY)/($n*$sumX2-$sumX*$sumX);
        $this->intercept = ($sumY-$this->slope*$sumX)/$n;
    }
    
    public function getSlope() {
        return $this->slope;
    }
    
    public function getIntercept() {
        return $this->intercept;
    }
    
}
