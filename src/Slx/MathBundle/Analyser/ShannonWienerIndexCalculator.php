<?php

namespace Slx\MathBundle\Analyser;

/**
 * Calculates Shannon-Wiener diversity Index
 * 
 * Input an two column array, 1st column is spieces and 2nd is population
 * eg array( array('sp1', 1000), array('sp2', 1000))
 * 
 * reference: Krebs C.J. 1999 Ecological Methodology 2nd ed Addison-Wesley Educational Publishers, Inc.
 *          see krebs_chapter_13_2014.pdf downloaded from http://www.zoology.ubc.ca/~krebs/books.html on 25-12-2015
 *
 * example usage:
 * 
 * $calc = new ShannonWienerIndexCalculator();
 * $index = $calc->setData($data, 0, 1)->getIndex();
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 1 Ιαν 2016
 */
class ShannonWienerIndexCalculator {
        
    private $data;
    private $spKey;
    private $popKey;
    private $index;
    
    /**
     * Set data to Shannon-Wiener Index calculator
     * 
     * @param array $data               data array
     * @param string|integer $spKey     array index of spieces
     * @param string|integer $popKey    array index of population
     * @return \Slx\MathBundle\Analyser\ShannonWienerIndexCalculator
     */
    public function setData($data, $spKey, $popKey) {
        $this->data=$data;
        $this->spKey = $spKey;
        $this->popKey = $popKey;
        $this->calculate();
        return $this;
    }
    
    /**
     * Does the actual calculation
     * 
     * index = - Sum( pi * ln(pi) ) where pi=population(i)/total population
     * 
     */
    private function calculate() {
        $total = 0;
        array_walk($this->data, function($item) use (&$total) { $total+=$item[$this->popKey]; });
        if( $total==0 ) {   // no population
            $this->index=-1;
            return;
        }
        $perc = array();
        $h=0;
        array_walk(
                $this->data, 
                function($item) use ($total, &$h) { 
                    $p = $item[$this->popKey]/$total;
                    $h += $p * log($p);                    
                }
            );
        $this->index = -$h;
    }
    
    public function getIndex() {
        return $this->index;
    }
}
