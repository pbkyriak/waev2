<?php

namespace Slx\MetricsBundle\Tests;

use Slx\MathBundle\Analyser\ShannonWienerIndexCalculator;

/**
 * Description of callsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class shannonTest extends \PHPUnit_Framework_TestCase {
   
   /**
    * Equal distributed population
    */
   public function test1() {
       $data = array();
       $data[] = ['sp1', 10];
       $data[] = ['sp2', 10];
       $data[] = ['sp3', 10];
       $data[] = ['sp4', 10];
       $data[] = ['sp5', 10];
       $calc = new ShannonWienerIndexCalculator();
       $index = $calc->setData($data, 0, 1)->getIndex();
       $this->assertEquals(1.609437912,round($index,9));
   }
   
   public function test2() {
       $data = array();
       $data[] = ['sp1', 1];
       $data[] = ['sp2', 2];
       $data[] = ['sp3', 3];
       $data[] = ['sp4', 4];
       $data[] = ['sp5', 5];
       $calc = new ShannonWienerIndexCalculator();
       $index = $calc->setData($data, 0, 1)->getIndex();
       $this->assertEquals(1.489750319,round($index,9));
   }

   /**
    * no population
    */
   public function test3() {
       $data = array();
       $data[] = ['sp1', 0];
       $data[] = ['sp2', 0];
       $data[] = ['sp3', 0];
       $data[] = ['sp4', 0];
       $data[] = ['sp5', 0];
       $calc = new ShannonWienerIndexCalculator();
       $index = $calc->setData($data, 0, 1)->getIndex();
       $this->assertEquals(-1,round($index,9));
   }

}
