<?php

namespace Slx\MetricsBundle\Tests;

use Slx\MathBundle\Analyser\ShannonWienerIndexCalculator;
/**
 * Description of stopwatchTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 2, 2016
 */
class stopwatchTest extends \PHPUnit_Framework_TestCase {
    
    public function test1 () {
        $timer = new \Slx\MathBundle\Util\StopWatch();
        
        $timer->start('timer1');
        sleep(4);
        $timer->stop('timer1');
        $timer->start('timer2');
        sleep(4);
        $timer->stop('timer2');
        $timer->start('timer1');
        sleep(4);
        $timer->stop('timer1');
        print_r($timer->getTimers());
    }
}
