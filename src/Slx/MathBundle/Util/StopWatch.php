<?php
namespace Slx\MathBundle\Util;

/**
 * Description of StopWatch
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 2, 2016
 */
class StopWatch {

    private $timers;
    private $startTimes;
    
    public function __construct() {
        $this->timers = [];
        $this->startTimes = [];
        
    }
    
    public function start($timer) {
        if( !isset($this->timers[$timer]) ) {
            $this->timers[$timer] = 0;
        }
        $this->startTimes[$timer] = time();
    }
    
    public function stop($timer) {
        if( isset($this->startTimes[$timer]) ) {
            if( isset($this->timers[$timer]) ) {
                $this->timers[$timer] += time()-$this->startTimes[$timer];
            }
        }
    }
    
    public function getTimers() {
        return $this->timers;
    }
    
    public function reset($timer='') {
        if( $timer ) {
            if( isset($this->timers[$timer]) ) {
                $this->timers[$timer]=0;
            }
            if( isset($this->startTimes[$timer]) ) {
                unset($this->startTimes[$timer]);
            }            
        }
        else {
            $this->timers = [];
            $this->startTimes = [];
        }
    }
}
