<?php

namespace Slx\MediaBundle\Lib;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Slx\MediaBundle\Lib\MediaRepository;
/**
 * Description of MediaImage
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class MediaImage
{

    protected $validExtensions = array('jpg', 'jpeg', 'png', 'gif');
    private $errorMsg = '';
    private $maxImageSize = 900000;
    private $uploadRootDir = '';
    
    public function __construct()
    {
        $repo = new MediaRepository();
        $this->uploadRootDir = $repo->getUploadRootDir();
    }

    public function getUploadRootDir()
    {
        return $this->uploadRootDir;
    }

    public function getError() {
        return $this->errorMsg;
    }
    public function isMediaValid(UploadedFile $image)
    {
        if ($this->hasUploadSuccess($image)) {
            if ($this->hasUploadValidFileType($image)) {
                if ($this->hasUploadValidSize($image)) {
                    return true;
                }
            }
        }
        return false;
    }

    private function hasUploadSuccess(UploadedFile $image) {
        if( $image->getError()) {
            $this->errorMsg = 'media.edit.uploadError';
            return false;
        }
        return true;
    }
    
    private function hasUploadValidFileType(UploadedFile $image)
    {
        $ext = $image->getClientOriginalExtension();
        if (!in_array(strtolower($ext), $this->validExtensions)) {
            $this->errorMsg = 'media.edit.notSupportedFileType '.$ext;
            return false;
        }
        return true;
    }

    private function hasUploadValidSize(UploadedFile $image)
    {
        if ($image->getSize() > $this->maxImageSize) {
            $this->errorMsg = 'media.edit.fileTooBig';
            return false;
        }
        return true;
    }

    /**
     * @todo Αν υπάρχει αρχείο με το ιδιο όνομα να το κάνει ονομα-1 , ονομα-2 κλπ όπως στο paste sta windows. 
     * @todo Αν το όνομα του αρχείο έχει ελληνικά να τα κάνει greekglish
     * 
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $image
     */
    public function moveToUploadedMedia(UploadedFile $image)
    {
        $fname = $image->getClientOriginalName();
        $image->move($this->getUploadRootDir(), $fname);
    }

}
