<?php

namespace Slx\MediaBundle\Lib;

use Symfony\Component\Finder\Finder;

/**
 * Description of MediaRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class MediaRepository
{
    private $uploadRootDir;
    private $relativeDir = 'uploads/media';
    
    public function __construct()
    {
        $this->uploadRootDir = realpath(__DIR__ . '/../../../../web/') . DIRECTORY_SEPARATOR . $this->relativeDir;
        $this->checkMediaFolder();
    }
    
    public function getUploadRootDir()
    {
        return $this->uploadRootDir;
    }

    protected function checkMediaFolder()
    {
        $folder = $this->getUploadRootDir();
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }
    }
    
    public function getImageList() {
        $finder = new Finder();
        $finder->in($this->getUploadRootDir());
        $files = array();
        foreach ($finder as $file) {
            $files[] = array(
                'name'=> str_replace($this->getUploadRootDir().DIRECTORY_SEPARATOR, '', $file->getRealpath()),
                'asset'=>  $this->relativeDir . DIRECTORY_SEPARATOR . str_replace($this->getUploadRootDir(), '', $file->getRealpath())
                );
        }
        return $files;
    }
    
    public function deleteMedia($name) {
        $out = false;
        $fpName = $this->getUploadRootDir().DIRECTORY_SEPARATOR.$name;
        if( file_exists($fpName) ) {
            if( @unlink($fpName) ) {
                $out = true;
            }
        }
        return $out;
    }
    
}
