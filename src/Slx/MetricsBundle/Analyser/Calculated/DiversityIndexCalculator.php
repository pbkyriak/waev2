<?php

namespace Slx\MetricsBundle\Analyser\Calculated;

use Doctrine\ORM\EntityManager;
use Slx\MathBundle\Analyser\ShannonWienerIndexCalculator;

/**
 * Calculates for the give tag_id Shannon-Wiener diversity index and species richness
 * Writes metric_names SWH:MGI:xx amd SWS:MGI records.
 * Currently used with metric_group_id:
 *      - 202 for method calls (2) 
 *      - 203 for new object (3)
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 2 Ιαν 2016
 */
class DiversityIndexCalculator {
    
    /** @var Doctrine\ORM\EntityManager */
    private $em;
    private $targetMetricGroup;
    private $metricGroupId;
    private $tagId;
    private $metricNameFilter='';
    protected $alias = '';
    
    public function __construct($em, $targetMetricGroup) {
        $this->em=$em;
        $this->targetMetricGroup = $targetMetricGroup;
    }
    
    public function setMetricGroupId($metricGroupId) {
        $this->metricGroupId = $metricGroupId;
    }
    
    public function getMetricGroupId() {
        return $this->metricGroupId;
    }
    
    public function setMetricNameFilter($f) {
        $this->metricNameFilter = $f;
    }
    public function setUpdateMembersMembers($v) {
        
    }
    public function setAlias($v) {
        $this->alias = $v;
    }

    public function calculate($tagId) {
        $this->tagId = $tagId;
        $data = $this->getData();
        $calc = new ShannonWienerIndexCalculator();
        $swIndex = $calc->setData($data, 'mm', 'mmetric')->getIndex();
        $metrics = [
            'SWH:MGI:'.$this->targetMetricGroup => $swIndex, 
            'SWS:MGI:'.$this->targetMetricGroup =>count($data)
                ];
        return $metrics;
    }
    
    private function getData() {
        $sql = "
                select 
		substring_index(pm.metric_name,':',-2) as mm,
		sum(pm.metric) as mmetric
                from project_metric as pm 
                where pm.project_tag_id=:tId and pm.metric_group_id=:mgId and ctype='tag' 
                group by mm
                order by mm            
            ";
        if( $this->metricNameFilter ) {
            $sql .= sprintf(" AND (%s) ", $this->metricNameFilter);
        }
        $params = array('tId'=>$this->tagId, 'mgId'=>$this->targetMetricGroup);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }
}
