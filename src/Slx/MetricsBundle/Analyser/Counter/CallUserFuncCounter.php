<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of CallUserFuncCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 24 Δεκ 2015
 */
class CallUserFuncCounter extends AbstractContextAwareCounter {

    const METRIC_PREFIX = 'MC';
    
    private $funcs = ['CUF'=>'call_user_func', 'CUFA' => 'call_user_func_array', 'FSC'=>'forward_static_call', 'FSCA' => 'forward_static_call_array'];
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);
        $this->metricGroupId = 2;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter node class=%s\n", get_class($node));
        switch($node->getType()) {
            case ParserNodeTypes::NodeExprFuncCall:
                $fkey = array_search($node->name, $this->funcs);
                if( $fkey ) {
                    if( count($node->args) ) {
                        $firstArg = $node->args[0];
                        if( $firstArg->value->getType()==ParserNodeTypes::NodeExprArray ) {
                            //var_dump($firstArg->value);
                            $subKey = $this->visitCufFirstArg($firstArg->value);
                            if( $subKey ) {
                                $key = sprintf('%s:%s:%s',self::METRIC_PREFIX, $fkey,$subKey);
                                $this->incMetric($key, 1, false);
                            }
                        }
                    }
                }
                break;
        }
    }

    protected function visitCufFirstArg(PHPParser_Node $node ) {
        $items = array();
        $out = false;
        foreach($node->items as $inode) {
            if($inode->getType()=='Expr_ArrayItem') {
                $items[] = $this->getMethodNameType($inode->value);
            }
        }
        if( count($items)==2 ) {
            $out = implode(':', $items);
        }
        return $out;
    }
    
    private function getMethodNameType($nameNode) {
        $out = 'UNDEFINED';
        if(is_object($nameNode) ) {
            $nameType = $nameNode->getType();
            if( $nameType==ParserNodeTypes::NodeExprVariable ) {
                $nameNameType = is_object($nameNode->name) ? $nameType.'|'.$nameNode->name->getType() : $nameType;
                $out = $nameNameType;
            }
            else {
                $out = $nameType;
            }
        }
        return $out;
    }
    
    protected function ccLeaveNode(PHPParser_Node $node) {
        
    }

    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }
}
