<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of CcnCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 2 Μαρ 2016
 */
class CcnCounter extends AbstractContextAwareCounter {
    
    const METRIC = 'ccn';
    const METRIC2 = 'ccn2';

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);

        $this->metricGroupId = 1;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        $this->calcCCN($node);
    }

    protected function ccLeaveNode(PHPParser_Node $node) {  
        
    }
    
    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

    private function calcCCN($node) {
        // calculating ccn
        switch ($node->getType()) {
            case ParserNodeTypes::NodeStmtClassMethod:
            case ParserNodeTypes::NodeStmtFunction:
            case ParserNodeTypes::NodeStmtIf:
            case ParserNodeTypes::NodeStmtElseIf:
            case ParserNodeTypes::NodeStmtFor:
            case ParserNodeTypes::NodeStmtForeach:
            case ParserNodeTypes::NodeStmtCase:
            case ParserNodeTypes::NodeStmtWhile:
            case ParserNodeTypes::NodeStmtDo:
            case ParserNodeTypes::NodeExprTernary:
            case ParserNodeTypes::NodeStmtCatch:
                $this->incMetric(self::METRIC);
                $this->incMetric(self::METRIC2);
                break;
            case ParserNodeTypes::NodeExprBooleanAnd:
            //case 'PHPParser_Node_Expr_BooleanNot':
            case ParserNodeTypes::NodeExprBooleanOr:
            //case 'PHPParser_Node_Expr_LogicalXor':
            case ParserNodeTypes::NodeExprLogicalOr:
            case ParserNodeTypes::NodeExprLogicalAnd:
                $this->incMetric(self::METRIC2);
                break;
        }
    }

}
