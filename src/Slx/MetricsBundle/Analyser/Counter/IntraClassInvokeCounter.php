<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;
/**
 * Counts method invocation patters with class part EXPR_VARIABLE 
 * that are direct intra-class meaning that object (EXPR_VARIABLE) is $this
 *
 * receiver is the object (receives the calls)
 * calls as method calls to an object
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class IntraClassInvokeCounter extends AbstractContextAwareCounter {

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);
        $this->metricGroupId = 4;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
//        printf("enter node class=%s\n", get_class($node));
        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprMethodCall:
                if( $this->isCallOnThisObject($node) ) {
                    $key = $this->getCallDescr('IC', $node->var, $node->name);
                    $this->incMetric($key, 1, false);
                }
                break;
        }
    }

    private function isCallOnThisObject(PHPParser_Node $node) {
        // check for calls in $this variable
        $out = false;
        if ( $node->var->getType() == ParserNodeTypes::NodeExprVariable ) {
            if (is_scalar($node->var->name)) {
                if ($node->var->name == 'this') {
                    $out = true;
                }
            }
        }
        return $out;
    }

    private function getCallDescr($key, $varNode, $nameNode) {
        $var = $this->getMethodNameType($varNode);
        $method = $this->getMethodNameType($nameNode);
        return str_replace('PHPParser_Node_','',sprintf("%s:%s:%s",$key,$var,$method));
        //return sprintf("%s:%s:%s", $key, $var, $method);
    }

    /**
     * If node is object means that is not a method name, but it might be a variable
     * if node is expr variable we want to know its type
     * else return that type
     * 
     * @param PHPParser_Node|string $nameNode
     * @return string
     */
    private function getMethodNameType($nameNode) {

        $out = 'METHOD';
        if (is_object($nameNode)) {
            $nameType = $nameNode->getType();
            if ($nameType == ParserNodeTypes::NodeExprVariable ) {
                $nameNameType = is_object($nameNode->name) ? $nameType . '|' . $nameNode->name->getType() : $nameType;
                $out = $nameNameType;
            } else {
                $out = $nameType;
            }
        }
        return $out;
    }

    
    protected function ccLeaveNode(PHPParser_Node $node) {  
        
    }
    
    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }
}
