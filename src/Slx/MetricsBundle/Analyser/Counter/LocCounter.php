<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of LocCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 2 Μαρ 2016
 */
class LocCounter extends AbstractContextAwareCounter {

    const METRIC = 'loc';
    const METRIC2 = 'cloc';

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);

        $this->metricGroupId = 1;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        $clazz = $node->getType();
        //printf("%s\n", $clazz);
        $loc = $this->calcLoc($node);
        switch ($clazz) {
            case ParserNodeTypes::NodeStmtInterface:
            case ParserNodeTypes::NodeStmtClass:
            case ParserNodeTypes::NodeStmtTrait:
            case ParserNodeTypes::NodeStmtFunction:
                $this->incMetric(self::METRIC, $loc, false);
                break;
            case ParserNodeTypes::NodeStmtClassMethod:
                $this->incMetric(self::METRIC, $loc);
                break;
            default:
                // if file level context and node is a statement then count global loc
                if ($this->metricContext && $this->metricContext->getCType() == 'file' && !$this->lastNode->getLastNode()) {
                    $this->incMetric(self::METRIC, $loc, true);
                }
        }
        if (ParserNodeTypes::isStmtType($clazz)) {
            $this->countComments($node);
        }
    }

    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
        
    }

    private function calcLoc(PHPParser_Node $node) {
        $loc = 0;
        //if (ParserNodeTypes::isStmtType($node->getType())) {
            $startLine = $node->getAttribute('startLine');
            $loc = $node->getAttribute('endLine') - $startLine + 1;
        //}
        return $loc;
    }

    private function countComments(PHPParser_Node $node) {
        $comments = $node->getAttribute('comments');
        if ($comments) {
            foreach ($comments as $comment) {
                $this->incMetric(self::METRIC2, count(array_filter(explode(PHP_EOL, $comment->getText()))));
            }
        }
    }

}
