<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of CallsCounter
 *
 * receiver is the object (receives the calls)
 * calls as method calls to an object
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MethInvokeCounter extends AbstractContextAwareCounter {

    const METRIC_PREFIX_STATIC = 'MC:DC';   // MethodCall:DoubleColon
    const METRIC_PREFIX_OBJECT = 'MC:OP';   // MethodCall:ObjectOperator

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);
        $this->metricGroupId = 2;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter node class=%s\n", get_class($node));
        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprMethodCall:
                $key = $this->getCallDescr(self::METRIC_PREFIX_OBJECT, $node->var, $node->name);
                $this->incMetric($key, 1, false);
                break;
            case ParserNodeTypes::NodeExprStaticCall:
                $key = $this->getCallDescr(self::METRIC_PREFIX_STATIC, $node->class, $node->name);
                $this->incMetric($key, 1, false);
                break;
        }
    }

    private function getCallDescr($key, $varNode, $nameNode) {
        $var = $this->getMethodNameType($varNode);
        $method = $this->getMethodNameType($nameNode);
        return str_replace('PHPParser_Node_','',sprintf("%s:%s:%s",$key,$var,$method));
        //return sprintf("%s:%s:%s", $key, $var, $method);
    }

    /**
     * If node is object means that is not a method name, but it might be a variable
     * if node is expr variable we want to know its type
     * else return that type
     * 
     * @param PHPParser_Node|string $nameNode
     * @return string
     */
    private function getMethodNameType($nameNode) {

        $out = 'METHOD';
        if (is_object($nameNode)) {
            $nameType = $nameNode->getType();
            if ($nameType == ParserNodeTypes::NodeExprVariable) {
                $nameNameType = is_object($nameNode->name) ? $nameType . '|' . $nameNode->name->getType() : $nameType;
                $out = $nameNameType;
            } else {
                $out = $nameType;
            }
        }
        return $out;
    }
    

    protected function ccLeaveNode(PHPParser_Node $node) {  
        
    }
    
    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

}
