<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * counts Classes, Interfaces, Traits, methods, functions
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 20 Dec 2014
 */
class NodeCounter  extends AbstractContextAwareCounter {

    const METRIC1 = 'noc';
    const METRIC2 = 'noi';
    const METRIC3 = 'not';
    const METRIC4 = 'nom';
    const METRIC5 = 'nof';

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);

        $this->metricGroupId = 1;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        $this->calc($node);
    }

    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
        
    }

    private function calc($node) {
        switch ($node->getType()) {
            case ParserNodeTypes::NodeStmtInterface:
                $this->incMetric(self::METRIC2);
                break;
            case ParserNodeTypes::NodeStmtClass:
                $this->incMetric(self::METRIC1);
                break;
            case ParserNodeTypes::NodeStmtTrait:
                $this->incMetric(self::METRIC3);
                break;
            case ParserNodeTypes::NodeStmtFunction:
                $this->incMetric(self::METRIC5);
                break;
            case ParserNodeTypes::NodeStmtClassMethod:
                $this->incMetric(self::METRIC4);
                break;
        }
    }

}
