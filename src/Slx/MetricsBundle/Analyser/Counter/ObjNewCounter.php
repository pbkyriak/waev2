<?php

namespace Slx\MetricsBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of ObjNewCounter
 *
 * receiver is the object (receives the calls)
 * calls as method calls to an object
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ObjNewCounter extends AbstractContextAwareCounter {

    const METRIC_PREFIX_NEW = 'OI:NEW'; 
    const METRIC_PREFIX_CAST = 'OI:CAST';

    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm, false);
        $this->metricGroupId = 3;
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter node class=%s\n", get_class($node));
        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprNew:
                $key = $this->getCallDescr(self::METRIC_PREFIX_NEW, $node->class);
                $this->incMetric($key, 1, false);
                break;
            case ParserNodeTypes::NodeExprCastObject:
                //printf("enter node class=%s\n", get_class($node));
                $key = $this->getCallDescr(self::METRIC_PREFIX_CAST, $node->expr);
                $this->incMetric($key, 1, false);
                break;
        }
    }

    private function getCallDescr($key,$node) {
        $var = $this->getMethodNameType($node);
        return str_replace('PHPParser_Node_','',sprintf("%s:%s",$key,$var));
        //return sprintf("%s:%s:%s",$key,$var,$method);
    }
    
    /**
     * If node is object means that is not a method name, but it might be a variable
     * if node is expr variable we want to know its type
     * else return that type
     * 
     * @param PHPParser_Node|string $nameNode
     * @return string
     */
    private function getMethodNameType($nameNode) {
        $out = 'METHOD';
        if(is_object($nameNode) ) {
            $nameType = $nameNode->getType();
            if( $nameType==ParserNodeTypes::NodeExprVariable ) {
                $nameNameType = is_object($nameNode->name) ? $nameType.'|'.$nameNode->name->getType() : $nameType;
                $out = $nameNameType;
            }
            else {
                $out = $nameType;
            }
        }
        return $out;
    }
    
    protected function ccLeaveNode(PHPParser_Node $node) {  
        
    }
    
    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

}
