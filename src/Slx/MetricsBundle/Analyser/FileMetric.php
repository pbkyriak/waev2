<?php
namespace Slx\MetricsBundle\Analyser;
/**
 * Description of ClassMetric
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 20 Dec 2014
 */
class FileMetric {
    private $metrics = array();
    private $ctype = '';
    private $name = '';
    private $members = array();
    private $parent = null;
    private $metricCounter = array();
    
    public function __construct($ctype, $name) {
        $this->ctype = $ctype;
        $this->name = $name;
    }
    
    public function addMember($name, FileMetric $member) {
        if( isset($this->members[$name]) ) {
            return $this->members[$name];
        }
        else {
            $member->setParent($this);
            $this->members[$name] = $member;
            return $member;
        }
    }
    
    private function updaterMetricCounter($metric, $counter) {
        $this->metricCounter[$metric] = $counter;
    }
    public function getMetricCounter($metric) {
        $out = false;
        if( iset($this->metricCounter[$metric])) {
            $out = $this->metricCounter[$metric];
        }
        return $out;
    }
    public function getMetricCounters() {
        return $this->metricCounter;
    }
    public function setMetric($metric, $v, $counter) {
        $this->metrics[$metric] = $v;
        $this->updaterMetricCounter($metric, $counter);
    }
    
    public function incMetric($metric, $v, $updParent, $counter) {
        $this->updaterMetricCounter($metric, $counter);
        $this->metrics[$metric] = $this->getMetric($metric) + $v;
        if( $updParent && $this->getParent() ) {
            $this->getParent()->incMetric($metric, $v, $updParent, $counter);
        }
    }
    
    public function getMetric($metric) {
        if( isset($this->metrics[$metric]) ) {
            return $this->metrics[$metric];
        }
        else {
            return 0;
        }
    }
    
    public function getMetrics() {
        return $this->metrics;
    }
    
    public function getCType() {
        return $this->ctype;
    }
    
    public function getName() {
        return $this->name;
    }
    
    public function getMembers() {
        return $this->members;
    }
    
    public function setParent($v) {
        $this->parent = $v;
    }
    
    public function getParent() {
        return $this->parent;
    }
    /**
     * 
     * @param string $name
     * @return FileMetric|boolean
     */
    public function getMember($name) {
        if( isset($this->members[$name]) ) {
            return $this->members[$name];
        }
        else {
            return false;
        }
    }
    
    public function free() {
        foreach($this->members as $k => $v) {
            $v->free();
            $this->members[$k]=null;
        }
        $this->parent = null;
        $this->metrics=null;
    }
    
    public function __toString() {
        return sprintf("%s %s", $this->ctype, $this->name);
    }
}
