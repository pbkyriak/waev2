<?php

namespace Slx\MetricsBundle\Analyser;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of MetricsFactory
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 20 Δεκ 2015
 */
class MetricsFactory {

    /** @var ContainerInterface */
    private $container;
    private $taskDefs;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->taskDefs = array();
    }

    public function addCounterDef($serviceId, $alias, $metricGroupId, $active, $updateMemberMembers) {
        $this->taskDefs[$alias] = array(
            'id' => $serviceId, 
            'metric_group_id'=> $metricGroupId,
            'active' => $active,
            'update_member_members' => $updateMemberMembers,
            'alias' => $alias,
            );
    }

    public function getDefinitions() {
        return $this->taskDefs;
    }
    /**
     * Creates an object instance of the counter classes
     *
     * @param  string $className
     * 
     */
    public function getObject($alias) {
        $out = null;
        if (isset($this->taskDefs[$alias])) {
            $out = $this->container->get($this->taskDefs[$alias]['id']);
            $out->setUpdateMembersMembers($this->taskDefs[$alias]['update_member_members']);
            $out->setAlias($alias);
        }
        return $out;
    }

}
