<?php

namespace Slx\MetricsBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;

/**
 * Description of CalcTagMetricsTask
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 2 Ιαν 2016
 */
class CalcTagMetricsTask  extends AbstractTagTask {

    private $metricGroupIds = array();
    private $counterFactory;
    private $counterDefs;
    private $calculators;
    
    public function setCounterFactory($factory) {
        $this->counterFactory = $factory;
    }
    
    public function addMetricGroupId($metricGroupId) {
        if( !isset($this->metricGroupIds[$metricGroupId]) ) {
            $this->metricGroupIds[] = $metricGroupId;
        }
    }
    
    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->getCounterDefs();
        if( count($this->counterDefs)==0 ) {
            printf("No counters active for group %s \n", implode(',',$this->metricGroupIds));
            return true;
        }
        try {
            $this->setupCalculators();
            $this->cleanExistingData($this->tagId);
            $this->process($this->tagId);
        }
        catch(\Exception $ex) {
            printf("Error: %s\n", $ex->getMessage());
        }        
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;
        return true;
    }

    public function getTaskName() {
        return 'Calculate method invocation metrics';
    }

    public function onFail() {
        return true;
    }

    private function cleanExistingData($tagId) {
        foreach($this->metricGroupIds as $metricGroupId) {
            $this->em->createQuery("DELETE FROM SlxMetricsBundle:ProjectMetric a WHERE a.projectTag=:tagid and a.metric_group_id=:mgid ")
                ->setParameter('tagid', $tagId)
                ->setParameter('mgid', $metricGroupId)
                ->execute();
        }

    }
    
    private function process($tagId) {
        foreach($this->calculators as $c) {
            $metrics = $c->calculate($tagId);
            $this->updateTagMetrics($metrics, $c->getMetricGroupId());
        }
    }
    
    private function updateTagMetrics($metrics, $metricGroupId) {
        foreach($metrics as $mk => $mv) {
            $data = array(
                'project_tag_id' => $this->tag->getId(),
                'ctype' => 'tag',
                'cname' => 'tag',
                'metric_name' => $mk,
                'metric' => $mv,
                'metric_group_id' => $metricGroupId,
            );
            $this->em->getConnection()->insert('project_metric', $data);                
        }
    }

    private function getCounterDefs() {
        $this->counterDefs = array();
        $this->updateMemberMembers = false;
        foreach($this->counterFactory->getDefinitions() as $alias => $def) {
            if( in_array($def['metric_group_id'],$this->metricGroupIds) && $def['active']) {
                printf("Counter: %s\n", $alias);
                $this->counterDefs[] = $def;
            }
        }
        return count($this->counterDefs);
    }
    
    private function setupCalculators() {
        $this->calculators = array();
        foreach($this->counterDefs as $def) {
            $this->calculators[] = $this->counterFactory->getObject($def['alias']);
        }
    }
}


// ALTER TABLE `waev`.`project_metric` CHANGE COLUMN `metric` `metric` DOUBLE NOT NULL ;