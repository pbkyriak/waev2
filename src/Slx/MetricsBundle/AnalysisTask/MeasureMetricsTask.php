<?php

namespace Slx\MetricsBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\ReflectionBundle\Reflection\Traverser;
use Slx\MetricsBundle\Analyser\FileMetric;
/**
 * Description of MethInvokeMetricsTask
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 3 Dec 2015
 * @since 2 Mar 2016
 */
class MeasureMetricsTask  extends AbstractTagTask {

    private $tagMetrics;
    private $metricGroupIds = array();
    private $updateMemberMembers = array();
    private $counterFactory;
    private $counterDefs;
    private $tagMetricCounters = array();
    private $dbTime=0;
    private $fhandle;
    private $rootDir=null;
    
    public function setCounterFactory($factory) {
        $this->counterFactory = $factory;
    }
    
    public function addMetricGroupId($metricGroupId) {
        if( !isset($this->metricGroupIds[$metricGroupId]) ) {
            $this->metricGroupIds[] = $metricGroupId;
        }
    }
    
    public function setRootDir($dir) {
        $this->rootDir = $dir;
    }
    
    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->getCounterDefs();
        if( count($this->counterDefs)==0 ) {
            printf("No counters active for groups %s \n", implode(',',$this->metricGroupIds));
            return true;
        }
        if( !$this->rootDir ) {
            printf("Root dir not set \n");
            return true;
        } 
        $this->tagMetrics = array();
        try {
            $datafile = tempnam($this->rootDir."/web/uploads/tmp/", 'metrics');
            printf("output file:%s\n",$datafile);
            $this->fhandle = fopen($datafile, "w");
            if($this->fhandle ) {
                $fileList = $this->getFileList();
                foreach($fileList as $file) {
                    $this->processFile($file);
                }
                $time1= time();
                $this->updateTagMetrics();
                fclose($this->fhandle);
                $this->importData($datafile);
            }
            else {
                printf("failed to use tmp file\n");
            }
        }
        catch(\Exception $ex) {
            printf("Error: %s\n", $ex->getMessage());
        }        
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;
        printf("db time=%s \n", $this->dbTime);
        return true;
    }

    public function getTaskName() {
        return 'Calculate method invocation metrics';
    }

    public function onFail() {
        return true;
    }

    private function cleanExistingData($tagId) {
        foreach($this->metricGroupIds as $metricGroupId) {
            $this->em->createQuery("DELETE FROM SlxMetricsBundle:ProjectMetric a WHERE a.projectTag=:tagid and a.metric_group_id=:mgid ")
                ->setParameter('tagid', $tagId)
                ->setParameter('mgid', $metricGroupId)
                ->execute();
        }
        $this->em->flush();
    }
    /**
     * 
     * @param array $file   expects to have $file['id'] and $file['fname']
     * @return boolean
     */
    private function processFile($file) {
        $ast = $this->scHelper->getFileAst($file);
        if( $ast ) {
            $traverser = new Traverser();
            $fm = new FileMetric('file', $file['fname']);
            $this->setupTraverser($fm, $traverser);
            $traverser->traverseAst($ast);
            try {
                $time1 = time();
                $this->updateFileMetrics($fm);
                $this->dbTime += (time()-$time1);
                $this->addToTagMetrics($fm);
            }
            catch(\Exception $ex) {
                printf("Exception: %s\n", $ex->getMessage());
                printf("Processing file: %s\n", $file['fname']);
            }
            $ast = null;
            $fm->free();
            $fm=null;
            gc_collect_cycles();
            time_nanosleep(0, 10000000);
            return true;
        }
        return false;
    }

    private function setupTraverser($fm, $traverser) {
        foreach($this->counterDefs as $def) {
            $counter = $this->counterFactory->getObject($def['alias']);
            $counter->configure($fm);
            $traverser->addVisitor($counter);
        }
    }
    
    private function getFileList() {
        $list = $this->scHelper->getTagSourceFilesDB($this->em, true, ['test']);
        return $list;
    }
    
    private function updateFileMetrics(\Slx\MetricsBundle\Analyser\FileMetric $fm) {
        $fname = str_replace($this->tag->getSourceCodePath(), '', $fm->getName());
        $counters = $fm->getMetricCounters();
        $this->tagMetricCounters = array_merge($this->tagMetricCounters, $counters);
        $this->updateMetric('file', $fname, $fm->getMetrics(), false,$counters);
        $qCnt=0;
        foreach($fm->getMembers() as $m) {
            $counters = $m->getMetricCounters();            
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics(),false, $counters);
            if($m->getMembers()) {
                foreach($m->getMembers() as $mm) {
                    $counters = $mm->getMetricCounters();
                    $qCnt += $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics(),true,$counters);
                }
            }
        }
    }
   
    private function updateMetric($key, $name, $metrics, $checkUpdateMemberMembers, $counters) {
        $out = 0;
        foreach($metrics as $mk=>$mv) {
            $go = true;
            if( $checkUpdateMemberMembers  && !$this->updateMemberMembers[$counters[$mk]]) {
                $go = false;
            }
            if( $go ) {
                //printf("metric=%s alias=%s def=%s\n", $mk, $counters[$mk],$this->counterDefs[$counters[$mk]]['metric_group_id']);
                $data = array(
                    'project_tag_id' => $this->tag->getId(),
                    'ctype' => $key,
                    'cname' => $this->em->getConnection()->quote($name, \PDO::PARAM_STR),
                    'metric_name' => $mk,
                    'metric' => $mv,
                    'metric_group_id' => $this->counterDefs[$counters[$mk]]['metric_group_id'],
                );
                //$this->em->getConnection()->insert('project_metric', $data);                
                $data['cname'] = substr($data['cname'], 1,  strlen($data['cname'])-2);
                fputcsv($this->fhandle, $data, "\t");
                $out++;
            }
        }        
        return $out;
    }
    
    private function updateTagMetrics() {
        //var_dump($this->tagMetrics);
        foreach($this->tagMetrics as $mk => $mv) {
            $data = array(
                'project_tag_id' => $this->tag->getId(),
                'ctype' => 'tag',
                'cname' => 'tag',
                'metric_name' => $mk,
                'metric' => $mv,
                'metric_group_id' => $this->counterDefs[$this->tagMetricCounters[$mk]]['metric_group_id'],
            );
            //$this->em->getConnection()->insert('project_metric', $data);     
            $data['cname'] = substr($data['cname'], 1,  strlen($data['cname'])-2);
            fputcsv($this->fhandle, $data, "\t");
        }        
    }

    private function addToTagMetrics(\Slx\MetricsBundle\Analyser\FileMetric $fm) {
        foreach($fm->getMetrics() as $mk=>$mv) {
            if( !isset($this->tagMetrics[$mk])) {
                $this->tagMetrics[$mk] = 0;
            }
            $this->tagMetrics[$mk] += $mv;
        }
    }
    
    private function getCounterDefs() {
        $this->counterDefs = array();
        $this->updateMemberMembers = array();
        foreach($this->counterFactory->getDefinitions() as $alias => $def) {
            if( in_array($def['metric_group_id'],$this->metricGroupIds) && $def['active']) {
                printf("Found Counter: %s\n", $alias);
                $this->counterDefs[$def['alias']] = $def;
                $this->updateMemberMembers[$def['alias']] = ($def['update_member_members'] ? true : false);
            }
        }
        
        return count($this->counterDefs);
    }
    
    private function importData($datafile) {
        printf("loading data from file to database...\n");
        $q = "load data local infile '%s' ignore into table project_metric fields terminated by '\\t' optionally enclosed by '\"' (project_tag_id, ctype, cname,metric_name, metric,metric_group_id);";
        $q = sprintf($q, $datafile);        
        $time1= time();
        for($tt=0; $tt<3; $tt++) {
            try {
                $this->cleanExistingData($this->tagId);
                $this->em->getConnection()->executeQuery($q);
                break;
            }
            catch(\PDOException $ex) {
                printf("pdo error %s\n Will retry (%s) in 10 seconds.\n", $tt+1, $ex->getMessage());
                sleep(10);
            }
        }
        //unlink($datafile);
        $this->dbTime += (time()-$time1);

    }
}
