<?php

namespace Slx\MetricsBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of MethInvokeMetricsTask
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 3 Δεκ 2015
 */
class MethInvokeMetricsTask  extends AbstractTagTask {

    private $tagMetrics;
    private $metricGroupId = 0;
    private $updateMemberMembers = array();
    private $counterFactory;
    private $counterDefs;
    
    public function setCounterFactory($factory) {
        $this->counterFactory = $factory;
    }
    
    public function setMetricGroup($metricGroupId) {
        $this->metricGroupId = $metricGroupId;
    }
    
    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->getCounterDefs();
        if( count($this->counterDefs)==0 ) {
            printf("No counters active for group %s \n", $this->metricGroupId);
            return true;
        }
        $this->tagMetrics = array();
        try {
            $this->cleanExistingData($this->tagId);
            $fileList = $this->getFileList();
            foreach($fileList as $file) {
                $this->processFile($file);
            }
            $this->updateTagMetrics();
            
        }
        catch(\Exception $ex) {
            printf("Error: %s\n", $ex->getMessage());
        }        
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;
        return true;
    }

    public function getTaskName() {
        return 'Calculate method invocation metrics';
    }

    public function onFail() {
        return true;
    }

    private function cleanExistingData($tagId) {
        $this->em->createQuery("DELETE FROM SlxMetricsBundle:ProjectMetric a WHERE a.projectTag=:tagid and a.metric_group_id=:mgid ")
            ->setParameter('tagid', $tagId)
            ->setParameter('mgid', $this->metricGroupId)
            ->execute();
    }
    /**
     * 
     * @param array $file   expects to have $file['id'] and $file['fname']
     * @return boolean
     */
    private function processFile($file) {
        $ast = $this->scHelper->getFileAst($file);
        if( $ast ) {
            $traverser = new Traverser();
            $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $file['fname']);
            $this->setupTraverser($fm, $traverser);
            $traverser->traverseAst($ast);
            try {
                $this->updateFileMetrics($fm);
                $this->addToTagMetrics($fm);
            }
            catch(\Exception $ex) {
                printf("Exception: %s\n", $ex->getMessage());
                printf("Processing file: %s\n", $file['fname']);
            }
            $ast = null;
            $fm->free();
            $fm=null;
            gc_collect_cycles();
            time_nanosleep(0, 10000000);
            return true;
        }
        return false;
    }

    private function setupTraverser($fm, $traverser) {
        foreach($this->counterDefs as $def) {
            $counter = $this->counterFactory->getObject($def['alias']);
            $counter->configure($fm);
            $traverser->addVisitor($counter);
        }
    }
    
    private function getFileList() {
        $list = $this->scHelper->getTagSourceFilesDB($this->em, true, ['test']);
        return $list;
        /*
        $out = array();
        foreach($list as $item) {
            if($item['id']==3466894) {
                $out[] = $item;
            }
        }
        return $out;
         * 
         */
    }
    
    private function updateFileMetrics(\Slx\MetricsBundle\Analyser\FileMetric $fm) {
        $this->em->getConnection()->query("SET autocommit=0;"); 
        $fname = str_replace($this->tag->getSourceCodePath(), '', $fm->getName());
            //$counters = $fm->getMetricCounters();
            //var_dump($counters);printf("%s %s\n",'file', $fname);
        $this->updateMetric('file', $fname, $fm->getMetrics(), false,array());
        foreach($fm->getMembers() as $m) {
            $counters = $m->getMetricCounters();            
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics(),false, $counters);
            if($m->getMembers()) {
                foreach($m->getMembers() as $mm) {
                    $counters = $mm->getMetricCounters();
                    $this->updateMetric($mm->getCType(), sprintf("%s::%s", $m->getName(), $mm->getName()), $mm->getMetrics(),true,$counters);
                }
            }
        }
        $this->em->getConnection()->query("COMMIT;");
        $this->em->getConnection()->query("SET autocommit=1;"); 

    }
   
    private function updateMetric($key, $name, $metrics, $checkUpdateMemberMembers, $counters) {
        foreach($metrics as $mk=>$mv) {
            $go = true;
            if( $checkUpdateMemberMembers  && !$this->updateMemberMembers[$counters[$mk]]) {
                $go = false;
            }
            if( $go ) {
                $data = array(
                    'project_tag_id' => $this->tag->getId(),
                    'ctype' => $key,
                    'cname' => $name,
                    'metric_name' => $mk,
                    'metric' => $mv,
                    'metric_group_id' => $this->metricGroupId,
                );
                $this->em->getConnection()->insert('project_metric', $data);                
            }
        }        
    }
    
    private function updateTagMetrics() {
        foreach($this->tagMetrics as $mk => $mv) {
            $data = array(
                'project_tag_id' => $this->tag->getId(),
                'ctype' => 'tag',
                'cname' => 'tag',
                'metric_name' => $mk,
                'metric' => $mv,
                'metric_group_id' => $this->metricGroupId,
            );
            $this->em->getConnection()->insert('project_metric', $data);                
        }
    }

    private function addToTagMetrics(\Slx\MetricsBundle\Analyser\FileMetric $fm) {
        foreach($fm->getMetrics() as $mk=>$mv) {
            if( !isset($this->tagMetrics[$mk])) {
                $this->tagMetrics[$mk] = 0;
            }
            $this->tagMetrics[$mk] += $mv;
        }
    }
    
    private function getCounterDefs() {
        $this->counterDefs = array();
        $this->updateMemberMembers = array();
        foreach($this->counterFactory->getDefinitions() as $alias => $def) {
            if( $def['metric_group_id']==$this->metricGroupId && $def['active']) {
                printf("Found Counter: %s\n", $alias);
                $this->counterDefs[] = $def;
                $this->updateMemberMembers[$def['alias']] = ($def['update_member_members'] ? true : false);
            }
        }
        
        return count($this->counterDefs);
    }
}
