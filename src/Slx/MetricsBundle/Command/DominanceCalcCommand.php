<?php

namespace Slx\MetricsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of DominanceCalcCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 14, 2016
 */
class DominanceCalcCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    private $objPatterns;
    private $methPatterns;
    private $ambiguity;
    private $results;
    private $fresults;
    
    protected function configure()
    {
        $this
            ->setName('waev:dominance:calc')
            ->setDescription("Species Dominance calculation")
        ;
        
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->getGroupingInfo();
        //print_R($this->objPatterns);
        //print_R($this->methPatterns);
        //print_R($this->ambiguity);
        $data = $this->fetchData();
        $this->groupData($data);
        $this->sortResults();
        $this->calculateDominance();
        $this->sortFResults();
        $this->dumpResults();
    }
    
    private function getGroupingInfo() {
        $mp = $this->getContainer()->get('waev.MethPatternGroupingInfo');
        $this->objPatterns = $mp->getObjPatterns();
        $this->methPatterns = $mp->getMethPatterns();
        $this->ambiguity = $mp->getAmbiguity();
    }
    
    private function fetchData() {
        printf("Fetch data from db...\n");
        
        $sql = "select substring_index(pm.metric_name,':',-2) as metricname, sum(pm.metric) as metric, group_concat(distinct pt.project_id) as stations
                from project_metric pm
                left join project_tag pt on (pm.project_tag_id=pt.id) 
                where pm.metric_group_id=2 
                and pm.ctype='tag'
                group by metricname;";

        $data = $this->em->getConnection()->fetchAll($sql);
        printf("Fetched.\n");
        return $data;
    }
    
    private function groupData($data) {
        $this->results = array();
        $total = 0;
        $nfTotal = 0;
        foreach($data as $row) {
            list($op, $mp) = explode(':', $row['metricname']);
            if( isset($this->objPatterns[$op]) && isset($this->methPatterns[$mp])) {
                $sp = sprintf("%s:%s",$this->objPatterns[$op],$this->methPatterns[$mp]);
                if( !isset($this->results[$sp]) ) {
                    $this->results[$sp] = array(
                        'a' => sprintf("%s%s", $this->ambiguity[$this->objPatterns[$op]], $this->ambiguity[$this->methPatterns[$mp]]),
                        'sp' => sprintf("%s-%s",$this->objPatterns[$op],$this->methPatterns[$mp]),
                        'opp' => $this->objPatterns[$op],
                        'mpp' => $this->methPatterns[$mp],
                        'op'=> $op,
                        'mp'=> $mp,
                        'c' => 0,
                        'stations' => array(),
                    );
                }
                $this->results[$sp]['c'] += $row['metric'];
                $cstations = explode(',', $row['stations']);
                $this->results[$sp]['stations'] = array_unique(array_merge($this->results[$sp]['stations'], $cstations));
                $total = $total + $row['metric'];
            }
            else {
                $nfTotal = $nfTotal + $row['metric'];
                printf("NOT FOUND!! %s (%s) %s (%s)\n", 
                        $op,
                        (isset($this->objPatterns[$op]) ? 'Y' : 'N'), 
                        $mp,
                        (isset($this->methPatterns[$mp]) ? 'Y' : 'N'),
                        $row['metric']
                        );
            }
        }
        foreach($this->results as $idx => $row) {
            $this->results[$idx]['stations'] = count($row['stations']);
        }
        printf("metric total=%s\n", $total);
        printf("metric nftotal=%s\n", $nfTotal);
        printf("metric Gtotal=%s\n", $total+$nfTotal);
        
    }
    
    public function sortComp($a, $b) {
        $out = 0;
        if( $a['a']==$b['a'] ) {
            $out = strcmp($a['sp'], $b['sp']);
        }
        else {
            $out = strcmp($a['a'], $b['a']);
        }
        return $out;
    }
        
    private function sortResults() {
        usort($this->results, array($this, 'sortComp'));
    }
    
    public function sortFComp($a, $b) {
        if( $a['Y']==$b['Y'])
            return 0;
        return $a['Y']>$b['Y'] ? -1 : 1;
    }
    private function sortFResults() {
        usort($this->fresults, array($this, 'sortFComp'));
    }
    
    private function calculateDominance() {
        $N = 0;
        $stationsN = 0;
        $this->fresults = array();
        foreach($this->results as $row) {
            $N += $row['c'];
            $stationsN = max($stationsN, $row['stations']);
        }
        foreach($this->results as $row) {
            $nN = $row['c']/$N;
            $f = $row['stations']/$stationsN;
            $Y = $nN*$f;
            $this->fresults[] = array(
                'sp' => $row['sp'],
                'population' => $row['c'],
                'stations' => $row['stations'],
                'nN' => $nN,
                'f' => $f,
                'Y' => $Y,
                'D' => $Y>=0.02 ? 'Yes' : '',
            );
        }
    }
    
    private function dumpResults() {
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir');
        $fn = $rootDir.'/../web/uploads/a1516/dominance-calc.csv';
        $fidx = 1;
        $handle = fopen($fn, "w+");
        foreach($this->fresults as $row) {
            if( $fidx==1 ) {
                fputcsv($handle, array_keys($row), "\t");                        
            }
            fputcsv($handle, $row, "\t");
            $fidx++;
        }
        fclose($handle);
    }
    
}
