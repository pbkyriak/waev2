<?php

namespace Slx\MetricsBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * create data for patterns usage timeline
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 28 Μαρ 2016
 */
class SpeceisCountPerProjectCommand extends ContainerAwareCommand {
    

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    private $results;
    private $tagCount;
    protected function configure()
    {
        $this
            ->setName('waev:speciescountperproject')
            ->setDescription("Counts total appeared Method invocation species for a project")
            ->addArgument('pid', InputArgument::REQUIRED)
        ;
        
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $pId = $input->getArgument('pid');
        $tagIds = $this->getProjectTags($pId);
        $this->tagCount = count($tagIds);
        $this->results = array();
        $idx = 0;
        foreach($tagIds as $tId) {
            $data = $this->fetchData($tId);
            $this->groupData($data, $idx);
            $idx++;
        }
        $this->dumpResults();
    }
        
    private function getProjectTags($pId) {
        $sql = "SELECT id FROM project_tag WHERE project_id=:pid order by id";
        $data = $this->em->getConnection()->fetchAll($sql, array('pid'=>$pId));
        $out = array();
        foreach($data as $row) {
            $out[] = $row['id'];
        }
        return $out;
    }
/*
        $sql = "select substring_index(pm.metric_name,':',-2) as mmm, sum(metric) as m
                from project_metric pm 
                left join project_file pf on ( pm.cname=pf.fname )
                where pm.project_tag_id=:tid and pm.ctype='file' and pm.metric_group_id=2 
                    and pf.section='system'
                group by mmm 
                order by mmm;";

 */    
    private function fetchData($tagId) {
        printf("Fetch data from db for tag=%s...\n", $tagId);

        $sql = "select substring_index(pm.metric_name,':',-2) as mmm, sum(metric) as m
                from project_metric pm 
                where pm.project_tag_id=:tid and pm.ctype='tag' and pm.metric_group_id=2 
                group by mmm 
                order by mmm;";
        $data = $this->em->getConnection()->fetchAll($sql, array('tid'=>$tagId));
        printf("Fetched.\n");
        return $data;
    }
    
    private function groupData($data, $idx) {
        
        $total = 0;
        $nfTotal = 0;
        foreach($data as $row) {
            $sp = $row['mmm'];
            if( !isset($this->results[$sp]) ) {
                $srow = array();
                $this->results[$sp] = array_pad(array(), $this->tagCount, '');
            }
            $this->results[$sp][$idx]='X';
        }        
    }
    
    private function dumpResults() {
        $i=1;
        foreach($this->results as $sp => $cnt) {
            printf("%s, %s, %s\n", $i++, $sp, implode(',', $cnt));
            //print_R($cnt);
        }
    }
}
