<?php

namespace Slx\MetricsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of SpeceisGroupingCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 24 Φεβ 2016
 */
class SpeciesGroupingIdiomCommand extends ContainerAwareCommand {

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $output;
    private $objPatterns;
    private $methPatterns;
    private $regularity;
    private $results;
    private $fresults;

    protected function configure() {
        $this
                ->setName('waev:speciesgroupingidiom')
                ->setDescription("Groups Method invocation species idiom or not")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->getGroupingInfo();
        $data = $this->fetchData();
        $this->groupData($data);
        $this->sortResults();
        $this->groupFamilyResults();
        $this->dumpResults();
    }

    private function getGroupingInfo() {
        $mp = $this->getContainer()->get('waev.MethPatternGroupingInfo');
        $this->objPatterns = $mp->getObjPatterns();
        $this->methPatterns = $mp->getMethPatterns();
        $this->regularity = $mp->getRegularity();
    }

    private function fetchData() {
        printf("Fetch data from db...\n");

        $sql = "select substring_index(pm.metric_name,':',-2) as metricname, sum(pm.metric) as metric, group_concat(distinct pt.project_id) as stations
                from project_metric pm
                left join project_tag pt on (pm.project_tag_id=pt.id) 
                where pm.metric_group_id=2 
                and pm.ctype='tag'
                and pt.project_id<66
                group by metricname;";

        $data = $this->em->getConnection()->fetchAll($sql);
        printf("Fetched.\n");
        return $data;
    }

    private function compineSpeciesFamilies($fA, $fB) {
        $out = 'R';
        $f = sprintf("%s%s", $fA, $fB);
        switch ($f) {
            case 'RR':
                $out = 'R';
                break;
            case 'RI':
            case 'IR':
            case 'II':
                $out = 'I';
                break;
        }
        return $out;
    }

    private function groupData($data) {
        $this->results = array();
        $total = 0;
        $nfTotal = 0;
        foreach ($data as $row) {
            list($op, $mp) = explode(':', $row['metricname']);
            if (isset($this->objPatterns[$op]) && isset($this->methPatterns[$mp])) {
                $sp = sprintf("%s:%s", $this->objPatterns[$op], $this->methPatterns[$mp]);
                if (!isset($this->results[$sp])) {
                    $this->results[$sp] = array(
                        'a' => $this->compineSpeciesFamilies($this->regularity[$this->objPatterns[$op]], $this->regularity[$this->methPatterns[$mp]]),
                        'sp' => sprintf("%s-%s", $this->objPatterns[$op], $this->methPatterns[$mp]),
                        'opp' => $this->objPatterns[$op],
                        'mpp' => $this->methPatterns[$mp],
                        'op' => $op,
                        'mp' => $mp,
                        'c' => 0,
                        'stations' => array(),
                    );
                }
                $this->results[$sp]['c'] += $row['metric'];
                $cstations = explode(',', $row['stations']);
                $this->results[$sp]['stations'] = array_unique(array_merge($this->results[$sp]['stations'], $cstations));
                $total = $total + $row['metric'];
            } else {
                $nfTotal = $nfTotal + $row['metric'];
                printf("NOT FOUND!! %s (%s) %s (%s)\n", $op, (isset($this->objPatterns[$op]) ? 'Y' : 'N'), $mp, (isset($this->methPatterns[$mp]) ? 'Y' : 'N'), $row['metric']
                );
            }
        }
        foreach ($this->results as $idx => $row) {
            $this->results[$idx]['stations'] = count($row['stations']);
        }
        printf("metric total=%s\n", $total);
        printf("metric nftotal=%s\n", $nfTotal);
        printf("metric Gtotal=%s\n", $total + $nfTotal);
    }

    private function groupFamilyResults() {
        $this->fresults = array();
        foreach ($this->results as $row) {
            if (!isset($this->fresults[$row['a']])) {
                $this->fresults[$row['a']] = array('family' => $row['a'], 'population' => 0);
                switch ($row['a']) {
                    case 'R':
                        $this->fresults[$row['a']]['name'] = 'R';
                        break;
                    case 'I':
                        $this->fresults[$row['a']]['name'] = 'I';
                        break;
                }
            }
            $this->fresults[$row['a']]['population'] += $row['c'];
        }
        print_r($this->fresults);
    }

    public function sortComp($a, $b) {
        $out = 0;
        if ($a['a'] == $b['a']) {
            $out = strcmp($a['sp'], $b['sp']);
        } else {
            $out = strcmp($a['a'], $b['a']);
        }
        return $out;
    }

    private function sortResults() {
        usort($this->results, array($this, 'sortComp'));
    }

    private function dumpResults() {
        $rootDir = $this->getContainer()->getParameter('kernel.root_dir');
        $outPath = $rootDir . '/../web/uploads/a1516/reg-irreg-patterns';
        $fidx = 1;
        foreach ($this->fresults as $frow) {
            $fnF = sprintf("%s/f-%s.csv", $outPath, $fidx);
            $handleF = fopen($fnF, "w+");
            $odata = ['family' => $frow['name'], 'population' => $frow['population']];
            fputcsv($handleF, array_keys($odata), "\t");
            fputcsv($handleF, $odata, "\t");
            fclose($handleF);
            printf("%s\n", implode(",", $frow));
            $fnFS = sprintf("%s/fs-%s.csv", $outPath, $fidx);
            printf("SP File: %s\n", $fnFS);
            $handleS = fopen($fnFS, "w+");
            $sidx = 1;
            foreach ($this->results as $row) {
                if ($row['a'] == $frow['family']) {
                    $odata = ['spicies' => $row['sp'], 'population' => $row['c'], 'projects' => $row['stations']];
                    if ($sidx == 1) {
                        fputcsv($handleS, array_keys($odata), "\t");
                    }
                    fputcsv($handleS, $odata, "\t");
                    printf("%s\n", implode(",", $row));
                    $sidx++;
                }
            }
            fclose($handleS);
            $fidx++;
        }
    }

}
