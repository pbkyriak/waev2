<?php

namespace Slx\MetricsBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Description of MetricCountersFactoryBuilderPass
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 20/12/15
 */
class MetricCountersFactoryBuilderPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if( !$container->hasDefinition('waev.metrics.factory') ) {
            printf("no factory class defined in services\n");
            return;
        }
        $defintion = $container->getDefinition('waev.metrics.factory');
        
        $taggedServiceIds = $container->findTaggedServiceIds('waev.metric.counter');
        foreach($taggedServiceIds as $serviceId => $tags) {
            foreach($tags as $tagAttributes) {
                if( isset($tagAttributes['alias']) && $tagAttributes['metric.group.id']) {
                    $args = array(
                        $serviceId, 
                        $tagAttributes['alias'], 
                        $tagAttributes['metric.group.id'], 
                        $tagAttributes['active'], 
                        (isset($tagAttributes['update_member_members']) ? $tagAttributes['update_member_members'] : false), 
                    );
                    $defintion->addMethodCall('addCounterDef', $args);
                }
            }
        }
    }

}
