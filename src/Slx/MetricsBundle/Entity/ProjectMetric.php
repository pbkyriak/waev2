<?php

namespace Slx\MetricsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectMetric
 */
class ProjectMetric
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $ctype;

    /**
     * @var string
     */
    private $cname;

    /**
     * @var string
     */
    private $metric_name;

    /**
     * @var integer
     */
    private $metric;

    /**
     * @var \Slx\MetricsBundle\Entity\ProjectTag
     */
    private $projectTag;

    private $metric_group_id;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ctype
     *
     * @param string $ctype
     * @return ProjectMetric
     */
    public function setCtype($ctype)
    {
        $this->ctype = $ctype;
    
        return $this;
    }

    /**
     * Get ctype
     *
     * @return string 
     */
    public function getCtype()
    {
        return $this->ctype;
    }

    /**
     * Set cname
     *
     * @param string $cname
     * @return ProjectMetric
     */
    public function setCname($cname)
    {
        $this->cname = $cname;
    
        return $this;
    }

    /**
     * Get cname
     *
     * @return string 
     */
    public function getCname()
    {
        return $this->cname;
    }

    /**
     * Set metric_name
     *
     * @param string $metricName
     * @return ProjectMetric
     */
    public function setMetricName($metricName)
    {
        $this->metric_name = $metricName;
    
        return $this;
    }

    /**
     * Get metric_name
     *
     * @return string 
     */
    public function getMetricName()
    {
        return $this->metric_name;
    }

    /**
     * Set metric
     *
     * @param integer $metric
     * @return ProjectMetric
     */
    public function setMetric($metric)
    {
        $this->metric = $metric;
    
        return $this;
    }

    /**
     * Get metric
     *
     * @return integer 
     */
    public function getMetric()
    {
        return $this->metric;
    }

    /**
     * Set projectTag
     *
     * @param \Slx\MetricsBundle\Entity\ProjectTag $projectTag
     * @return ProjectMetric
     */
    public function setProjectTag(\Slx\MetricsBundle\Entity\ProjectTag $projectTag = null)
    {
        $this->projectTag = $projectTag;
    
        return $this;
    }

    /**
     * Get projectTag
     *
     * @return \Slx\MetricsBundle\Entity\ProjectTag 
     */
    public function getProjectTag()
    {
        return $this->projectTag;
    }
    
    public function getMetricGroupId() {
        return $this->metric_group_id;
    }
    
    public function setMetricGroupId($v) {
        $this->metric_group_id = $v;
        return $this;
    }
}
