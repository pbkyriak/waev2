Metrics Bundle
##############

Metrics measurement bundle. 

Tasks
=====
SimpleMetricsTask
-----------------
Measures very basic metrics traversing AST:

    * LOC (Analyser/LocCounter.php)
    * CLOC (Analyser/LocCounter.php)
    * CCN (Analyser/CcnCounter.php)
    * CCN2 (Analyser/CcnCounter.php)
    * **nof** number of functions (Analyser/NodeCounter.php)
    * **noc** number of classes (Analyser/NodeCounter.php)
    * **nom** number of methods (Analyser/NodeCounter.php)
    * **noi** number of interfaces (Analyser/NodeCounter.php)

also counts global code LOC, CCN and CCN2.

Metrics are aggregated to file and project (tag) level.


