<?php

namespace Slx\MetricsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Slx\MetricsBundle\DependencyInjection\Compiler\MetricCountersFactoryBuilderPass;

class SlxMetricsBundle extends Bundle {

    public function build(ContainerBuilder $container) {
        parent::build($container);
        $container->addCompilerPass(new MetricCountersFactoryBuilderPass());
    }

}
