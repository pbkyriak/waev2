<?php

namespace Slx\MetricsBundle\Tests;
use \PHPParser_NodeVisitor;
use \PHPParser_Node;

/**
 * Description of TestVisitor
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 4 Φεβ 2016
 */
class TestVisitor implements PHPParser_NodeVisitor {
    public $lastLine;
    
    public function afterTraverse(array $nodes) {
        
    }

    public function beforeTraverse(array $nodes) {
        
    }

    public function enterNode(PHPParser_Node $node) {
        printf("node %s line %s\n", $node->getType(), $node->getLine());
        if( $node->getLine()>$this->lastLine) {
            $this->lastLine = $node->getLine();
        }
    }

    public function leaveNode(PHPParser_Node $node) {
        
    }

}
