<?php

$rc = new ReflectionClass($className);
if( $rc->hasMethod($methodName) ) {
    $rm = $rc->getMethod($methodName);
    $rm->invoke($object);
}

