<?php
// create new object from reflection
$rc = new ReflectionClass($className);
$object = $rc->newInstance($args);  
$rm = $rc->getMethod($methodName);
$rm->invoke($object);

