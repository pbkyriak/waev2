<?php

class MyCache {

    public function getCached() {
        return $this->node->getItem();
    }

    public function getFromCache() {
        $this->node = unserialize($data);
    }

    public function getCachedAgain() {
        return $this->node->getItem();
    }

}