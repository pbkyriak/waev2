<?php

class MyCache {
    
    public function getFromCache() {
        $this->node = unserialize($data);
    }

    public function getCached() {
        return $this->node->getItem();
    }

    public function getCachedAgain() {
        return $this->node->getItem();
    }

}