<?php


${$objName.$Variable}->methodA();
$object->methodA();
${$namePart.$nameOtherPart}->methodA();
${lcfirst($nameOtherPart)}->methodA();
${"obj$name"}->methodA();
${'object'}->methodA();
${$arr[1]}->methodA();
$$objNameVariable->methodA();
$c[1]->getId();
$d[$b->getId()]->getRow();
$varStaticClassName::$method()->methodB();

${$namePart.$nameOtherPart}->{$namePart.ucfirst($nameOtherPart)}();
$obj->$varMethod();
$obj->{"methodname"}();
getObject()->{meth()}();
$obj->$methodA()->$methodB();


singleton::getId();        
$singleton::getInstance(); 
self::get();                
parent::get();             
static::get();             

signletonClass::$methodName();


$varFuncName(); 
$a->methodA();
$b->methodA()->methodB();                  // +1
getInstance()->methodB();

singleton::getId();         // PHPParser_Node_Name_FullyQualified
$singleton::getInstance();  // PHPParser_Node_Expr_Variable
self::get();                // PHPParser_Node_Name
parent::get();              // PHPParser_Node_Name
static::get();              // PHPParser_Node_Name

singleton::getInstance()->methodB();
$c[1]->getId();
$d[$b->getId()]->getRow();
$obj->$varMethod();
$varStaticClassName::$method()->methodB();

static::getRow();
parent::getRow();
self::getRow();

$a = new ClassA();
$a->methodA(1,2);                   // +1
$a->methodB(1, 2);                  // +1
$a->methodA($a->methodB(1, 2), 3);  // +2
$x = funcA(1);                      // +1
$x = funcA($a->methodA(1, 2));      // +2

function funcA($a) {
    return abs($a);
}

class ClassA {
    public function methodA($a, $b) {
        return $a+$b;
    }
    
    public function methodB($a,$b) {
        $this->methodA($a, $b);         // 0
        
    }
}

class ClassB {
    public function methodC($c, $d) {
        return $this;
    }
    
    public function methodD($c, $d) {
        return $this;
    }
    
    public function methodE() {
        $out = $this->methodC($c, $d)->methodD($c, $d); // 0
        $out = abs($out);
        return $out;
    }
}


