<?php

namespace ns1\ns2;
/**
 * this is classA comment block
 */
class classA {
    public function f1() {
        if( 1<2 && 3>2) {
            echo 'line1';
        }
        else {
            echo 'line2';
        }
    }
    /**
     * 
     * @param type $a
     * @param type $b
     */
    private function f2($a, $b) {
        for($i=0; $i<100; $i++) {
            echo 'f2';
        }
    }
    
}

/**
 * this is an interface
 */
interface iface1 {
    public function if1();
    public function if2(array $a,
            $b, 
            $c);
}

trait trait1 {
    public function tf1($a, $b, $sign) {
        if($sign=='+') {
            return $a+$b;
        }
        else {
            return $a-$b;
        }
    }
}

/**
 * this is global function gf1
 */
function gf1() {
    echo 'this is global function 1';
}

echo 'this a global ';
if( 1>2 ) {
    $a=3;
}