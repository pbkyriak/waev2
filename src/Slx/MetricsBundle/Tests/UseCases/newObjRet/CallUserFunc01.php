<?php

class CallUserFunc01 {

    public function test() {
        call_user_func(array($classname, 'say_hello'));
        call_user_func($classname .'::say_hello'); // As of 5.2.3
        call_user_func(array($myobject, 'say_hello'));
        call_user_func(array($myobject, $mymethod));
        call_user_func(array($myobject, $$mymethod));
        call_user_func_array(array($myobject, ${"we".$mymethod}), $params);
    }
    
}
