<?php
// signleton
class ObjCreator {
    
    static $instance;
    
    public function getInstance() {
        if( static::$instance==null ) {
            static::$instance = new static();
        }
        return static::$instance;
    }
    
}