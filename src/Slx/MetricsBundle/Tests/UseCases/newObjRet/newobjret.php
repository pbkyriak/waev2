<?php

class ObjCreator {
    
    private $o1;
    public $o2;
    
    public function create0() {
        $o = new Object();
    }
    
    public function create1() {
        $o = new Object();
        return $o;
    }
    
    public function create2($cargo) {
        $out = true;
        $cargo->prop = new Object();
        return $out;
    }

    public function create3( ) {
        $this->o1 = new Object();
    }
    
    public function create4() {
        $this->o2 = new Object();
    }
    public function create5() {
        static::$o3 = new Object();
    }
    public function getO1() {
        return $this->o1;
    }
    
    public function getO3() {
        return static::$o3;
    }
    
    public function test() {
        if ($product_download = new ProductDownload($id) && !$product_download->delete(true))
                return false;

    }
    
}