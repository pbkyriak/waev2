<?php

class SingletonClass {
    public static function getInstance() {
        printf("%s called\n", __METHOD__);
    }
}

class ObjectClass {
    
    public function __construct() {
        printf("%s constructor called\n", __CLASS__);
    }
    
    public function methodA() {
        printf("%s called\n", __METHOD__);
    }
}

// *************************************************************************
printf("Static Case 1\n");
    SingletonClass::getInstance();

// *************************************************************************
printf("Static Case 5\n");
    $singletonName = 'SingletonClass';
    $obj = $singletonName::getInstance();

printf("Static Case 6\n");
    $className = 'SingletonClass';
    $classNameVariable = 'className';
    ${$classNameVariable}::getInstance();

printf("Static Case 7\n");
    $className = 'SingletonClass';
    $namePart = "class";
    $nameOtherPart = "Name";
    ${$namePart.$nameOtherPart}::getInstance();

printf("Static Case 8\n");
    $ClassName = 'SingletonClass';
    $nameOtherPart = "className";
    ${ucfirst($nameOtherPart)}::getInstance();

printf("Static Case 9\n");
    $className = 'SingletonClass';
    $name = 'Name';
    ${"class$name"}::getInstance();

printf("Static Case 10\n");
    $className = 'SingletonClass';
    ${'className'}::getInstance();

printf("Static Case 11\n");
    $className = 'SingletonClass';
    $arr = array('dummy', 'className');
    ${$arr[1]}::getInstance();
    
// *************************************************************************
printf("Static Case 12\n");
    $methodName = 'getInstance';
    SingletonClass::$methodName();	
printf("Static Case 13\n");
    $methodName = 'getInstance';
    $methodNameVariable = 'methodName';
    SingletonClass::${$methodNameVariable}();
printf("Static Case 14\n");
    $methodName = 'getInstance';
    $namePart = 'method';
    $nameOtherPart = 'Name';
    SingletonClass::${$namePart.$nameOtherPart}();
printf("Static Case 15\n");
    $MethodName = 'getInstance';
    $name = 'methodName';
    SingletonClass::${ucfirst($name)}();
printf("Static Case 16\n");
    $methodName = 'getInstance';
    $name = 'Name';
    SingletonClass::${"method$name"}();
printf("Static Case 17\n");
    $methodName = 'getInstance';
    SingletonClass::${'methodName'}();
printf("Static Case 18\n");
    $methodName = 'getInstance';
    $arr = array('dummy', 'methodName');
    SingletonClass::${$arr[1]}();

// *************************************************************************
    
$object = new ObjectClass();

$object->methodA();

// build object variable's name
$namePart='obj';
$nameOtherPart='ect';
${$namePart.$nameOtherPart}->methodA();

// build object variable's name
$nameOtherPart='Object';
${lcfirst($nameOtherPart)}->methodA();

// build object variable's name
$name = 'ect';
${"obj$name"}->methodA();

// literal object variable's name
${'object'}->methodA();

// get from array object variable's name
$arr = array('dummy', 'object');
${$arr[1]}->methodA();

// variable with the name of object's variable name
$objNameVariable = 'object';
${$objNameVariable}->methodA();
$$objNameVariable->methodA();

// -------------------------------------------------------------
class Test1 {
    public static $sClassName= 'ObjectClass';
    public $className= 'ObjectClass';
    public function getClassName() {
        return $this->getClassName();
    }
}

function getMyClassName() {
    return 'ObjectClass';
}
$myclass = 'ObjectClass';
$o = new $myclass();
$myVarName = 'myclass';
$o = new $$myVarName();

$a = ['ObjectClass'];
$o = new $a[0]();

$a = new Test1();
$o = new $a->className();
$o = new Test1::$sClassName();
echo "-----------\n";
$oo = (object)"ObjectClass";
echo get_class($oo);
echo "-----------\n";