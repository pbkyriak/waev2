<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of callsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class callsTest extends WebTestCase {

    private $container;
    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }

   public function AtestCallCount1() {
        $fn =  __DIR__.'UseCases/calls2.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        //$l1 = new \Slx\MetricsBundle\Analyser\LocCounter($fm);
        //$l2 = new \Slx\MetricsBundle\Analyser\CcnCounter($fm);
        //$l3 = new \Slx\MetricsBundle\Analyser\NodeCounter($fm);
        $l4 = new \Slx\MetricsBundle\Analyser\Counter\CallUserFuncCounter();
        $l4->configure($fm);
        //$l4 = new \Slx\MetricsBundle\Analyser\ObjNewCounter($fm);
       
        $traverser = new Traverser();
        //$traverser->addVisitor($l1);
        //$traverser->addVisitor($l2);
        //$traverser->addVisitor($l3);
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);
        
        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
        /*
        printf("file = %s \n ", 
                $metrics->getName()
                );
        $this->dumpMetrics($metrics->getMetrics());
        foreach($metrics->getMembers() as $m) {
            printf("\tctype=%s name=%s", 
                    $m->getCType(), 
                    $m->getName()
                    );
            $this->dumpMetrics($m->getMetrics());
            if( $m->getMembers() ) {
                foreach($m->getMembers() as $mm) {
                    printf("\t\tctype=%s name=%s fqn=%s ", 
                            $mm->getCType(), 
                            $mm->getName(),
                            sprintf("%s::%s", $m->getName(), $mm->getName())
                            );
                    $this->dumpMetrics($mm->getMetrics());
                }
            }
            else {
                printf("no member members\n");
            }
        }
         * 
         */
    }

   public function AtestCount1() {
        $fn =  __DIR__.'UseCases/newObjRet/newobjret10.php';
        //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        
        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ObjNewUseCounter();
        $l4->configure($fm);
       
        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);
        
        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
        //$vars = $l4->getPendingProperties();
        //print_R($vars);
    }
    
   public function testCount2() {
        $fn =  __DIR__.'UseCases/newObjRet/CallUserFunc01.php';
        //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        
        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\MetricsBundle\Analyser\Counter\CallUserFuncCounter();
        $l4->configure($fm);
       
        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);
        
        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
        //$vars = $l4->getPendingProperties();
        //print_R($vars);
    }
    
    private function dumpMetrics($metrics) {
        printf("\n-------------------\n");
        foreach($metrics as $mk => $mv) {
            printf(" %s=%s\n", $mk, $mv);
        }
        printf("-------------------\n");
    }
    
    private function updateFileMetrics($metrics) {
        printf("-----------\n");
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if($m->getMembers()) {
                foreach($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }
    
    private function updateMetric($key, $name, $metrics) {
        foreach($metrics as $mk=>$mv) {
            printf("%s, %s, %s, %s\n",
                    $key,
                    substr($name,0,100),
                    $mk,
                    $mv
                    );
        }        
    }
}
