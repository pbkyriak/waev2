<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\GitMinerBundle\Analyser\GiniCalculator;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of counterTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 20 Δεκ 2015
 */
class counterTest extends WebTestCase {

    private $container;
    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }
       /*    
    public function testSetup() {
 
        $mfactory = $this->container->get('waev.metrics.factory');
        $defs = $mfactory->getDefinitions();
        var_dump($defs);
        
        $stmtT = $this->em->getConnection()->executeQuery("select id from project_tag where project_id=32");
        $tIds = $stmtT->fetchAll(\PDO::FETCH_COLUMN);
        printf("tId\tgini\tparticipants\n");
        foreach($tIds as $tId) {
            $stmt = $this->em->getConnection()->executeQuery("select metric from project_metric where metric_group_id=2 and project_tag_id=:tid and ctype='tag'", array('tid'=>$tId));
            $data = $stmt->fetchAll(\PDO::FETCH_COLUMN);
            $gini = new GiniCalculator();
            $gini->setData($data);
            printf("%s\t%s\t%s\n", $tId, $gini->getGini(), count($data));
        }
    }
    * 
    */

    public function testNewObjUCounter() {
        $scHelper = $this->container->get('waev.SourceCodeHelper');
        $tag = $this->em->getRepository("SlxGitMinerBundle:ProjectTag")->find(5172);
        $scHelper->setProjectTag($tag);
        $file = array('id'=>3779592, 'file'=>'/tests/ArticleTest.php');
        $ast = $scHelper->getFileAst($file);
        
        if( $ast ) {
          
            $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $file['file']);
            $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ObjNewUseCounter();
            //$l4 = new \Slx\MetricsBundle\Analyser\Counter\ObjNewCounter();
            $l4->configure($fm);
            $traverser = new Traverser();
            $traverser->addVisitor($l4);
            $traverser->traverseAst($ast);
            $metrics = $l4->getMetrics();
            $this->updateFileMetrics($metrics);
            
  /*          
            $tv = new TestVisitor();
            $traverser = new Traverser();
            $traverser->addVisitor($tv);
            $traverser->traverseAst($ast);
            printf("lastline %s\n",$tv->lastLine);
*/
            
        }
        else {
            printf("no AST \n");
        }
    }
    
    private function updateFileMetrics($metrics) {
        printf("-----------\n");
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if($m->getMembers()) {
                foreach($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }
    
    private function updateMetric($key, $name, $metrics) {
        foreach($metrics as $mk=>$mv) {
            printf("%s, %s, %s, %s\n",
                    $key,
                    substr($name,0,100),
                    $mk,
                    $mv
                    );
        }        
    }
}
