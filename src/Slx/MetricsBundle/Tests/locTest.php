<?php

namespace Slx\MetricsBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of Namespaces1
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 20 Dec 2014
 */
class Loc1 extends \PHPUnit_Framework_TestCase {

    private $results = array();

    public function testLOCandCCN1() {
        $fn =  __DIR__.'UseCases/loc1.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, locglobal, 6',
            'file, loc, 65',
            'file, cloc, 14',
            'file, noc, 1',
            'file, ccn, 9',
            'file, ccn2, 10',
            'file, nom, 5',
            'file, noi, 1',
            'file, not, 1',
            'file, nof, 1',
            'file, ccnglobal, 1',
            'file, ccn2global, 1',
            'class, loc, 34',
            'class, cloc, 8',
            'method, loc, 8',
            'method, loc, 5',
            'method, cloc, 5',
            'class, noc, 1',
            'class, ccn, 4',
            'class, ccn2, 5',
            'class, nom, 2',
            'method, ccn, 2',
            'method, ccn2, 3',
            'method, nom, 1',
            'method, ccn, 2',
            'method, ccn2, 2',
            'method, nom, 1',
            'interface, loc, 10',
            'interface, cloc, 3',
            'method, loc, 1',
            'method, loc, 3',
            'interface, noi, 1',
            'interface, ccn, 2',
            'interface, ccn2, 2',
            'interface, nom, 2',
            'method, ccn, 1',
            'method, ccn2, 1',
            'method, nom, 1',
            'method, ccn, 1',
            'method, ccn2, 1',
            'method, nom, 1',
            'trait, loc, 18',
            'method, loc, 8',
            'trait, not, 1',
            'trait, ccn, 2',
            'trait, ccn2, 2',
            'trait, nom, 1',
            'method, ccn, 2',
            'method, ccn2, 2',
            'method, nom, 1',
            'function, loc, 3',
            'function, cloc, 3',
            'function, ccn, 1',
            'function, ccn2, 1',
            'function, nof, 1',
        );
        //print_R($this->results);
        $this->assertEquals($expected, $this->results);
    }

    public function testLOCandCCN2() {
        $fn =  __DIR__.'UseCases/loc2.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, locglobal, 5',
            'file, clocglobal, 5',
            'file, ccnglobal, 1',
            'file, ccn2global, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function testLOCandCCN3() {
        $fn =  __DIR__.'UseCases/loc3.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, loc, 7',
            'file, ccn, 2',
            'file, ccn2, 4',
            'file, nof, 1',
            'function, loc, 7',
            'function, ccn, 2',
            'function, ccn2, 4',
            'function, nof, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    
    private function doCountOnFile($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l1 = new \Slx\MetricsBundle\Analyser\Counter\LocCounter();
        $l1->configure($fm);
        $l2 = new \Slx\MetricsBundle\Analyser\Counter\CcnCounter($fm);
        $l2->configure($fm);
        $l3 = new \Slx\MetricsBundle\Analyser\Counter\NodeCounter($fm);
        $l3->configure($fm);
        $traverser = new Traverser();
        $traverser->addVisitor($l1);
        $traverser->addVisitor($l2);
        $traverser->addVisitor($l3);
        $traverser->traverseAst($ast);

        $metrics = $l1->getMetrics();
        $this->updateFileMetrics($metrics);
    }

    private function updateFileMetrics($metrics) {
        $this->results = array();
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach ($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if ($m->getMembers()) {
                foreach ($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }

    private function updateMetric($key, $name, $metrics) {
        foreach ($metrics as $mk => $mv) {
            $this->results[] = sprintf("%s, %s, %s", $key, $mk, $mv
            );
        }
    }

}
