<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of objnewRetTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class objnewRetTest extends WebTestCase {

    private $container;

    /** @var use Doctrine\ORM\EntityManager */
    private $em;

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }

    private $tagMetrics = array();
    private $pendingVars = array();
    /** @var \Slx\ParserBundle\Util\SourceCodeHelper */
    protected $scHelper=null;
    
    public function atestCount1() {
        $fn =  __DIR__.'UseCases/newObjRet/newobjret10.php';
        //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ObjNewRetCounter();
        $l4->configure($fm);

        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);

        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
        $this->pendingVars = $this->pendingVars + $l4->getPendingProperties();
    }

    
    public function testCount() {
        $tagId=4131; 
        $metricGroupId=7;
        $this->scHelper = $this->container->get('waev.SourceCodeHelper');
        $tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        $this->scHelper->setProjectTag($tag);
        $fileList = $this->getFileList();
        foreach($fileList as $file) {
            $this->processFile($file);
        }

        $cc = new \Slx\TypeAnalysisBundle\Analyser\ContextVarsReturnsCounter($tagId, $metricGroupId);
        $cc->setVars($this->pendingVars);
        $cc->count();
        print_R($cc->getNewMetrics());
    }
    
    private function processFile($file) {
        $ast = $this->scHelper->getFileAst($file);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $file['fname']);
        $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ObjNewRetCounter();
        $l4->configure($fm);

        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);

        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
        $this->pendingVars = $this->pendingVars + $l4->getPendingProperties();        
    }
    
    private function getFileList() {
        $list = $this->scHelper->getTagSourceFilesDB($this->em, true, ['test']);
        return $list;
    }
    
    private function updateFileMetrics($metrics) {
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach ($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
        }
    }

    private function updateMetric($key, $name, $metrics) {
        foreach ($metrics as $mk => $mv) {
            //printf("%s, %s, %s, %s\n", $key, substr($name, 0, 100), $mk, $mv);
            $this->tagMetrics[] = array($key, $name, $mk, $mv);
        }
    }

    public function atestMatch() {
        $a = file_get_contents('/var/www/waev/web/uploads/pv1.txt');
        $pendingVars = unserialize($a);
        //print_R($pendingVars);
        $tagId=666; $metricGroupId=7;
        $cc = new \Slx\TypeAnalysisBundle\Analyser\ContextVarsReturnsCounter($tagId, $metricGroupId);
        $cc->setVars($pendingVars);
        $cc->count();
        
    }
}
