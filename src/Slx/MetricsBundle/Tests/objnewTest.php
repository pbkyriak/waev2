<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of callsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class objnewTest extends WebTestCase {

    private $container;

    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    private $results = array();

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }

    //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';

    public function testCounts() {

       $this->mtestCount01();
       $this->mtestCount01_1();

    }

    public function mtestCount01() {
        $fn =  __DIR__.'UseCases/newObj/case_01.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:CAST:Expr_Array, 1',
        );

        $this->assertEquals($expected, $this->results);
    }
    public function mtestCount01_1() {
        $fn =  __DIR__.'UseCases/newObj/case_01_1.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:NEW:Name_FullyQualified, 4',
        );

        $this->assertEquals($expected, $this->results);
    }
    
    private function doCountOnFile($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));

        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\MetricsBundle\Analyser\Counter\ObjNewCounter();
        $l4->configure($fm);

        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);

        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
    }

    private function updateFileMetrics($metrics) {
        $this->results = array();
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach ($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if ($m->getMembers()) {
                foreach ($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }

    private function updateMetric($key, $name, $metrics) {
        foreach ($metrics as $mk => $mv) {
            $this->results[] = sprintf("%s, %s, %s", $key, $mk, $mv
            );
        }
    }

}
