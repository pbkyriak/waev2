<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of callsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class reflectionTest extends WebTestCase {

    private $container;
    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    private $results = array();
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }
        //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';

    public function testCounts() {
        
        $this->mtestCount01();
        $this->mtestCount01_1();
        $this->mtestCount01_2();
        $this->mtestCount01_3();
        $this->mtestCount02();
        $this->mtestCount03();
        $this->mtestCount03_1();
        $this->mtestCount03_2();
        $this->mtestCount05();
        $this->mtestCount06();
        $this->mtestCount07();
        $this->mtestCount07_1();
        $this->mtestCount08();

        $this->mtestCount04();
        $this->mtestCount04_1();
    }
    
   public function mtestCount00() {
        $fn =  __DIR__.'UseCases/Reflection/case00.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
            'file, MC:RES:Expr_Variable:Expr_Variable, 1'
        );
        
        $this->assertEquals($expected,$this->results);
   }
    
   public function mtestCount01() {
        $fn =  __DIR__.'UseCases/Reflection/case01.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
            'file, MC:RES:Expr_Variable:Expr_Variable, 1'
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount01_1() {
        $fn =  __DIR__.'UseCases/Reflection/case01_1.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
            'file, MC:RES:Expr_MethodCall:Expr_Variable, 1'
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount01_2() {
        $fn =  __DIR__.'UseCases/Reflection/case01_2.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
            'file, MC:RES:Expr_PropertyFetch:Expr_Variable, 1'
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount01_3() {
        $fn =  __DIR__.'UseCases/Reflection/case01_3.php';
        $this->doCountOnFile($fn);

        $expected = array(
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount02() {
        $fn =  __DIR__.'UseCases/Reflection/case02.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount03() {
        $fn =  __DIR__.'UseCases/Reflection/case03.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount03_1() {
        $fn =  __DIR__.'UseCases/Reflection/case03_1.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount03_2() {
        $fn =  __DIR__.'UseCases/Reflection/case03_2.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount05() {
        $fn =  __DIR__.'UseCases/Reflection/case05.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
    
   public function mtestCount06() {
        $fn =  __DIR__.'UseCases/Reflection/case06.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount07() {
        $fn =  __DIR__.'UseCases/Reflection/case07.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount07_1() {
        $fn =  __DIR__.'UseCases/Reflection/case07_1.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:RES:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount08() {
        $fn =  __DIR__.'UseCases/Reflection/case08.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_New:Scalar_String, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }
    
   public function mtestCount04() {
        $fn =  __DIR__.'UseCases/Reflection/case04.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:REO:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

   public function mtestCount04_1() {
        $fn =  __DIR__.'UseCases/Reflection/case04_1.php';
        $this->doCountOnFile($fn);

        $expected = array(
            'file, MC:RES:Expr_Variable:Expr_Variable, 1',
        );
        
        $this->assertEquals($expected,$this->results);
    }

    private function doCountOnFile($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));
        
        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ReflectionInvokeCounter();
        $l4->configure($fm);
       
        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);
        
        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
         
    }
    
    private function dumpMetrics($metrics) {
        printf("\n-------------------\n");
        foreach($metrics as $mk => $mv) {
            printf(" %s=%s\n", $mk, $mv);
        }
        printf("-------------------\n");
    }
    
    private function updateFileMetrics($metrics) {
        $this->results = array();
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if($m->getMembers()) {
                foreach($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }
    
    private function updateMetric($key, $name, $metrics) {
        foreach($metrics as $mk=>$mv) {
            /*
            $this->results[] = sprintf("%s, %s, %s, %s\n",
                    $key,
                    substr($name,0,100),
                    $mk,
                    $mv
                    );
             * 
             */
            $this->results[] = sprintf("%s, %s, %s",
                    $key,
                    $mk,
                    $mv
                    );
        }        
    }
}
