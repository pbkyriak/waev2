<?php

namespace Slx\MetricsBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Description of callsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class unserializeTest extends WebTestCase {

    private $container;

    /** @var use Doctrine\ORM\EntityManager */
    private $em;
    private $results = array();

    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
        $this->em = $client->getContainer()->get('Doctrine');
    }

    //$fn = '/var/www/waev/web/uploads/project-data/63/5150/mediawiki-1.4.0/tests/ArticleTest.php';

    public function testCounts() {

       $this->mtestCount01();
       $this->mtestCount01_1();
       $this->mtestCount01_2();
       $this->mtestCount02();
       $this->mtestCount02_1();
       $this->mtestCount02_2();
       $this->mtestCount02_3();
       $this->mtestCount02_4();
    }

    public function mtestCount01() {
        $fn =  __DIR__.'UseCases/Unserialize/case01.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_FuncCall, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount01_1() {
        $fn =  __DIR__.'UseCases/Unserialize/case01_1.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_Variable, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount01_2() {
        $fn =  __DIR__.'UseCases/Unserialize/case01_2.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Scalar_String, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount02() {
        $fn =  __DIR__.'UseCases/Unserialize/case02.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_Variable, 1',
            'class, OI:UNS:Expr_Variable, 1',
            'method, OI:UNS:Expr_Variable, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount02_1() {
        $fn =  __DIR__.'UseCases/Unserialize/case02_1.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_Variable, 1',
            'class, OI:UNS:Expr_Variable, 1',
            'method, OI:UNS:Expr_Variable, 1',
        );

        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount02_2() {
        $fn =  __DIR__.'UseCases/Unserialize/case02_2.php';
        $this->doCountOnFile($fn);
        $expected = array(
        );
        $this->assertEquals($expected, $this->results);
    }

    public function mtestCount02_3() {
        $fn =  __DIR__.'UseCases/Unserialize/case02_3.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_Variable, 1',
            'class, OI:UNS:Expr_Variable, 1',
            'method, OI:UNS:Expr_Variable, 1',
        );

        $this->assertEquals($expected, $this->results);
    }
    
    public function mtestCount02_4() {
        $fn =  __DIR__.'UseCases/Unserialize/case02_4.php';
        $this->doCountOnFile($fn);
        $expected = array(
            'file, OI:UNS:Expr_Variable, 1',
            'class, OI:UNS:Expr_Variable, 1',
            'method, OI:UNS:Expr_Variable, 1',
        );

        $this->assertEquals($expected, $this->results);
    }
    
    private function doCountOnFile($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $ast = $p->parse(file_get_contents($fn));

        //var_dump($ast);
        $fm = new \Slx\MetricsBundle\Analyser\FileMetric('file', $fn);
        $l4 = new \Slx\TypeAnalysisBundle\Analyser\Counter\ObjNewUnserializeCounter();
        $l4->configure($fm);

        $traverser = new Traverser();
        $traverser->addVisitor($l4);
        $traverser->traverseAst($ast);

        $metrics = $l4->getMetrics();
        $this->updateFileMetrics($metrics);
    }

    private function updateFileMetrics($metrics) {
        $this->results = array();
        $this->updateMetric('file', $metrics->getName(), $metrics->getMetrics());
        foreach ($metrics->getMembers() as $m) {
            $this->updateMetric($m->getCType(), $m->getName(), $m->getMetrics());
            if ($m->getMembers()) {
                foreach ($m->getMembers() as $mm) {
                    $this->updateMetric($mm->getCType(), $mm->getName(), $mm->getMetrics());
                }
            }
        }
    }

    private function updateMetric($key, $name, $metrics) {
        foreach ($metrics as $mk => $mv) {
            $this->results[] = sprintf("%s, %s, %s", $key, $mk, $mv
            );
        }
    }

}
