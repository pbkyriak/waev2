<?php
namespace Slx\MetricsBundle\Util;

/**
 * Description of MethPatternGroupingInfo
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since May 15, 2016
 */
class MethPatternGroupingInfo {
    private $rootDir;
    private $objPatterns;
    private $methPatterns;
    private $ambiguity;
    private $regularity;
    
    public function __construct($rootDir) {
        $this->rootDir = $rootDir;
        $this->loadGroupingInfo();
    }
    
    public function getObjPatterns() {
        return $this->objPatterns;
    }
    
    public function getMethPatterns() {
        return $this->methPatterns;
    }
    
    public function getAmbiguity() {
        return $this->ambiguity;
    }
    
    public function getRegularity() {
        return $this->regularity;
    }
    private function loadGroupingInfo() {
        $this->objPatterns = array();
        $this->methPatterns = array();
        $this->ambiguity = array();
        $this->regularity = array();
        $working = &$this->objPatterns;
        $handle = fopen($this->rootDir.'/web/uploads/invoke-patterns.txt', "r");
        if( $handle) {
            while($row = fgetcsv($handle, 200, "\t")) {
                if( count($row)==1 && $row[0] ) {
                    $working = &$this->methPatterns;
                    continue;
                }
                $key = $row[0];
                $patterns = explode(',',$row[3]);
                foreach($patterns as $pattern) {
                    $pattern = trim($pattern);
                    if( !isset($working[$pattern])) {
                        $working[$pattern] = $key;
                    }
                }
                $this->ambiguity[$key] =$row[1];
                $this->regularity[$key] =$row[2];
            }
            fclose($handle);
        }
    }
}
