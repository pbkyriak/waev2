<?php

namespace Slx\MetronicBundle\Annotation;

/**
 * Description of CurrentMenuItem
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @Annotation
 */
class CurrentMenuItem
{
    
    private $routeName;
    private $menu = 'main_builder'; // main (admin menu) or home (public menu)
    
    public function __construct($data)
    {
        if (isset($data['value'])) {
            $this->routeName = $data['value'];
            unset($data['value']);
        }
        if( isset($data['menu']) ) {
            $this->menu = $data['menu'].'_builder';
            unset($data['menu']);
        }
        
    }
    
    public function getRouteName() {
        return $this->routeName;
    }
    
    public function getMenuName() {
        return $this->menu;
    }
}
