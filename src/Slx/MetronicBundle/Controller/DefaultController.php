<?php

namespace Slx\MetronicBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('SlxMetronicBundle:Default:index.html.twig', array('name' => $name));
    }
}
