<?php

namespace Slx\MetronicBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Description of MenuBuilderPass
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class MenuBuilderPass implements CompilerPassInterface
{
    
    public function process(ContainerBuilder $container)
    {
        $taggedServiceIds = $container->findTaggedServiceIds('slx_metronic.menu');
        $menuItemListenerDefinition = $container->getDefinition('slx_metronic.annotation.listener');
        foreach($taggedServiceIds as $serviceId => $tags) {
            foreach($tags as $tagAttributes) {
                $menuItemListenerDefinition->addMethodCall('setMenuBuilder', array($tagAttributes['alias'],new Reference($serviceId)));
            }
        }
    }

}
