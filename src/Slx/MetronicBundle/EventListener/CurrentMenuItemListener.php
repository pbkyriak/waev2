<?php

namespace Slx\MetronicBundle\EventListener;

use Doctrine\Common\Annotations\Reader;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Slx\MetronicBundle\Menu\MenuBuilderInterface;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Slx\MetronicBundle\Exception\NoMainMenuBuilderDefined;

/**
 * Description of CurrentMenuItemListener
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class CurrentMenuItemListener
{
    /**
     * @var Reader An Reader instance
     */
    protected $reader;

    /**
     *
     * @var MenuBuilderInterface A MenuBuilder instance
     */
    protected $menuBuilder;

    /**
     * Constructor.
     *
     * @param Reader $reader An Reader instance
     * @param MenuBuilderInterface $menuBuilder A menu instance
     */
    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
        $this->menuBuilder = array();
    }

    public function setMenuBuilder($name, MenuBuilderInterface $menuBuilder) {
        $this->menuBuilder[$name] = $menuBuilder;
    }
    /**
     * @param FilterControllerEvent $event A FilterControllerEvent instance
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controller = $event->getController())) {
            return;
        }

        // Annotations from class
        $class = new \ReflectionClass($controller[0]);

        // Manage JMSSecurityExtraBundle proxy class
        if (false !== $className = $this->getRealClass($class->getName())) {
            $class = new \ReflectionClass($className);
        }

        if ($class->isAbstract()) {
            throw new \InvalidArgumentException(sprintf('Annotations from class "%s" cannot be read as it is abstract.', $class));
        }

        if ($event->getRequestType() == HttpKernelInterface::MASTER_REQUEST) {
            $this->setCurrentMenuItemFromAnnotations($this->reader->getClassAnnotations($class));

            // Annotations from method
            $method = $class->getMethod($controller[1]);
            $this->setCurrentMenuItemFromAnnotations($this->reader->getMethodAnnotations($method));
        }
    }

    /**
     * Set the current menu item from annotations
     *
     * @param Array $annotations Array of CurrentMenuItem annotations
     */
    private function setCurrentMenuItemFromAnnotations(array $annotations)
    {
        // requirements (@CurrentMenuItem)
        foreach ($annotations as $annotation) {
            if ($annotation instanceof CurrentMenuItem) {
                $routeName = $annotation->getRouteName();
                $builderName = $annotation->getMenuName();
                if( !isset($this->menuBuilder[$builderName]) ) {
                    throw new NoMainMenuBuilderDefined(sprintf('No menu builder tag with alias `%s` is defined in your application', $builderName));
                }                
                $this->menuBuilder[$builderName]->setCurrentMenuItemRouteName($routeName);
            }
        }
    }

    private function getRealClass($className)
    {
        if (false === $pos = strrpos($className, '\\__CG__\\')) {
            return false;
        }

        return substr($className, $pos + 8);
    }
}
