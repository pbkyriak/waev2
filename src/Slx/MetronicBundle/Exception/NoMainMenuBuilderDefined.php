<?php

namespace Slx\MetronicBundle\Exception;

/**
 * Description of NoMainMenuBuilderDefined
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class NoMainMenuBuilderDefined extends \RuntimeException
{
    public function __construct($message = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct($message, $code, $previous);
    }
}
