<?php

namespace Slx\MetronicBundle\Lib;

/**
 * Description of GreekText
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class GreekText
{

    public static function toGreeklishOld($word)
    {

        $m = array('/ου/', '/ού/', '/ευ/', '/εύ/', '/αυ/', '/αύ/',
            '/α/', '/β/', '/γ/', '/δ/', '/ε/', '/ζ/', '/η/', '/θ/', '/ι/', '/κ/', '/λ/', '/μ/', '/ν/',
            '/ξ/', '/ο/', '/π/', '/ρ/', '/σ/', '/τ/', '/υ/', '/φ/', '/χ/', '/ψ/', '/ω/',
            '/ς/',
            '/ά/', '/έ/', '/ό/', '/ί/', '/ύ/', '/ώ/', '/ή/', '/ϊ/', '/ϋ/', '/ΐ/', '/ΰ/',
            '/Ά/', '/Ό/', '/Ί/', '/Έ/', '/Ύ/', '/Ώ/', '/Ή/', '/Ϊ/', '/Ϋ/',
            '/Α/', '/Β/', '/Γ/', '/Δ/', '/Ε/', '/Ζ/', '/Η/', '/Θ/', '/Ι/', '/Κ/', '/Λ/', '/Μ/', '/Ν/',
            '/Ξ/', '/Ο/', '/Π/', '/Ρ/', '/Σ/', '/Τ/', '/Υ/', '/Φ/', '/Χ/', '/Ψ/', '/Ω/', '/ΟΥ/'
        );

        $r = array('ou', 'ou', 'ef', 'ef', 'af', 'af',
            'a', 'b', 'g', 'd', 'e', 'z', 'i', 'th', 'i', 'k', 'l', 'm', 'n',
            'ks', 'o', 'p', 'r', 's', 't', 'i', 'f', 'h', 'ps', 'o',
            's',
            'a', 'e', 'o', 'i', 'i', 'o', 'i', 'i', 'i', 'i', 'i',
            'a', 'o', 'i', 'e', 'i', 'o', 'i', 'i', 'i',
            'A', 'B', 'G', 'D', 'E', 'Z', 'I', 'Th', 'I', 'K', 'L', 'M', 'N',
            'Ks', 'O', 'P', 'R', 'S', 'T', 'Y', 'F', 'H', 'Ps', 'O', 'U'
        );

        return( preg_replace($m, $r, $word) );
    }

    public static function toGreeklish($string)
    {
        return strtolower(strtr($string,
            array(
            'Α' => 'A', 'Β' => 'V', 'Γ' => 'G', 'Δ' => 'D', 'Ε' => 'E', 'Ζ' => 'Z', 'Η' => 'I', 'Θ' => 'TH', 'Ι' => 'I', 'Κ' => 'K', 'Λ' => 'L',
            'Μ' => 'M', 'Ν' => 'N', 'Ξ' => 'KS', 'Ο' => 'O', 'Π' => 'P', 'Ρ' => 'R', 'Σ' => 'S', 'Τ' => 'T', 'Υ' => 'Y', 'Φ' => 'F', 'Χ' => 'X', 'Ψ' => 'PS', 'Ω' => 'O',
            'α' => 'a', 'β' => 'v', 'γ' => 'g', 'δ' => 'd', 'ε' => 'e', 'ζ' => 'z', 'η' => 'i',
            'θ' => 'th', 'ι' => 'i', 'κ' => 'k', 'λ' => 'l', 'μ' => 'm', 'ν' => 'n', 'ξ' => 'ks', 'ο' => 'o', 'π' => 'p', 'ρ' => 'r',
            'σ' => 's', 'τ' => 't', 'υ' => 'y', 'φ' => 'f', 'χ' => 'x', 'ψ' => 'ps', 'ω' => 'o', 'ς' => 's',
            'ά' => 'a', 'έ' => 'e', 'ή' => 'i', 'ί' => 'i', 'ό' => 'o', 'ύ' => 'y', 'ώ' => 'o', 'ϊ' => 'i', 'ϋ' => 'y',
            'ΐ' => 'i', 'ΰ' => 'y'
        )));
    }

    public static function slugify($text)
    {
        $slug = strtolower(GreekText::toGreeklish(trim($text)));
        $slug = preg_replace('/[^A-Za-z0-9-]+/', '_', $slug);
        return $slug;
    }

// end function to_greeklish -----------------------------------
}
