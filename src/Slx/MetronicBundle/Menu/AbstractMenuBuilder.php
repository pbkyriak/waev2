<?php

namespace Slx\MetronicBundle\Menu;

use Slx\MetronicBundle\Menu\MenuBuilderInterface;
use Knp\Menu\ItemInterface;

/**
 * Description of AbstractMenuBuilder
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
abstract class AbstractMenuBuilder implements MenuBuilderInterface
{

    protected $currentItemRouteName;
    protected $currentItemUri;
    
    public function setCurrentMenuItemRouteName($routeName) {
        $this->currentItemRouteName = $routeName;
    }
    
    public function setCurrentMenuItemUri($uri) {
        $this->currentItemUri=$uri;
    }
    public function getCurrentMenuItemUri() {
        return $this->currentItemUri;
    }
    public function setCurrentMenuItem(ItemInterface $menu, $routeName)
    {
        /** @var \Knp\Menu\MenuItem $child  */
        foreach ($menu->getChildren() as $child) { 
           
            $route = $child->getRoute();
            if( $route==$routeName ) {
                $child->setCurrent(true);
                break;
            }
            $this->setCurrentMenuItem($child, $routeName);
        } 
        return $menu;
    }
    
    public function setCurrentMenuItemFromUri(ItemInterface $menu, $uri) {
        foreach ($menu->getChildren() as $child) { 
            $itemUri = $child->getUri();
            if( $uri==$itemUri ) {
                $child->setCurrent(true);
                $menu->setCurrent(true);
                break;
            } 
            else {
                $this->setCurrentMenuItemFromUri($child, $uri);
            }
        } 
        return $menu;
    }
}
