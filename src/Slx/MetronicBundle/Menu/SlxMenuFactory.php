<?php

namespace Slx\MetronicBundle\Menu;

use Knp\Menu\Silex\RouterAwareFactory;
use Slx\MetronicBundle\Menu\SlxMenuItem as MenuItem;
/**
 * Description of SlxMenuFactory
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxMenuFactory extends RouterAwareFactory
{
    public function createItem($name, array $options = array())
    {
        if (!empty($options['route'])) {
            $params = isset($options['routeParameters']) ? $options['routeParameters'] : array();
            $absolute = isset($options['routeAbsolute']) ? $options['routeAbsolute'] : false;
            $options['uri'] = $this->generator->generate($options['route'], $params, $absolute);
        }
        $item = new MenuItem($name, $this);
        $options = array_merge(
            array(
                'uri' => null,
                'label' => null,
                'attributes' => array(),
                'linkAttributes' => array(),
                'childrenAttributes' => array(),
                'labelAttributes' => array(),
                'extras' => array(),
                'display' => true,
                'displayChildren' => true,
                'route' => '',
            ),
            $options
        );

        $item
            ->setUri($options['uri'])
            ->setLabel($options['label'])
            ->setAttributes($options['attributes'])
            ->setLinkAttributes($options['linkAttributes'])
            ->setChildrenAttributes($options['childrenAttributes'])
            ->setLabelAttributes($options['labelAttributes'])
            ->setExtras($options['extras'])
            ->setDisplay($options['display'])
            ->setDisplayChildren($options['displayChildren'])
            ->setRoute($options['route'])
        ;

        return $item;
    }
}
