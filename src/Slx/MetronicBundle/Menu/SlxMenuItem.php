<?php

namespace Slx\MetronicBundle\Menu;

use Knp\Menu\MenuItem;
/**
 * Description of SlxMenuItem
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxMenuItem extends MenuItem
{
    
    protected $route = '';
    
    public function setRoute($name) {
        $this->route=$name;
        return $this;
    }
    public function getRoute() {
        return $this->route;
    }

}
