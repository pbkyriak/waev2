Για να παίξει πρέπει:

το route του admin homepage να είναι metronic_admin_homepage
το route του public να είναι metronic_public_homepage

Στο bundle που φτιάχνει το admin menu οριζουμε το service
services:
    metronic_admin.menu_builder:
        class: Gnosis\ElectionsAdminBundle\Menu\MenuBuilder <------------ αυτή είναι η class του menu builder για την τρέχουσα εφαρμογή
        arguments: ["@service_container"]  
        tags:
            - { name: slx_metronic.menu, alias: main_builder }
