if (!window.slxCharts)
  var slxCharts = new Object();

slxCharts.Methods = {
  areaChart: function(containerId, dataSeries, stack) {
    $.plot($(containerId), dataSeries, {
      series: {
        stack: stack
        ,
        lines: {
          show: true,
          fill: true,
          steps: false
        }
      },
      colors: ["#EDC240", "#CB4B4B", "#AFD8F8"]
    });
  },
  lineChart: function(containerId, dataSeries) {
    $.plot($(containerId), dataSeries, {
      canvas: true,
      series: {
        lines: {
          show: true
        },
        points: {
          show: true
        }
      },
      xaxis: {
        mode: "categories"
      },
      yaxis: {
      },
      grid: {
        backgroundColor: {
          colors: ["#fff", "#eee"]
        },
        hoverable: true,
        clickable: true
      }
    });
    
    $("<div id='chart_tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
    
    $(containerId).bind("plothover", function (event, pos, item) {

				if (item) {
					var x = item.datapoint[0],
						y = item.datapoint[1];
                    
					$("#chart_tooltip").html(item.series.label + " of " + x + " = " + y)
						.css({top: item.pageY+5, left: item.pageX+5})
						.fadeIn(200);
				} else {
					$("#chart_tooltip").hide();
				}
			
		});
  }
};

jQuery.extend(slxCharts, slxCharts.Methods);