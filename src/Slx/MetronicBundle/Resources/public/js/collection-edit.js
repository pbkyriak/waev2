
jQuery(document).ready(function() {

  var select2ResultsFormatFunc = function(movie) {
    var markup = "<table class='movie-result'><tr>";
    if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
      markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "'/></td>";
    }
    markup += "<td valign='top'><h5>" + movie.title + "</h5>";
    if (movie.critics_consensus !== undefined) {
      markup += "<div class='movie-synopsis'>" + movie.critics_consensus + "</div>";
    } else if (movie.synopsis !== undefined) {
      markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
    }
    markup += "</td></tr></table>"
    return markup;
  }

  var select2SelectionFormatFunc = function(movie) {
    return movie.title;
  }


  var initSelect2Func = function() {
    selects = $('.select2obj').each(function() {
      var obj = $(this);
      if (0 == obj.data('inited')) {
        var urlList = obj.data('list-source');
        var urlItem = obj.data('item-source');
        obj.data('inited', 1);
        obj.select2({
          placeholder: "Επιλέξτε...",
          minimumInputLength: 1,
          ajax: {// instead of writing the function to execute the request we use Select2's convenient helper
            url: urlList,
            dataType: 'jsonp',
            data: function(term, page) {
              return {
                q: term // search term
              };
            },
            results: function(data, page) { // parse the results into the format expected by Select2.
              // since we are using custom formatting functions we do not need to alter remote JSON data
              return {
                results: data.movies
              };
            }
          },
          initSelection: function(element, callback) {
            // the input tag has a value attribute preloaded that points to a preselected movie's id
            // this function resolves that id attribute to an object that select2 can render
            // using its formatResult renderer - that way the movie name is shown preselected
            var id = $(element).val();
            if (id !== "") {
              $.ajax(urlItem, {
                data: {
                  q: id
                },
                dataType: "jsonp"
              }).done(function(data) {
                callback(data);
              });
            }
          },
          formatResult: select2ResultsFormatFunc, // omitted for brevity, see the source of this page
          formatSelection: select2SelectionFormatFunc, // omitted for brevity, see the source of this page
          dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
          escapeMarkup: function(m) {
            return m;
          } // we do not want to escape markup since we are displaying html in results
        });
        clearBtn = obj.parent().parent().find(".select2-cleaner").click(function() {
          obj.select2("val", "");
          return false;
        });
      }
    });

  };

  var articlePickerSelectAll = function() {
    var set = $('#notApplyArticlesModal #not_apply_articles input[type=checkbox]');
    set.prop('checked', true);
    jQuery.uniform.update(set);
    return false;
  };

  var articlePickerDeSelectAll = function() {
    var set = $('#notApplyArticlesModal #not_apply_articles input[type=checkbox]');
    set.prop('checked', false);
    jQuery.uniform.update(set);
    return false;
  };
  var countSelectedBoxSelections = function(select) {
    var options = $('option', select);
    var cnt = 0;
    for (i = 0; i < options.length; i++) {
      if ($(options[i]).prop('selected')) {
        cnt++;
      }
    }
    return cnt;
  };
  var countCheckedSelections = function(container) {
    var cnt = 0;
    $('input[type=checkbox]', container).each(function() {
      if ($(this).prop('checked'))
        cnt++;
    });
    return cnt;
  }

  var initMultiSelectPickFunc = function() {
    $("#notApplyArticlesModal #select_all").click(articlePickerSelectAll);
    $("#notApplyArticlesModal #clear_all").click(articlePickerDeSelectAll);
    $(".multiselectpick").each(function() {
      var obj = $(this);
      if (0 == obj.data('inited')) {
        obj.data('inited', 1);
        var p0 = obj.parent();
        obj.hide();
        p0.append('<span class="info" style="margin: 0 8px 0 8px">Επιλεγμένα ' + countSelectedBoxSelections(obj) + '</span><a href="#notApplyArticlesModal" data-toggle="modal" class="btn default" >...</a>');
        $('.btn', p0).click(function() {
          var p = $(this).parent();
          var container = $("#notApplyArticlesModal #not_apply_articles");
          var options = $('option', obj);
          container.text('');
          if (options.length) {
            var content = '';
            for (i = 0; i < options.length; i++) {
              var opt = $(options[i]);
              content = content + '<label class="checkbox line"><input type="checkbox" class="checkboxess" style="margin-left:0" value="' + opt.val() + '" ' + (opt.prop('selected') ? 'checked="checked"' : '') + ' />' + opt.text() + '</label>';
            }
            content = content + '';
            container.append(content);
            $('.checkboxess').uniform();
          }

          $('#notApplyArticlesModal #submit').unbind('click').click(function() {
            var container = $("#notApplyArticlesModal #not_apply_articles");
            $('.info', p).text('Επιλεγμένα ' + countCheckedSelections(container));
            options.each(function() {
              $(this).prop('selected', false);
            });
            $('input[type=checkbox]', container).each(function() {
              if ($(this).prop('checked')) {
                for(i=0; i<options.length; i++) {
                  if( $(options[i]).val() === $(this).val() ) {
                    $(options[i]).prop('selected', true);
                    break;
                  }
                }
              }
            });
            $('#notApplyArticlesModal').modal('hide');
          });
        });
      }
    });
  };

  initSelect2Func();
  initMultiSelectPickFunc();


  $('div.collectionholder').each(function() {
    var collectionHolder = $(this);
    collectionHolder.data('index', collectionHolder.find(':input').length);

    var addFunc = function add(inBtn) {
      var btn = $(inBtn);
      var collHolder = btn.closest('div.collectionholder');
      // Get the data-prototype explained earlier
      var prototype = collectionHolder.data('prototype');

      // get the new index
      var index = collectionHolder.data('index');

      // Replace '__name__' in the prototype's HTML to
      // instead be a number based on how many items we have
      var newForm = prototype.replace(/__name__/g, index);

      // increase the index with one for the next item
      collectionHolder.data('index', index + 1);
      $('table tbody', collectionHolder).append(newForm);
      initSelect2Func();
      initMultiSelectPickFunc();
    };

    $('a.remove', collectionHolder).live('click', function(event) {
      event.preventDefault();
      $(this).closest('tr').remove();
      if ($('table tbody tr', collectionHolder).length === 0) {
        addFunc();
      }
    });

    $('a.add', collectionHolder).live('click', function(event) {
      event.preventDefault();
      addFunc(this);
    });

  });

});