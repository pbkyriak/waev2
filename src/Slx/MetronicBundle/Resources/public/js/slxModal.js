
if (!window.jsLinkConfirm)
  var jsLinkConfirm = new Object();


jsLinkConfirm.Methods = {
  trg_url: '',
  show: function(msg, trg_url) {
    this.trg_url = trg_url;
    var odialog = $('<div title="ΕΠΙΒΕΒΑΙΩΣΗ" class="hide"><p><span class="icon fa-warning-sign"></span>' + msg + '</p></div>');
    //odialog.html();
    odialog.dialog({
      dialogClass: 'ui-dialog-green',
      resizable: false,
      height: 210,
      modal: true,
      buttons: [
        {
          'class': 'btn red',
          "text": "Ναι",
          click: function() {
            $(this).dialog('close');
            jsLinkConfirm.pressedYes();
          }
        },
        {
          'class': 'btn',
          "text": "Όχι",
          click: function() {
            $(this).dialog("close");
          }
        }
      ]

    });

    return false;
  },
  pressedYes: function() {
    window.location = this.trg_url;
  },
  pressedNo: function() {

  }

};

jQuery.extend(jsLinkConfirm, jsLinkConfirm.Methods);

