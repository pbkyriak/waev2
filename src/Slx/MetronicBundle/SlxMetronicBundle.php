<?php

namespace Slx\MetronicBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Slx\MetronicBundle\DependencyInjection\Compiler\MenuBuilderPass;

class SlxMetronicBundle extends Bundle
{
    public function build(\Symfony\Component\DependencyInjection\ContainerBuilder $container)
    {
        $container->addCompilerPass(new MenuBuilderPass());
    }
}
