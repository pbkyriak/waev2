<?php

namespace Slx\MetronicBundle\Twig;

/**
 * Description of SlxExtension
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxExtension extends \Twig_Extension
{

    public function getFunctions()
    {
        return array(
            'checkbox_choicelabel' => new \Twig_Function_Node(
                'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode',
                array('is_safe' => array('html'))
            ),
            'checkbox_choicelabelend' => new \Twig_Function_Node(
                'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode',
                array('is_safe' => array('html'))
            ),
            'checkbox_choicewidget' => new \Twig_Function_Node(
                'Symfony\Bridge\Twig\Node\SearchAndRenderBlockNode',
                array('is_safe' => array('html'))
            ),
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('paginator_header_class',
                array($this, 'paginatorHeaderClassFilter')),
            new \Twig_SimpleFilter('boolToText', array($this, 'boolToText')),
            new \Twig_SimpleFilter('percTo', array($this, 'percTo')),
        );
    }

    public function paginatorHeaderClassFilter($pagination, $fieldName)
    {
        $class = 'sorting';
        $params = $pagination->getParams();
        $direction = '';
        if (in_array('asc', $params))
            $direction = 'asc';
        if (in_array('desc', $params))
            $direction = 'desc';
        if ($pagination->isSorted($fieldName))
            $class = 'sorting_' . $direction;
        return $class;
    }

    public function boolToText($bool)
    {
        if ($bool)
            return 'Ναι';
        else
            return 'Όχι';
    }

    public function percTo($part, $whole)
    {
        return round(($part / $whole * 100), 1);
    }

    public function getName()
    {
        return 'slx_extension';
    }

}
