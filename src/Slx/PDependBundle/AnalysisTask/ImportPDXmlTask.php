<?php

namespace Slx\PDependBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\PDependBundle\DataReader\PDependXmlReader;
use Slx\GitMinerBundle\Entity\ProjectTag;
/**
 * Imports PDepend output. If something goes wrong it drops the tag. Non critical to abort run
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ImportPDXmlTask extends AbstractTagTask
{
    public function execute()
    {
        $out = true;
        $importer = new PDependXmlReader($this->em, $this->tag->getId());
        try {
            $this->em->getConnection()->query("SET autocommit=0;"); 
            $importer->loadPDFile($this->tag->getPdependXmlFilename());
            $this->setTagStatus(ProjectTag::STATUS_DP_IMPORT);
            $this->em->getConnection()->query("COMMIT;");
        } catch (\Exception $ex) {
            // some logging here
            $this->isCriticalFail=false;
            $this->em->getConnection()->query("ROLLBACK;");
            $out = false;
        }
        $this->em->getConnection()->query("SET autocommit=1;"); 
        return $out;
    }

    public function onFail()
    {
        return $this->dropTag($this->tag);
    }
    
    public function getTaskName()
    {
        return 'Import PDepend xml output';
    }
}
