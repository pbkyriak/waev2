<?php

namespace Slx\PDependBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
/**
 * PDepend tag's source code. If something goes wrong it drops the tag. Non critical to abort run
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class PDependTask extends AbstractTagTask
{

    private $cacheFolder = '';
    /**
     * 
     * @param string $f the folder where pdepends stores its cache, so we can clean up to save disk space
     */
    public function setCacheFolder($f) {
        $this->cacheFolder = $f;
        //add trailing slash if missing
        if(substr($this->cacheFolder, strlen($this->cacheFolder)-1,1)!='/') {
            $this->cacheFolder .= '/';
        }
    }
    public function execute()
    {
        // "pdepend --configuration=%s --summary-xml=%s %s",
        $command = sprintf( 
            "pdepend --summary-xml=%s --suffix=%s %s",
            $this->tag->getPdependXmlFilename(),
            $this->tag->getProject()->getFileExtensions(),
            $this->tag->getSourceCodePath()
        );
        printf("%s\n", $command);
        $command2 = sprintf("rm -rf %s*", $this->cacheFolder);
        try {
            exec($command);
            if( strlen($this->cacheFolder)>10 ) {
                exec($command2);
            }
        } catch (\Exception $ex) {
            return false;
        }
        if (file_exists($this->tag->getPdependXmlFilename())) {
            $this->setTagStatus(ProjectTag::STATUS_DP_DONE);
        } else {
            // some logging here
            $this->isCriticalFail = false;
            return false;
        }

        return true;
    }

    public function onFail()
    {
        return $this->dropTag($this->tag);
    }
    
    public function getTaskName()
    {
        return 'PDepent source code';
    }
}
