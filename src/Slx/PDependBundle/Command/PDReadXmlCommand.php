<?php

namespace Slx\PDependBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\PDependBundle\DataReader\PDependXmlReader;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * @todo drop this file
 */

class PDReadXmlCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('waev:import:pdxml')
            ->setDescription('Imports pdepent xml output')
            ->addArgument('tagid', InputArgument::REQUIRED, 'Tag Id to attach the import?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$tag) {
            throw new Exception(spinrtf('ProjectTag with id %s not found!',
                $tagId));
        }

        $importer = new PDependXmlReader($em, $tagId);
        $output->writeln('importing file '. $tag->getPdependXmlFilename());
        //try {
            $importer->loadPDFile($tag->getPdependXmlFilename());
            $tag->setStatus(ProjectTag::STATUS_DP_IMPORT);
            $em->persist($tag);
            $em->flush();
        //}
        //catch(\Exception $ex) {
        //    $output->writeln($ex);
        //}
        // humanize execution time ;)
        $fTime = time()-$iTime;
        $sec = $fTime % 60;
        $min = ($fTime-$sec)/60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }
}