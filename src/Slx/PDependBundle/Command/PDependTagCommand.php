<?php

namespace Slx\PDependBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * @todo drop this file
 */
class PDependTagCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('waev:pd:tag')
            ->setDescription("PDepend a tag's source code")
            ->addArgument('tagid', InputArgument::REQUIRED,
                'Project Tag Id to pdepend source code?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $iTime = time();
        $tagId = $input->getArgument('tagid');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tag = $em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$tag) {
            throw new Exception(spinrtf('ProjectTag with id %s not found!',
                $tagId));
        }
        $command = sprintf("pdepend --summary-xml=%s %s", $tag->getPdependXmlFilename(), $tag->getSourceCodePath());
        exec($command);
        if(file_exists($tag->getPdependXmlFilename())) {
            $output->writeln("PDepend done");
            $tag->setStatus(ProjectTag::STATUS_DP_DONE);
            $em->persist($tag);
            $em->flush();
        }
        else {
            $output->writeln("PDepend failed");
        }
        // humanize execution time ;)
        $fTime = time() - $iTime;
        $sec = $fTime % 60;
        $min = ($fTime - $sec) / 60;
        $output->writeln(sprintf('finished in %s min %s sec', $min, $sec));
    }

}

