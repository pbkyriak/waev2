<?php

namespace Slx\PDependBundle\DataReader;

use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectCodeConstruct;

/**
 * Parses pDepend xml output and loads to database
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class PDependXmlReader
{
    /*
      <metrics generated="2013-08-31T12:17:43" pdepend="1.1.1"
     * ahh="0.09375" andc="0.11111111111111" 
     * calls="4045" ccn="4364" ccn2="4816" cloc="2674" 
     * clsa="0" clsc="36" eloc="20733" fanout="40" 
     * leafs="33" lloc="14686" loc="27415" maxDIT="1" 
     * ncloc="24741" noc="36" nof="715" noi="0" nom="387" nop="2" roots="3">

     */

    private $tagMetrics;
    private $fileMetrics;
    private $classMetrics;
    private $functionMetrics;
    private $methodMetrics;
    private $ccMetrics;
    /**
     *
     * @var EntityManager
     */
    private $em;
    /**
     *
     * @var ProjectTag
     */
    private $tag;
    private $fileList = array();
    private $lastFile = null;

    public function __construct(EntityManager $em, $tagId)
    {
        $this->em = $em;
        $this->tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$this->tag) {
            throw new Exception(sprintf('Project tag with id=%s not found',
                $tagId));
        }
        // cleanup prev data import for this tag
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectFile a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $tagId)
            ->execute();
        // maybe those will have to move to a better location
        $this->tagMetrics = array(
            "ahh",
            "andc",
            "calls",
            "ccn",
            "ccn2",
            "cloc",
            "clsa",
            "clsc",
            "eloc",
            "fanout",
            "leafs",
            "lloc",
            "loc",
            "maxDIT",
            "ncloc",
            "noc",
            "nof",
            "noi",
            "nom",
            "nop",
            "roots",
        );
        $this->fileMetrics = array(
            'cloc', 'eloc', 'lloc', 'loc', 'ncloc',
        );
        $this->classMetrics = array(
            "ca",
            "cbo",
            "ce",
            "cis",
            "cloc",
            "cr",
            "csz",
            "dit",
            "eloc",
            "impl",
            "lloc",
            "loc",
            "ncloc",
            "noam",
            "nocc",
            "nom",
            "noom",
            "npm",
            "rcr",
            "vars",
            "varsi",
            "varsnp",
            "wmc",
            "wmci",
            "wmcnp",
        );

        $this->functionMetrics = array(
            "ccn", "ccn2", "cloc", "eloc", "lloc", "loc", "ncloc", "npath"
        );

        $this->methodMetrics = array(
            "ccn", "ccn2", "cloc", "eloc", "lloc", "loc", "ncloc", "npath"
        );
        
        $this->ccMetrics = array_merge($this->classMetrics, $this->functionMetrics, $this->methodMetrics);
    }

    public function loadPDFile($fn)
    {

        if (file_exists($fn)) {
            $xmlReader = new \XMLReader();
            $xmlReader->open($fn);
            while ($xmlReader->read()) {
                if ($xmlReader->nodeType == \XMLReader::ELEMENT) {
                    if ($xmlReader->localName == 'metrics') {
                        $this->updateObjectMetrics($xmlReader, $this->tag,
                            $this->tagMetrics);
                        $this->em->persist($this->tag);
                    }
                    if ($xmlReader->localName == 'files') {
                        // get files and their metrics
                        $this->readFilesInfo($xmlReader);
                    }
                    if ($xmlReader->localName == 'class') {
                        $this->readClassInfo($xmlReader);
                    }
                    if ($xmlReader->localName == 'function') {
                        $this->readFunctionInfo($xmlReader);
                    }
                }
            }
            $xmlReader->close();
        } else {
            throw new \Exception('File not found');
        }
    }

    private function updateObjectMetrics(\XMLReader $xmlReader, $obj, $metrics)
    {
        foreach ($metrics as $metric) {
            $attrV = $xmlReader->getAttribute($metric);
            if ($attrV) {
                $setter = "set" . ucfirst($metric);
                if (method_exists($obj, $setter)) {
                    call_user_func_array(array($obj, $setter), array($attrV));
                }
            }
        }
    }

    private function updateDataMetrics(\XMLReader $xmlReader, $data, $metrics)
    {
        foreach ($metrics as $metric) {
            $attrV = $xmlReader->getAttribute($metric);
            if ($attrV) {
               $data[$metric] = (float)$attrV;
            }
        }
        return $data;
    }

    private function newCCMetrics() {
        $out = array_fill_keys($this->ccMetrics, 0);
        return $out;
    }
    
    private function cleanFilename($fname) {
        return str_replace($this->tag->getSourceCodePath(),'', $fname);
    }
    
    private function readFilesInfo(\XMLReader $xmlReader)
    {
        while ($xmlReader->read()) {
            if ($xmlReader->nodeType == \XMLReader::ELEMENT) {
                if ($xmlReader->localName == 'file') {
                    $data = array();
                    $fullpathFname = $xmlReader->getAttribute('name');
                    $data['fname']=$this->cleanFilename($fullpathFname);
                    $data['dir_depth'] = substr_count($data['fname'],'/');
                    $data['project_tag_id']=$this->tag->getId();
                    $data = $this->updateDataMetrics($xmlReader, $data, $this->fileMetrics);
                    $data['sha1_hash'] = sha1_file($fullpathFname);
                    $this->em->getConnection()->insert('project_file', $data);
                    $fileId = $this->em->getConnection()->lastInsertId();
                    $this->fileList[$data['fname']] = $fileId;
                }
            }
            if ($xmlReader->nodeType == \XMLReader::END_ELEMENT && $xmlReader->localName == 'files') {
                break;
            }
        }
    }

    private function readClassInfo(\XMLReader $xmlReader)
    {
        $data = $this->newCCMetrics();
        $data['cname']=$xmlReader->getAttribute('name');
        $data['ctype']='class';
        $data['project_tag_id']=$this->tag->getId();
        $data = $this->updateDataMetrics($xmlReader, $data, $this->classMetrics);
        $classFileId = 0;
        $classId = 0;
        // look for class's file
        while ($xmlReader->read()) {
            if ($xmlReader->localName == 'file') {
                $classFileId = $this->getFileIdByName($this->cleanFilename($xmlReader->getAttribute('name')));
                if ($classFileId) {
                    $data['project_file_id']=$classFileId;
                    $this->em->getConnection()->insert('project_code_construct', $data);
                    $classId = $this->em->getConnection()->lastInsertId();
                }
            }
            if ($xmlReader->localName == 'method') {
                if ($classFileId) {
                    $mdata = $this->newCCMetrics();
                    $mdata['cname']= sprintf("%s::%s",$data['cname'], $xmlReader->getAttribute('name'));
                    $mdata['ctype'] = 'method';
                    $mdata['project_tag_id'] = $this->tag->getId();
                    $mdata = $this->updateDataMetrics($xmlReader, $mdata, $this->methodMetrics);
                    $mdata['project_file_id']=$classFileId;
                    $this->em->getConnection()->insert('project_code_construct', $mdata);
                    //$methodId = $this->em->getConnection()->lastInsertId();
                }
            }
            if ($xmlReader->nodeType == \XMLReader::END_ELEMENT && $xmlReader->localName == 'class') {
                break;
            }
        }
    }

    private function readFunctionInfo(\XmlReader $xmlReader)
    {
        $data = $this->newCCMetrics();
        $data['cname']=$xmlReader->getAttribute('name');
        $data['ctype']='function';
        $data['project_tag_id']=$this->tag->getId();
        $data = $this->updateDataMetrics($xmlReader, $data, $this->functionMetrics);
        // look for functions's file
        while ($xmlReader->read()) {
            if ($xmlReader->localName == 'file') {
                $fileId = $this->getFileIdByName($this->cleanFilename($xmlReader->getAttribute('name')));
                if ($fileId) {
                    $data['project_file_id']=$fileId;
                }
                break;
            }
            if ($xmlReader->nodeType == \XMLReader::END_ELEMENT && $xmlReader->localName == 'function') {
                break;
            }
        }
        $this->em->getConnection()->insert('project_code_construct', $data);
    }

    private function getFileIdByName($fname)
    {
        $fid = isset($this->fileList[$fname]) ? $this->fileList[$fname] : 0;
        return $fid;
    }

}

