<?php
namespace Slx\ParserBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
/**
 * Description of gridfsTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 30 Μαρ 2016
 */
class gridfsTest extends WebTestCase {
    
    private $container;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
    }

    public function atestInsert() {
        $mgrid = $this->container->get('waev.gridfs');
        $bucket = $mgrid->setup(1,1);
        $metadata = ['filename'=> 'test01'];
        $id = $mgrid->insert('test01', '1234567890');
        $this->assertNotEquals(0, $id);
        $this->assertEquals(1, $bucket->getCollectionsWrapper()->getFilesCollection()->count());
        $bucket->delete($id);
    }

    public function testLoad() {
        $mgrid = $this->container->get('waev.gridfs');
        $bucket = $mgrid->setup(1,1);
        $id = $mgrid->insert('test01', '1234567890');
        $content = $mgrid->load('test01');
        $this->assertEquals("1234567890", $content);
        $bucket->delete($id);
    }
    
    public function testNotFound() {
        $mgrid = $this->container->get('waev.gridfs');
        $bucket = $mgrid->setup(1,1);
        $content = $mgrid->load('test01');
        $this->assertEquals(false, $content);
    }

    public function testDelete() {
        $mgrid = $this->container->get('waev.gridfs');
        $bucket = $mgrid->setup(1,1);
        $id = $mgrid->insert('test01', '1234567890');
        $aff1 = $mgrid->delete('test01');
        $aff2 = $mgrid->delete('test02');
        $this->assertEquals(1, $aff1);
        $this->assertEquals(0, $aff2);
    }
}
