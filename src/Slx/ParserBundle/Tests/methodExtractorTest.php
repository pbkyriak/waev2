<?php

namespace Slx\ParserBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Description of methodExtractorTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 23, 2016
 */
class methodExtractorTest extends WebTestCase {
    
    private $container;
    
    public function setUp() {
        $client = static::createClient();
        $this->container = $client->getContainer();
    }

    public function testGetFileAst() {
        $extractor = $this->container->get('waev.MethodASTExtractor');
        $em = $this->container->get('doctrine.orm.entity_manager');
        $projectTag = $em->getRepository("SlxGitMinerBundle:ProjectTag")->find(4900);
        $fileId = 2195069;
        $method = '\JInstallerAdapterPlugin::install';
        $ast = $extractor->getMethodAST($projectTag, $fileId, $method);
        if( $ast ) {
            $nodeCount = count($ast);
        }
        else {
            $nodeCount = 0;
        }
        $this->assertEquals(47, $nodeCount);
    }
}
