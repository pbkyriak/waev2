<?php

namespace Slx\ParserBundle\Util;

use Slx\ReflectionBundle\Reflection\Traverser;
use Slx\TypeAnalysisBundle\CodeContext\AbstractCodeContextVisitor;
/**
 * Description of MethodASTExtractor
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since Apr 23, 2016
 */
class MethodASTExtractor extends AbstractCodeContextVisitor {

    /** @var \Slx\ParserBundle\Util\SourceCodeHelper */
    protected $scHelper=null;
    private $qMethod;
    private $qAST = null;
    private $ast = null;
    private $fileId = 0;
    /**
     * Called from Dependency injection container to set sourceCodeHelper
     * setuped with mongoGridFs connection
     * 
     * @param type $scHelper
     */
    public function setSourceCodeHelper($scHelper) {
        $this->scHelper = $scHelper;
    }

    /**
     * 
     * @param type $projectTag
     * @param type $file        array expects $file['id'] to be present
     * @param type $method
     */
    public function getMethodAST($projectTag, $fileId, $method) {
        $out = null;
        if( $this->scHelper->getProjectTagId()!=$projectTag->getId() ) {
            $this->scHelper->setProjectTag($projectTag);
        }
        if( $this->fileId!=$fileId ) {
            $this->ast = $this->scHelper->getFileAst(array('id'=>$fileId));
            $this->fileId = $fileId;
        }
        $this->qMethod = $method;
        $this->qAST = null;
        if( $this->ast ) {
            $this->configureVisitor('fname', true);
            $traverser = new Traverser();
            $traverser->addVisitor($this);
            $traverser->traverseAst($this->ast);
            if( $this->qAST ) {
                $out = $this->qAST;
            }
        }
        return $out;
    }
    
    
    protected function cContextDown(\PHPParser_Node $node) {
        
    }

    protected function cContextUp(\PHPParser_Node $node) {
        if( $this->getContext()->getFQName()==$this->qMethod ) {
            $this->qAST = $node->stmts;
        }
    }

    protected function cEnterNode(\PHPParser_Node $node) {
        
    }

    protected function cLeaveNode(\PHPParser_Node $node) {
        
    }


}
