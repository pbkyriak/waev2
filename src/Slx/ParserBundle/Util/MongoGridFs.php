<?php
namespace Slx\ParserBundle\Util;

//use Mongo;
use MongoDB\GridFS\Bucket;
/**
 * Description of MongoConnection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class MongoGridFs {
    
    /* @var $bucket \MongoDB\GridFS\Bucket */
    private $bucket;
    private $host;
    private $dbname;
    private $dbprefix;
    private $projectId;
    private $tagId;
    private $manager;
    
    public function __construct($host, $dbname) {
        $this->host=$host;
        $this->dbprefix=$dbname;
        $this->manager = null;
        $this->bucket=null;
    }
    
    private function getManager() {
        if( !$this->manager ) {
            $this->manager = new \MongoDB\Driver\Manager("mongodb://".$this->host);
        }
        return $this->manager;
    }
    
    /**
     * 
     * @return \MongoGridFs
     */
    public function setup($projectId, $tagId) {
        if( !$this->bucket || !($projectId==$this->projectId && $tagId==$this->tagId) ) {
            $this->dbname = $this->dbprefix.$projectId.'_'.$tagId;
            $this->projectId = $projectId;
            $this->tagId = $tagId;
            $this->bucket = new Bucket($this->getManager(), $this->dbname);
            //printf("mongodb = %s\n", $this->bucket->getDatabaseName());
        }
        return $this->bucket;
    }
    
    public function insert($filename, $content) {
        $bytes = gzencode($content);
        $id = $this->bucket->uploadFromStream($filename, $this->generateStream($bytes));
        return $id;
    }
    
    public function load($filename) {
        $content = false;
        try {
            $bytes = stream_get_contents($this->bucket->openDownloadStreamByName($filename));
            $content = gzdecode($bytes);
        }
        catch(\MongoDB\Exception\GridFSFileNotFoundException $e) {
            $content = false;
        }
        catch(\Exception $ex) {
            $content = false;
        }
        return $content;
    }
    
    public function delete($filename) {
        $affected = 0;
        $cursor = $this->bucket->find(["filename" => $filename]);
        foreach($cursor as $file){
            $this->bucket->delete($file->_id);
            $affected++;
        }
        return $affected;
    }
    
    public function fileExists($filename) {
        $out = false;
        $cursor = $this->bucket->find(["filename" => $filename]);
        foreach($cursor as $file){
            $out = true;
            break;
        }
        return $out;
    }
    
    private function generateStream($input) {
        $stream = fopen('php://temp', 'w+');
        fwrite($stream, $input);
        rewind($stream);
        return $stream;
    }

}
