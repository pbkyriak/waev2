<?php

namespace Slx\ParserBundle\Util;

use Symfony\Component\Finder\Finder;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of SourceCodeHelper
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class SourceCodeHelper {
    /* @var $tag ProjectTag */
    private $tag;
    private $tagId = 0;
    /* @var $grid \MongoGridFS */
    private $mgrid;
    private $astFilePrefix;
    
    public function __construct($mgrid) {
        $this->mgrid = $mgrid;
    }

    public function setProjectTag(ProjectTag $tag) {
        $this->tag = $tag;
        $this->astFilePrefix = sprintf("ast-%s-%s-", $this->tag->getProject()->getId(), $this->tag->getId());        
        $this->mgrid->setup($this->tag->getProject()->getId(),$this->tag->getId());
    }
    
    public function getProjectTagId() {
        $out = 0;
        if( $this->tag ) {
            $out = $this->tag->getId();
        }
        return $out;
    }
    /**
     * Reads files from file system
     * @return array
     */
    public function getTagSourceFiles() {
        $codePath = $this->tag->getSourceCodePath();
        $exts = explode(',', $this->tag->getProject()->getFileExtensions());
        if (!$exts) {
            $exts = array('php');
        }
        $finder = new Finder();
        $finder->in($codePath);
        foreach ($exts as $ext) {
            $finder->name(sprintf('*.%s', $ext));
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRealpath();
        }

        return $files;
    }

    /**
     * Reads project_file db table
     * @return array
     */
    public function getTagSourceFilesDB($em, $onlyGood = true, $excludeSections = array()) {
        $data = array();
        $sql = "SELECT id,fname FROM project_file Where project_tag_id=:tid ";
        if ($onlyGood) {
            $sql .= "and parser='good'";
        }
        if( count($excludeSections) ) {
            // not in ('test','test')
            $sql .= " and section not in ('".implode("','", $excludeSections)."')";
        }
        $params = array(
            'tid' => $this->tag->getId()
        );
        $stmt = $em
                ->getConnection()
                ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        array_walk($data, function(&$item) {
            $item['fname'] = $this->tag->getSourceCodePath() . $item['fname'];
        });
        return $data;
    }

    /**
     * Get the xml file with the ast for the $file.
     * @param array $file   expects index 'id' with file id
     * @return string
     */
    public function getFileAstXmlFn($file) {
        $out = sprintf("%s/%s/%s.xml", $this->tag->getTagWorkingFolder(), 'ast', $file['id']);
        if (!file_exists(dirname($out))) {
            mkdir(dirname($out), 0777, true);
        }
        return $out;
    }

    /**
     * Load and unserialize the AST of the $file
     * @param array $file   expects index 'id' with file id
     * @return array
     */
    public function getFileAst($file) {
        $ast = null;
        try {
            $content = $this->getAstFileFromStore($file['id']);
            if (!empty($content)) {
                $unserializer = new \PHPParser_Unserializer_XML();
                $ast = $unserializer->unserialize($content);
            }
            else {
                printf("file not found %s\n",$file['id']);
            }
        } catch (\PHPParser_Error $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\DomainException $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\InvalidArgumentException $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\Exception $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (Exception $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        }
        return $ast;
    }

    /**
     * 
     * @param integer $fileId
     * @param boolean $checkOnly
     * @return string
     */
    public function getAstFileFromStore($fileId) {
        $out = false;
        //printf("filename=%s\n",$this->astFilePrefix.$fileId);
        $file = $this->mgrid->load($this->astFilePrefix.$fileId);
        if( $file ) {
            $out = $file;
        }
        else {
            //printf("File not found in store : %s \n", $this->astFilePrefix.$fileId);
            $out = false;
        }
        return $out;
    }

    /**
     * 
     * @param integer $fileId
     * @param string $data
     * @return boolean
     */
    public function setAstFileToStore($fileId, $data) {
        $filename=$this->astFilePrefix.$fileId;
        try {
            if( $this->mgrid->fileExists($filename) ) {
                $this->mgrid->delete($filename);
            }
            $id = $this->mgrid->insert($filename, $data);
            //printf("stored ast fileId=%s _id=%s id=%s\n", $fileId, $metadata['filename'], $id);
        }
        catch(\Exception $ex) {
            printf("Exception storing to gridFs %s\n", $ex->getMessage());
            $id= false;
        }
        return $id;
    }

}
