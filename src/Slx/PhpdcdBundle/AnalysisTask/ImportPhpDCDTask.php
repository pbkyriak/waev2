<?php

namespace Slx\PhpdcdBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\PhpdcdBundle\DataReader\PhpDCDReader;
use Slx\GitMinerBundle\Entity\ProjectTag;
/**
 * Imports phpDCD output. If something goes wrong it drops the tag. Non critical to abort run
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ImportPhpDCDTask extends AbstractTagTask
{
    public function execute()
    {
        $importer = new PhpDCDReader($this->em, $this->tag->getId());
        try {
            $importer->loadFile($this->tag->getPhpDCDFilename());
            $this->setTagStatus(ProjectTag::STATUS_DCD_IMPORT);
        } catch (\Exception $ex) {
            // some logging here
            $this->isCriticalFail=false;
            return false;
        }
        return true;
    }

    public function onFail()
    {
        return $this->dropTag($this->tag);
    }
    
    public function getTaskName()
    {
        return 'Import phpDCD output';
    }
    
    
}
