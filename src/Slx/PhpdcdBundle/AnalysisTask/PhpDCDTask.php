<?php

namespace Slx\PhpdcdBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
/**
 * PDepend tag's source code. If something goes wrong it drops the tag. Non critical to abort run
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class PhpDCDTask extends AbstractTagTask
{

    public function execute()
    {
        $command = sprintf( 
            "phpdcd %s %s > %s",
            $this->getExcludes(),
            $this->tag->getSourceCodePath(),
            $this->tag->getPhpDCDFilename()
            
        );
        printf("%s\n", $command);
        
        try {
            exec($command);
        
        } catch (\Exception $ex) {
            return false;
        }
        if (file_exists($this->tag->getPhpDCDFilename())) {
            $this->setTagStatus(ProjectTag::STATUS_DCD_DONE);
        } else {
            // some logging here
            $this->isCriticalFail = false;
            return false;
        }

        return true;
    }

    private function getExcludes() {
        $tests = $this->tag->getProject()->getTests();
        $namesAr = explode("\n", str_replace("\r", "", $tests));
        $out = '';
        foreach($namesAr as $name) {
            $name = preg_replace(array('/^\//', '/\/%$/', '/^%\//'),'', $name);
            $out .= ' --exclude '.$name;
        }
        return $out;
    }
    
    public function onFail()
    {
        return true;
    }
    
    public function getTaskName()
    {
        return 'PhpDCD source code';
    }
}
