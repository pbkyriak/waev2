<?php

namespace Slx\PhpdcdBundle\DataReader;

use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\ProjectFile;

/**
 * Description of PhpDCDReader
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class PhpDCDReader
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    /**
     *
     * @var ProjectTag
     */
    private $tag;
    private $fileList = array();
    private $total;

    public function __construct(EntityManager $em, $tagId)
    {
        $this->em = $em;
        $this->tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if (!$this->tag) {
            throw new Exception(sprintf('Project tag with id=%s not found',
                $tagId));
        }
        $this->loadTagFiles();
    }

    private function loadTagFiles()
    {
        $oFiles = $this->tag->getProjectFiles();
        foreach ($oFiles as $file) {
            $this->fileList[$file->getFname()] = $file->getId();
        }
    }

    public function loadFile($fn)
    {

        if (file_exists($fn)) {
            $handle = fopen($fn, "r");
            if ($handle) {
                $this->resetDeadFlag();
                while ($line = fgets($handle, 2000)) {
                    if(substr($line, 0, 4)=='  - ') {
                        $line2 = fgets($handle, 2000);
                        $this->processLine($line, $line2);
                    }
                }
                fclose($handle);
            }
        } else {
            throw new Exception('File not found');
        }
    }

    private function resetDeadFlag() {
        $this->em->getConnection()->update(
            'project_code_construct', 
            array('dead'=>0), 
            array('project_tag_id'=>$this->tag->getId())
        );

    }
    /**
     * 
     * @param array $data raw data from phploc file
     */
    private function processLine($line1, $line2)
    {
        $fname = $this->stripFname($line2);
        $cname = $this->stripCname($line1);
        
            $this->em->getConnection()->update(
                'project_code_construct', 
                array('dead'=>1), 
                array('project_tag_id'=>$this->tag->getId(), 'cname'=>$cname)
            );
/*        
        if (isset($this->fileList[$fname])) {
            $this->em->getConnection()->update(
                'project_code_construct', 
                array('dead'=>1), 
                array('project_tag_id'=>$this->tag->getId(), 'cname'=>$cname, 'project_file_id'=>$this->fileList[$fname])
            );
        } 
        else {
            printf("BAD tid:%s f:%s c:%s-\n", $this->tag->getId(), $fname, $cname);
        }
 * 
 */
    }

    private function stripFname($line) {
        $out = $line;
        if(preg_match('/declared in (.+):/', $line, $matches) ) {
            $out = '/'.$matches[1];
        }
        return $out;
    }
    
    private function stripCname($line) {
        $out = $line;
        if(preg_match('/- (.+)\(\)/', $line, $matches) ) {
            $out = $matches[1];
        }
        return $out;
    }
}
