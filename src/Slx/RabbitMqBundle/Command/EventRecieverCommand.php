<?php

namespace Slx\RabbitMqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\RabbitMqBundle\Rabbitmq\Connection;
use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use PhpAmqpLib\Message\AMQPMessage;
use Slx\RabbitMqBundle\Event\Transformer\MqMessageToJobEvent;
/**
 * Description of EventRecieverCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class EventRecieverCommand extends ContainerAwareCommand {
    /** @var \Symfony\Component\Console\Output\OutputInterface */
    private $output;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    private $mq;
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    /** @var \Slx\RabbitMqBundle\Event\Transformer\MqMessageToJobEvent */
    private $trans;
    
    protected function configure()
    {
        $this
            ->setName('waev:rabbitmq:eventreciever')
            ->setDescription("Recieves event messages from rabbitmq")
            ->addArgument('queue', InputArgument::REQUIRED, 'Queue name to listen' )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->mq = $this->getContainer()->get('slx_rabbit_mq.rabbitmq');

        $queue = $input->getArgument('queue');
        $validQueueNames = $this->mq->getQueueKeys();
        if( !in_array($queue, $validQueueNames)) {
            $output->writeln(sprintf('Queue name can be %s', implode(' or ', $validQueueNames)));
            return false;
        }
        
        $this->trans = new MqMessageToJobEvent();
        $this->mq->setConsumer($queue, array($this, 'onMessage'));
        $this->mq->waitForMessages();
        $this->mq->close();
    }
    
    public function onMessage(AMQPMessage $msg) {
        $this->trans->transform($msg);
        printf("%s\n", $this->trans->dumpEvent());
        $eventDispatcher = $this->getContainer()->get('event_dispatcher');
        $eventDispatcher->dispatch($this->trans->getEventName(), $this->trans->getEvent());
        
        $this->mq->messageAck($msg);
    }

}
