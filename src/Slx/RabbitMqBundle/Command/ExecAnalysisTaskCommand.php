<?php

namespace Slx\RabbitMqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Description of ExecAnalysisTaskCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ExecAnalysisTaskCommand  extends ContainerAwareCommand {
    private $output;
    private $em;
    private $factory;
    
    protected function configure()
    {
        $this
            ->setName('waev:exec:analysisTask')
            ->setDescription("Executes an analysis task.")
            ->addArgument('task', InputArgument::REQUIRED, 'Analysis task to run' )
            ->addArgument('projectId', InputArgument::REQUIRED, 'Project Id' )
            ->addArgument('tagId', InputArgument::OPTIONAL, 'Tag Id' )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $out = 0;
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->factory = $this->getContainer()->get('waev.task.factory');
        
        // get input
        $taskName = $input->getArgument('task');
        $projectId = $input->getArgument('projectId');
        $tagId = $input->hasArgument('tagId') ? $input->getArgument('tagId') : null;
        // get task type
        $type = $this->factory->getTypeFromAlias($taskName);
        try {
            switch($type) {
                case 'project':
                    $out = $this->executeProjectTask($taskName, $projectId);
                    break;
                case 'tag':
                    $out = $this->executeTagTask($taskName, $projectId, $tagId);
                    break;
                default:
                    $this->output->writeln(sprintf("Bad task type %s", $type));
                    $out = 101;
            }
            if($out!==true) {
                $out = 99;
            }
        }
        catch(\Exception $ex) {
            $this->output->writeln($ex->getMessage());
            $out = $ex->getCode();
        }
        return $out;
    }
    
    private function executeProjectTask($taskName, $projectId) {
        $project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if( !$project ) {
            throw new \Exception(sprintf('Unknown project %s', $projectId), 102);
        }
        $params = array(
           'project_id' => $projectId,
        );
        return $this->execTask($taskName, $params);
    }
    
    private function executeTagTask($taskName, $projectId, $tagId) {
        if( !$tagId ) {
            throw new \Exception('Missing tag id for tag task', 103);
        }
        
        $tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if( !$tag ) {
            throw new \Exception(sprintf('Unknown tag %s', $tagId), 104);
        }
        $params = array(
            'project_id' => $projectId,
            'tag_id' => $tagId,
        );
        return $this->execTask($taskName, $params);
    }
    
    private function execTask($taskName, $params) {
        $out = true;
        $this->output->writeln(sprintf("About to execute task %s with params %s",$taskName, print_R($params, true)));
        
        $task = $this->factory->getObject($taskName);
        if( $task ) {
            if( $task->configure($params) ) {
                try {
                    // caution! some tasks to $em->clear() ! all entities are detached!
                    if( !$task->execute() ) {
                        $out = $task->isCriticalFail();
                    }
                } catch (\Exception $ex) {
                    $out = false;
                }

                if (!$out) {
                    $task->onFail();
                }
            }
        }
        return $out;
    }
} 
