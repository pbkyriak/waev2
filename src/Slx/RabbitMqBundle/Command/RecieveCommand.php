<?php
namespace Slx\RabbitMqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Slx\RabbitMqBundle\Rabbitmq\Connection;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqJob;
use Doctrine\ORM\EntityManager;
/**
 * Description of RecieveCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RecieveCommand  extends ContainerAwareCommand {
    private $output;
    private $mq;
    
    /** @var Doctrine\ORM\EntityManager */
    private $em;
    private $process;
    private $job;
    private $canceled = false;
    
    protected function configure()
    {
        $this
            ->setName('waev:rabbitmq:recieve')
            ->setDescription("Test command that recieves messages from rabbitmq")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->mq = $this->getContainer()->get('slx_rabbit_mq.rabbitmq');
        $this->mq->setConsumer(RabbitMqConstants::SLOW_LANE_QUEUE, array($this, 'onMessage'));
        while(count($this->mq->getChannel()->callbacks)) {
            $this->output->writeln('waiting...');
            $this->mq->getChannel()->wait();
        }
        $this->mq->close();
    }
    
    public function onMessage($msg) {
        $this->output->writeln(sprintf("Recieved msg: %s", $msg->body));
        $toks = explode('.', $msg->body);
        $this->job = $this->em->getRepository('SlxRabbitMqBundle:MqJob')->find($toks[2]);
        if( $this->job ) {
            $this->process = $this->job->getMqProcess();
            $this->execJob($this->job);
            $this->replyToMessage($msg);
        }
        $this->mq->messageAck($msg);
        $this->canceled = false;
    }
    
    private function replyToMessage($msg) {
        $replyType = $this->job->getReplyOnComplete();
        $messageData = null;
        switch( $replyType ) {
            case RabbitMqConstants::REPLY_MESSAGE_TYPE_JOBDONE:
                $messageData = sprintf('job.done.%s', $this->job->getId());
                break;
            case RabbitMqConstants::REPLY_MESSAGE_TYPE_PROCESSDONE:
                $messageData = sprintf('process.done.%s', $this->job->getMqProcess()->getId());
        }
        if( $messageData ) {
            $this->mq->publishResponse($msg, $messageData);
        }
    }
    
    private function execJob(MqJob $job) {
        $this->output->writeln(sprintf("\tRunning job: %s", $job->getId()));
        $this->setJobTaskStatus($job, RabbitMqConstants::PROC_STATUS_RUNNING);
        try {
            foreach($job->getTasks() as $task) {
                if( $this->checkProcessStatus() ) { 
                    $this->execTask($task);
                }
            }
            // maybe will remove the command feature from the job it self. Attach a task to exec something.
            if( $job->getClass() && $this->checkProcessStatus() ) {
                $this->execCommand($job->getClass(), $job->getParams());
            }
            if( $this->checkProcessStatus() ) {
                $this->setJobTaskStatus($job, RabbitMqConstants::PROC_STATUS_DONE);
            }
        }
        catch(\Exception $ex) {
            $this->setJobStatus($job, RabbitMqConstants::PROC_STATUS_FAILED);
        }
    }
    
    private function execTask($task) {
        $this->setJobTaskStatus($task, RabbitMqConstants::PROC_STATUS_RUNNING);
        $this->output->writeln(sprintf("\trunning task %s", $task->getId()));
        $this->execCommand($task->getClass(), $task->getParams());
        $this->setJobTaskStatus($task, RabbitMqConstants::PROC_STATUS_DONE);        
    }
    
    private function execCommand($class, $parameters) {
        $this->output->writeln("\t\tcmd: ". $class);
        sleep(10);
    }
    
    private function setJobTaskStatus($obj, $status) {
        $obj->setStatus($status);
        $this->em->persist($obj);
        $this->em->flush();
    }
    
    private function checkProcessStatus() {
        $out = true;
        if( !$this->canceled ) {
            $this->em->refresh($this->process);
            if( $this->process->getStatus()==RabbitMqConstants::PROC_STATUS_CANCELING) {
                $this->setJobTaskStatus($this->job, RabbitMqConstants::PROC_STATUS_CANCELED);
                $this->canceled = true;
                $out = false;
            }
        }
        else {
            $out = false;
        }
        return $out;
    }
}
