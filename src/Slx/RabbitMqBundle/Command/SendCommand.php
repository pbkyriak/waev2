<?php
namespace Slx\RabbitMqBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqProcess;
use Slx\RabbitMqBundle\Entity\MqJob;
use Slx\RabbitMqBundle\Entity\MqJobTask;

/**
 * Description of SendCommand
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class SendCommand extends ContainerAwareCommand {
    
    private $em;
    
    protected function configure()
    {
        $this
            ->setName('waev:rabbitmq:send')
            ->setDescription("Test command that sends messages to rabbitmq")
            ->addArgument('message', InputArgument::REQUIRED, 'Message to send')
            ->addArgument('pId', InputArgument::REQUIRED, 'Project/process Id send with message')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mq = $this->getContainer()->get('slx_rabbit_mq.rabbitmq');
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        
        $message = null;
        $pId= $input->getArgument('pId');
        if( $input->getArgument('message')=='init' ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_INIT_LV1, $pId);
        }
        elseif( $input->getArgument('message')=='cancel' ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_CANCEL, $pId);
        }
        elseif( $input->getArgument('message')=='resume' ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_RESUME, $pId);
        }
        if( $message ) {
            $output->writeln(sprintf("Publishing message: %s", $message));
            $mq->publish($message, RabbitMqConstants::FAST_TRACK_QUEUE);
        }
        else {
            $output->writeln('Unknown message');
        }
    }
    
}
