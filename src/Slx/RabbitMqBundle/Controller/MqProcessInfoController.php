<?php

namespace Slx\RabbitMqBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Event\MqUiEvent;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;

/**
 * Project controller.
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")

 */
class MqProcessInfoController extends Controller
{
    public function processInfoAction($prjId, $pId)
    {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($prjId);
        $this->get("apy_breadcrumb_trail")->add($project->getPname(), 'project_show', array('id'=>$project->getId()));
        $this->get("apy_breadcrumb_trail")->add('Process information');

        if( $pId==0 ) {
            $process = $em->getRepository('SlxRabbitMqBundle:MqProcess')->getActiveProcessByProject($project);
        }
        else {
            $process = $em->getRepository('SlxRabbitMqBundle:MqProcess')->getProcessByIdAndProject($project, $pId);
        }
        if( $process ) {
            $cnt = $em->createQuery("SELECT count(a.id) FROM SlxRabbitMqBundle:MqJob a where a.mqProcess=:pid")
                    ->setParameter('pid', $process->getId())
                    ->getSingleScalarResult();
            
            $cnt2 = $em->createQuery("SELECT count(b.id) FROM SlxRabbitMqBundle:MqJob a left join a.tasks b where a.mqProcess=:pid")
                    ->setParameter('pid', $process->getId())
                    ->getSingleScalarResult();
        }
        $oldProcesses = $em->getRepository('SlxRabbitMqBundle:MqProcess')->getAllProcessesByProject($project);

        return $this->render('SlxRabbitMqBundle:MqProcessInfo:processInfo.html.twig', 
                array(
                    'project' => $project,
                    'process' => $process,
                    'oldProcesses' => $oldProcesses,
                )
            );
    }
    
    private function getProject($id) {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($id);
        if(!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        return $project;
    }
    
    /**
     * AJAX response
     */
    public function taskInfoAction($taskId) {
        $em = $this->getDoctrine()->getManager();
        $task = $em->getRepository('SlxRabbitMqBundle:MqJobTask')->find($taskId);
        return $this->render('SlxRabbitMqBundle:MqProcessInfo:taskInfo.html.twig', 
                array(
                    'task' => $task,
                )
            );
    }
    
    public function deleteProcessAction($pId) {
        $em = $this->getDoctrine()->getManager();
        $process = $this->fetchProcess($pId);
        $projectId = $process->getProjectId();
        $em->remove($process);
        $em->flush();
        $this->get('session')->getFlashBag()->add('info', 'waev.rabbitmq.process_deleted');
        return $this->redirect($this->generateUrl('project_processInfo', array('prjId'=>$projectId)));
    }
    
    public function deleteAllProcessesAction($prjId) {
        $em = $this->getDoctrine()->getManager();
        $project = $this->getProject($prjId);
        $em->getRepository('SlxRabbitMqBundle:MqProcess')->deleteAllProcessesByProject($project);
        $this->get('session')->getFlashBag()->add('info', 'waev.rabbitmq.process_deleted');
        return $this->redirect($this->generateUrl('project_processInfo', array('prjId'=>$prjId)));
    }
    
    public function cancelProcessAction($pId) {
        $process = $this->fetchProcess($pId);
        $projectId = $process->getProjectId();
        $this->dispatchEvent(RabbitMqConstants::EVT_UI_PROCESS_CANCEL, $process->getId());
        return $this->redirect($this->generateUrl('project_processInfo', array('prjId'=>$projectId)));
    }
    
    public function resumeProcessAction($pId) {
        $process = $this->fetchProcess($pId);
        $projectId = $process->getProjectId();
        $this->dispatchEvent(RabbitMqConstants::EVT_UI_PROCESS_RESUME, $process->getId());
        return $this->redirect($this->generateUrl('project_processInfo', array('prjId'=>$projectId)));        
    }
    
    private function fetchProcess($pId) {
        $em = $this->getDoctrine()->getManager();
        $process = $em->getRepository('SlxRabbitMqBundle:MqProcess')->find($pId);
        if( !$process ) {
            throw $this->createNotFoundException('Unable to find Process entity.');
        }
        return $process;
    }
    
    public function initProcessAction($prjId) {
        $em = $this->getDoctrine()->getManager();
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($prjId);
        if(!$project) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $this->dispatchEvent(RabbitMqConstants::EVT_UI_PROCESS_INIT, $project->getId());
        return $this->redirect($this->generateUrl('project_processInfo', array('prjId'=>$project->getId())));        
    }
    
    private function dispatchEvent($eventName, $id) {
        $event = new MqUiEvent($id);
        $eventDispatcher = $this->get('event_dispatcher');
        $eventDispatcher->dispatch($eventName, $event);        
        $this->get('session')->getFlashBag()->add($event->getSuccess() ? 'info' : 'error', $event->getMessage());
    }
}
