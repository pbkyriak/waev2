<?php

namespace Slx\RabbitMqBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('slx_rabbit_mq');
        $rootNode->children()
                ->scalarNode('fast')->cannotBeEmpty()->defaultValue('fast_track')->end()
                ->scalarNode('slow')->cannotBeEmpty()->defaultValue('slow_track')->end()
            ->end();
        return $treeBuilder;
    }
}
