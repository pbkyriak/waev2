<?php

namespace Slx\RabbitMqBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class SlxRabbitMqExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $container->setParameter('slx_rabbit_mq.fast_key', RabbitMqConstants::FAST_TRACK_QUEUE);
        $container->setParameter('slx_rabbit_mq.slow_key', RabbitMqConstants::SLOW_LANE_QUEUE);
        $container->setParameter('slx_rabbit_mq.fast_name', $config['fast']);
        $container->setParameter('slx_rabbit_mq.slow_name', $config['slow']);
        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
