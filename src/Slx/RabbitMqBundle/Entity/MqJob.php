<?php

namespace Slx\RabbitMqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MqJob
 */
class MqJob
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var integer
     */
    private $reply_on_complete=0;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $tasks;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tasks = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MqJob
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reply_on_complete
     *
     * @param integer $replyOnComplete
     * @return MqJob
     */
    public function setReplyOnComplete($replyOnComplete)
    {
        $this->reply_on_complete = $replyOnComplete;
    
        return $this;
    }

    /**
     * Get reply_on_complete
     *
     * @return integer 
     */
    public function getReplyOnComplete()
    {
        return $this->reply_on_complete;
    }

    /**
     * Add tasks
     *
     * @param \Slx\RabbitMqBundle\Entity\MqJobTask $tasks
     * @return MqJob
     */
    public function addTask(\Slx\RabbitMqBundle\Entity\MqJobTask $tasks)
    {
        $this->tasks[] = $tasks;
    
        return $this;
    }

    /**
     * Remove tasks
     *
     * @param \Slx\RabbitMqBundle\Entity\MqJobTask $tasks
     */
    public function removeTask(\Slx\RabbitMqBundle\Entity\MqJobTask $tasks)
    {
        $this->tasks->removeElement($tasks);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTasks()
    {
        return $this->tasks;
    }
    /**
     * @var \Slx\RabbitMqBundle\Entity\MqProcess
     */
    private $mqProcess;


    /**
     * Set mqProcess
     *
     * @param \Slx\RabbitMqBundle\Entity\MqProcess $mqProcess
     * @return MqJob
     */
    public function setMqProcess(\Slx\RabbitMqBundle\Entity\MqProcess $mqProcess = null)
    {
        $this->mqProcess = $mqProcess;
    
        return $this;
    }

    /**
     * Get mqProcess
     *
     * @return \Slx\RabbitMqBundle\Entity\MqProcess 
     */
    public function getMqProcess()
    {
        return $this->mqProcess;
    }
    /**
     * @var integer
     */
    private $duration=0;

    /**
     * @var \DateTime
     */
    private $started_at;

    /**
     * @var \DateTime
     */
    private $ended_at;


    /**
     * Set duration
     *
     * @param integer $duration
     * @return MqJob
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set started_at
     *
     * @param \DateTime $startedAt
     * @return MqJob
     */
    public function setStartedAt($startedAt)
    {
        $this->started_at = $startedAt;
    
        return $this;
    }

    /**
     * Get started_at
     *
     * @return \DateTime 
     */
    public function getStartedAt()
    {
        return $this->started_at;
    }

    /**
     * Set ended_at
     *
     * @param \DateTime $endedAt
     * @return MqJob
     */
    public function setEndedAt($endedAt)
    {
        $this->ended_at = $endedAt;
    
        return $this;
    }

    /**
     * Get ended_at
     *
     * @return \DateTime 
     */
    public function getEndedAt()
    {
        return $this->ended_at;
    }
}