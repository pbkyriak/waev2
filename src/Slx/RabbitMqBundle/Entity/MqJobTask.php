<?php

namespace Slx\RabbitMqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MqJobTask
 */
class MqJobTask
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $job_id;

    /**
     * @var integer
     */
    private $status;

    /**
     * @var string
     */
    private $class;

    /**
     * @var string
     */
    private $params;

    /**
     * @var \Slx\RabbitMqBundle\Entity\MqJob
     */
    private $mqJob;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set job_id
     *
     * @param integer $jobId
     * @return MqJobTask
     */
    public function setJobId($jobId)
    {
        $this->job_id = $jobId;
    
        return $this;
    }

    /**
     * Get job_id
     *
     * @return integer 
     */
    public function getJobId()
    {
        return $this->job_id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return MqJobTask
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set class
     *
     * @param string $class
     * @return MqJobTask
     */
    public function setClass($class)
    {
        $this->class = $class;
    
        return $this;
    }

    /**
     * Get class
     *
     * @return string 
     */
    public function getClass()
    {
        return $this->class;
    }

    /**
     * Set params
     *
     * @param string $params
     * @return MqJobTask
     */
    public function setParams($params)
    {
        $this->params = $params;
    
        return $this;
    }

    /**
     * Get params
     *
     * @return string 
     */
    public function getParams()
    {
        return $this->params;
    }

    public function getParamsArray() {
        return unserialize($this->params);
    }
    /**
     * Set mqJob
     *
     * @param \Slx\RabbitMqBundle\Entity\MqJob $mqJob
     * @return MqJobTask
     */
    public function setMqJob(\Slx\RabbitMqBundle\Entity\MqJob $mqJob = null)
    {
        $this->mqJob = $mqJob;
    
        return $this;
    }

    /**
     * Get mqJob
     *
     * @return \Slx\RabbitMqBundle\Entity\MqJob 
     */
    public function getMqJob()
    {
        return $this->mqJob;
    }
    /**
     * @var string
     */
    private $output='';

    /**
     * @var string
     */
    private $error_output='';


    /**
     * Set output
     *
     * @param string $output
     * @return MqJobTask
     */
    public function setOutput($output)
    {
        $this->output = $output;
    
        return $this;
    }

    /**
     * Get output
     *
     * @return string 
     */
    public function getOutput()
    {
        return $this->output;
    }

    /**
     * Set error_output
     *
     * @param string $errorOutput
     * @return MqJobTask
     */
    public function setErrorOutput($errorOutput)
    {
        $this->error_output = $errorOutput;
    
        return $this;
    }

    /**
     * Get error_output
     *
     * @return string 
     */
    public function getErrorOutput()
    {
        return $this->error_output;
    }
    /**
     * @var integer
     */
    private $duration=0;


    /**
     * Set duration
     *
     * @param integer $duration
     * @return MqJobTask
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }
}