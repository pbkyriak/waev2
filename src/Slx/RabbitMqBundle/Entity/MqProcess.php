<?php

namespace Slx\RabbitMqBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MqProcess
 */
class MqProcess
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $project_id;

    /**
     * @var integer
     */
    private $num_of_jobs;

    /**
     * @var integer
     */
    private $num_of_jobs_done;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $jobs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->jobs = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project_id
     *
     * @param integer $projectId
     * @return MqProcess
     */
    public function setProjectId($projectId)
    {
        $this->project_id = $projectId;
    
        return $this;
    }

    /**
     * Get project_id
     *
     * @return integer 
     */
    public function getProjectId()
    {
        return $this->project_id;
    }

    /**
     * Set num_of_jobs
     *
     * @param integer $numOfJobs
     * @return MqProcess
     */
    public function setNumOfJobs($numOfJobs)
    {
        $this->num_of_jobs = $numOfJobs;
    
        return $this;
    }

    /**
     * Get num_of_jobs
     *
     * @return integer 
     */
    public function getNumOfJobs()
    {
        return $this->num_of_jobs;
    }

    /**
     * Set num_of_jobs_done
     *
     * @param integer $numOfJobsDone
     * @return MqProcess
     */
    public function setNumOfJobsDone($numOfJobsDone)
    {
        $this->num_of_jobs_done = $numOfJobsDone;
    
        return $this;
    }

    /**
     * Get num_of_jobs_done
     *
     * @return integer 
     */
    public function getNumOfJobsDone()
    {
        return $this->num_of_jobs_done;
    }

    /**
     * Add jobs
     *
     * @param \Slx\RabbitMqBundle\Entity\MqJob $jobs
     * @return MqProcess
     */
    public function addJob(\Slx\RabbitMqBundle\Entity\MqJob $jobs)
    {
        $this->jobs[] = $jobs;
    
        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Slx\RabbitMqBundle\Entity\MqJob $jobs
     */
    public function removeJob(\Slx\RabbitMqBundle\Entity\MqJob $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }
    /**
     * @var integer
     */
    private $status=0;


    /**
     * Set status
     *
     * @param integer $status
     * @return MqProcess
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    /**
     * @var boolean
     */
    private $active=false;


    /**
     * Set active
     *
     * @param boolean $active
     * @return MqProcess
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }
    /**
     * @var \DateTime
     */
    private $started_at;

    /**
     * @var \DateTime
     */
    private $ended_at;

    /**
     * @var integer
     */
    private $duration;


    /**
     * Set started_at
     *
     * @param \DateTime $startedAt
     * @return MqProcess
     */
    public function setStartedAt($startedAt)
    {
        $this->started_at = $startedAt;
    
        return $this;
    }

    /**
     * Get started_at
     *
     * @return \DateTime 
     */
    public function getStartedAt()
    {
        return $this->started_at;
    }

    /**
     * Set ended_at
     *
     * @param \DateTime $endedAt
     * @return MqProcess
     */
    public function setEndedAt($endedAt)
    {
        $this->ended_at = $endedAt;
    
        return $this;
    }

    /**
     * Get ended_at
     *
     * @return \DateTime 
     */
    public function getEndedAt()
    {
        return $this->ended_at;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return MqProcess
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    
        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }
}