<?php
namespace Slx\RabbitMqBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of MqProcessRepository
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqProcessRepository extends EntityRepository {
    
    public function getActiveProcessByProject($project) {
        $out = $this->createQueryBuilder('p')
                ->leftJoin('p.jobs', 'j')
                ->leftJoin('j.tasks', 't')
                ->addSelect('j')
                ->addSelect('t')
                ->andWhere("p.project_id=:prj and p.active=:ac")
                ->getQuery()
                ->setParameter('prj', $project->getId())
                ->setParameter('ac', 1)
                ->getOneOrNullResult();
        return $out;
    }
    
    public function getNonActiveProcessesByProject($project) {
        return $this
                ->getEntityManager()
                ->createQuery("SELECT p FROM SlxRabbitMqBundle:MqProcess p WHERE p.project_id=:prj and p.active=:ac ORDER BY p.id DESC")
                ->setParameter('prj', $project->getId())
                ->setParameter('ac', 0)
                ->getResult();        
    }

    public function getAllProcessesByProject($project) {
        return $this
                ->getEntityManager()
                ->createQuery("SELECT p FROM SlxRabbitMqBundle:MqProcess p WHERE p.project_id=:prj ORDER BY p.id DESC")
                ->setParameter('prj', $project->getId())
                ->getResult();        
    }
    
    public function deleteAllProcessesByProject($project) {
        $this->getEntityManager()->createQuery("DELETE FROM SlxRabbitMqBundle:MqProcess p WHERE p.project_id=:prj")
                ->setParameter('prj', $project)
                ->execute();
    }
    
    public function getProcessByIdAndProject($project, $pId) {
        try {
            $out = $this
                ->getEntityManager()
                ->createQuery("SELECT p FROM SlxRabbitMqBundle:MqProcess p WHERE p.project_id=:prj and p.id=:pid")
                ->setParameter('prj', $project->getId())
                ->setParameter('pid', $pId)
                ->getSingleResult();
        }
        catch(\Doctrine\ORM\NoResultException $ex ) {
            $out = null;
        }
        return $out;
        
    }

}
