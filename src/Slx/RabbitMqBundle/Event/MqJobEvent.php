<?php
namespace Slx\RabbitMqBundle\Event;

use Symfony\Component\EventDispatcher\Event;
/**
 * Description of JobDoneEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqJobEvent extends Event {
    
    private $data;
    private $bIsRPC=false;
    private $replyTo;
    private $correlationId;
    
    public function __construct($data) {
        $this->data = $data;
    }
        
    public function setData($data) {
        $this->data = $data;
    }
    
    public function getData() {
        return $this->data;
    }

    public function setRPC($replyTo, $correlationId) {
        $this->bIsRPC = true;
        $this->replyTo = $replyTo;
        $this->correlationId = $correlationId;
    }
    
    public function isRPC() {
        return $this->bIsRPC;
    }
    
    public function getReplyTo() {
        return $this->replyTo;
    }
    
    public function getCorrelationId() {
        return $this->correlationId;
    }
    
}
