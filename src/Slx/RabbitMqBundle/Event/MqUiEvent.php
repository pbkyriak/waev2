<?php
namespace Slx\RabbitMqBundle\Event;

use Symfony\Component\EventDispatcher\Event;
/**
 * Description of MqUiEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqUiEvent extends Event {
    
    private $id;
    private $success;
    private $message;
    
    public function __construct($id) {
        $this->id = $id;
    }
        
    public function setObjId($id) {
        $this->id = $id;
    }
    
    public function getObjId() {
        return $this->id;
    }
    
    public function setResponse($success, $message) {
        $this->success = $success;
        $this->message = $message;
    }
    
    public function getSuccess() {
        return $this->success;
    }
    
    public function getMessage() {
        return $this->message;
    }
}
