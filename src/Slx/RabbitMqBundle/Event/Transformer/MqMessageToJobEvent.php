<?php
namespace Slx\RabbitMqBundle\Event\Transformer;

use PhpAmqpLib\Message\AMQPMessage;
use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of MqMessageToJobEvent
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqMessageToJobEvent {
    private $eventName;
    /** @var \Slx\RabbitMqBundle\Event\MqJobEvent */
    private $event;
    
    public function transform(AMQPMessage $msg) {
        $data = $msg->body;
        $isRpc = $this->checkIfRPC($data);
        $toks = explode('.',$data);
        $this->eventName = sprintf("rabbitmq.%s%s", ucfirst($toks[0]), ucfirst($toks[1]));
        $eventData = isset($toks[2]) ? $toks[2] : '';        
        $this->event = new MqJobEvent($eventData);
        if( $isRpc ) {
            $this->event->setRPC($msg->get('reply_to'), $msg->get('correlation_id'));
        }    
    }
    
    /**
     * Checks if message data has the 'rpc:' prefix. if so, return true and removes the prefix from data
     * 
     * @param string $data
     * @return boolean
     */
    private function checkIfRPC(&$data) {
        $out = false;
        if(strpos($data, RabbitMqConstants::MSG_PREFIX_RPC)!==false) { // it is rpc
            $data = substr($data, strlen(RabbitMqConstants::MSG_PREFIX_RPC));   // remove rpc: prefix
            $out = true;
        }
        return $out;
    }
    
    public function getEventName() {
        return $this->eventName;
    }
    
    public function getEvent() {
        return $this->event;
    }
    
    public function dumpEvent() {
        return sprintf("Event: %s data: %s %s", 
                $this->eventName, 
                $this->event->getData(),
                $this->event->isRPC() ? '(RPC corId:' . $this->event->getCorrelationId() .' reply:'.$this->event->getReplyTo().')' : ''
                );

    }
}
