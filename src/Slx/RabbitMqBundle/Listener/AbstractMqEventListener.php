<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\Connection as MqConnection;
use Doctrine\ORM\EntityManager;

/**
 * Description of AbstractMqEventListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractMqEventListener {
    /** @var \Doctrine\ORM\EntityManager */
    protected $em;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    protected $mq;
    
    public function __construct(MqConnection $mq, EntityManager $em) {
        $this->em = $em;
        $this->mq = $mq;
    }

    /**
     * 
     * @return \Slx\RabbitMqBundle\Rabbitmq\Connection
     */
    protected function getMQ() {
        return $this->mq;
    }
    
    /**
     * 
     * @return \Doctrine\ORM\EntityManager
     */
    protected function getEM() {
        return $this->em;
    }
    
    public function onEvent(MqJobEvent $event) {
        $ret = $this->processEvent($event);
        if( $event->isRPC() ) {
            $this->responseToRPC($event, $ret);
        }
    }
    
    private function responseToRPC(MqJobEvent $event, $ret) {
        $msg = $this->getMQ()->createMessage(
                $ret,
                array(
                    'correlation_id' => $event->getCorrelationId(),
                    )
                );
        $this->getMQ()->publishMessage($msg, $event->getReplyTo());
    }
    
    abstract protected function processEvent(MqJobEvent $event);
}
