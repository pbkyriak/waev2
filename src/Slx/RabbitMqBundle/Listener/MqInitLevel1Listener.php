<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqProcess;
use Slx\RabbitMqBundle\Entity\MqJob;
use Slx\RabbitMqBundle\Entity\MqJobTask;

/**
 * Description of MqInitLevel1Listener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqInitLevel1Listener extends AbstractMqEventListener {

    protected function processEvent(MqJobEvent $event) {
        $this->initLv1($event->getData());
    }

    private function initLv1($projectId) {
        $project = $this->getEM()->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        //printf("Clean old project data...\n");
        //$this->getEM()->getRepository('SlxGitMinerBundle:Project')->cleanProjectData($project); // has to move in a seperate task
        //printf("Cleaned.\n Go on with rest of the process...\n");
        // set previous project processes as non-active
        $this->getEM()->getConnection()->update("mq_process", array('active'=>FALSE), array('project_id'=>$projectId));
        $process = $this->createProcess($projectId);
        $messages =  array();
        //
        $q = $this->getEM()->createQuery("SELECT b FROM SlxTaskCoreBundle:ProjectAnalysisTaskTag b JOIN b.analysis a WHERE a.project=:pid AND a.active=:act ORDER BY b.position")
            ->setParameter('pid', $project->getId())
            ->setParameter('act', true);
        $tTasks = $q->getResult();
        //
        foreach ($project->getProjectTags() as $tag) {
            $jobId = $this->addTagJob($process, $project, $tag, $tTasks);
            $messages[] = sprintf(RabbitMqConstants::MSG_MASK_JOB_EXEC, $jobId);
        }
        //
        $process->setNumOfJobs(count($messages));
        $this->getEM()->persist($process);
        $this->getEM()->flush();
        foreach($messages as $message) {
            $this->getMQ()->publish($message, RabbitMqConstants::SLOW_LANE_QUEUE);
        }
    }
    
    private function createProcess($projectId) {
        $process = new MqProcess();
        $process->setProjectId($projectId);
        $process->setNumOfJobs(0);
        $process->setNumOfJobsDone(0);
        $process->setActive(true);
        $process->setDuration(0);
        $this->getEM()->persist($process);
        $this->getEM()->flush();
        return $process;
    }

    private function addTagJob($process, $project, $tag, $tTasks) {
        $job = new MqJob();
        $job->setMqProcess($process)
                ->setStatus(RabbitMqConstants::PROC_STATUS_PENDING)
                ->setReplyOnComplete(RabbitMqConstants::REPLY_MESSAGE_TYPE_JOBDONE)
                ;
        foreach( $tTasks as $tTask ) {
            $jobTask = new MqJobTask();           
            $jobTask->setClass($tTask->getAlias())
                    ->setParams(serialize(array('project_id'=>$project->getId(), 'tag_id'=>$tag->getId())))
                    ->setStatus(RabbitMqConstants::PROC_STATUS_PENDING)
                    ->setMqJob($job);
            $job->addTask($jobTask);
            $this->getEM()->persist($jobTask);
        }
        $this->getEM()->persist($job);
        $this->getEM()->flush();
        return $job->getId();
        
    }

}
