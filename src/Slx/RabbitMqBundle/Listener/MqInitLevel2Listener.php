<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqProcess;
use Slx\RabbitMqBundle\Entity\MqJob;
use Slx\RabbitMqBundle\Entity\MqJobTask;

/**
 * Description of MqInitLevel2Listener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqInitLevel2Listener extends AbstractMqEventListener {
    
    protected function processEvent(MqJobEvent $event) {
        $process = $this->getEM()->getRepository('SlxRabbitMqBundle:MqProcess')->find($event->getData());
        $project = $this->getEM()->getRepository('SlxGitMinerBundle:Project')->find($process->getProjectId());
        //
        $q = $this->getEM()->createQuery("SELECT b FROM SlxTaskCoreBundle:ProjectAnalysisTaskProject b JOIN b.analysis a WHERE a.project=:pid AND a.active=:act ORDER BY b.position")
            ->setParameter('pid', $project->getId())
            ->setParameter('act', true);
        $tTasks = $q->getResult();
        //
        $jobId = $this->createJob($process, $project, $tTasks); 
        $process->setNumOfJobs($process->getNumOfJobs()+1);
        $this->getEM()->persist($process);
        $this->getEM()->flush();
        $message = sprintf(RabbitMqConstants::MSG_MASK_JOB_EXEC, $jobId);
        $this->getMQ()->publish($message, RabbitMqConstants::SLOW_LANE_QUEUE);
    }
    
    private function createJob(MqProcess $process,$project, $tTasks) {
        $job = new MqJob();
        $job->setMqProcess($process)
                ->setStatus(RabbitMqConstants::PROC_STATUS_PENDING)
                ->setReplyOnComplete(RabbitMqConstants::REPLY_MESSAGE_TYPE_PROCESSDONE)
                ;
        foreach($tTasks as $tTask) {
            $jobTask = new MqJobTask();
            $jobTask->setClass($tTask->getAlias())
                    ->setParams(serialize(array('project_id'=>$project->getId())))
                    ->setStatus(RabbitMqConstants::PROC_STATUS_PENDING)
                    ->setMqJob($job);
            $job->addTask($jobTask);
            $this->getEM()->persist($jobTask);            
        }
        $this->getEM()->persist($job);
        $this->getEM()->flush();
        return $job->getId();
    }
    
}
