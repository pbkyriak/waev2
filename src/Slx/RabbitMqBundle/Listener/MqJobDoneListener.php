<?php
namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\Connection;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of MqJobDoneListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqJobDoneListener extends AbstractMqEventListener {
    
    /**
     * if all jobs of the process are done then process can move to next level.
     * This event is fired ONLY from level1 analysis jobs.
     * 
     * @param MqJobEvent $event
     */
    protected function processEvent(MqJobEvent $event) {
        $jobId = $event->getData();
        $this->job = $this->getEM()->getRepository("SlxRabbitMqBundle:MqJob")->find($jobId);
        if( $this->job ) {
            $this->getEM()->refresh($this->job);
            $process = $this->job->getMqProcess();
            $process->setNumOfJobsDone($process->getNumOfJobsDone()+1);
            if($this->job->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED ) {  
                $process->setStatus(RabbitMqConstants::PROC_STATUS_FAILED);
            }
            $this->getEM()->persist($process);
            $this->getEM()->flush();
            if( $process->getNumOfJobsDone()>=$process->getNumOfJobs() ) {  // all process's (level 1) jobs are done
                $this->sendNextMessage($process);
            }
        }
    }
    
    private function sendNextMessage($process) {
        $messageData = null;
        // if process is canceling do not initLevel2, send process.done to speed up things
        if( $process->getStatus()==RabbitMqConstants::PROC_STATUS_CANCELING || $process->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED ) {
            $messageData = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_DONE, $this->job->getMqProcess()->getId());
        }
        else {  // otherwise, send init.level2 message
            $replyType = $this->job->getReplyOnComplete();
            switch( $replyType ) {
                case RabbitMqConstants::REPLY_MESSAGE_TYPE_JOBDONE:
                    printf("Ready for Level2");
                    $messageData = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_INIT_LV2, $this->job->getMqProcess()->getId()); 
                    break;
                case RabbitMqConstants::REPLY_MESSAGE_TYPE_PROCESSDONE:
                    $messageData = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_DONE, $this->job->getMqProcess()->getId());
                    break;
            }
        }

        if( $messageData ) {
            $this->getMQ()->publish($messageData, RabbitMqConstants::FAST_TRACK_QUEUE);
        }        
    }
}
