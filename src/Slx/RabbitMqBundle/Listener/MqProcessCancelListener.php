<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;

/**
 * Description of MqJobCancelListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqProcessCancelListener extends AbstractMqEventListener {
    
    /**
     * Sets process status to CANCELING so job messages that will be consumed to 
     * know that they will have to cancel their execution
     * 
     * @param MqJobEvent $event
     */
    protected function processEvent(MqJobEvent $event) {
        $procId = $event->getData();
        $process = $this->getEM()->getRepository("SlxRabbitMqBundle:MqProcess")->find($procId);
        if( $process ) {
            $process->setStatus(RabbitMqConstants::PROC_STATUS_CANCELING);
            $this->getEM()->persist($process);
            $this->getEM()->flush();
        }
        return 'ok';
    }
    
}
