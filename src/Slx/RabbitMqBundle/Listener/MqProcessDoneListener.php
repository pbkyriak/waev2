<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of MqProcessDoneListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqProcessDoneListener extends AbstractMqEventListener {

    protected function processEvent(MqJobEvent $event) {
        $procId = intval($event->getData());
        $process = $this->em->getRepository("SlxRabbitMqBundle:MqProcess")->find($procId);
        if( $process ) {
            $newStatus = RabbitMqConstants::PROC_STATUS_DONE;
            if( $process->getStatus()==RabbitMqConstants::PROC_STATUS_CANCELING ) {
                $newStatus = RabbitMqConstants::PROC_STATUS_CANCELED;
            }
            if( $process->getStatus()!=RabbitMqConstants::PROC_STATUS_FAILED ) {
                $endedAt = new \DateTime();
                $duration = $process->getStartedAt() ? ($endedAt->format('U')-$process->getStartedAt()->format('U')) : -1;
                $process->setStatus($newStatus);
                $process->setEndedAt($endedAt);
                $process->setDuration($duration);
                $this->em->persist($process);
                $this->em->flush();
            }
        }
        return 'ok';
    }
}
