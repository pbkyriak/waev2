<?php
namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Entity\MqJob;

/**
 * Resumes a failed process
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqProcessResumeListener extends AbstractMqEventListener {
    /** @var \Slx\RabbitMqBundle\Entity\MqProcess */
    private $process;

    protected function processEvent(MqJobEvent $event) {
        $procId = intval($event->getData());
        $this->process = $this->getEM()->getRepository("SlxRabbitMqBundle:MqProcess")->find($procId);
        if( $this->process ) {
            $this->em->refresh($this->process);
            if( $this->process->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED ) {
                $this->resumeProcess();
            }
        }
        return 'ok';
    }
    
    private function resumeProcess() {
        $jobs = $this->process->getJobs();
        $numOfJobsDone = 0;
        $messages = array();
        foreach($jobs as $job) {
            if( $job->getStatus()==RabbitMqConstants::PROC_STATUS_DONE ) {
                $numOfJobsDone++;
            }
            if( $job->getStatus()==RabbitMqConstants::PROC_STATUS_PENDING ) {
                $messages[] = sprintf('job.exec.%s', $job->getId());
            }
            if( $job->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED ) {
                $this->resumeJob($job);
                $messages[] = sprintf('job.exec.%s', $job->getId());
            }
        }     
        $this->updateProcess($numOfJobsDone);
        $this->publishMessages($messages);
    }    
    
    private function resumeJob(MqJob $job) {
        $tasks = $job->getTasks();
        foreach($tasks as $task) {
            if($task->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED) {
                $task->setStatus(RabbitMqConstants::PROC_STATUS_PENDING);
                $this->getEM()->persist($task);
                $this->getEM()->flush();
            }
        }
        $job->setStatus(RabbitMqConstants::PROC_STATUS_PENDING);
        $this->getEM()->persist($job);
        $this->getEM()->flush();
    }
    
    private function updateProcess($numOfJobsDone) {
        $this->process->setNumOfJobsDone($numOfJobsDone);
        $this->process->setStatus(RabbitMqConstants::PROC_STATUS_PENDING);
        $this->getEM()->persist($this->process);        
        $this->getEM()->flush();
    }
    
    private function publishMessages($messages) {
        foreach($messages as $message) {
            $this->getMQ()->publish($message, RabbitMqConstants::SLOW_LANE_QUEUE);
        }
    }
}
