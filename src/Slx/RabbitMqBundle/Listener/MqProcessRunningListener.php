<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Event\MqJobEvent;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;

/**
 * Description of ProcessRunningListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class MqProcessRunningListener extends AbstractMqEventListener {
    
    /**
     * Mark Processs as running
     * 
     * @param MqJobEvent $event
     */
    protected function processEvent(MqJobEvent $event) {
        $procId = $event->getData();
        $process = $this->getEM()->getRepository("SlxRabbitMqBundle:MqProcess")->find($procId);
        if( $process ) {
            $this->getEM()->refresh($process);
            if( $process->getStatus()==RabbitMqConstants::PROC_STATUS_PENDING ) {
                $process->setStatus(RabbitMqConstants::PROC_STATUS_RUNNING);
                $process->setDuration(0);
                $process->setStartedAt(new \DateTime());
                $this->getEM()->persist($process);
                $this->getEM()->flush();
            }
        }
        return 'ok';
    }
}
