<?php

namespace Slx\RabbitMqBundle\Listener;

use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
use Slx\RabbitMqBundle\Event\MqUiEvent;
/**
 * Description of UiProcessInitListener
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class UiProcessListener {
    /** @var \Doctrine\ORM\EntityManager */
    private $em;
    /** @var \Slx\RabbitMqBundle\Rabbitmq\Connection */
    private $mq;
    
    public function __construct($mq, $em) {
        $this->em = $em;
        $this->mq = $mq;
    }


    public function onInitEvent(MqUiEvent $event) {
        $prjId = intval($event->getObjId());
        $project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($prjId);
        if( !$this->hasProjectRunningProcess($project) ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_INIT_LV1,$project->getId());
            $this->mq->publish($message, RabbitMqConstants::FAST_TRACK_QUEUE);                    
            $event->setResponse(true, 'waev.rabbitmq.process_initing');            
        }
        else {
            $event->setResponse(false, 'waev.rabbitmq.process_cant_init');
        }
    }
    
    public function onCancelEvent(MqUiEvent $event) {
        $pId = intval($event->getObjId());
        $process = $this->em->getRepository('SlxRabbitMqBundle:MqProcess')->find($pId);
        if( $process->getStatus()==RabbitMqConstants::PROC_STATUS_RUNNING ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_CANCEL,$process->getId());
            $this->mq->publish($message, RabbitMqConstants::FAST_TRACK_QUEUE);                    
            $event->setResponse(true, 'waev.rabbitmq.process_canceling');
        }
        else {
            $event->setResponse(false, 'waev.rabbitmq.process_cant_cancel');
        }
    }
    
    public function onResumeEvent(MqUiEvent $event) {
        $pId = intval($event->getObjId());
        $process = $this->em->getRepository('SlxRabbitMqBundle:MqProcess')->find($pId);
        if( $process->getStatus()==RabbitMqConstants::PROC_STATUS_FAILED ) {
            $message = sprintf(RabbitMqConstants::MSG_MASK_PROCESS_RESUME,$process->getId());
            $this->mq->publish($message, RabbitMqConstants::FAST_TRACK_QUEUE);                    
            $event->setResponse(true, 'waev.rabbitmq.process_resuming '.$message);
        }
        else {
            $event->setResponse(false, 'waev.rabbitmq.process_cant_resume');
        }
    }
   
    private function hasProjectRunningProcess($project) {
        $out = false;
        $process = $this->em->getRepository('SlxRabbitMqBundle:MqProcess')->getActiveProcessByProject($project);
        if( $process ) {
            if( $process->getStatus()<=RabbitMqConstants::PROC_STATUS_RUNNING ) {
                $out = true;
            }
        }
        return $out;
    }
}
