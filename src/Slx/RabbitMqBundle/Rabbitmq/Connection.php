<?php
namespace Slx\RabbitMqBundle\Rabbitmq;

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;
use Slx\RabbitMqBundle\Rabbitmq\RabbitMqConstants;
/**
 * Description of Connection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Connection {

    private $host = 'localhost';
    private $port = 5672;
    private $user = 'guest';
    private $pass = 'guest';
    private $connection = null;
    private $channel = null;
    private $queueNames;
    
    public function __construct($host, $port, $user, $pass) {
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->pass = $pass;
        $this->queueNames = array();
    }

    public function addQueueName($queueKey, $queueName) {
        $this->queueNames[$queueKey] = $queueName;
    }
    
    public function IsQueueKeyListed($queueKey) {
        return isset($this->queueNames[$queueKey]);
    }
    
    public function getQueueKeys() {
        return array_keys($this->queueNames);
    }
    
    /**
     * 
     * @return AMQPConnection
     */
    public function getConnection() {
        if( $this->connection==null ) {
            $this->connection = new AMQPConnection($this->host, $this->port, $this->user, $this->pass);
        }
        return $this->connection;
    }
    
    /**
     * 
     * @return AMQPChannel
     */
    public function getChannel() {
        if( $this->channel==null ) {
            $this->channel = new AMQPChannel($this->getConnection());
            foreach($this->queueNames as $queueName) {
                $this->channel->queue_declare($queueName, false, true, false, false);
            }
        }
        
        return $this->channel;
    }
    
    public function declareAnonymousQueue() {
        list($queueName,,) = $this->getChannel()->queue_declare('',false,false,true,false);
        return $queueName;
    }
    
    public function publish($data, $queueKey) {
        $msg = new AMQPMessage($data, array('delivery_mode'=>2));
        $this->getChannel()->basic_publish($msg, '', $this->getQueueName($queueKey));
    }
    
    public function publishMessage($msg, $queueKey) {
        $this->getChannel()->basic_publish($msg, '', $this->getQueueName($queueKey));
    }
    public function messageAck(AMQPMessage $msg) {
        $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
    }
        
    public function setConsumer($queueKey, $callback) {
        $queueName = $this->getQueueName($queueKey);
        $this->getChannel()->basic_qos(null, 1, null);
        $this->getChannel()->basic_consume($queueName, '', false, false, false, false, $callback);
    }
    
    public function waitForMessages() {
        while(count($this->getChannel()->callbacks)) {
            printf("waiting...\n");
            $this->getChannel()->wait();
        }
    }
    
    public function wait() {
        $this->getChannel()->wait();
    }
    
    public function close() {
        $this->getChannel()->close();
        $this->getConnection()->close();
    }
    
    private function getQueueName($queueKey) {
        return isset($this->queueNames[$queueKey]) ? $this->queueNames[$queueKey] : $queueKey;
    }
    
    public function createMessage($body, $options=null) {
        if( !$options ) {
            $msg = new AMQPMessage($body, array('delivery_mode'=>2));
        }
        else {
            $msg = new AMQPMessage($body, $options);
        }
        return $msg;
    }
}
