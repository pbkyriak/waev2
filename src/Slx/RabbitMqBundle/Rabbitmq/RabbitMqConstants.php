<?php

namespace Slx\RabbitMqBundle\Rabbitmq;

/**
 * Description of RabbitMqConstants
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class RabbitMqConstants {

    /* rabbitmq queue keys */
    const FAST_TRACK_QUEUE = 'fast';
    const SLOW_LANE_QUEUE = 'slow';

    /* masks for amq messages */
    const MSG_MASK_PROCESS_INIT_LV1 = 'init.level1.%s';
    const MSG_MASK_PROCESS_INIT_LV2 = 'init.level2.%s';
    const MSG_MASK_PROCESS_RUNNING = 'process.running.%s';
    const MSG_MASK_PROCESS_CANCEL = 'process.cancel.%s';
    const MSG_MASK_PROCESS_RESUME = 'process.resume.%s';
    const MSG_MASK_PROCESS_DONE = 'process.done.%s';
    const MSG_MASK_JOB_EXEC = 'job.exec.%s';
    const MSG_MASK_JOB_DONE = 'job.done.%s';
    
    const MSG_PREFIX_RPC = 'rpc:';
    
    /* process/job/task statuses */
    const PROC_STATUS_PENDING = 0;
    const PROC_STATUS_RUNNING = 1;
    
    const PROC_STATUS_CANCELING = 6;
    const PROC_STATUS_CANCELED = 7;
    
    const PROC_STATUS_FAILED = 8;
    const PROC_STATUS_DONE = 9;
    
    /* job reply message types */
    const REPLY_MESSAGE_TYPE_JOBDONE = 1;
    const REPLY_MESSAGE_TYPE_PROCESSDONE = 2;
    
    /* symfony kernel event names */
    const EVT_UI_PROCESS_CANCEL = 'rabbitmqui.process.cancel';
    const EVT_UI_PROCESS_RESUME = 'rabbitmqui.process.resume';
    const EVT_UI_PROCESS_INIT = 'rabbitmqui.process.init';
    
    public static function nextJobStatus($pStatus, $jStatus) {
        $out = $jStatus;
        if( $pStatus==RabbitMqConstants::PROC_STATUS_FAILED ) {
            $out = RabbitMqConstants::PROC_STATUS_PENDING;
        }
        elseif( $pStatus==RabbitMqConstants::PROC_STATUS_CANCELING || $pStatus==RabbitMqConstants::PROC_STATUS_CANCELED ) {
            $out = RabbitMqConstants::PROC_STATUS_CANCELED;
        }
        printf("ps=%s js=%s ns=%s\n", $pStatus, $jStatus, $out);
        return $out;
    }
    
}

