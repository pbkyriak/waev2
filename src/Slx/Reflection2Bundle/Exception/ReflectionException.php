<?php

namespace Slx\Reflection2Bundle\Exception;

/**
 * Description of ReflectionException
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ReflectionException extends \Exception
{
    
}
