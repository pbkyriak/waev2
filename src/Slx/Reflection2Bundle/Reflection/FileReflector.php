<?php

namespace Slx\Reflection2Bundle\Reflection;

use PHPParser_Node;
use PHPParser_NodeVisitor;
use PHPParser_Error;
use PHPParser_Parser;
use PHPParser_Lexer;
use Slx\Reflection2Bundle\Reflection\ReflectorInterface;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;

/**
 * Reflects a php code file. Based on phpDocumentor reflection
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileReflector implements ReflectorInterface, PHPParser_NodeVisitor {

    protected $classes = array();
    protected $traits = array();
    protected $interfaces = array();
    protected $functions = array();
    protected $constants = array();
    protected $filename = '';
    protected $fileId = 0;
    protected $contents = '';
    protected $ast = null;
    protected $context;
    protected $ignoreParams = false;
    protected $namespace;

    /**
     * Opens the file and retrieves its contents. 
     * 
     * @param string $file file to reflect
     * @param int $fileId   database fileId
     */
    public function __construct($file, $fileId) {
        $this->filename = $file;
        $this->fileId = $fileId;
        $this->context = new \SplStack();
        $this->context->push($this);
    }

    /**
     * parse code
     */
    public function parse() {
        try {
            $this->contents = file_get_contents($this->filename);
            $this->ast = $this->createParser()->parse($this->contents);
            return true;
        } catch (PHPParser_Error $e) {
            printf('file: %sParse Error: %s\n', $this->filename, $e->getMessage());
            return false;
        }
    }

    /**
     * Creates a parser object using our own Lexer.
     *
     * @return PHPParser_Parser
     */
    protected function createParser() {
        return new PHPParser_Parser(new PHPParser_Lexer());
    }

    /**
     * Traverser leaveNode.
     * 
     * @param PHPParser_Node $node
     */
    public function leaveNode(PHPParser_Node $node) {
        $nodeClass = get_class($node);
        switch ($nodeClass) {
            case 'PHPParser_Node_Stmt_Interface':
                //printf("leaving interface\n");
                $this->interfaces[] = $this->context->pop();
                break;
            case 'PHPParser_Node_Stmt_Class':
                //printf("leaving class\n");
                $this->classes[] = $this->context->pop();
                break;
            case 'PHPParser_Node_Stmt_Trait':
                //printf("leaving trait\n");
                $this->traits[] = $this->context->pop();
                break;
            case 'PHPParser_Node_Stmt_Function':
                //printf("leaving function\n");
                $this->functions[] = $this->context->pop();
                break;
            case 'PHPParser_Node_Stmt_ClassMethod':
                //printf("leaving classMethod\n");
                $m = $this->context->pop();
                $this->addMethodToContext($m);
                break;
            case 'PHPParser_Node_Expr_Closure':
                $this->ignoreParams = false;
                break;
            case 'PHPParser_Node_Stmt_Property':
                $ps = $this->context->pop();
                $this->addPropertiesToContext($ps);
                break;
        }
    }

    public function enterNode(PHPParser_Node $node) {
        $nodeClass = get_class($node);
        switch ($nodeClass) {
            case 'PHPParser_Node_Stmt_Use':
                foreach ($node->uses as $use) {
                    //printf("use alias=%s ns=%s\n", $use->alias, ReflectionConsts::NS_DELIMETER . implode(ReflectionConsts::NS_DELIMETER, $use->name->parts));
                }
                break;
            case 'PHPParser_Node_Stmt_Interface':
                //printf("entering interface\n");
                $c = new Reflections\InterfaceReflection($node, $this->namespace);
                $this->context->push($c);
                break;
            case 'PHPParser_Node_Stmt_Class':
                //printf("entering class %s \n", $node->name);
                $c = new Reflections\ClassReflection($node, $this->namespace);
                $this->context->push($c);
                break;
            case 'PHPParser_Node_Stmt_Trait':
                $c = new Reflections\TraitReflection($node, $this->namespace);
                $this->context->push($c);
                break;
            case 'PHPParser_Node_Stmt_Function':
                $f = new Reflections\FunctionReflection($node, $this->namespace);
                $this->context->push($f);
                break;
            case 'PHPParser_Node_Stmt_ClassMethod':
                $f = new Reflections\ClassMethodReflection($node, $this->namespace, $this->context->top()->getName());
                $this->context->push($f);
                break;
            case 'PHPParser_Node_Stmt_Const':
                foreach ($node->consts as $constant) {
                    printf("const: %s\n", $constant);
                }
                break;
            case 'PHPParser_Node_Stmt_Namespace':
                $ns = ReflectionConsts::NS_DELIMETER . implode(ReflectionConsts::NS_DELIMETER, $node->name->parts);
                $this->context->top()->setNamespace($ns);
                break;
            case 'PHPParser_Node_Param':
                if (!$this->ignoreParams) {
                    $a = new Reflections\ArgumentReflection($node, $this->getContextDocBlock());
                    $this->addArgumentToContext($a);
                }
                break;
            case 'PHPParser_Node_Expr_Closure':
                $this->ignoreParams = true;
                break;
            case 'PHPParser_Node_Stmt_TraitUse':
                $this->context->top()->addTraits($node->traits);
                break;
            case 'PHPParser_Node_Stmt_Throw':
                $this->addExceptionToContext($node);
                break;
            case 'PHPParser_Node_Stmt_Property':
                $p = new Reflections\PropertiesReflection($node);
                $this->context->push($p);
                break;
            case 'PHPParser_Node_Stmt_PropertyProperty':
                $this->addPropertyToContext($node);
                break;
//            case 'PHPParser_Node_Stmt_Return':
//               var_dump($node);
//                break;
            default:
            //printf("\t%s\n", $nodeClass);
        }
    }

    public function afterTraverse(array $nodes) {
        
    }

    public function beforeTraverse(array $nodes) {
        
    }

    private function addMethodToContext($meth) {
        if (!$this->context->isEmpty()) {
            if (method_exists($this->context->top(), 'addMethod')) {
                $this->context->top()->addMethod($meth);
            }
        }
    }

    /**
     * Adds argument to current context (function or method)
     * 
     * @param \Reflections\ArgumentReflection $arg
     */
    private function addArgumentToContext($arg) {
        if (!$this->context->isEmpty()) {
            if (method_exists($this->context->top(), 'addArgument')) {
                $this->context->top()->addArgument($arg);
            }
        }
    }

    private function addExceptionToContext($node) {
        //$a = new \PHPParser_Node_Stmt_Throw;
        if (in_array('class', $node->expr->getSubNodeNames())) {
            $exc = ReflectionConsts::NS_DELIMETER . implode(ReflectionConsts::NS_DELIMETER, $node->expr->class->parts);
            if (!$this->context->isEmpty()) {
                if (method_exists($this->context->top(), 'addException')) {
                    $this->context->top()->addException($exc);
                }
            }
        }
    }

    /**
     * adds properties to class
     * @param type $ps
     */
    private function addPropertiesToContext($ps) {
        if (!$this->context->isEmpty()) {
            if (method_exists($this->context->top(), 'addProperties')) {
                $this->context->top()->addProperties($ps->getProperties());
            }
        }
    }

    /**
     * Adds property to properties
     * @param type $node
     */
    private function addPropertyToContext($node) {
        if (!$this->context->isEmpty()) {
            if (method_exists($this->context->top(), 'addProperty')) {
                $this->context->top()->addProperty($node);
            }
        }
    }

    private function getContextDocBlock() {
        $out = null;
        if (!$this->context->isEmpty()) {
            if (method_exists($this->context->top(), 'getDocBlock')) {
                $out = $this->context->top()->getDocBlock();
            }
        }

        return $out;
    }

    /**
     * 
     * @param string $v     namespace
     */
    public function setNamespace($v) {
        $this->namespace = $v;
    }

    /**
     * 
     * @return string
     */
    public function getNamespace() {
        return $this->namespace;
    }

    /**
     * Returns array of function nodes.
     * 
     * @return array
     */
    public function getFunctions() {
        return $this->functions;
    }

    /**
     * Returns array of class nodes.
     * 
     * @return array
     */
    public function getClasses() {
        return $this->classes;
    }

    /**
     * Returns array of interface nodes.
     * 
     * @return array
     */
    public function getInterfaces() {
        return $this->interfaces;
    }

    /**
     * Returns array of global constants.
     * 
     * @return array
     */
    public function getConstants() {
        return $this->constants;
    }

    /**
     * Returns array of trait nodes.
     * 
     * @return array
     */
    public function getTraits() {
        return $this->traits;
    }

    public function getAST() {
        return $this->ast;
    }

    public function getFilename() {
        return $this->filename;
    }

    public function getContent($includeDocblock = true) {
        return '';
    }

    public function getDocComment() {
        return '';
    }

    public function getEndLine() {
        return 0;
    }

    public function getName() {
        return $this->filename;
    }

    public function getStartLine($includeDocComment = false) {
        return 0;
    }

    public function getFileId() {
        return $this->fileId;
    }
    
    public function free() {
        if ($this->ast) {
            foreach ($this->ast as $node) {
                $node->free();
            }
        }
        $this->ast = null;
        unset($this->ast);
        $this->classes = null;
        $this->constants = null;
        $this->contents = null;
        $this->context = null;
        $this->functions = null;
        $this->interfaces = null;
        $this->traits = null;
    }

}
