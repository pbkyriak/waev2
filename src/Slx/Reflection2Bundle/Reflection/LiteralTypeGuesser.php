<?php

namespace Slx\Reflection2Bundle\Reflection;

use PHPParser_PrettyPrinter_Default;

/**
 * Guesses the type of a literal. Covers the cases:
 * 123;          -> int
 * 12.3;         -> float
 * 'abc';        -> string
 * true;         -> bool
 * array(...)    -> array
 *
 * added 9 Feb 2015
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class LiteralTypeGuesser {
    
    private $returnTypeCleared = false;
    
    /**
     * PHP AST pretty printer used to get representations of values.
     *
     * @var PHPParser_PrettyPrinterAbstract
     */
    protected static $prettyPrinter = null;

    /**
     * Returns a simple human readable output for a value.
     *
     * @param PHPParser_Node_Expr $value The value node as provided by
     * PHP-Parser.
     *
     * @return string
     */
    public function getRepresentationOfValue($value = null) {
        if (null === $value) {
            return '';
        }

        if (!self::$prettyPrinter) {
            self::$prettyPrinter = new PHPParser_PrettyPrinter_Default();
        }

        return self::$prettyPrinter->prettyPrintExpr($value);
    }

    public function getDefaultValueType($node) {
        $value = $this->getRepresentationOfValue($node);
        $out = '';
        $this->returnTypeCleared = false;
        $clazz = get_class($node);
        printf("%s \n",$clazz);
        switch( $clazz ) {
            case 'PHPParser_Node_Scalar_String':
                $out = 'string';
                break;
            case 'PHPParser_Node_Scalar_DNumber':
                $out = 'float';
                break;
            case 'PHPParser_Node_Scalar_LNumber':
                $out = 'int';
                break;
            case 'PHPParser_Node_Expr_ConstFetch':
                if(in_array(strtolower($value), array('true', 'false'))) {
                    $out = 'bool';
                }
                break;
            case 'PHPParser_Node_Expr_Array':
                $out = 'array';
                break;
        }
        if( $out ) {
            $this->returnTypeCleared = true;
        }
        return $out;
    }
    
    public function isReturnTypeCleared() {
        return $this->returnTypeCleared;
    }
    
}
