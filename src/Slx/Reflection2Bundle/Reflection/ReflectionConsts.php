<?php

namespace Slx\Reflection2Bundle\Reflection;

/**
 * Description of ReflectionConsts
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ReflectionConsts {

    const NS_DELIMETER = '\\';
    
    public static $typeHintIntTypes = array('array', 'callable');
    
    
}
