<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

use Slx\Reflection2Bundle\Reflection\ReflectionConsts;
/**
 * Description of AbstractClassReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractClassReflection {
    protected $name;
    protected $namespace;
    protected $modifiers = array();
    protected $methods = array();
    protected $extends;
    protected $interfaces = array();
    protected $traits = array();
    protected $properties = array();
    
    public function __construct(\PHPParser_Node $node, $namespace) {
        $this->name = $node->name;
        $this->namespace = $namespace;
        $this->setExtends($node->extends);
        $this->collectModifiers($node);
        $this->collectInterfaces($node);
    }
    
    protected function collectModifiers(\PHPParser_Node $node) {
        $this->modifiers = array();
        if( $node->isAbstract() ) {
            $this->modifiers[] = 'abstract';
        }
        if( $node->isFinal() ) {
            $this->modifiers[] = 'final';
        }        
    }
    
    protected function collectInterfaces(\PHPParser_Node $node) {
        $this->interfaces = array();
        foreach($node->implements as $iface) {
            $this->interfaces[] = ReflectionConsts::NS_DELIMETER. $iface;
        }
    }
    
    protected function setExtends($extends) {
        $this->extends = '';
        if($extends) {
            $this->extends = ReflectionConsts::NS_DELIMETER. implode(ReflectionConsts::NS_DELIMETER, $extends->parts);
        }
    }


    public function addMethod($meth) {
        $this->methods[] = $meth;
    }
    
    public function getMethods() {
        return $this->methods;
    }
    
    public function addTraits($traits) {
        foreach($traits as $trait) {
            $this->traits[] = ReflectionConsts::NS_DELIMETER. implode(ReflectionConsts::NS_DELIMETER, $trait->parts);
        }
    }
    
    public function getTraits() {
        return $this->traits;
    }
        
    public function addProperties($props) {
        $this->properties = array_merge($this->properties, $props);
    }
    
    public function getProperties() {
        return $this->properties;
    }
    
    public function getFQName() {
        return $this->namespace .ReflectionConsts::NS_DELIMETER . $this->name;
    }
    
    public function getName() {
        return $this->name;
    }

}
