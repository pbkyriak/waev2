<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

use Slx\DocBlockBundle\DocBlock\DocBlock;
use Slx\Reflection2Bundle\Exception\ReflectionException;
use Slx\Reflection2Bundle\Reflection\ReturnTypeGuesser;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;

/**
 * Description of AbstractFunctionReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractFunctionReflection extends AbstractReflection {
    
    protected $namespace = '';
    protected $name = '';
    protected $className = ''; 
    
    protected $arguments = array();
    protected $returnType;
    protected $returnTypeCleared = false;
    protected $modifiers = array();
    protected $exceptions = array();

    protected $bDeprecated = false;
    protected $bAccessTag = false;
    
    protected $codeBlockStartLine = -1;
    protected $codeBlockEndLine = -1;

    public function construct(\PHPParser_Node $node, $namespace, $className='') {
        $this->namespace = $namespace;
        $this->name = $node->name;
        $this->className = $className;
        $this->processDocBlock($node->getDocComment());
        $this->arguments = array();
        $this->collectModifiers($node); 
        $this->setReturnType($node);
        $this->findStartEndLine($node);
        $docBlock = $this->getDocBlock();
        if( $docBlock ) {
            $this->bAccessTag = $docBlock->hasAccessTag();
            $this->bDeprecated = $docBlock->isDeprecated();
        }
    }
    
    public function addArgument($arg) {
        $this->arguments[] = $arg;
    }

    /**
     * 
     * @return DocBlock
     */
    public function getDocBlock() {
        return $this->docBlock;
    }

    protected function collectModifiers($node) {
        $this->modifiers = array();
        $this->modifiers[] = $this->getVisibility();
    }

    protected function setReturnType(\PHPParser_Node $node) {
        $type = '';
        if( $this->docBlock ) {
            if( $this->docBlock->hasReturnTag() ) {
                $type = $this->docBlock->getReturnType();
                $this->returnTypeCleared = true;
            }
        }
        if( !$type ) {
            $rtg = new ReturnTypeGuesser();
            $type = $rtg->guess($node);
            $this->returnTypeCleared = $rtg->isReturnTypeCleared();
        }
        $this->returnType = $type;
    }

    /**
     * function visibility. If there is access docBlock use it, 
     * if name starts with _ or __ private, else public
     * 
     * @return string
     */
    protected function getVisibility() {
        $out = null;
        if( $this->docBlock ) {
            if( $this->docBlock->hasAccessTag() ) {
                $out = $this->docBlock->getAccessType();
            }            
        }
        if( !$out ) {
            if( substr($this->name,0,1)=='_' && substr($this->name,0,2)!='__' && strlen($this->name)>1 ) {
                $out = 'private';
            }
        }
        return $out;
    }

    public function getFQName() {
        $out = $this->namespace .ReflectionConsts::NS_DELIMETER;
        if( $this->className ) {
            $out .= $this->className . '::';
        }
        $out .= $this->name;
        return $out;
    }

    public function addException($exc) {
        $this->exceptions[] = $exc;
    }
    
    public function getExceptions() {
        return $this->exceptions;
    }

    public function isDeprecated() {
        return $this->bDeprecated;
    }
    
    public function hasAccessTag() {
        return $this->bAccessTag;
    }
    
    protected function findStartEndLine($node) {
        // find block start and end lines
        foreach ($node->stmts as $stmt) {
            $this->codeBlockStartLine = $this->codeBlockStartLine == -1 ? $stmt->getAttribute('startLine') : $this->codeBlockStartLine;
            $this->codeBlockEndLine = $stmt->getAttribute('endLine');
        }
    }
    
    protected function getCodeBlockContent($filename)
    {
        $content = '';
        $out = '';
        if(file_exists($filename)) {
            $content = file($filename);
        }
        $lineI = $this->getCodeBlockStartLine();
        $lineF = $this->getCodeBlockEndLine() - $this->getCodeBlockStartLine();
        if( $content && ($lineF>=$lineI) ) {
            $out = implode("\n",
                array_splice(
                    $content, $lineI, $lineF, true
                )
            );
        }
        return $out;
    }

    public function getCodeBlockStartLine()
    {
        return $this->codeBlockStartLine;
    }

    public function getCodeBlockEndLine()
    {
        return $this->codeBlockEndLine;
    }

}
