<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

/**
 * Description of AbstractMethodReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractMethodReflection extends AbstractFunctionReflection {
    
    protected function collectModifiers($node) {
        $this->modifiers = array();
        if( $node->isAbstract() ) {
            $this->modifiers[] = 'abstract';
        }
        if( $node->isFinal() ) {
            $this->modifiers[] = 'final';
        }
        if( $node->isPrivate() ) {
            $this->modifiers[] = 'private';
        }
        if( $node->isProtected() ) {
            $this->modifiers[] = 'protected';
        }
        if( $node->isPublic() ) {
            $tVis = $this->getVisibility();
            $this->modifiers[] = $tVis ? $tVis : 'public';
        }
        if( $node->isStatic() ) {
            $this->modifiers[] = 'static';
        }
    }

}
