<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

use PHPParser_PrettyPrinter_Default;
use Slx\DocBlockBundle\DocBlock\DocBlock;
/**
 * Description of AbstractReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractReflection {

    protected $docBlock=null;
    
    /**
     * 
     * @param  null|PHPParser_Comment_Doc Doc comment object or null
     */
    protected function processDocBlock(\PHPParser_Comment_Doc $docComment=null) {
        if( $docComment ) {
            try {
                $this->docBlock = new DocBlock($docComment->getText());
            } catch (ReflectionException $ex) {
                $this->docBlock = null;
            }    
        }
    }
  
}
