<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

use Slx\DocBlockBundle\DocBlock\DocBlock;
use Slx\Reflection2Bundle\Reflection\LiteralTypeGuesser;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;
/**
 * Description of ArgumentReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ArgumentReflection extends AbstractReflection {

    private $name;
    private $byRef=false;
    private $optional = false;
    private $default = null;
    private $type = '';
    private $typeCleared = false;
    
    public function __construct(\PHPParser_Node_Param $param, DocBlock $docBlock=null) {
        $this->name = '$'.$param->name;
        $this->byRef = $param->byRef;
        $defValType = '';
        if ($param->default) {
            $this->optional = true;
            $lg= new LiteralTypeGuesser();
            $this->default = $lg->getRepresentationOfValue($param->default);
            $defValType = $lg->getDefaultValueType($param->default);
        }
        $this->type = $param->type;
        // not empty means that there is type hint. If not array or callable then its a class. Add \ prefix for namespace 
        if( !empty($this->type) && !in_array($this->type, ReflectionConsts::$typeHintIntTypes) ) {
            $this->type = ReflectionConsts::NS_DELIMETER . $this->type;
        }
        if( empty($this->type) && $docBlock ) {
            $this->type = $docBlock->getVariableParam($this->name);
        }
        if( empty($this->type) && $defValType ) {
            $this->type = $defValType;
            $this->typeCleared = true;
        }
        if( empty($this->type) ) {
            $this->type = 'mixed';
        }
        else {
            $this->typeCleared = true;
        }
    }
    
    public function __toString() {
        $parts = array();
        if($this->byRef) {
            $parts[] = 'ByRef';
        }
        if($this->type) {
            $parts[] = $this->type;
        }
        $parts[] = $this->name;
        if( $this->default ) {
            $parts[] = '='.$this->default;
        }
        $out = implode(' ', $parts);
        if( $this->optional ) {
            $out = sprintf("[%s]", $out);
        }
        return $out;
    }
}
