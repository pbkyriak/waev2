<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

/**
 * Description of ClassMethodReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ClassMethodReflection extends AbstractMethodReflection {
    
    public function __construct(\PHPParser_Node $node, $namespace, $className) {
        $this->construct($node, $namespace, $className);
    }
    
    public function getSignature() {
        $outArg = implode(',', $this->arguments);
        $outMod = implode(',', $this->modifiers);
        $out = sprintf("%s %s %s(%s)", $outMod, $this->returnType, $this->getFQName(), $outArg);
        
        return $out;
    }

}
