<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

/**
 * Description of ClassReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ClassReflection extends AbstractClassReflection {
        
    public function __toString() {
        $out = '';
        if( $this->modifiers ) {
            $out .= implode(',',$this->modifiers).' ';
        }
        $out .= $this->getFQName();
        if($this->extends) {
            $out .= ' extends '.$this->extends;
        }
        if( $this->interfaces ) {
            $out .= ' implements '.implode(',',$this->interfaces);
        }
        return $out;
    }
}
