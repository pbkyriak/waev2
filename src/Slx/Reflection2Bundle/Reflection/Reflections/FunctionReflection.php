<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;


/**
 * Description of FunctionReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FunctionReflection  extends AbstractFunctionReflection {

    
    public function __construct(\PHPParser_Node $node, $namespace) {
        $this->construct($node, $namespace);
    }
              
    public function getSignature() {
        $outArg = implode(',', $this->arguments);
        $out = sprintf("%s %s(%s)", $this->returnType, $this->getFQName(), $outArg);
        
        return $out;
    }
    
}
