<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

/**
 * Description of PropertiesReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PropertiesReflection extends AbstractReflection {
    
    protected $properties = array();
    protected $modifiers = array();
    
    public function __construct(\PHPParser_Node_Stmt_Property $node) {
        $this->collectModifiers($node);
        $this->processDocBlock($node->getDocComment());
    }
    
    protected function collectModifiers($node) {
        $this->modifiers = array();
        if( $node->isPrivate() ) {
            $this->modifiers[] = 'private';
        }
        if( $node->isProtected() ) {
            $this->modifiers[] = 'protected';
        }
        if( $node->isPublic() ) {
            $this->modifiers[] = 'public';
        }
        if( $node->isStatic() ) {
            $this->modifiers[] = 'static';
        }
    }

    public function addProperty($node) {
        $prop = new PropertyReflection($node, $this->modifiers, $this->docBlock);
        $this->properties[] = $prop;
    }
    
    public function getProperties() {
        return $this->properties;
    }
}
