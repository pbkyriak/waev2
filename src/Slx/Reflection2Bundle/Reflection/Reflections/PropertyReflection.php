<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

use Slx\Reflection2Bundle\Reflection\LiteralTypeGuesser;
/**
 * Description of FieldReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class PropertyReflection extends AbstractReflection {
    
    private $name;
    private $modifiers = array();
    private $type = '';
    private $typeCleared = false;
    
    public function __construct($node, $modifiers, $docBlock) {
        $this->modifiers = $modifiers;
        $this->name = $node->name;
        $this->setVariableType($node, $docBlock);
    }
    
    private function setVariableType(\PHPParser_Node_Stmt_PropertyProperty $node, $docBlock) {
        if( $docBlock ) {
            $this->type = $docBlock->getVariableVariable('$'.$this->name);
        }
        else {
            $lg= new LiteralTypeGuesser();
            $this->type = $lg->getDefaultValueType($node->default);
        }
        if( !$this->type ) {
            $this->type = 'mixed';
        }
        else {
            $this->typeCleared = true;
        }
    }
    
    public function __toString() {
        return sprintf("%s %s %s", 
                implode(',', $this->modifiers),
                $this->name,
                $this->type
                );
    }
}
