<?php

namespace Slx\Reflection2Bundle\Reflection\Reflections;

/**
 * Description of TraitReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class TraitReflection extends AbstractClassReflection {

    
    protected function collectModifiers(\PHPParser_Node $node) {
        $this->modifiers = array();
    }
    
    protected function collectInterfaces(\PHPParser_Node $node) {
        $this->interfaces = array();
    }

    public function __toString() {
        $out = $this->getFQName();
        $out .= ' '.$this->extends;
        $out .= ' '.implode(',',$this->interfaces);
        $out .= ' '.implode(',',$this->modifiers);
        return $out;
    }

}
