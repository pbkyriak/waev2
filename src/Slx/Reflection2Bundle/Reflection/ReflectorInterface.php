<?php

namespace Slx\Reflection2Bundle\Reflection;

/**
 * Description of ReflectorInterface
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
interface ReflectorInterface
{
   public function getName();

    /**
     * Returns node starting line.
     * 
     * @param bool $includeDocComment
     * @return int
     */
    public function getStartLine($includeDocComment = false);
    
    /**
     * Node ending line
     * 
     * @return int
     */
    public function getEndLine();
    
    /**
     * Node filename.
     * 
     * @return string 
     */
    public function getFilename();
    
    /**
     * File database Id
     * 
     * @return int
     */
    public function getFileId();
    /**
     * Returns node's doc comment.
     * 
     * @return string|null
     */
    public function getDocComment();
    
    /**
     * Returns the block of code that the node represents.
     * 
     * @param bool $includeDocblock
     * 
     * @return string
     */
    public function getContent($includeDocblock = true);
}
