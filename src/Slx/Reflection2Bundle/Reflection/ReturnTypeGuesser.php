<?php

namespace Slx\Reflection2Bundle\Reflection;

use Slx\Reflection2Bundle\Reflection\StmtFinder;
use Slx\Reflection2Bundle\Reflection\LiteralTypeGuesser;

/**
 * ReturnType Guesser for 1st level of static analysis.
 * Finds the last return statement in function/method and if
 * return value is literal it tries to guess its type, or if return value
 * is array returns array using LiteralTypeGuesser.
 * Covers the cases (same with LiteralTypeGuesser):
 * 
 * return 123;          -> int
 * return 12.3;         -> float
 * return 'abc';        -> string
 * return true;         -> bool
 * return array(...)    -> array
 *
 * in any other case returns mixed and isReturnTypeCleared=false;
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ReturnTypeGuesser {

    private $returnTypeCleared = false;
    
    public function guess($node) {
        // traverse function nodes to find return statements
        $returns = StmtFinder::getInstance()
                ->find($node->stmts, 'PHPParser_Node_Stmt_Return')
                ->getResults();
        if (count($returns)) {
            $lreturn = array_pop($returns);
            $lg = new LiteralTypeGuesser();
            $type = $lg->getDefaultValueType($lreturn->expr);
            if (!$type) {
                $type = 'mixed';
            } else {
                $this->returnTypeCleared = true;
            }
        } else {
            $type = 'void';
        }
        return $type;
    }
    
    public function isReturnTypeCleared() {
        return $this->returnTypeCleared;
    }
}
