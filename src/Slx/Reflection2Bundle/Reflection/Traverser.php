<?php

namespace Slx\Reflection2Bundle\Reflection;

use PHPParser_NodeTraverser;
use PHPParser_NodeVisitor;
use PHPParser_NodeVisitor_NameResolver;
use PHPParser_NodeVisitorAbstract;

/**
 * The source code traverser that scans the given source code and transforms
 * it into tokens.
 *
 * @author Mike van Riel <mike.vanriel@naenius.com> 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class Traverser
{
    private $ast;
    
    /**
     * List of visitors to apply upon traversing.
     *
     * @see traverse()
     *
     * @var PHPParser_NodeVisitorAbstract[]
     */
    public $visitors = array();

    /**
     * 
     *
     * @param string $contents The source code of the file that is to be scanned
     *
     * @return void
     */
    public function traverseAst($ast) {
        $this->ast = $ast;
        $this->createTraverser()->traverse(
            $this->ast
        );      
    }
    
    /**
     * Adds a visitor object to the traversal process.
     *
     * With visitors it is possible to extend the traversal process and
     * modify the found tokens.
     *
     * @param PHPParser_NodeVisitor $visitor
     *
     * @return void
     */
    public function addVisitor(PHPParser_NodeVisitor $visitor)
    {
        $this->visitors[] = $visitor;
    }

    /**
     * Creates a new traverser object and adds visitors.
     *
     * @return PHPParser_NodeTraverser
     */
    protected function createTraverser()
    {
        $node_traverser = new PHPParser_NodeTraverser();
        $node_traverser->addVisitor(new PHPParser_NodeVisitor_NameResolver());

        foreach ($this->visitors as $visitor) {
            $node_traverser->addVisitor($visitor);
        }

        return $node_traverser;
    }
    
    public function free() {
        $this->ast = null;
    }
}
