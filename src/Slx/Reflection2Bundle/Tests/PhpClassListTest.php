<?php

namespace Slx\Reflection2Bundle\Tests;
use Slx\Reflection2Bundle\Util\PhpClassList;
/**
 * Description of PhpClassListTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Ιαν 2016
 */
class PhpClassListTest extends \PHPUnit_Framework_TestCase {

    public function test01() {
        $ci = PhpClassList::getInstance();
        $this->assertEquals(true,$ci->isPhpClass('\\Yar_Client'));
        $this->assertEquals(false,$ci->isPhpClass('\\MyException'));
    }

}
