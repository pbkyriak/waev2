<?php

namespace Slx\Reflection2Bundle\Tests;

use Slx\Reflection2Bundle\Reflection\Traverser;

/**
 * Traits tests
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Signature2 extends \PHPUnit_Framework_TestCase {

    private function getAST($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        return $stmts;
    }

    private function getFileReflector($fn) {
        $fr = new \Slx\Reflection2Bundle\Reflection\FileReflector($fn, 66);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        return $fr;
    }

    public function stest01() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature01.php');
        $f = array_pop($fr->getFunctions());
        $this->assertEquals('void \fA(mixed $a)',$f->getSignature());
        
    }
    
    public function stest02() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature02.php');
        $f = array_pop($fr->getFunctions());
        $this->assertEquals('void \Fname(mixed $a,ByRef mixed $b,[ByRef bool $c =true])',$f->getSignature());
    }

    public function stest03() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature03.php');
        $f = array_pop($fr->getFunctions());
        $this->assertEquals('void \Fname2(int $a,ByRef string $b,array $c)',$f->getSignature());
    }

    /**
     * return types test with int
     */
    public function test04() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature04.php');
        $f = array_shift($fr->getFunctions());
        $this->assertEquals('int \Fname1(mixed $a,mixed $b)',$f->getSignature());
        
    }

    /**
     * return types test for string, bool, mixed
     */
    public function test05() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature05.php');
        $funcs = $fr->getFunctions();
        $f = array_shift($funcs);
        $this->assertEquals('string \Fname2(mixed $a,mixed $b)',$f->getSignature());
        $f = array_shift($funcs);
        $this->assertEquals('bool \Fname3(mixed $a,mixed $b)',$f->getSignature());
        $f = array_shift($funcs);
        $this->assertEquals('mixed \Fname4(mixed $a,mixed $b)',$f->getSignature());
    }

    public function test15() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature06.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('abstract \Slx\Myspace\Bar extends \Slx\Myspace\Decor implements \Slx\Myspace\CL1,\Slx\Myspace\CL2',(string)$c);
        $c = array_shift($classes);
        $this->assertEquals('\Slx\Myspace\Foo',(string)$c);
        $meths = $c->getMethods();
        $m = array_shift($meths);
        $this->assertEquals('public int \Slx\Myspace\Foo::methA(int $a,Bar $b)',$m->getSignature());
        $m = array_shift($meths);
        $this->assertEquals('public mixed \Slx\Myspace\Foo::methB(mixed $a,\Slx\Myspace\Bar $b)',$m->getSignature());
    }

    /**
     * test class namespace in file with multiple namespaces
     */
    public function test16() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/Signature07.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\WithNamespaces',(string)$c);
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns02\WithNamespaces',(string)$c);
    }

    /**
     * test namespace for multiple namespace in same file
     */
    public function test17() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces02.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName extends \Ns10\Ns11\ParentClass',(string)$c);
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns02\CName2 extends \Ns00\Ns02\ParentClass',(string)$c);
    }

    /**
     * test namespace for extends
     */
    public function test18() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces03.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName extends \Ns10\Ns11\ParentClass',(string)$c);
    }
    
    /**
     * test namespace alias for extends
     */
    public function test19() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces04.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName extends \Ns10\Ns11\ParentClass',(string)$c);
    }

    /**
     * test namespace alias for extends and implements with alias only
     */
    public function test20() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces05.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName extends \Ns10\Ns11\ParentClass implements \Ns10\Ns12\ParentClass',(string)$c);
    }

    /**
     * test namespace alias for extends and implements with alias and with out alias. Interface1 belongs to the same namespace with the class
     */
    public function test21() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces06.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName extends \Ns10\Ns11\ParentClass implements \Ns00\Ns01\Interface1,\Ns10\Ns12\ParentClass2',(string)$c);
    }

    /**
     * test namespace alias for implements with FQN in interface
     */
    public function test22() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces07.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName2 implements \Ns10\Ns13\Interface1',(string)$c);
    }

    /**
     * test namespace with relative namespace in extends and implements
     */
    public function test23() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces08.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $this->assertEquals('\Ns00\Ns01\CName2 extends \Ns00\Ns01\Ns012\Parent1 implements \Ns00\Ns01\Ns011\Interface1',(string)$c);
    }

    /**
     * test traits with namespaces
     */
    public function test24() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/WithNamespaces10.php');
        $classes = $fr->getClasses();
        $c = array_shift($classes);
        $traits = $c->getTraits();
        $t = array_shift($traits);
        $this->assertEquals('\Ns00\Ns01\Trait1',(string)$t);
        $t = array_shift($traits);
        $this->assertEquals('\Ns10\Ns11\Trait2',(string)$t);
        $t = array_shift($traits);
        $this->assertEquals('\Ns00\Ns01\Ns011\Ns0111\Trait2',(string)$t);
    }

    public function test25() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/LitRetVal01.php');
        $this->dumpArtifacts($fr);
    }
    
    public function test26() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/Reflection2Bundle/Tests/UseCases/adodb.inc.php');
        $this->dumpArtifacts($fr);
    }
    
    private function dumpArtifacts(\Slx\Reflection2Bundle\Reflection\FileReflector $fr) {
        printf("\n========================================\n");
        foreach($fr->getClasses() as $c) {
            printf("C: %s \n", $c);
            $this->dumpCProps($c);
            $this->dumpCMethods($c);
            foreach($c->getTraits() as $t) {
                printf("\tT: %s \n", $t);
            }        
        }
        foreach($fr->getTraits() as $c) {
            printf("T: %s \n", $c);
            $this->dumpCMethods($c);
        }
        foreach($fr->getInterfaces() as $c) {
            printf("I: %s \n", $c);
            $this->dumpCMethods($c);
        }
        foreach($fr->getFunctions() as $f) {
            printf("F: %s \n", $f->getSignature());
        }
    }
    
    private function dumpCMethods($c) {
        foreach($c->getMethods() as $m) {
            printf("\tM: %s \n", $m->getSignature());
            foreach($m->getExceptions() as $e) {
                printf("\t\tE: %s \n", $e);
            }
        }                    
    }
    
    private function dumpCProps($c) {
        foreach($c->getProperties() as $p) {
            printf("\tP: %s\n", $p);
        }
    }
    
}
