<?php

function myFunc3($a) {
    return 'ab'.'cd';
}

function myFunc4($a) {
    return $a.'ed';
}

function myFunc5($a, $b) {
    return $a.$b;
}
