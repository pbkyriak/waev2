<?php
namespace Slx\Myspace;

abstract class Bar extends Decor implements CL1, CL2 {
    
} 

class Foo {
    
    /**
     * 
     * @param int $a
     * @param Bar $b
     * @return int
     */
    public function methA($a, $b) {
        return 1;
    }
    
    public function methB($a, Bar $b) {
        return $a;
    }
}