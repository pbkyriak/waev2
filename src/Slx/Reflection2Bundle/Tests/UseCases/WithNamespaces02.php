<?php
/**
 * the case is to check if FQN of the extends is collected right
 * class FQN is \Ns00\Ns01\CName
 * extends \Ns10\Ns11\ParentClass
 */
namespace Ns00\Ns01;

class CName extends \Ns10\Ns11\ParentClass {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}

namespace Ns00\Ns02;

class CName2 extends ParentClass {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}