<?php

namespace Slx\Reflection2Bundle\Util;

/**
 * Description of PhpClassIndex
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Ιαν 2016
 */
class PhpClassList {

    private static $instance = null;
    private $classList;

    private function __construct() {
        $this->loadClassList();
    }

    public static function getInstance() {
        if (null === static::$instance) {
            static::$instance = new static();
        }
        return static::$instance;
    }

    private function loadClassList() {
        $this->classList = array();
        $handle = fopen(__DIR__ . '/../Resources/data/phpdefs.csv', "r");
        if ($handle) {
            while ($row = fgetcsv($handle)) {
                if(strpos($row[0], '::')>1) {  // its a method
                    $name = '\\'.str_replace('"','', substr($row[0], 0, strpos($row[0], '::')));
                    if( !in_array($name, $this->classList)) {
                        $this->classList[] = $name;
                    }
                }
            }
            fclose($handle);
        }
    }

    public function isPhpClass($name) {
        $out = false;
        if( in_array($name, $this->classList) ) {
            $out = true;
        }
        return $out;
    }
    
    public function dump() {
        foreach($this->classList as $name) {
            printf("%s\n", $name);
        }
    }
}
