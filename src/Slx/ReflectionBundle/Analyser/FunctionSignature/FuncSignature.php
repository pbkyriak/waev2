<?php

namespace Slx\ReflectionBundle\Analyser\FunctionSignature;

use Slx\ReflectionBundle\Analyser\FunctionSignature\FuncSignatureArgument;
/**
 * Description of FuncSignature
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncSignature
{
    private $name;
    private $returnType;
    private $arguments;
    private $codeBlockHash;
    private $namespace;
    private $modifiers=array();
    private $is_deprecated = false;
    private $has_access_tag = false;
    private $bIsReturnCleared = false;
    
    public function __construct($name, $returnType, $codeBlockHash, $namespace)
    {
        $this->name = $name;
        $this->returnType = $returnType;
        $this->arguments = array();
        $this->codeBlockHash = $codeBlockHash;
        $this->namespace = $namespace;
    }
    
    public function setModifiers($modifiers) {
        $this->modifiers = $modifiers;
        return $this;
    }
    
    public function getModifiers() {
        return $this->modifiers;
    }
    
    public function addNewArgument($byRef, $type, $optional, $defaultValue) {
        $this->arguments[] = new FuncSignatureArgument($byRef, $type, $optional, $defaultValue);
        return $this;
    }
    
    public function addArgument(FuncSignatureArgument $argument) {
        $this->arguments[] = $argument;
        return $this;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getReturnType()
    {
        return $this->returnType;
    }

    public function getArguments()
    {
        return $this->arguments;
    }

    public function getCodeBlockHash()
    {
        return $this->codeBlockHash;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setReturnType($returnType)
    {
        $this->returnType = $returnType;
        return $this;
    }

    public function setArguments($arguments)
    {
        $this->arguments = $arguments;
        return $this;
    }

    public function setCodeBlockHash($codeBlockHash)
    {
        $this->codeBlockHash = $codeBlockHash;
        return $this;
    }

    public function __toString()
    {
        return $this->toString();
    }
    
    public function toString() {
        $params = array();
        foreach($this->arguments as $arg) {
            $params[] = $arg->toString();
        }
        $out = sprintf(
            "%s(%s)", 
            $this->getName(), 
            implode(',', $params)
        );
        if( $this->getModifiers() ) {
            $out = sprintf("%s %s", implode(' ', $this->getModifiers()), $out);
        }
        return $out;
    }
    
    public function setIsDeprecated($v) {
        $this->is_deprecated = $v;
        return $this;
    }
    
    public function isDeprecated() {
        return $this->is_deprecated;
    }
    
    public function setHasAccessTag($v) {
        $this->has_access_tag =  $v;
        return $this;
    }
    
    public function hasAccessTag() {
        return $this->has_access_tag;
    }
    
    public function getNamespace() {
        return $this->namespace;
    }
    
    public function isReturnCleared() {
        return $this->bIsReturnCleared;
    }
    
    public function setIsReturnCleared($v) {
        $this->bIsReturnCleared = $v;
        return $this;
    }
}
