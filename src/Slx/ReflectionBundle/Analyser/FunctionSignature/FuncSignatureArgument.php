<?php

namespace Slx\ReflectionBundle\Analyser\FunctionSignature;

/**
 * Description of FuncSignatureArgument
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncSignatureArgument
{

    private $type;
    private $byRef;
    private $optional;
    private $defaultValue;

    public function __construct($byRef, $type, $optional, $defaultValue)
    {
        $this->byRef = $byRef;
        $this->type = $type;
        $this->optional = $optional;
        $this->defaultValue = $defaultValue;
    }

    public function isByRef() {
        return $this->byRef;
    }
    
    public function getType() {
        return $this->type;
    }
    
    public function isOptional() {
        return $this->optional;
    }
    
    public function getDefaultValue() {
        return $this->defaultValue;
    }
    
    public function toString()
    {
        $type = $this->getType();
        if ($this->isOptional()) {
            $type = sprintf('[%s=%s]', $type, $this->getDefaultValue());
        }
        return ($this->isByRef() ? 'byref ' : '') . $type;
    }

}
