<?php

namespace Slx\ReflectionBundle\Analyser;

use Symfony\Component\Finder\Finder;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Mongo;

/**
 * Description of SourceCodeHelper
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class SourceCodeHelper {
    /* @var $tag ProjectTag */
    private $tag;
    /* @var $grid \MongoGridFS */
    private $grid;
    private $astFilePrefix;
    
    public function __construct(ProjectTag $tag) {
        $con = new Mongo("mongodb://192.168.1.106"); // Connect to Mongo Server
        $db = $con->selectDB("waev"); // Connect to Database
        $this->grid = $db->getGridFS();
        $this->tag = $tag;
        $this->astFilePrefix = sprintf("ast-%s-%s-", $this->tag->getProject()->getId(), $this->tag->getId());
    }

    public function setProjectTag(ProjectTag $tag) {
        $this->tag = $tag;
        $this->astFilePrefix = sprintf("ast-%s-%s-", $this->tag->getProject()->getId(), $this->tag->getId());        
    }
    
    /**
     * Reads files from file system
     * @return array
     */
    public function getTagSourceFiles() {
        $codePath = $this->tag->getSourceCodePath();
        $exts = explode(',', $this->tag->getProject()->getFileExtensions());
        if (!$exts) {
            $exts = array('php');
        }
        $finder = new Finder();
        $finder->in($codePath);
        foreach ($exts as $ext) {
            $finder->name(sprintf('*.%s', $ext));
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRealpath();
        }

        return $files;
    }

    /**
     * Reads project_file db table
     * @return array
     */
    public function getTagSourceFilesDB($em, $onlyGood = true) {
        $data = array();
        $sql = "SELECT id,fname FROM project_file Where project_tag_id=:tid ";
        if ($onlyGood) {
            $sql .= "and parser='good'";
        }
        $params = array(
            'tid' => $this->tag->getId()
        );
        $stmt = $em
                ->getConnection()
                ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        array_walk($data, function(&$item) {
            $item['fname'] = $this->tag->getSourceCodePath() . $item['fname'];
        });
        return $data;
    }

    /**
     * Get the xml file with the ast for the $file.
     * @param string $file
     * @return string
     */
    public function getFileAstXmlFn($file) {
        $out = sprintf("%s/%s/%s.xml", $this->tag->getTagWorkingFolder(), 'ast', $file['id']);
        if (!file_exists(dirname($out))) {
            mkdir(dirname($out), 0777, true);
        }
        return $out;
    }

    /**
     * Load and unserialize the AST of the $file
     * @param string $file
     * @return array
     */
    public function getFileAst($file) {
        $ast = null;
        try {
            //$astFile = $this->getFileAstXmlFn($file);
            //if (file_exists($astFile)) {
           //    $content = file_get_contents($astFile);
                $content = $this->getAstFileFromStore($file['id']);
                if (!empty($content)) {
                    $unserializer = new \PHPParser_Unserializer_XML();
                    $ast = $unserializer->unserialize($content);
                }
           //}
        } catch (\PHPParser_Error $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\DomainException $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\InvalidArgumentException $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (\Exception $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        } catch (Exception $e) {
            //printf("%s file: %s Exception: %s\n",__CLASS__,$file['id'], $e->getMessage());
            $ast = false;
        }
        return $ast;
    }

    public function getAstFileFromStore($fileId, $checkOnly=false) {
        $out = false;
        $file = $this->grid->findOne(['filename'=>$this->astFilePrefix.$fileId]);
        if( $file ) {
            if($checkOnly) {
                $out = true;
            }
            else {
                $out = gzdecode($file->getBytes());
            }
        }
        else {
            printf("File not found in store : %s \n", $this->astFilePrefix.$fileId);
        }
        return $out;
    }

    public function setAstFileToStore($fileId, $data) {
        $metadata = ['filename'=> $this->astFilePrefix.$fileId];
        $bytes = gzencode($data);
        try {
            if( $this->getAstFileFromStore($fileId, true) ) {
                $this->grid->remove($metadata);
            }
            $id = $this->grid->storeBytes($bytes, $metadata);
            //printf("stored ast fileId=%s _id=%s id=%s\n", $fileId, $metadata['filename'], $id);
        }
        catch(\Exception $ex) {
            printf("Exception storing to gridFs %s\n", $ex->getMessage());
            $id= false;
        }
        return $id;
    }

}
