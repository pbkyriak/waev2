<?php

namespace Slx\ReflectionBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreFileReflection;
use Slx\ReflectionBundle\Reflection\FileReflector;
use Slx\ReflectionBundle\Reflection\Traverser;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
/**
 * Description of FuncSignaturesTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class FuncSignaturesTask extends AbstractTagTask
{

    private $badFiles = array();
    
    public function onFail() {
        return true;
    }

    public function getTaskName()
    {
        return 'Collects function and method signatures.';
    }

    public function execute() 
    {
        $mem1 = (memory_get_usage() / 1024);
        $this->badFiles = array();
        // cleanup prev data import for this tag
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectFuncSignature a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $this->tag->getId())
            ->execute();
        $this->em->getConnection()->delete('project_class', ['project_tag_id'=>$this->tag->getId()]);
        $this->em->getConnection()->delete('project_trait', ['project_tag_id'=>$this->tag->getId()]);
        $this->em->getConnection()->delete('project_interface', ['project_tag_id'=>$this->tag->getId()]);
        $files = $this->scHelper->getTagSourceFilesDB($this->em);
        foreach ($files as $file) {
            $this->processFile($file);
        }
        $this->updateClassRelations();
        $this->setTagStatus(ProjectTag::STATUS_RFGC_DONE);
        if( count($this->badFiles)) {
            $i=1;
            printf("Failed to read ast for the files\n");
            foreach($this->badFiles as $bf) {
                printf("  %s. %s\n", $i, $bf['id']);
                $i++;
            }
        }
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;

        return true;
    }

    private function processFile($file) {

        try {
            $ast = $this->scHelper->getFileAst($file);
        } catch (\Exception $e) {
            printf("%s Exception: %s\n", __CLASS__, $e->getMessage());
            $ast = null;
            gc_collect_cycles();
        } catch (Exception $e) {
            printf("%s Exception: %s\n", __CLASS__, $e->getMessage());
            $ast = null;
            gc_collect_cycles();
        }
        //printf("get ast for %s\n", $file);
        if( $ast ) {
            try {
                $reflector = new FileReflector($file);
                $traverser = new Traverser();
                $traverser->addVisitor($reflector);
                $traverser->traverseAst($ast);
                $this->em->clear();

                $this->tag = $this->getTagById($this->tagId);
                $this->em->getConnection()->query("SET autocommit=0;"); 
                $store = new StoreFileReflection($this->em, $this->tag, $file['id']);
                $store->persist($reflector);
                $this->em->getConnection()->query("COMMIT;");
                $this->em->clear();
                $reflector->free();
                $reflector=null;
                $traverser->free();
                $traverser=null;
                gc_collect_cycles();
            } catch(\InvalidArgumentException $e) {
                $this->em->getConnection()->query("ROLLBACK;");
                printf("%s file: %s InvalidArgumentException: %s\n",__CLASS__,$file['fname'], $e->getMessage());
            } catch (\Exception $e) {
                $this->em->getConnection()->query("ROLLBACK;");
                printf("%s file: %s Exception: %s\n",__CLASS__,$file['fname'], $e->getMessage());
            }
            $this->em->getConnection()->query("SET autocommit=1;"); 
        }                
        else {
            $this->badFiles[] = $file;
        }
        time_nanosleep(0, 10000000);
        
    }
    
    private function updateClassRelations() {
        $sql = "update project_class a, project_class b set a.parent_id=b.id
                where a.project_tag_id=:tid and a.extends!='' and b.project_tag_id=a.project_tag_id and a.extends=b.fqname and not isnull(b.id);";
        $this->em->getConnection()->executeUpdate($sql, ['tid'=>$this->tag->getId()]);
    }
}
