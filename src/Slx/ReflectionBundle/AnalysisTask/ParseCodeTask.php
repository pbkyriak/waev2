<?php

namespace Slx\ReflectionBundle\AnalysisTask;

use Symfony\Component\Finder\Finder;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\ReflectionBundle\Analyser\SourceCodeHelper;
use PHPParser_Error;
use PHPParser_Parser;
use PHPParser_Lexer;

/**
 * Description of ParseTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 16 Αυγ 2014
 */
class ParseCodeTask extends AbstractTagTask {

    public function onFail() {
        return true;
    }

    public function getTaskName() {
        return 'Parses source code and stores AST to disk';
    }

    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        // cleanup prev data import for this tag
        $badFiles = array();
        $files = $this->scHelper->getTagSourceFilesDB($this->em, false);
        foreach ($files as $file) {
            $res = $this->processFile($file);

            if (!($res == 'good')) {
                $this->markBadFile($file, $res);
            }
        }
        $this->setTagStatus(ProjectTag::STATUS_RFGC_DONE);
        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;

        return true;
    }
    
    private function processFile($file) {
        $result = 'good';
        if ($this->syntaxCheckFile($file)) {
            $ast = $this->parse($file);
            if ($ast) {
                $astFile = $this->storeAst($file, $ast);
                if (!$this->testAstFile($file)) {
                    $result = 'bad ast';
                }
            } else {
                $result = 'no ast';
            }
        } else {
            $result = 'syntax';
        }
        return $result;
    }

    /**
     * Creates a parser object using our own Lexer.
     *
     * @return PHPParser_Parser
     */
    protected function createParser() {
        return new PHPParser_Parser(new PHPParser_Lexer());
    }

    private function syntaxCheckFile($file) {
        $out = false;
        $command = sprintf("php -l %s", $file['fname']);
        $result = exec($command);
        if (strpos($result, 'No syntax errors detected') === 0) {
            $out = true;
        }
        return $out;
    }

    /**
     * parse code
     */
    private function parse($file) {
        try {
            $contents = file_get_contents($file['fname']);
            return $this->createParser()->parse($contents);
        } catch (PHPParser_Error $e) {
            //printf('file: %sParse Error: %s\n',$file, $e->getMessage());
            return false;
        } catch (\Exception $e) {
            //printf('file: %sParse Error: %s\n',$file, $e->getMessage());
            return false;
        }
    }

    private function storeAst($file, $ast) {
        //$outFile = $this->scHelper->getFileAstXmlFn($file);
        $serializer = new \PHPParser_Serializer_XML();
        $xml = $serializer->serialize($ast);
        $outFile = $this->scHelper->setAstFileToStore($file['id'], $xml);
        //file_put_contents($outFile, $xml);
        return $outFile;
    }

    private function testAstFile($file) {
        $out = false;
        $ast = $this->scHelper->getFileAst($file);
        //$ast = $this->scHelper->getAstFileFromStore($file['id']);
        if ($ast) {
            $ast = null;
            gc_collect_cycles();
            time_nanosleep(0, 1000000);
            $out = true;
        }
        return $out;
    }

    private function markBadFile($file, $res) {

        $data = array('parser' => $res);
        $whr = array(
            'project_tag_id' => $this->tagId,
            'id' => $file['id'],
        );
        $this->em->getConnection()->update('project_file', $data, $whr);
    }

}
