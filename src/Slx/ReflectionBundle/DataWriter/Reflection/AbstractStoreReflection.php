<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\Reflection\BaseReflector;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of AbstractStoreReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractStoreReflection
{

    /** @var EntityManager */
    protected $em;
    protected $constructType = 'unknown';
    /** @var ProjectTag */
    protected $tag;
    protected $file;
    
    /**
     * 
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Slx\GitMinerBundle\Entity\ProjectTag $tag
     * @param \Slx\GitMinerBundle\Entity\ProjectFile|int $file
     */
    public function __construct(EntityManager $em, ProjectTag $tag, $file)
    {
        $this->em = $em;
        $this->tag = $tag;
        $this->file = $file;
    }

    abstract public function persist(ReflectorInterface $reflector);

    protected function getConstruct(ReflectorInterface $reflector)
    {
        $construct = $this->em
            ->getRepository('SlxGitMinerBundle:ProjectCodeConstruct')
            ->findOneBy(array(
            'projectTag' => $this->tag,
            'cname' => $this->formatCName($reflector),
            'ctype' => $this->constructType,
            )
        );
        return $construct;
    }

    protected function storeConstruct(ReflectorInterface $reflector, $signature, $hash) {
        return true;
        $query = "UPDATE project_code_construct SET signature=:sign, hash=:hash WHERE project_tag_id=:tag and cname=:cname and ctype=:ctype";
        $params = array(
            'tag'=>$this->tag->getId(),
            'cname'=>$this->formatCName($reflector),
            'ctype'=>$this->constructType,
            'sign' => $signature,
            'hash' => $hash,
        );
        return $this->em->getConnection()->executeUpdate($query, $params);    
    }
    
    protected function formatCName(ReflectorInterface $reflector)
    {
        return $reflector->getName();
    }
}
