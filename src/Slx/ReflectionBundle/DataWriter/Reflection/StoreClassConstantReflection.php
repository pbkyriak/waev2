<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreConstantReflection;

/**
 * Description of StoreClassConstantReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreClassConstantReflection extends StoreConstantReflection
{
    protected $constructType = 'class:constant';
    protected $className;
    
    public function __construct(EntityManager $em, ProjectTag $tag, ProjectFile $file, $classname)
    {
        parent::__construct($em,$tag,$file);
        $this->className=$classname;
    }
    
    protected function formatCName(ReflectorInterface $reflector)
    {
        return sprintf("%s::%s", $this->className, $reflector->getName());
    }

}
