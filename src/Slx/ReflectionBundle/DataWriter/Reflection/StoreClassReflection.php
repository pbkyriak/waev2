<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\DataWriter\Reflection\AbstractStoreReflection;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreMethodReflection;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of StoreClassReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreClassReflection extends AbstractStoreReflection
{

    protected $constructType = 'class';
    
    public function persist(ReflectorInterface $reflector)
    {
        $classId = $this->storeClassDefinition($reflector);
        if( $classId ) {
            $this->storeInterfaces($reflector, $classId);
            $this->storeTraits($reflector, $classId);
            $this->storeMethods($reflector, $classId);
            $this->storeConstants($reflector, $classId);
        }
    }
    
    private function storeClassDefinition(ReflectorInterface $reflector) {
        $classId = 0;
        $query = "INSERT INTO project_class (project_tag_id, fname, modifiers, namespace, extends, project_file_id, fqname) "
            . "VALUES (:tagId, :fname, :modifiers, :namespace, :extends, :fid, :fqn)";
        $params = array(
            'tagId'=>$this->tag->getId(),
            'fname'=>$reflector->getName(),
            'modifiers' => implode(',',$reflector->getModifiers()),
            'namespace' => $reflector->getNamespace(),
            'extends' => $reflector->getParentClass(),
            'fid' => $this->file->getId(),
            'fqn' => $reflector->getFQName(),
        );
        if( $params['fname'] ) {
            try {
                $this->em->getConnection()->executeUpdate($query, $params);    
                $classId = $this->em->getConnection()->lastInsertId();
            } catch (Exception $e) {
                $classId = 0;
            }
        }
        return $classId;
    }
    
    private function storeInterfaces(ReflectorInterface $reflector, $classId) {
        $query = "INSERT INTO project_class_interface (project_class_id, fname, namespace) VALUES (:classId, :fname, :ns)";
        foreach($reflector->getInterfaces() as $iface) {
            $params = array(
                'classId' => $classId,
                'fname' => $iface,
                'ns' => $iface
            );
            $this->em->getConnection()->executeUpdate($query, $params);    
        }
    }
    
    private function storeTraits(ReflectorInterface $reflector, $classId) {
        $query = "INSERT INTO project_class_traits (project_class_id, fname, namespace) VALUES (:classId, :fname, :ns)";
        foreach($reflector->getTraits() as $trait) {
            $params = array(
                'project_class_id' => $classId,
                'fname' => $trait,
                'ns' => $trait
            );
            $this->em->getConnection()->executeUpdate($query, $params);                
        }
    }
    
    private function storeMethods(ReflectorInterface $reflector, $classId)
    {
        foreach ($reflector->getMethods() as $mrefl) {
            $store = new StoreMethodReflection($this->em, $this->tag, $this->file, $reflector->getName());
            $store->setClassId($classId);
            $store->persist($mrefl);
        }        
    }
    
    private function storeConstants(ReflectorInterface $reflector, $classId) {
        foreach ($reflector->getConstants() as $crefl) {
            $store = new StoreClassConstantReflection($this->em, $this->tag, $this->file, $reflector->getName());
            $store->persist($crefl);
        }                
    }
}
