<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use \Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\DataWriter\Reflection\AbstractStoreReflection;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\Reflection\ConstantReflector;
use Slx\GitMinerBundle\Entity\ProjectCodeConstruct;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\ProjectFile;
/**
 * Description of StoreConstantReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreConstantReflection extends AbstractStoreReflection
{
    
    protected $constructType = 'constant';
    protected $tag;
    
    public function persist(ReflectorInterface $reflector)
    {
        if( !$this->storeConstruct($reflector, $reflector->getSignature(), '') ) {
            $construct = new ProjectCodeConstruct();
            $construct->setProjectFile($this->file)
                ->setProjectTag($this->tag)
                ->setCtype($this->constructType)
                ->setSignature($reflector->getSignature())
                ->setHash('')
                ->setCname($this->formatCName($reflector));
            $this->em->persist($construct);
        }
    }

}
