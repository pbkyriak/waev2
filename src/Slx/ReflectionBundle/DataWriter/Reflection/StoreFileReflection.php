<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\Reflection\BaseReflector;
use Slx\ReflectionBundle\Reflection\FileReflector;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\DataWriter\Reflection\AbstractStoreReflection;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreFunctionReflection;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreClassReflection;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreInterfaceReflection;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Exception;

/**
 * Description of StoreFileReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreFileReflection extends AbstractStoreReflection {

    public function persist(ReflectorInterface $reflector) {
        $this->file = $this->getProjectFile($this->file);
        if (!$this->file) {
            return false;
        }
        $this->em->getConnection()->query("SET autocommit=0;");
        $this->storeFunctions($reflector);
        $this->storeClasses($reflector);
        $this->storeInterfaces($reflector);
        $this->storeInterfaces($reflector);
        $this->storeConstants($reflector);
        $this->em->flush();
        $this->em->getConnection()->query("COMMIT;");
        $this->em->getConnection()->query("SET autocommit=1;");
        return true;
    }

    /**
     * 
     * @param type $reflector
     * @return ProjectFile 
     */
    private function getProjectFile($file) {
        $out = null;
        if (is_numeric($this->file)) {
            $out = $this->em
                    ->getRepository('SlxGitMinerBundle:ProjectFile')
                    ->find($file);
        } elseif ($file instanceof Slx\GitMinerBundle\Entity\ProjectFile) {
            $out = $file;
        }
        return $out;
    }

    private function storeFunctions(ReflectorInterface $reflector) {
        /** @var FunctionReflector $refl */
        foreach ($reflector->getFunctions() as $refl) {
            $store = new StoreFunctionReflection($this->em, $this->tag, $this->file);
            try {
                $store->persist($refl);
            } catch (\Exception $ex) {
                printf("Error: %s\n", $ex->getMessage());
            }
        }
    }

    private function storeClasses(ReflectorInterface $reflector) {
        /** @var ClassReflector $crefl */
        foreach ($reflector->getClasses() as $crefl) {
            $store = new StoreClassReflection($this->em, $this->tag, $this->file);
            try {
                $store->persist($crefl);
            } catch (\Exception $ex) {
                printf("Error: %s\n", $ex->getMessage());
            }
        }
    }

    private function storeInterfaces(ReflectorInterface $reflector) {
        /** @var ClassReflector $crefl */
        foreach ($reflector->getInterfaces() as $crefl) {
            $store = new StoreInterfaceReflection($this->em, $this->tag, $this->file);
            try {
                $store->persist($crefl);
            } catch (\Exception $ex) {
                printf("Error: %s\n", $ex->getMessage());
            }
        }
    }

    private function storeTraits(ReflectorInterface $reflector) {
        /** @var ClassReflector $crefl */
        foreach ($reflector->getTraits() as $crefl) {
            $store = new StoreTraitReflection($this->em, $this->tag, $this->file);
            try {
                $store->persist($crefl);
            } catch (\Exception $ex) {
                printf("Error: %s\n", $ex->getMessage());
            }
        }
    }

    private function storeConstants(ReflectorInterface $reflector) {
        foreach ($reflector->getConstants() as $crefl) {
            $store = new StoreConstantReflection($this->em, $this->tag, $this->file);
            try {
                $store->persist($crefl);
            } catch (\Exception $ex) {
                printf("Error: %s\n", $ex->getMessage());
            }
        }
    }

}
