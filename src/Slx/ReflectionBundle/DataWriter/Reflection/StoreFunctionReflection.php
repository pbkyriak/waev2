<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Slx\ReflectionBundle\DataWriter\Reflection\AbstractStoreReflection;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\Reflection\FunctionReflector;

/**
 * Description of StoreFunctionReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreFunctionReflection extends AbstractStoreReflection
{

    protected $constructType = 'function';
    protected $classId = 0;
    
    public function setClassId($v) {
        $this->classId = $v;
    }
    
    public function getClassId() {
        return $this->classId;
    }
    
    /**
     * 
     * @param \Slx\GitMinerBundle\Reflection\FunctionReflector $reflector
     */
    public function persist(ReflectorInterface $reflector) {
        //$this->storeConstruct($reflector, $reflector->getSignature(), $reflector->getCodeBlockHash());
        $signature = $reflector->getSignature();
        if( !$signature->getName() ) {
            return;
        }
        $signature->setName($this->formatCName($reflector));
        $query = "INSERT INTO project_func_signature (project_tag_id, ftype, modifiers, fname, signature, hash, return_type, project_file_id, is_deprecated, has_access_tag, namespace, project_class_id, namespaced) "
            . "VALUES (:tagId, :ftype, :modifiers, :fname, :signature, :hash, :return_type, :fid, :isd, :hat, :namespace, :pcid, :namespaced)";
        $params = array(
            'tagId'=>$this->tag->getId(),
            'ftype'=> $this->constructType,
            'modifiers' => implode(',',$signature->getModifiers()),
            'fname'=>$signature->getName(),
            'signature' => $signature->toString(),
            'hash' => $signature->getCodeBlockHash(),
            'return_type' => $signature->getReturnType(),
            'fid' => $this->file->getId(),
            'isd' => $signature->isDeprecated(),
            'hat' => $signature->hasAccessTag(),
            'namespace' => $signature->getNamespace(),
            'pcid' => $this->getClassId() ? $this->getClassId() : 0,
            'namespaced' => $signature->getNamespace().'\\'.$signature->getName(),
        );
        $this->em->getConnection()->executeUpdate($query, $params);    
        $signatureId = $this->em->getConnection()->lastInsertId();
        $argIdx = 0;
        $argOptIdx = 0;
        foreach($signature->getArguments() as $argument) {
            if( $argument->isOptional() ) {
                $argOptIdx++;
            }
            $argIdx++;
            $query = "INSERT INTO project_func_signature_argument (project_func_signature_id, arg_type, is_optional, default_value, by_ref, arg_idx) VALUES (:sid, :atype, :iso, :dv, :bf, :aidx)";
            $params = array(
                'sid' => $signatureId,
                'atype'=>$argument->getType(),
                'iso' => $argument->isOptional(),
                'dv' => $argument->getDefaultValue()===null ? '' : $argument->getDefaultValue(),
                'bf' => $argument->isByRef(),
                'aidx' => $argument->isOptional() ? $argOptIdx : $argIdx,
            );
            $this->em->getConnection()->executeUpdate($query, $params);    
        }
    }
}
