<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\DataWriter\Reflection\AbstractStoreReflection;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreMethodReflection;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\GitMinerBundle\Entity\ProjectFile;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of StoreInterfaceReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreInterfaceReflection extends AbstractStoreReflection
{

    protected $constructType = 'class';
    
    public function persist(ReflectorInterface $reflector)
    {
        $this->storeInterfaceDefinition($reflector);
    }
    
    private function storeInterfaceDefinition(ReflectorInterface $reflector) {
        $query = "INSERT INTO project_interface (project_tag_id, fname, namespace, project_file_id, fqname) "
            . "VALUES (:tagId, :fname, :namespace, :fid, :fqn)";
        $params = array(
            'tagId'=>$this->tag->getId(),
            'fname'=>$reflector->getName(),
            'namespace' => $reflector->getNamespace(),
            'fid' => $this->file->getId(),
            'fqn' => $reflector->getFQName(),
        );
        $this->em->getConnection()->executeUpdate($query, $params);    
        $classId = $this->em->getConnection()->lastInsertId();
        return $classId;
    }
    
}
