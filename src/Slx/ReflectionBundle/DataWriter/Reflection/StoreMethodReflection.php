<?php

namespace Slx\ReflectionBundle\DataWriter\Reflection;

use Doctrine\ORM\EntityManager;
use Slx\ReflectionBundle\DataWriter\Reflection\StoreFunctionReflection;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\Reflection\ClassReflector\MethodReflector;
use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\GitMinerBundle\Entity\ProjectFile;

/**
 * Description of StoreMethodReflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StoreMethodReflection extends StoreFunctionReflection
{
    protected $constructType='method';
    protected $className;
    
    public function __construct(EntityManager $em, ProjectTag $tag, ProjectFile $file, $className)
    {
        parent::__construct($em, $tag, $file);
        $this->className = $className;
    }
    
    protected function formatCName(ReflectorInterface $reflector)
    {
        return sprintf("%s::%s", $this->className, $reflector->getName());
    }
}
