<?php

namespace Slx\ReflectionBundle\Reflection;

use PHPParser_NodeAbstract;
use PHPParser_PrettyPrinter_Default;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\Reflection\ReflectionContext;
/**
 * Basic reflection.
 * 
 * Based on phpDocumentor and zend framework 1.2 reflection
 * 
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class BaseReflector implements ReflectorInterface
{

    /**
     *
     * @var PHPParser_Node_Stmt
     */
    protected $node;
    /**
     *
     * @var ReflectionContext
     */
    protected $context;
    protected $startLine = 0;
    protected $endLine = 0;
    protected $namespace = '';
    
    /**
     * PHP AST pretty printer used to get representations of values.
     *
     * @var PHPParser_PrettyPrinterAbstract
     */
    protected static $prettyPrinter = null;

    public function __construct(ReflectionContext $context, PHPParser_NodeAbstract $node)
    {
        $this->node = $node;
        $this->context= $context;
        $this->namespace = $this->context->getNamespace();
        if( $node->hasAttribute('startLine') ) {
            $this->startLine = $node->getAttribute('startLine');
        }
        if( $node->hasAttribute('endLine') ) {
            $this->endLine = $node->getAttribute('endLine');
        }
    }

    /**
     * Node name
     * 
     * @return string
     */
    public function getName()
    {
        return $this->node->name;
    }

    /**
     * Returns node starting line.
     * 
     * @param bool $includeDocComment
     * @return int
     */
    public function getStartLine($includeDocComment = false) {
        if ($includeDocComment) {
            if ($this->getDocComment()) {
                return $this->getDocComment()->getLine();
            }
        }

        return $this->startLine;
    }
    
    /**
     * Node ending line
     * 
     * @return int
     */
    public function getEndLine() {
        return $this->endLine;
    }
    
    /**
     * Node filename.
     * 
     * @return string 
     */
    public function getFilename() {
        return $this->context->getFilename();
    }
    
    /**
     * Returns node's doc comment.
     * 
     * @return string|null
     */
    public function getDocComment() {
        return $this->node->getDocComment();
    }
    
    /**
     * Returns a simple human readable output for a value.
     *
     * @param PHPParser_Node_Expr $value The value node as provided by
     * PHP-Parser.
     *
     * @return string
     */
    protected function getRepresentationOfValue($value = null)
    {
        if (null === $value) {
            return '';
        }

        if (!self::$prettyPrinter) {
            self::$prettyPrinter = new PHPParser_PrettyPrinter_Default();
        }

        return self::$prettyPrinter->prettyPrintExpr($value);
    }
    
    /**
     * Returns the block of code that the node represents.
     * 
     * @param bool $includeDocblock
     * 
     * @return string
     */
    public function getContent($includeDocblock = true) {
        $content = file($this->getFileName());
        return implode("\n",
            array_splice(
                $content,
                $this->getStartLine($includeDocblock),
                ($this->getEndLine() - $this->getStartLine()),
                true
                )
            );

    }
    
    public function getNamespace() {
        return $this->namespace;
    }
}
