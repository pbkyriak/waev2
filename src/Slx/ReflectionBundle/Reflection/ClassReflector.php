<?php

namespace Slx\ReflectionBundle\Reflection;

use Slx\ReflectionBundle\Reflection\InterfaceReflector;
use PHPParser_Node_Name;
use PHPParser_Node_Stmt_Class;

/**
 * Provides static reflection for a class.
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ClassReflector extends InterfaceReflector
{

    /** @var \PHPParser_Node_Stmt_Class */
    protected $node;

    /** @var string[] */
    protected $traits = array();

    public function parseSubElements()
    {
        if( $this->node->stmts) {
            /** @var \PHPParser_Node_Stmt_TraitUse $stmt */
            foreach ($this->node->stmts as $stmt) {
                if ($stmt instanceof \PHPParser_Node_Stmt_TraitUse) {
                    foreach ($stmt->traits as $trait) {
                        $this->traits[] = '\\' . (string) $trait;
                    }
                }
            }
        }
        parent::parseSubElements();
    }

    /**
     * Returns whether this is an abstract class.
     *
     * @return bool
     */
    public function isAbstract()
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_ABSTRACT);
    }

    /**
     * Returns whether this class is final and thus cannot be extended.
     *
     * @return bool
     */
    public function isFinal()
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_FINAL);
    }

    
    public function isStatic() 
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_STATIC);
    }
    /**
     * Returns a list of the names of traits used in this class.
     *
     * @return string[]
     */
    public function getTraits()
    {
        return $this->traits;
    }

    /**
     * Returns the name of the class that this one extends (parent class)
     * @return string
     */
    public function getParentClass()
    {
        return $this->node->extends ? '\\' . (string) $this->node->extends : '';
    }

    /**
     *
     * @return string[] Names of interfaces the class implements.
     */
    public function getInterfaces()
    {
        $names = array();
        if ($this->node->implements) {
            /** @var PHPParser_Node_Name */
            foreach ($this->node->implements as $node) {
                $names[] = '\\' . (string) $node;
            }
        }
        return $names;
    }
    
    /**
     * 
     * @return string Function signature
     */
    public function getSignature() {
        $t = $this->getModifiers();
        $t[] = $this->getFQName();
        $parent = $this->getParentClass();
        if( $parent ) {
            $t[] = 'extends '.$parent;
        }
        $interfaces = $this->getInterfaces();
        if( $interfaces ) {
            $t[] = 'implements '.implode(',', $interfaces);
        }
        return implode(' ', $t);
    }
    
    public function getModifiers() {
        $t = array();
        if( $this->isAbstract() ) {
            $t[] = 'abstract';
        }

        if( $this->isFinal() ) {
            $t[] = 'final';
        }
        return $t;
    }
}
