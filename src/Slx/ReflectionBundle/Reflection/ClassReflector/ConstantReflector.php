<?php

/**
 * phpDocumentor
 *
 * PHP Version 5.3
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @copyright 2010-2012 Mike van Riel / Naenius (http://www.naenius.com)
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 * @link http://phpdoc.org
 */

namespace Slx\ReflectionBundle\Reflection\ClassReflector;

use Slx\ReflectionBundle\Reflection\BaseReflector;
use Slx\ReflectionBundle\Reflection\ConstantReflector as BaseConstantReflector;
use Slx\ReflectionBundle\Reflection\ReflectionContext;
use PHPParser_Node_Const;
use PHPParser_Node_Stmt_ClassConst;

class ConstantReflector extends BaseConstantReflector
{

    /** @var PHPParser_Node_Stmt_ClassConst */
    protected $constant;

    /**
     * Registers the Constant Statement and Node with this reflector.
     *
     * @param string $filename
     * @param PHPParser_Node_Stmt_Const $stmt
     * @param PHPParser_Node_Const $node
     */
    public function __construct(
        ReflectionContext$context,
        PHPParser_Node_Stmt_ClassConst $stmt, 
        PHPParser_Node_Const $node
    )
    {
        BaseReflector::__construct($context,$node);
        $this->constant = $stmt;
    }
}
