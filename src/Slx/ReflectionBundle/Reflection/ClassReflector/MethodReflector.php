<?php

/**
 * Based on phpDocumentor by  Mike van Riel <mike.vanriel@naenius.com>
 * 
 */

namespace Slx\ReflectionBundle\Reflection\ClassReflector;

use Slx\ReflectionBundle\Reflection\FunctionReflector;
use PHPParser_Node_Stmt_Class;
use PHPParser_Node_Stmt_ClassMethod;
use Slx\ReflectionBundle\Analyser\FunctionSignature\FuncSignature;

class MethodReflector extends FunctionReflector
{

    /** @var PHPParser_Node_Stmt_ClassMethod */
    protected $node;

    /**
     * Returns the visibility for this item.
     *
     * The returned value should match either of the following:
     *
     * * public
     * * protected
     * * private
     *
     * If a method has no visibility set in the class definition this method
     * will return 'public'.
     * @return string
     */
    public function getVisibility()
    {
        if ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_PROTECTED) {
            return 'protected';
        }
        if ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_PRIVATE) {
            return 'private';
        }
        if( substr($this->getName(),0,1)=='_' && substr($this->getName(),0,2)!='__' ) {
            return 'private';
        }
        return 'public';
    }

    /**
     * Returns whether this method is static.
     *
     * @return bool
     */
    public function isAbstract()
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_ABSTRACT);
    }

    /**
     * Returns whether this method is static.
     *
     * @return bool
     */
    public function isStatic()
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_STATIC);
    }

    /**
     * Returns whether this method is final.
     *
     * @return bool
     */
    public function isFinal()
    {
        return (bool) ($this->node->type & PHPParser_Node_Stmt_Class::MODIFIER_FINAL);
    }
    
    /**
     * Function signature
     * 
     * @return string
     */
    public function getSignature() {
        $return = $this->getSignatureReturnType();
        $modifiers = $this->getSignatureModifiers();
        
        $signature = new FuncSignature(
            $this->getName(), 
            $return,
            $this->getCodeBlockHash(),
            $this->getNamespace()
            );
        $signature->setModifiers($modifiers);
        $signature->setIsReturnCleared($this->bIsReturnCleared);
        foreach ($this->getArguments() as $argument) {
            $signature->addArgument(
                $this->getSignatureArgument($argument)
                );
        }
        return $signature;
    }
    
    /**
     * 
     * @return array
     */
    protected function getSignatureModifiers() {
        $modifiers = array();
        $modifiers[] = $this->getVisibility();
        if( $this->isAbstract() ) {
            $modifiers[] = 'abstract';
        }
        if( $this->isStatic() ) {
            $modifiers[] = 'static';
        }
        if( $this->isFinal() ) {
            $modifiers[] = 'final';
        }
        return $modifiers;
    }
}
