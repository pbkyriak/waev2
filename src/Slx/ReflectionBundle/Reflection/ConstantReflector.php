<?php

/**
 * phpDocumentor
 *
 * PHP Version 5.3
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @copyright 2010-2012 Mike van Riel / Naenius (http://www.naenius.com)
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 * @link http://phpdoc.org
 */

namespace Slx\ReflectionBundle\Reflection;

use PHPParser_Node_Const;
use PHPParser_Node_Stmt_Const;

/**
 * Provides Static Reflection for file-level constants.
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 * @license http://www.opensource.org/licenses/mit-license.php MIT
 * @link http://phpdoc.org
 */
class ConstantReflector extends BaseReflector
{

    /** @var PHPParser_Node_Stmt_Const */
    protected $constant;

    /** @var PHPParser_Node_Const */
    protected $node;

    /**
     * Registers the Constant Statement and Node with this reflector.
     *
     * @param PHPParser_Node_Stmt_Const $stmt
     * @param PHPParser_Node_Const $node
     */
    public function __construct(
        $context, 
        PHPParser_Node_Stmt_Const $stmt, 
        PHPParser_Node_Const $node
    )
    {
        parent::__construct(
            $context,
            $node
        );
        $this->constant = $stmt;
    }

    /**
     * Returns the value contained in this Constant.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->getRepresentationOfValue($this->node->value);
    }
    
    public function getSignature() {
        $out = sprintf("%s=%s", $this->getName(), $this->getValue());
        return $out;
    }
}
