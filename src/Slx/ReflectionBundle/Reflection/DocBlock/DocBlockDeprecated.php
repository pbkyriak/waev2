<?php

namespace Slx\ReflectionBundle\Reflection\DocBlock;

use Slx\ReflectionBundle\Reflection\ReflectionException;

/**
 * access docblock tag
 * 
 * Based on zend framework 1.2 reflection.
 * @since 14.01.20
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DocBlockDeprecated
{
    private $type=false;
    
    public function __construct($tagDocblockLine)
    {
        $matches = array();

        if (!preg_match('#^@(\w+)\s+([^\s]+)(?:\s+(\$\S+))?(?:\s+(.*))?#s', $tagDocblockLine, $matches)) {
            throw new ReflectionException('Provided docblock line is does not contain a valid tag');
        }

        if ($matches[1] != 'deprecated') {
            throw new ReflectionException('Provided docblock line is does not contain a valid @deprecated tag');
        }

        $this->name = 'deprecated';
        $this->type = true;

    }
        
    public function getType() {
        return $this->type;
    }   
}
