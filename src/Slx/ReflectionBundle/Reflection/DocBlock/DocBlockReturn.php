<?php

namespace Slx\ReflectionBundle\Reflection\DocBlock;

use Slx\ReflectionBundle\Reflection\ReflectionException;

/**
 * DocBlock Return tag reflection.
 * 
 * Based on zend framework 1.2 reflection
 * 
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class DocBlockReturn extends DocBlockTag
{
    private $type;
    
    public function __construct($tagDocblockLine)
    {
        if (!preg_match('#^@(\w+)\s+([^\s]+)(?:\s+(.*))?#', $tagDocblockLine, $matches)) {
            throw new ReflectionException('Provided docblock line is does not contain a valid tag');
        }

        if ($matches[1] != 'return') {
            throw new ReflectionException('Provided docblock line is does not contain a valid @return tag');
        }

        $this->name = 'return';
        $this->type = $matches[2];
        if (isset($matches[3])) {
            $this->description = $matches[3];
        }
    }
    
    public function getType() {
        return $this->type;
    }
}
