<?php

namespace Slx\ReflectionBundle\Reflection;

use PHPParser_Node;
use PHPParser_NodeVisitor;
use PHPParser_Error;
use PHPParser_Parser;
use PHPParser_Lexer;
use Slx\ReflectionBundle\Reflection\ReflectorInterface;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;

/**
 * Reflects a php code file. Based on phpDocumentor reflection
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileReflector implements ReflectorInterface, PHPParser_NodeVisitor
{

    protected $classes = array();
    protected $traits = array();
    protected $interfaces = array();
    protected $functions = array();
    protected $constants = array();
    protected $filename = '';
    protected $fileId = 0;
    protected $contents = '';
    protected $ast = null;
    protected $context;
    
    /**
     * Opens the file and retrieves its contents. 
     * 
     * @param string $file file to reflect
     */
    public function __construct($file)
    {
        $this->filename = $file['fname'];
        $this->fileId = $file['id'];
        $this->context = new ReflectionContext($this->filename);
    }

    /**
     * Traverser leaveNode.
     * 
     * @param PHPParser_Node $node
     */
    public function leaveNode(PHPParser_Node $node)
    {
        //printf("Stmt:%s %s\n",get_class($node), $node->getType());return;
        switch ($node->getType()) {
            case ParserNodeTypes::NodeStmtUse:
                /** @var PHPParser_Node_Stmt_UseUse $use */
                foreach ($node->uses as $use) {
                    $this->context->setNamespaceAlias(
                        $use->alias,
                        implode('\\', $use->name->parts)
                    );
                }
                break;
            case ParserNodeTypes::NodeStmtInterface:
                $interface = new InterfaceReflector($this->context,
                    $node);
                $interface->parseSubElements();
                $this->interfaces[] = $interface;
                break;
            case ParserNodeTypes::NodeStmtClass:
                $class = new ClassReflector(
                    $this->context,
                    $node);
                $class->parseSubElements();
                $this->classes[] = $class;
                break;
            case ParserNodeTypes::NodeStmtTrait:
                $trait = new TraitReflector($this->context, $node);
                $trait->parseSubElements();
                $this->traits[] = $trait;
                break;
            case ParserNodeTypes::NodeStmtFunction:
                $function = new FunctionReflector($this->context, $node);
                $this->functions[] = $function;
                break;
            case ParserNodeTypes::NodeStmtConst:
                foreach ($node->consts as $constant) {
                    $reflector = new ConstantReflector(
                        $this->context,
                        $node,
                        $constant
                    );
                    $this->constants[] = $reflector;
                }
                break;
        }
    }

    public function enterNode(PHPParser_Node $node)
    {
        //printf("Stmt:%s %s\n",get_class($node), $node->getType());return;
        if( $node->getType()==ParserNodeTypes::NodeStmtNamespace ) {
            $ns = '\\'.implode('\\',$node->name->parts);
            $this->context->setNamespace($ns);
        }
    }

    public function afterTraverse(array $nodes)
    {
        
    }

    public function beforeTraverse(array $nodes)
    {
        
    }

    /**
     * Returns array of function nodes.
     * 
     * @return array
     */
    public function getFunctions()
    {
        return $this->functions;
    }

    /**
     * Returns array of class nodes.
     * 
     * @return array
     */
    public function getClasses()
    {
        return $this->classes;
    }

    /**
     * Returns array of interface nodes.
     * 
     * @return array
     */
    public function getInterfaces()
    {
        return $this->interfaces;
    }
    
    /**
     * Returns array of global constants.
     * 
     * @return array
     */
    public function getConstants() {
        return $this->constants;
    }
    
    /**
     * Returns array of trait nodes.
     * 
     * @return array
     */
    public function getTraits() {
        return $this->traits;
    }
    
    public function getAST() {
        return $this->ast;
    }
    
    public function getFilename() {
        return $this->filename;
    }
    
    public function getContent($includeDocblock = true)
    {
        return '';
    }

    public function getDocComment()
    {
        return '';
    }

    public function getEndLine()
    {
        return 0;
    }

    public function getName()
    {
        return $this->filename;
    }

    public function getStartLine($includeDocComment = false)
    {
        return 0;
    }
 
    public function free()
    {
        if( $this->ast ) {
            foreach($this->ast as $node) {
                $node->free();
            }
        }
        $this->ast=null;
        unset($this->ast);
        $this->classes=null;
        $this->constants=null;
        $this->contents=null;
        $this->context=null;
        $this->functions=null;
        $this->interfaces=null;
        $this->traits=null;
    }
}
