<?php

namespace Slx\ReflectionBundle\Reflection;

use PHPParser_Node_Param;
use PHPParser_Node_Stmt;
use PHPParser_Node_Stmt_Return;
use Slx\ReflectionBundle\Reflection\BaseReflector;
use Slx\ReflectionBundle\Reflection\DocBlock;
use Slx\ReflectionBundle\Reflection\ReflectionContext;
use Slx\ReflectionBundle\Reflection\FunctionReflector\ArgumentReflector;
use Slx\ReflectionBundle\Analyser\FunctionSignature\FuncSignature;
use Slx\ReflectionBundle\Analyser\FunctionSignature\FuncSignatureArgument;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
/**
 * Function reflection. Based on phpDocumentor/Reflection
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FunctionReflector extends BaseReflector
{

    private $arguments = array();
    private $isVoid = true;
    protected $codeBlockStartLine = -1;
    protected $codeBlockEndLine = -1;
    /** @var \Slx\GitMinerBundle\Reflection\DocBlock $docBlock */
    protected $docBlock = null;
    protected $bIsReturnCleared = false;
    
    public function __construct(ReflectionContext $context,
        PHPParser_Node_Stmt $node)
    {
        parent::__construct($context, $node);
        /** @var PHPParser_Node_Param $param */
        foreach ($node->params as $param) {
            $reflector = new ArgumentReflector($this->context,
                $param);
            $this->arguments[$reflector->getName()] = $reflector;
        }
        if ($node->stmts) {
            // find block start and end lines
            foreach ($node->stmts as $stmt) {
                $this->codeBlockStartLine = $this->codeBlockStartLine == -1 ? $stmt->getAttribute('startLine') : $this->codeBlockStartLine;
                $this->codeBlockEndLine = $stmt->getAttribute('endLine');
            }
            // traverse function nodes to find return statements
            $returns = \Slx\ReflectionBundle\Reflection\StmtFinder::getInstance()
                    ->find($node->stmts, ParserNodeTypes::typeToClassName(ParserNodeTypes::NodeStmtReturn))
                    ->getResults();
            if( count($returns) ) {
                $this->isVoid = false;
            }
        }
        $this->docBlock = $this->getDocBlock();
    }

    /**
     * Function arguments
     * 
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * Function signature
     * 
     * @return \Slx\ReflectionBundle\Analyser\FunctionSignature\FuncSignature
     */
    public function getSignature()
    {
        $return = $this->getSignatureReturnType();
        $modifiers = $this->getSignatureModifiers();
        $signature = new FuncSignature(
            $this->getName(), 
            $return,
            $this->getCodeBlockHash(),
            $this->namespace
            );
        $signature->setModifiers($modifiers);
        $signature->setIsReturnCleared($this->bIsReturnCleared);
        if( $this->docBlock ) {
            $signature->setIsDeprecated($this->docBlock->isDeprecated());
            $signature->setHasAccessTag($this->docBlock->hasAccessTag());
        }
        foreach ($this->getArguments() as $argument) {
            $signature->addArgument(
                $this->getSignatureArgument($argument)
                );
        }
        return $signature;
    }

    protected function getSignatureReturnType()
    {
        $this->bIsReturnCleared = false;
        $return = 'mixed';
        if ($this->isVoid) {
            $return = 'void';
            $this->bIsReturnCleared = true;
        } else if ($this->docBlock) {
            $return = $this->docBlock->getReturnType();
            if( $this->docBlock->hasReturnTag() ) {
                $this->bIsReturnCleared = true;
            }
        }
        return $return;
    }

    protected function getSignatureArgument($argument)
    {
        $type = 'mixed';
        if ($argument->getType()) {
            $type = $argument->getType();
        } elseif ($this->docBlock) {
            $type = $this->docBlock->getVariableParam($argument->getName());
        }
        $optional = false;
        $def = (string)$argument->getDefault();
        if( !($def==='') ) {
            $optional = true;
        }
        return new FuncSignatureArgument($argument->isByRef(),$type, $optional, $def );
    }

    /**
     * Get function's docBlock.
     * 
     * @return \Slx\ReflectionBundle\Reflection\DocBlock
     */
    public function getDocBlock()
    {
        try {
            $docblock = new DocBlock($this->getDocComment());
        } catch (ReflectionException $ex) {
            $docblock = null;
        }
        return $docblock;
    }

    /**
     * Get function sha1 hash.
     * 
     * @return string
     */
    public function getHash()
    {
        return sha1($this->getContent());
    }

    public function getCodeBlockHash()
    {
        return sha1($this->getCodeBlockContent());
    }

    public function getCodeBlockStartLine()
    {
        return $this->codeBlockStartLine;
    }

    public function getCodeBlockEndLine()
    {
        return $this->codeBlockEndLine;
    }

    protected function getCodeBlockContent()
    {
        $content = '';
        $out = '';
        if(file_exists($this->getFileName())) {
            $content = file($this->getFileName());
        }
        $lineI = $this->getCodeBlockStartLine();
        $lineF = $this->getCodeBlockEndLine() - $this->getCodeBlockStartLine();
        if( $content && ($lineF>=$lineI) ) {
            $out = implode("\n",
                array_splice(
                    $content, $lineI, $lineF, true
                )
            );
        }
        return $out;
    }

    /**
     * Returns function visibility.
     * 
     * Function visibility is always public. In php there is visibility for functions. 
     * But visibility maybe defined in docblock @access tag, so the public api can be separated from 'private' functions.
     * If not access tag is defined in docblock, it checks if function name starts with _ then it considers that function also private.
     * 
     * @since 14.01.20
     * @return string
     */
    public function getVisibility() {
        $out = 'public';
        
        if( substr($this->getName(),0,1)=='_' && substr($this->getName(),0,2)!='__' && strlen($this->getName())>1 ) {
            return 'private';
        } elseif ($this->docBlock) {
            $out = $this->docBlock->getAccessType();
        }

        return $out;
    }
    
    /**
     * 
     * @return array
     */
    protected function getSignatureModifiers() {
        $modifiers = array();
        $modifiers[] = $this->getVisibility();
        return $modifiers;
    }
    
    public function getNamespace() {
        return $this->namespace;
    }
}
