<?php

namespace Slx\ReflectionBundle\Reflection\FunctionReflector;

use PHPParser_Node_Param;
use PHPParser_Node_Stmt;
use Slx\ReflectionBundle\Reflection\BaseReflector;

/**
 * Function Argument reflection. 
 * 
 * Based on phpDocumentor reflection
 * 
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ArgumentReflector extends BaseReflector
{

    /** @var \PHPParser_Node_Param */
    protected $node;

    /**
     * Checks whether the argument is passed by reference.
     *
     * @return bool TRUE if the argument is by reference, FALSE otherwise.
     */
    public function isByRef()
    {
        return $this->node->byRef;
    }

    /**
     * Returns the default value or null is none is set.
     *
     * @return string|null
     */
    public function getDefault()
    {
        $result = null;
        if ($this->node->default) {
            $result = $this->getRepresentationOfValue($this->node->default);
        }
        return $result;
    }

    /**
     * Returns the typehint, or null if none is set.
     *
     * @return string|null
     */
    public function getType()
    {
        $type = (string) $this->node->type;

        // in case of the callable of array keyword; do not prefix with a \
        if ($type == 'callable' || $type == 'array' || $type == 'self' || $type == '$this'
        ) {
            return $type;
        }

        return $type ? '\\' . $type : '';
    }

    /**
     * Argument name.
     * 
     * @return string
     */
    public function getName()
    {
        return '$' . parent::getName();
    }
}
