<?php

namespace Slx\ReflectionBundle\Reflection;

use Slx\ReflectionBundle\Reflection\BaseReflector;
use PHPParser_Node_Name;
use PHPParser_Node_Stmt_Interface;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
/**
 * Provides static reflection for a class.
 * 
 * Based on phpDocumentor/Reflection
 *
 * @author Mike van Riel <mike.vanriel@naenius.com>
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class InterfaceReflector extends BaseReflector
{

    protected $constants = array();
    protected $properties = array();
    protected $methods = array();
        
    public function parseSubElements()
    {
        if(!$this->node->stmts) {
            return false;
        }
        foreach ($this->node->stmts as $stmt) {
            switch ($stmt->getType()) {
                case ParserNodeTypes::NodeStmtProperty:
                    foreach ($stmt->props as $property) {
                        $this->properties[] = new ClassReflector\PropertyReflector(
                            $this->context,
                            $stmt,
                            $property
                        );
                    }
                    break;
                case ParserNodeTypes::NodeStmtClassMethod:
                    $this->methods[] = new ClassReflector\MethodReflector(
                        $this->context,
                        $stmt
                    );
                    break;
                case ParserNodeTypes::NodeStmtClassConst:
                    foreach ($stmt->consts as $constant) {
                        $this->constants[] = new ClassReflector\ConstantReflector(
                            $this->context,
                            $stmt,
                            $constant
                        );
                    }
                    break;
            }
        }
    }

    /**
     * Return parent interfaces.
     * 
     * @return string
     */
    public function getParentInterfaces()
    {
        $names = array();
        if ($this->node instanceof PHPParser_Node_Stmt_Interface && $this->node->extends
        ) {
            /** @var PHPParser_Node_Name */
            foreach ($this->node->extends as $node) {
                $names[] = '\\' . (string) $node;
            }
        }
        return $names;
    }

    /**
     * Interface constants.
     * 
     * @return array
     */
    public function getConstants()
    {
        return $this->constants;
    }

    /**
     * Interface properties.
     * 
     * @return array
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Interface methods.
     * 
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * Get method reflection.
     * 
     * @param string $name
     * @return ClassReflector\MethodReflector
     */
    public function getMethod($name)
    {
        return $this->methods[$name];
    }
    
    public function getFQName() {
        if( !empty($this->namespace) ) {
            return sprintf("\\%s\\%s",$this->namespace,$this->getName());
        }
        else {
            return sprintf("\\%s",$this->getName());
        }
    }
}
