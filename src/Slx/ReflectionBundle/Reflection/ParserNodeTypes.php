<?php

namespace Slx\ReflectionBundle\Reflection;

/**
 * Description of ParserNodeTypes
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 6 Ιαν 2016
 */
class ParserNodeTypes {
    const NodeConst                 = 'Const';

    const NodeStmtUse               = 'Stmt_Use';
    const NodeStmtInterface         = 'Stmt_Interface';
    const NodeStmtClass             = 'Stmt_Class';
    const NodeStmtTrait             = 'Stmt_Trait';
    const NodeStmtFunction          = 'Stmt_Function';
    const NodeStmtConst             = 'Stmt_Const';
    const NodeStmtNamespace         = 'Stmt_Namespace';
    const NodeStmtTraitUse          = 'Stmt_TraitUse';
    const NodeStmtProperty          = 'Stmt_Property';
    const NodeStmtClassMethod       = 'Stmt_ClassMethod';
    const NodeStmtClassConst        = 'Stmt_ClassConst';
    const NodeStmtReturn            = 'Stmt_Return';
    const NodeStmtIf                = 'Stmt_If';
    const NodeStmtElseIf            = 'Stmt_ElseIf';
    const NodeStmtFor               = 'Stmt_For';
    const NodeStmtForeach           = 'Stmt_Foreach';
    const NodeStmtCase              = 'Stmt_Case';
    const NodeStmtWhile             = 'Stmt_While';
    const NodeStmtDo                = 'Stmt_Do';
    const NodeStmtCatch             = 'Stmt_Catch';

    const NodeExprFuncCall          = 'Expr_FuncCall';
    const NodeExprMethodCall        = 'Expr_MethodCall';
    const NodeExprVariable          = 'Expr_Variable';
    const NodeExprStaticCall        = 'Expr_StaticCall';
    const NodeExprNew               = 'Expr_New';
    const NodeExprCastObject        = 'Expr_Cast_Object';
    const NodeExprArray             = 'Expr_Array';
    const NodeExprTernary           = 'Expr_Ternary';
    const NodeExprBooleanAnd        = 'Expr_BooleanAnd';
    const NodeExprBooleanOr         = 'Expr_BooleanOr';
    const NodeExprLogicalOr         = 'Expr_LogicalOr';
    const NodeExprLogicalAnd        = 'Expr_LogicalAnd';
    const NodeExprClosure           = 'Expr_Closure';
    const NodeExprAssign            = 'Expr_Assign';
    const NodeExprAssignRef         = 'Expr_AssignRef';
    const NodeExprPropertyFetch     = 'Expr_PropertyFetch';
    const NodeExprStaticPropertyFetch = 'Expr_StaticPropertyFetch';
    const NodeExprArrayDimFetch     = 'Expr_ArrayDimFetch';
    
    public static function typeToClassName($type) {
        return 'PHPParser_Node_'.$type;
    }
    
    public static function isStmtType($type) {
        return strpos($type, 'Stmt_')===0;
    }
}
