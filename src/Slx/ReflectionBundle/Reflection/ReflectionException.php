<?php

namespace Slx\ReflectionBundle\Reflection;

/**
 * Description of ReflectionException
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class ReflectionException extends \Exception
{
    
}
