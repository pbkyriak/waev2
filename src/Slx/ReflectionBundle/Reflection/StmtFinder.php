<?php
namespace Slx\ReflectionBundle\Reflection;
use Slx\ReflectionBundle\Reflection\Traverser;
/**
 * Description of StmtFinder
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class StmtFinder implements \PHPParser_NodeVisitor {
    
    private $className;
    private $result;
    
    public static function getInstance() {
        return new StmtFinder();
    }
    
    public function find($nodes, $className) {
        $this->className = $className;
        $this->result = array();
        $traverser = new Traverser();
        $traverser->addVisitor($this);
        $traverser->traverseAst($nodes);
        return $this;
    }
    
    public function afterTraverse(array $nodes) {
        
    }

    public function beforeTraverse(array $nodes) {
        
    }

    public function enterNode(\PHPParser_Node $node) {
        if( get_class($node)==$this->className ) {
            $this->result[] = $node;
        }
    }

    public function leaveNode(\PHPParser_Node $node) {
        
    }

    public function getResults() {
        return $this->result;
    }
}
