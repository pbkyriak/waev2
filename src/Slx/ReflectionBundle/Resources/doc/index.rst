Reflection Bundle
#################

Parses source code and collects function and method signatures.

Tasks
=====

ParseCode
---------
Parses source code and stores temporarily AST to file system.

FuncSignatures
--------------
Traverses AST and collects function and method signatures.
