<?php
namespace Slx\ReflectionBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;
/**
 * Description of Namespaces1
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Namespaces1 extends \PHPUnit_Framework_TestCase
{

    public function testAdd() {
        $fn = '/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces01.php';
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());

        foreach ($fr->getClasses() as $class) {
            $this->assertEquals('\\Ns00\\Ns01\\WithNamespaces',$class->getFQName());  
        }
    }
}
