<?php
namespace Slx\ReflectionBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;
/**
 * Description of Namespaces1
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Namespaces1c extends \PHPUnit_Framework_TestCase
{

    private function getFileReflector($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        return $fr;
    }
    
    public function test01c() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces01c.php');

        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[] = $class->getFQName();  
        }
        $this->assertEquals(
            array(
                '\Ns00\Ns01\WithNamespaces',
                '\Ns00\Ns02\WithNamespaces',
                ),
            $classes
            ); 
    }
    
    public function testNs02() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces02.php');

        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = $class->getParentClass();  
        }
        $this->assertEquals(
            array(
                '\Ns00\Ns01\CName' => '\Ns10\Ns11\ParentClass',
                '\Ns00\Ns01\CName2' => '\Ns00\Ns01\ParentClass',
                ),
            $classes
            );         
    }
    
    public function testNs03() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces03.php');

        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = $class->getParentClass();  
        }
        $this->assertEquals(
            array(
                '\Ns00\Ns01\CName' => '\Ns10\Ns11\ParentClass',
                ),
            $classes
            );         
    }
    
    public function testNs04() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces04.php');

        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = $class->getParentClass();  
        }
        $this->assertEquals(
            array(
                '\Ns00\Ns01\CName' => '\Ns10\Ns11\ParentClass',
                ),
            $classes
            );         
    }

    public function testNs05() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces05.php');

        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = $class->getParentClass();  
        }
        $this->assertEquals(
            array(
                '\Ns00\Ns01\CName' => '\Ns10\Ns11\ParentClass',
                ),
            $classes
            );         
    }

    
}
