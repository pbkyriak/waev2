<?php
namespace Slx\ReflectionBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;
/**
 * Description of Namespaces1
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Namespaces2 extends \PHPUnit_Framework_TestCase
{

    private function getFileReflector($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        return $fr;
    }
    
    public function test01() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces06.php');
        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = array(
                'extends' => $class->getParentClass(),
                'interface' => $class->getInterfaces()
            );
        }
        $check = array(
            '\\Ns00\\Ns01\\CName' => array(
                'extends' => '\\Ns10\\Ns11\\ParentClass',
                'interface' => array(
                    '\\Ns00\\Ns01\\Interface1','\\Ns10\\Ns12\\ParentClass2'
                )
            )
        );
        $this->assertEquals($check,$classes);
    }
    
    public function test02() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces07.php');
        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = array(
                'extends' => $class->getParentClass(),
                'interface' => $class->getInterfaces()
            );
        }
        $check = array(
            '\\Ns00\\Ns01\\CName2' => array(
                'extends' => '',
                'interface' => array(
                    '\\Ns10\\Ns13\\Interface1'
                )
            )
        );
        $this->assertEquals($check,$classes);
    }
    
    /**
     * checks relative namespacing for extends,interfaces
     */
    public function test03() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces08.php');
        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = array(
                'extends' => $class->getParentClass(),
                'interface' => $class->getInterfaces()
            );
        }
        $check = array(
            '\\Ns00\\Ns01\\CName2' => array(
                'extends' => '\\Ns00\\Ns01\\Ns012\\Parent1',
                'interface' => array(
                    '\\Ns00\\Ns01\\Ns011\\Interface1'
                )
            )
        );
        $this->assertEquals($check,$classes);
    }
}
