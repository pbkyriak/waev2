<?php
namespace Slx\ReflectionBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;
/**
 * Traits tests
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Namespaces3 extends \PHPUnit_Framework_TestCase
{

    private function getFileReflector($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        return $fr;
    }
    
    public function test01() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces09.php');
        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = array(
                'traits' => $class->getTraits()
            );
        }
        $check = array(
            '\\Ns00\\Ns01\\Cname' => array(
                'traits' => array('\\Ns00\\Ns01\\Trait1')
            )
        );
        $this->assertEquals($check,$classes);
    }

    /**
     * check relative and absolute namespace in traits
     */
    public function test02() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces10.php');
        $classes = array();
        foreach ($fr->getClasses() as $class) {
            $classes[$class->getFQName()] = array(
                'traits' => $class->getTraits()
            );
        }
        $check = array(
            '\\Ns00\\Ns01\\Cname' => array(
                'traits' => array(
                    '\\Ns00\\Ns01\\Trait1',
                    '\\Ns10\\Ns11\\Trait2',
                    '\\Ns00\\Ns01\\Ns011\\Ns0111\\Trait2'
                    )
            )
        );
        $this->assertEquals($check,$classes);
    }

    /**
     * test Trait method reflection
     */
    public function test03() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/WithNamespaces09.php');
        $result = array();
        foreach ($fr->getTraits() as $trait) {
            $result[$trait->getFQName()] = array();
            foreach ($trait->getMethods() as $meth) {
                $result[$trait->getFQName()][] = $meth->getSignature()->toString();
            }
        }
        
        $check = array(
            '\\Ns00\\Ns01\\Trait1' => array(
                'public meth1()','private meth2()', 'public abstract meth3()'
            )
        );
        
        $this->assertEquals($check,$result);
    }

}
