<?php

namespace Slx\ReflectionBundle\Tests;

use Slx\ReflectionBundle\Reflection\Traverser;

/**
 * Traits tests
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 15 Νοε 2014
 */
class Signature1 extends \PHPUnit_Framework_TestCase implements \PHPParser_NodeVisitor {

    private function getAST($fn) {
        $p = new \PHPParser_Parser(new \PHPParser_Lexer());
        $stmts = $p->parse(file_get_contents($fn));
        return $stmts;
    }

    private function getFileReflector($fn) {
        $fr = new \Slx\ReflectionBundle\Reflection\FileReflector($fn);
        $fr->parse();
        $traverser = new Traverser();
        $traverser->addVisitor($fr);
        $traverser->traverseAst($fr->getAST());
        return $fr;
    }

    
    /**
     * find return statements in function
     */
    public function teest01() {
        $nodes = $this->getAST('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/Signature01.php');
        $returnsCount =0;
        foreach ($nodes as $node) {
            if( $node instanceof \PHPParser_Node_Stmt_Function ) {
                $returns = \Slx\ReflectionBundle\Reflection\StmtFinder::getInstance()->find($node->stmts, 'PHPParser_Node_Stmt_Return')->getResults();
                if( $returns ) {
                    $returnsCount = count($returns);
                }
            }
        }
        $this->assertEquals(2,$returnsCount);
    }
            /*
            $traverser = new Traverser();
            $traverser->addVisitor($this);
            $traverser->traverseAst($node->stmts);
             * 
             */

    public function test02() {
        $fr = $this->getFileReflector('/var/www/waev/src/Slx/ReflectionBundle/Tests/UseCases/Signature02.php');
        foreach( $fr->getClasses() as $class ) {
            printf("C: %s \n", $class->getSignature());
            foreach($class->getMethods() as $meth) {
                //var_dump(get_class_methods(get_class($meth)));
                $sig = $meth->getSignature();
                printf("%s %s rc=%s\n",$sig, $sig->getReturnType(), $sig->isReturnCleared() ? 'y' : 'n');
            }
        }
    }
    
    
    public function afterTraverse(array $nodes) {
        
    }

    public function beforeTraverse(array $nodes) {
        
    }

    public function enterNode(\PHPParser_Node $node) {
    }

    public function leaveNode(\PHPParser_Node $node) {
        
    }

}
