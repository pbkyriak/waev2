<?php
/**
 * the case is to check is FQN of the class is correct 
 * class FQN is \Ns00\Ns01\CName
 */

namespace Ns00\Ns01 {

    class WithNamespaces 
    {
        public function meth1() {

        }

        private function meth2() {

        }
    }
}

namespace Ns00\Ns02 {

    class WithNamespaces 
    {
        public function meth1() {

        }

        private function meth2() {

        }
    }
}