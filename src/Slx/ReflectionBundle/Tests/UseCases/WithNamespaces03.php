<?php
/**
 * the case here is to check that extends replaced is FQN
 * 
 * class FQN is \Ns00\Ns01\CName
 * extends \Ns10\Ns11\ParentClass
 */
namespace Ns00\Ns01;

use \Ns10\Ns11\ParentClass;

class CName extends ParentClass {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}