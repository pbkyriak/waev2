<?php
/**
 * the case here is to check alias in use. pClass should be replaced with FQN
 * class FQN is \Ns00\Ns01\CName
 * extends \Ns10\Ns11\ParentClass
 */
namespace Ns00\Ns01;

use Ns10\Ns11\ParentClass as pClass,
    Ns10\Ns12\ParentClass2 as pClass2;

class CName extends pClass implements Interface1, pClass2 {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}
