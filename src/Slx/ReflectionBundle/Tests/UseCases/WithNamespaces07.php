<?php
/**
 * the case here is to check alias in use. pClass should be replaced with FQN
 * class FQN is \Ns00\Ns01\CName
 * extends \Ns10\Ns11\ParentClass
 */
namespace Ns00\Ns01;

class CName2 implements \Ns10\Ns13\Interface1 {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}