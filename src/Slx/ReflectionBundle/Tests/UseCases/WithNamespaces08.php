<?php
/**
 * the case here is to check alias in use. pClass should be replaced with FQN
 * extends \Ns00\Ns01\Ns012\Parent1
 * implements \Ns00\Ns01\Ns011\Interface1
 */
namespace Ns00\Ns01;

class CName2 extends Ns012\Parent1 implements Ns011\Interface1 {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}