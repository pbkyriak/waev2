<?php
/**
 * 
 */
namespace Ns00\Ns01;

interface IFace1 {
    public function ifMeth1();
    public static function ifMeth2( $a, $b='123');
    public function ifMeth3($a, $b, $c=1);
}

trait Trait1 {
    
    public function meth1 () {
        
    }
    /**
     * 
     * @param array $array
     */
    private function meth2($array) {
        array_walk($array, function($a) { return $a+1; });
        $f = function() { echo 'asd'; };
    }
    abstract public function meth3();
    
    function meth4() {
    
    }
    
    function _meth5() {
        
    }
}

class Cname extends AbClass {
    use Trait1;
    public $field1;
    /** @var int $field2 */
    private $field2;
    public $field3 = array();
    public $bar = <<<'EOT'
bar
EOT;
    private function meth3($arg1) {
        try {
            
        } catch (Exception $ex) {
            throw new \InvalidArgumentException('message');
        }
    }
}

function Fname($a, &$b, &$c=true) {
    
}

/**
 * 
 * @param int $a
 * @param string $b
 */
function Fname2($a, &$b, array $c) {
    
}