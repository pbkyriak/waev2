<?php
/**
 * check relative and absolute namespace in traits
 */
namespace Ns00\Ns01;

trait Trait1 {
    
    public function meth1 () {
        
    }
    
    private function meth2() {
        
    }
}

class Cname {
    use Trait1;
    use \Ns10\Ns11\Trait2;
    use Ns011\Ns0111\Trait2;
    
    private function meth3() {
        
    }
}