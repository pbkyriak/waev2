<?php

namespace Slx\SurvivalBundle\Analyser\Survival;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\SurvivalBundle\Analyser\Survival\DataKaplanMeier;

/**
 * Performs the "known" kaplan-meier analysis (no staggered entry)
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
abstract class AbstractFuncSurvival
{

    /**
     *
     * @var EntityManager
     */
    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    abstract public function setParameters($params);
    

    abstract protected function getSqlQuery();

    abstract protected function getQueryParams();

    public function analyse() {
        $data = $this->getSurvivalData();
        $km = new DataKaplanMeier($data, 'fname', 'lifetime', 'event');
        $results = $km->getResults();
        return $results;
    }

    private function getSurvivalData() {
        return $this->queryData($this->getSqlQuery(), $this->getQueryParams());
    }
    
    private function queryData($sql, $params) {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;        
    }
}
