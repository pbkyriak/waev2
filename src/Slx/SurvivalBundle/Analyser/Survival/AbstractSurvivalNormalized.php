<?php

namespace Slx\SurvivalBundle\Analyser\Survival;

use Doctrine\ORM\EntityManager;
use \PDO;
/**
 * Description of AbstractSurvivalNormalized
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 2 Ιουν 2014
 */
abstract class AbstractSurvivalNormalized
{
    /**
     *
     * @var EntityManager
     */
    protected $em;
    protected $aType;
    protected $projectId;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        $this->aType=15;
    }
    
    abstract public function setParameters($params);
    

    abstract protected function getSqlQuery();

    abstract protected function getQueryParams();

    public function analyse() {
        $data = $this->getSurvivalData();
        $km = new DataKaplanMeier($data, 'fname', 'lifetime', 'event');
        $results = $km->getResults();
        $this->persist($results);
    }

    private function getSurvivalData() {
        return $this->queryData($this->getSqlQuery(), $this->getQueryParams());
    }
    
    private function queryData($sql, $params) {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;        
    }
    
    private function persist($rows) {
        $this->em->getConnection()->delete('project_func_survival', array('project_id'=>$this->projectId, 'atype'=>$this->aType));
        $idx = 1;
        foreach($rows as $row) {
            $this->persistRow($this->projectId, $idx, $row);
            $idx++;
        }
    }
    
    private function persistRow($projectId, $idx, $result) {
        $data = array(
            'atype'=>$this->aType,
            'project_id'=> $projectId,
            'project_tag_id_from' => null,
            'label' => $result['day'],
            'idx' => $idx,
            'estimator' => $result['estimator'],
            'surv_func' => 0,
            'in_risk' => $result['inRisk'],
            'slaughtered' => $result['slaughtered'],
        );
        $this->em->getConnection()->insert('project_func_survival', $data);
    }   
}
