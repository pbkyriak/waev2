<?php

namespace Slx\SurvivalBundle\Analyser\Survival;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\GitMinerBundle\DataWriter\DataWriterFileUtils;

/**
 * Performs kaplan-meier with staggered entry
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 8 Μαϊ 2014
 */
abstract class AbstractSurvivalStaggeredEntry
{

    /** @var EntityManager */
    protected $em;
    protected $projectId;
    protected $project;
    protected $atype = 5;
    
    public function __construct(EntityManager $em, $projectId)
    {
        $this->em = $em;
        $this->projectId = $projectId;
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
    }

    public function analyse() {
        $data = $this->calcEstimator($this->getData());
        $this->persist($data);
    }
    
    protected function getData()
    {
        $sql = $this->getQuery();
        $params = array('pid'=>$this->projectId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        if( $data ) {
            // drop first line of data
            array_shift($data);
        }
        return $data;        
    }

    abstract protected function getQuery();
    
    private function calcEstimator($rows) {
        $ct = 1;
        foreach($rows as $key => $row) {
            $rows[$key]['st'] = round(1-$row['events']/$row['inRisk'],3);
            $rows[$key]['ct'] = round($ct * $rows[$key]['st'],3);
            $ct = $rows[$key]['ct'];
        }
        return $rows;
    }
    
    private function persist($rows) {
        $this->em->getConnection()->delete('project_func_survival', array('project_id'=>$this->projectId, 'atype'=>$this->atype));
        $idx = 1;
        foreach($rows as $row) {
            $this->persistRow($this->projectId, $idx, $row);
            $idx++;
        }
    }
    
    private function persistRow($projectId, $idx, $result) {
        $data = array(
            'atype'=>$this->atype,
            'project_id'=> $projectId,
            'project_tag_id_from' => $result['tag_id'],
            'label' => $result['tag'],
            'idx' => $idx,
            'estimator' => $result['ct'],
            'surv_func' => $result['st'],
            'in_risk' => $result['inRisk'],
            'slaughtered' => $result['events'],
        );
        $this->em->getConnection()->insert('project_func_survival', $data);
    }    

}
