<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemoval;
use Doctrine\ORM\EntityManager;
use Slx\SurvivalBundle\Analyser\Survival\AbstractSurvivalStaggeredEntry;
/**
 * Survival of functions without calls (FanIn=0) only. Staggered entry approach. aType=6
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncNCStaggeredEntry extends AbstractSurvivalStaggeredEntry
{
    
    public function __construct(EntityManager $em, $projectId)
    {
        parent::__construct($em, $projectId);
        $this->atype = 6;
    }

    protected function getQuery()
    {
        $sql = "
            select p.tag, p.tag_id,
                count(*) as inRisk, 
                sum( if(p.toe_id=p.tag_id,1,0) ) as events
            from (
                select pt.id as tag_id, pt.tag, fmr.*
                from project_tag as pt
                left join fm_registry as fmr on (
                        pt.project_id=fmr.project_id 
                        and fmr.toa_id<=pt.id 
                        and if( isnull(fmr.toe_id),true,pt.id<=fmr.toe_id) 
                        and ftype in ('function') 
                        and not fmr.section in ('test')
                        and not isnull(fmr.tod_id)
                    )
                where pt.project_id=:pid
            ) as p
            group by p.tag_id";
        
        return $sql;        
    }
    
}
