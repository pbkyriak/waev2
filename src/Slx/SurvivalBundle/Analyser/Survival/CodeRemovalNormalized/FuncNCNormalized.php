<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalNormalized;

use Slx\SurvivalBundle\Analyser\Survival\AbstractSurvivalNormalized;

/**
 * Description of FuncNormalized
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 2 Ιουν 2014
 */
class FuncNCNormalized extends AbstractSurvivalNormalized
{
    
    protected function getQueryParams()
    {
        return array('pid'=>$this->projectId);
    }

    protected function getSqlQuery()
    {
        $sql = "SELECT 
                a.fname,
                if(
                    isnull(a.toe_id), 
                    100, 
                    round(
                        a.age/(
                            select count(id) 
                                from project_tag 
                                where 
                                    project_id=a.project_id 
                                    and id between a.toa_id and (
                                        select max(id) from project_tag where project_id=a.project_id
                                    )
                            ) 
                            *100
                        )
                ) as lifetime,
                not isnull(a.toe_id) as event
                FROM func_registry a
                where a.project_id=:pid and a.nb_calls=0
                ";
        return $sql;
    }

    public function setParameters($params)
    {
        $this->projectId = $params['project_id'];
        $this->aType=16;
    }   
}
