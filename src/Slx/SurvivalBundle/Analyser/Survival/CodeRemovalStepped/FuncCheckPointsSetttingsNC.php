<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped;

use Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSurvivalSettingsInterface;
use Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\TagNCFuncSurvival;
use Doctrine\ORM\EntityManager;
/**
 * Survival of functions without calls (FanIn=0) only regarding deletion. Stepped approach. aType=0
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncCheckPointsSetttingsNC implements FuncCheckPointsSurvivalSettingsInterface
{
    private $projectId;
    
    public function setProjectId($v) {
        $this->projectId=$v;
    }
    
    public function getProjectId()
    {
        return $this->projectId;
    }
    
    public function getAType()
    {
        return 2;
    }
    
    public function getRemovalDataQuery()
    {
        $sql = "select a.project_tag_id_to as tag_id, d.tag as tag, (a.fnrem_nc)*100/count(c.id) as r
                from project_tag_diff a
                left join project_code_construct c on (a.project_tag_id_from=c.project_tag_id and (c.ctype='function'))
                left join project_tag d on (a.project_tag_id_to=d.id)
                where a.project_id=:pid
                group by a.project_tag_id_from;";
        return $sql;
    }
    
    public function getRemovelDataQueryParams()
    {
        return array('pid'=>$this->projectId);
    }
    
    public function getKM(EntityManager $em)
    {
        $km = new TagNCFuncSurvival($em);
        return $km;
    }
}
