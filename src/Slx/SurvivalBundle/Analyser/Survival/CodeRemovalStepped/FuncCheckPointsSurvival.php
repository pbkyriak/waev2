<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\MathBundle\Analyser\DataMaxima;
use Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSurvivalSettingsInterface;

/**
 * Κάνει Kaplan-Meier survival analysis για τις συναρτήσεις ενός έργου
 * Διαβάζει τον πληθυσμό και το πλήθος των διεγραμμένων συναρτήσεων από 
 * τον project_tag_diff. Διαλέγει την πρώτη γενιά και το τα 4 μεγαλύτερα
 * τοπικά μέγιστα διαγραφών (όσα βασικά λέει η σταθερά CHECK_POINTS_COUNT),
 * από αυτά ξεκινάει και κάνει survival analysis τα αποτελέσματα τα γράφει 
 * στον project_func_survival. Από αυτόν τον πίνακα τα παίρνει έτοιμα το 
 * interface και τα παρουσιάζει με γραφήματα.
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncCheckPointsSurvival
{

    /**
     *
     * @var EntityManager
     */
    private $em;
    private $projectId;
    /**
     *
     * @var FuncCheckPointsSurvivalSettingsInterface 
     */
    private $settings;
    private $atype;
    
    /**
     * how many peaks to get as starting points for survival analysis
     */
    const CHECK_POINTS_COUNT = 4;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
        
    }

    public function setParameters(FuncCheckPointsSurvivalSettingsInterface $settings) {
        $this->settings = $settings;
        $this->projectId = $settings->getProjectId();
        $this->atype = $settings->getAType();
    }
    
    public function analyse() {
        $this->em->getConnection()->delete('project_func_survival', array('project_id'=>$this->projectId, 'atype'=>$this->atype));
        $chkPoints = $this->getCheckPoints();
        foreach($chkPoints as $chkPoint) {
            $this->doKM($chkPoint);
        }
    }
    
    private function doKM($chkPoint) {
        $kms = $this->settings->getKM($this->em);
        $kms->setParameters(array('project_id'=>$this->projectId, 'project_tag_id'=>$chkPoint['tag_id']));
        $results = $kms->analyse();
        foreach($results as $result) {
            $this->persistData($this->projectId, $chkPoint, $result);
        }
    }
    
    private function getCheckPoints() {
        $out = array();
        $data = $this->getProjectRemovalData();
        $out[] = $data[0];  // keep first tag. Survival analysis for first tag have to be done anyway.
        $data = array_splice($data, 1); // remove it from data
        $dm = new DataMaxima();
        $out = $this->sortByTag(
            array_merge(
                $out,
                $dm->getMaximaPoints($data, 'r', self::CHECK_POINTS_COUNT)
            )
        );
        
        return $out;
    }
    
    private function sortByTag($data) {
        // sort them by tagId
        usort( 
            $data, 
            function($a,$b) { 
                if($a['tag_id']==$b['tag_id']) 
                    return 0; 
                return $a['tag_id']<$b['tag_id'] ? -1 : 1;
            }
        );
        
        return $data;
    }
        
    private function getProjectRemovalData() {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($this->settings->getRemovalDataQuery(), $this->settings->getRemovelDataQueryParams());
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $i=0;
        array_walk($data, function(&$v, $k) use (&$i) { $v['idx']=++$i; });
        return $data;
    }
    
    private function persistData($projectId, $chkPoint, $result) {
        $data = array(
            'atype'=>$this->atype,
            'project_id'=> $projectId,
            'project_tag_id_from' => $chkPoint['tag_id'],
            'label' => $chkPoint['tag'],
            'idx' => $chkPoint['idx']+$result['day'],
            'estimator' => $result['estimator'],
            'surv_func' => $result['inRisk'] ? 1-($result['slaughtered']/$result['inRisk']) : 0,
            'in_risk' => $result['inRisk'],
            'slaughtered' => $result['slaughtered'],
        );
        $this->em->getConnection()->insert('project_func_survival', $data);
    }    
}
