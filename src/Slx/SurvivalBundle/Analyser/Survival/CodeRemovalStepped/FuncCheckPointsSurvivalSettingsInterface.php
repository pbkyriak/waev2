<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped;

use Doctrine\ORM\EntityManager;

/**
 * Description of FuncCheckPointsSurvivalSettingsInterface
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
interface FuncCheckPointsSurvivalSettingsInterface
{
    public function setProjectId($v);
    public function getProjectId();
    public function getRemovalDataQuery();
    public function getRemovelDataQueryParams();
    public function getKM(EntityManager $em);
    public function getAType();
}
