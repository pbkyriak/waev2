<?php
namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped;

use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\DataWriter\DataWriterFileUtils;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of FuncSurvivalCPExport
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncSurvivalCPExport
{
    /** @var EntityManager */
    private $em;
    private $projectId;
    /** @var Project */
    private $project;
    private $data;
    private $dataSetCount=0;
    private $delData;
    
    public function __construct(EntityManager $em, $projectId)
    {
        $this->em=$em;
        $this->projectId = $projectId;
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        $this->collectData();
    }
    
    public function write($fn) {
        printf("dataset count=%s\n",$this->dataSetCount);
        for($i=1; $i<=$this->dataSetCount; $i++) {
            $this->writeDataSet($fn, $i);
        }
        $this->writeDataSet($fn, 'r');
    }
    
    private function writeDataSet($fn, $datasetId) {
        
        $fn = sprintf($fn, is_numeric($datasetId) ? $datasetId-1 : $datasetId);
        $ofn = DataWriterFileUtils::getInstance()->addProjectPath($this->projectId, $fn);
        if( $datasetId=='r' ) {
            $datasetId = 1;
            $data = $this->delData;
        }
        else {
            $data = $this->data;
        }
        printf("wrinting dataset id=%s\n", $datasetId);
        $handle = fopen($ofn, "w");
        if($handle) {
            $idx = 1;
            foreach($data as $row) {
                if( isset($row[$datasetId]) ) {
                    $out = array($idx, $row[$datasetId]);
                    fputcsv($handle, $out, "\t");
                }
                $idx++;
            }
        }
        fclose($handle);
    }
    
    /** 
     * get data from reports and create a table with all data
     */
    private function collectData() {
        $rpt1 = new \Slx\GitMinerBundle\Reports\ReportFuncRemKM($this->em);
        $rpt1->setParameters(array('project'=>$this->project));
        $rpt1->createResults();
        $this->data = $rpt1->getTabledData();
        $this->dataSetCount = $rpt1->getSeriesCount();
        
        $rpt2 = new \Slx\GraphRptBundle\Report\SimpleChartRpt($this->em);
        $rpt2->setRptEntityId(15)
            ->setParameters(array('project_id'=>$this->project->getId()))
            ->createResults();
        $this->delData = $rpt2->getTabledData();
    }
}
