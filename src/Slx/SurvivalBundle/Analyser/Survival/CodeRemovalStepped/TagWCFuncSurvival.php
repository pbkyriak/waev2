<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped;

use Slx\SurvivalBundle\Analyser\Survival\AbstractFuncSurvival;
/**
 * Tag's function survival, functions With Calls only
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class TagWCFuncSurvival extends AbstractFuncSurvival 
{
    private $projectId;
    private $projectTagId;
    
    protected function getQueryParams()
    {
        return array('pid'=>$this->projectId, 'tid'=>$this->projectTagId);
    }

    protected function getSqlQuery()
    {
        $sql = "select pc.cname as func, pc.project_tag_id, waev_get_age(pc.project_tag_id,if(isnull(fr.toe_id),0,fr.toe_id)) as lifetime, if(isnull(fr.toe_id),0,1) as event
                from project_code_construct as pc
                left join func_registry as fr on (fr.fname=pc.cname and project_id=:pid)
                where pc.ctype='function' and pc.project_tag_id=:tid and pc.dead=0 -- fr.nb_calls!=0
                order by pc.project_tag_id,func";
        return $sql;
    }

    public function setParameters($params)
    {
        $this->projectId = $params['project_id'];
        $this->projectTagId = $params['project_tag_id'];
    }

}
