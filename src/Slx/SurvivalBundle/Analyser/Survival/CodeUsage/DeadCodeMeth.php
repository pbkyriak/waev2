<?php

namespace Slx\SurvivalBundle\Analyser\Survival\CodeUsage;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\SurvivalBundle\Analyser\Survival\AbstractSurvivalStaggeredEntry;
/**
 * Survival of methods only regarding usage. Staggered entry approach. aType=11
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 8 Μαϊ 2014
 */
class DeadCodeMeth extends AbstractSurvivalStaggeredEntry
{
    public function __construct(EntityManager $em, $projectId)
    {
        parent::__construct($em, $projectId);
        $this->atype = 11;
    }

    protected function getQuery() {
        $sql ="
            select p.tag, p.tag_id,
                count(*) as inRisk, 
            sum( if(p.tod_id=p.tag_id,1,0) ) as events
            from (
                select distinct pt.id as tag_id, pt.tag, fmr.fname, fmr.tod_id
                from project_tag as pt
                left join fm_registry as fmr on (
                    pt.project_id=fmr.project_id 
                    and fmr.toa_id<=pt.id 
                    and if( isnull(fmr.toe_id),true,pt.id<fmr.toe_id) 
                    and ftype in ('method'))
                left join project_code_construct pcc on (fmr.tod_id=pcc.project_tag_id and fmr.ftype=pcc.ctype and fmr.fname=pcc.cname)
                left join project_file as pf on (pcc.project_file_id=pf.id)
                where pt.project_id=:pid %namefilter%
            ) as p
            group by p.tag_id";
        $nameFilter = $this->project->getTestNamesFilter('pf.fname');
        if( trim($nameFilter) ) {
            $nameFilter = sprintf("and (isnull(pf.fname) or not (%s))", $nameFilter);
        }
        $sql = str_replace('%namefilter%', $nameFilter, $sql);
        return $sql;
    }
}
