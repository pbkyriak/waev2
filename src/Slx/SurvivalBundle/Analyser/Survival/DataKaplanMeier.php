<?php

namespace Slx\SurvivalBundle\Analyser\Survival;
/**
 * Generic class for Kaplan-Meier curve calculation.
 * Works on arrays with data caseId, lifetime, event
 * Returns an array with columns day, inrisk, 
 * slaughtered (in event), estimator
 * - Dec 5, 2013
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DataKaplanMeier
{
    private $caseIdCol;
    private $lifeCol;
    private $eventCol;
    private $data;
    private $duration;
    private $results;
    /**
     * 
     * @param array $data
     * @param string $caseIdCol
     * @param string $lifeCol
     * @param string $eventCol
     */
    public function __construct($data, $caseIdCol, $lifeCol, $eventCol)
    {
        $this->data = $data;
        $this->caseIdCol = $caseIdCol;
        $this->lifeCol = $lifeCol;
        $this->eventCol = $eventCol;
        $this->duration = $this->getDataDuration();
        $this->results = array();
        $this->calculate();
    }
    
    private function calculate() {
        $estimator = 1;
        $this->results[] = array('day'=>0, 'inRisk'=>$this->getInRisk(1), 'slaughtered'=>0, 'estimator'=>1);
        for($i=1; $i<=$this->duration; $i++) {
            $inRisk = $this->getInRisk($i);
            $eliminated = $this->getEliminated($i);
            $estimator = $estimator * ($inRisk-$eliminated)/$inRisk;
            $this->results[] = array('day'=>$i, 'inRisk'=>$inRisk, 'slaughtered'=>$eliminated, 'estimator'=>round($estimator,3));
        }
    }
    
    public function getResults() {
        return $this->results;
    }
    
    private function getDataDuration() {
        $max = 0;
        array_walk(
            $this->data, 
            function($v, $k) use (&$max) { 
                if( $v[$this->lifeCol]>$max) {
                    $max = $v[$this->lifeCol];
                }
            }
        );
        return $max;
    }

    private function getInRisk($day) {
        $inRisk=0;
        array_walk(
            $this->data, 
            function($v, $k) use (&$inRisk, $day) { 
                if( $v[$this->lifeCol]>=$day) {
                    $inRisk++;
                }
            }
        );
        return $inRisk;
    }
    
    private function getEliminated($day) {
        $eliminated=0;
        array_walk(
            $this->data, 
            function($v, $k) use (&$eliminated, $day) { 
                if( $v[$this->lifeCol]==$day && $v[$this->eventCol]) {
                    $eliminated++;
                }
            }
        );        
        return $eliminated;
    }
}
