<?php

namespace Slx\SurvivalBundle\AnalysisTask\AggrTasks;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;
use PDO;

/**
 * Description of DeadCodeTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DeadCodeTask extends AbstractTask
{

    /** @var Project */
    protected $project;
    protected $projectId;

    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        return true;
    }

    public function execute()
    {
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeUsage\DeadCodeAll($this->em, $this->projectId);
        $an->analyse();
        return true;
    }

    protected function writeData($out, $fn) {
        $i=1;
        $handle = fopen($fn, "w");
        foreach($out as $row) {
            $row['tag'] = $i;
            fputcsv($handle, $row, "\t");
            $i++;
        }
        fclose($handle);
    }
        
    protected function getData()
    {
        $sql = $this->getQuery();
        $params = array('pid'=>$this->projectId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    protected function getQuery() {
        $sql ="
            select p.tag, count(*) as cnt, 
            sum( if(p.tod_id=p.tag_id,1,0) ) as events
            from (
                select distinct pt.id as tag_id, pt.tag, fmr.fname, fmr.tod_id
                from project_tag as pt
                left join fm_registry as fmr on (
                    pt.project_id=fmr.project_id 
                    and fmr.toa_id<=pt.id 
                    and if( isnull(fmr.toe_id),true,pt.id<fmr.toe_id) 
                    and ftype in ('function', 'method'))
                left join project_code_construct pcc on (fmr.tod_id=pcc.project_tag_id and fmr.ftype=pcc.ctype and fmr.fname=pcc.cname)
                left join project_file as pf on (pcc.project_file_id=pf.id)
                where pt.project_id=:pid %namefilter%
            ) as p
            group by p.tag_id";
        $nameFilter = $this->project->getTestNamesFilter('pf.fname');
        if( trim($nameFilter) ) {
            $nameFilter = sprintf("and (isnull(pf.fname) or not (%s))", $nameFilter);
        }
        $sql = str_replace('%namefilter%', $nameFilter, $sql);
        return $sql;
    }
    
    protected function calcEstimator($rows) {
        $ct = 1;
        // drop first line of data
        array_shift($rows);
        // calculate
        foreach($rows as $key => $row) {
            $rows[$key]['st'] = round(1-$row['events']/$row['cnt'],3);
            $rows[$key]['ct'] = round($ct * $rows[$key]['st'],3);
            $ct = $rows[$key]['ct'];
        }
        return $rows;
    }
    
    public function getTaskName()
    {
        return 'Dead code survival (Estimated code deaths)';
    }

    public function onFail()
    {
        return true;
    }

}
