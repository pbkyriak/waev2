<?php
namespace Slx\SurvivalBundle\AnalysisTask\AggrTasks;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of FuncSurvivalTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncSurvivalTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    
    protected $projectId;
    
    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        return true;
    }
    
    public function execute()
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        
        // stepped approach regarding code removal
        // for all functions
        $settings = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSetttingsAll();
        $settings->setProjectId($this->projectId);
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSurvival($this->em);
        $an->setParameters($settings);
        $an->analyse();

        // only for functions with calls
        $settings = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSetttingsWC();
        $settings->setProjectId($this->projectId);
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSurvival($this->em);
        $an->setParameters($settings);
        $an->analyse();
        
        // only for functions withOUT calls
        $settings = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSetttingsNC();
        $settings->setProjectId($this->projectId);
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalStepped\FuncCheckPointsSurvival($this->em);
        $an->setParameters($settings);
        $an->analyse();

        // normilized lifetime approach regarding code removal
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalNormalized\FuncNormalized($this->em);
        $an->setParameters(array('project_id'=>$this->projectId));
        $an->analyse();
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalNormalized\FuncNCNormalized($this->em);
        $an->setParameters(array('project_id'=>$this->projectId));
        $an->analyse();
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemovalNormalized\FuncWCNormalized($this->em);
        $an->setParameters(array('project_id'=>$this->projectId));
        $an->analyse();

        
        // staggered entry survival analysis regarding code removal
        // for all functions
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemoval\FuncStaggeredEntry($this->em, $this->projectId);
        $an->analyse();
        // for dead functions
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemoval\FuncNCStaggeredEntry($this->em, $this->projectId);
        $an->analyse();
        // for used functions (FanIn!=0)
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemoval\FuncWCStaggeredEntry($this->em, $this->projectId);
        $an->analyse();
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeRemoval\FMStaggeredEntry($this->em, $this->projectId);
        $an->analyse();

        // staggered entry survival analysis regarding code usage
        // for all functions+methods
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeUsage\DeadCodeAll($this->em, $this->projectId);
        $an->analyse();
        // for all functions+methods excluding library code
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeUsage\DeadCodeNoLibs($this->em, $this->projectId);
        $an->analyse();
        // for functions only
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeUsage\DeadCodeFunc($this->em, $this->projectId);
        $an->analyse();
        // for methods only
        $an = new \Slx\SurvivalBundle\Analyser\Survival\CodeUsage\DeadCodeMeth($this->em, $this->projectId);
        $an->analyse();
        
        return true;
    }

    public function getTaskName()
    {
        return 'Function survival';
    }

    public function onFail()
    {
        return true;
    }

}
