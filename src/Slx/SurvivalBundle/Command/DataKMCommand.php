<?php

namespace Slx\SurvivalBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;

/**
 * Description of dataKMCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 4 Ιουλ 2014
 */
class DataKMCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('waev:datakm')
            ->setDescription("Reads data does Kaplan-Meier and output results")
            ->addArgument('projectid',
                InputArgument::REQUIRED,
                'Project Id to get data?')
            ->addArgument('type',
                InputArgument::REQUIRED,
                'Type of survival (u)sage, (r)emoval?')
            ->addArgument('section',
                InputArgument::REQUIRED,
                'Sections in include? (s=system, l=library)')
            ->addArgument('outfn',
                InputArgument::REQUIRED,
                'file to write data?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $projectId = $input->getArgument('projectid');
        $fn = $input->getArgument('outfn');
        $sectionsOpt = $input->getArgument('section');
        $sections = $this->sectionOptsToSections($sectionsOpt);
        if( count($sections)==0 ) {
            $output->writeln('please define sections (s or l)');
            return;            
        }
        $type = $input->getArgument('type');
        if( $type=='u' ) {
            $data = $this->readDataForUsage($projectId, $sections);
        }
        elseif( $type=='r' ) {
            $data = $this->readDataForRemoval($projectId, $sections);
        }
        else {
            $output->writeln('Not supported type '.$type);
            return;
        }
        $an = new \Slx\SurvivalBundle\Analyser\Survival\DataKaplanMeier($data, 'id', 'lifetime', 'event');
        $results = $an->getResults();
        $this->writeOutput($fn, $results);

//            printf("day=%s inRisk=%s slaughtered=%s estimator=%s\n", $result['day'], $result['inRisk'], $result['slaughtered'], $result['estimator']);

        $output->writeln(sprintf("execution time: %s sec", time()-$t1));
    }

    private function readDataForUsage($projectId, $sections) {
        $sectionsSql = implode(',', $sections);
        printf("%s\n", $sectionsSql);
        $sql = "select id, waev_get_age(toa_id, if(isnull(tod_id),0,tod_id)) as lifetime, if(isnull(tod_id),0,1) as event
                from fm_registry where project_id=:pid and section in (".$sectionsSql.") having lifetime>1";
        $params = array('pid'=>$projectId);
        return $this->fetch($sql, $params);
    }

    private function readDataForRemoval($projectId, $sections) {
        $sectionsSql = implode(',', $sections);
        $sql = "select id, waev_get_age(toa_id, if(isnull(toe_id),0,toe_id)) as lifetime, if(isnull(toe_id),0,1) as event
                from fm_registry where project_id=:pid and section in (".$sectionsSql.")";
        $params = array('pid'=>$projectId);
        return $this->fetch($sql, $params);
    }
    
    private function sectionOptsToSections($opts) {
        $out = array();
        if( !(strpos($opts, 's')===false) ) {
            $out[] = "'system'";
        }
        if( !(strpos($opts, 'l')===false) ) {
            $out[] = "'lib'";
        }
        return $out;
    }
    
    private function fetch($sql, $params)
    {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        return $data;
    }
    
    private function writeOutput($fn, $data) {
        $handle = fopen($fn, "w");
        if( $handle ) {
            $header = null;
            reset($data);
            while( ($row=current($data)) ) {
                if( !$header ) {
                    $header = array_keys($row);
                    fputcsv($handle,$header,"\t");
                }
                fputcsv($handle,$row,"\t");
                next($data);
            }
            fclose($handle);
        }

    }

 }
