<?php

namespace Slx\SurvivalBundle\DataWriter\Survival;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\GitMinerBundle\DataWriter\DataWriterFileUtils;

/**
 * Description of FuncStaggeredExport
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncStaggeredExport
{

    /** @var EntityManager */
    private $em;
    private $projectId;

    public function __construct($em, $projectId)
    {
        $this->em = $em;
        $this->projectId = $projectId;
    }

    public function export()
    {
        /* 
        // removal
        $data = $this->mergeData(
            $this->getSurvivalData(5), 
            $this->getSurvivalData(6), 
            $this->getSurvivalData(7), 
            $this->getAdditionData()
        );
         * 
         */
        // usage
        $data = $this->mergeData(
            $this->getSurvivalData(8), 
            $this->getSurvivalData(9), 
            $this->getSurvivalData(9), 
            $this->getAdditionData()
        );
        $fn = DataWriterFileUtils::getInstance()->addProjectPath($this->projectId, 'func-all-usage.csv');
        $this->writeData($data, $fn);
    }

    private function getSurvivalData($atype)
    {
        //$sql = "select idx, estimator from project_func_survival where project_id=:pid and atype=:at";
        $sql = "select idx, 1-slaughtered/in_risk as estimator from project_func_survival where project_id=:pid and atype=:at";
        $params = array('pid' => $this->projectId, 'at'=>$atype);
        return $this->fetch($sql, $params);
    }

    private function getAdditionData()
    {
        $sql = "select 
                    pt.tag,
                    round(sum( if(pt.id=fmr.toa_id and fmr.ftype='method', 1,0) )/count(*)*100,2) as perc
                from project_tag as pt
                    left join fm_registry as fmr on (
                        pt.project_id=fmr.project_id 
                        and fmr.toa_id<=pt.id 
                        and if( isnull(fmr.toe_id),true,pt.id<=fmr.toe_id) 
                        and not fmr.section in ('test', 'lib')
                    )
                where 
                    pt.project_id=:pid
                group by pt.id";
        $params = array('pid' => $this->projectId);
        $data = $this->fetch($sql, $params);
        array_shift($data);
        //array_unshift($data, array('tag' => 0, 'perc'=>0));
        return $data;
    }

    private function fetch($sql, $params)
    {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }

    private function mergeData($sData1, $sData2,$sData3, $aData)
    {
        $data = array();
        $idx = 1;
        foreach ($sData1 as $row) {
            $sRow2 = array_shift($sData2);
            $sRow3 = array_shift($sData3);
            $aRow = array_shift($aData);
            $data[] = array(
                $idx,
                $row['estimator'],
                $sRow2['estimator'],
                $sRow3['estimator'],
                $aRow['perc']
            );
            $idx++;
        }
        return $data;
    }

    private function writeData($data, $fn)
    {
        $handle = fopen($fn, "w");
        if ($handle) {
            foreach ($data as $row) {
                fputcsv($handle, $row, "\t");
            }
        }
        fclose($handle);
    }

}
