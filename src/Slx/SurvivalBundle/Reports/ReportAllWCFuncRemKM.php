<?php
namespace Slx\SurvivalBundle\Reports;

use \Doctrine\ORM\EntityManager;
use Slx\GraphRptBundle\Report\AbstractGraphReport;
use Slx\SurvivalBundle\Analyser\Survival\CodeRemovalNormalized\AllWCFuncSurvival;
/**
 * Description of ReportAllFuncRemKM
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportAllWCFuncRemKM extends AbstractGraphReport
{
    /**
     *
     * @var Slx\GitMinerBundle\Entity\Project
     */
    protected $project;

    public function getReportTitle()
    {
        return 'Καμπύλη επιβιώσης όλων των συναρτήσεων με πληθυσμό εκκίνησης όλες τις συναρτήσεις που εμφανίστηκαν στο έργο και χρόνο ζωής την κανονικοποιημένη διάρκεια τους.';
    }
    
    public function setParameters($params)
    {
        $this->project = $params['project'];
    }

    public function createResults()
    {
        $this->series = array();
        $an = new AllWCFuncSurvival($this->em);
        $an->setParameters(array('project_id'=>$this->project->getId()));
        $results = $an->analyse();
        
        $sIdx = $this->addSeries();
        foreach($results as $result) {
            $this->addSeriesRow($sIdx, $result['day'],$result['estimator']);
        }
        $this->setLabel($sIdx,'All functions');
        $this->setColumnLabels($sIdx, 'Generation', 'Estimator');
    }
    
}
