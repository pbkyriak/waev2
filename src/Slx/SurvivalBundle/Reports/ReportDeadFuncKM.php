<?php

namespace Slx\SurvivalBundle\Reports;

use \Doctrine\ORM\EntityManager;
use Slx\GraphRptBundle\Report\AbstractGraphReport;
use \PDO;
/**
 * Creates data table report following Scanniello's paper
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportDeadFuncKM
{
    private $em;
    private $projectId;
    private $data = array();
    
    public function __construct(EntityManager $em, $projectId)
    {
        $this->em = $em;
        $this->projectId = $projectId;
        $this->data = $this->calcCumulativeSF($this->queryData());
    }
    
    private function getQuery() {
        return "select tag, atRisk, dead, censored, 1-dead/(atRisk-censored) as st, 1 ct
                from (
                select pt.tag, 
                count(*) as atRisk, 
                sum(if(ptfa.nb_calls=0,1,0)) as dead, 
                if(ptd.fnrem, ptd.fnrem,0) as censored
                from project_func_signature as pfs
                left join project_tag pt on (pt.id=pfs.project_tag_id)
                left join project_tag_diff as ptd on (pt.id=ptd.project_tag_id_to)
                left join project_tag_func_call_aggr as ptfa on (pfs.fname=ptfa.func and pfs.project_tag_id=ptfa.project_tag_id)
                where pt.project_id=:pid
                group by pt.id
                ) as a
                ";
    }
    
    private function queryData() {
        $params = array(
            'pid' => $this->projectId
        );
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($this->getQuery(), $params);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $data;        
    }
    
    /**
     * Calculates cumulative survival function for the dataset
     * 
     * @param array $data
     * @return array
     */
    private function calcCumulativeSF($data) {
        $ct = 1;
        foreach($data as $key => $row) {
            $data[$key]['st'] = round($row['st'],3);
            $data[$key]['ct'] = round($row['st']*$ct,3);
            $ct = $data[$key]['ct'];
        }
        return $data;
    }
    
    public function getResults() {
        return $this->data;
    }
    
    public function getLabels() {
        return array(
            'Tag',
            'At risk',
            'Event',
            'Censored',
            'S(t)',
            'C(t)'
        );
    }
}
