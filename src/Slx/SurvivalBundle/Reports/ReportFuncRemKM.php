<?php
namespace Slx\SurvivalBundle\Reports;

use \Doctrine\ORM\EntityManager;
use Slx\GraphRptBundle\Report\AbstractGraphReport;

/**
 * Description of ReportFuncRemKM
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ReportFuncRemKM extends AbstractGraphReport
{
    
    protected $project;
    protected $atype=0;

    public function getReportTitle()
    {
        return 'Καμπύλες επιβιώσης των συναρτήσεων με πληθυσμό εκκίνησης την πρώτη γενιά και στη συνέχεια, τις 4 με το μεγαλύτερο ποσοστό διαγραφών.';
    }
    
    public function setParameters($params)
    {
        $this->project = $params['project'];
        if( isset($params['atype']) ) {
            $this->atype = $params['atype'];
        }
    }

    public function createResults()
    {
        $tagIds = $this->getProjectTags();
        foreach($tagIds as $tagId) {
            $sIdx = $this->addSeries();
            $this->getTagSeries($sIdx, $tagId);
        }
    }
    
    private function getProjectTags() {
        $data = $this->em
            ->createQuery("select distinct b.id, a.label from SlxGitMinerBundle:ProjectFuncSurvival a left join a.projectTagFrom b where a.project=:project and a.atype=:atype")
            ->setParameters(array('project'=>$this->project, 'atype'=>$this->atype))
            ->getScalarResult();
        $tagIds = array();
        $sIdx = 0;
        foreach($data as $row) {
            $this->setLabel($sIdx, $row['label']);
            $tagIds[] = $row['id'];
            $sIdx++;
        }
        return $tagIds;
    }
    
    private function getTagSeries($sIdx, $tagId) {
        $data = $this->em
            ->createQuery("select a.idx, a.estimator from SlxGitMinerBundle:ProjectFuncSurvival a left join a.projectTagFrom b where b.id=:tagid and a.atype=:atype")
            ->setParameters(array('tagid'=>$tagId, 'atype'=>$this->atype))
            ->getScalarResult();
        $out = array();
        foreach($data as $row) {
            $this->addSeriesRow($sIdx, $row['idx'], $row['estimator']);
        }
        return $out;
    }
}
