Survival Bundle
###############

This Bundle contains all KM-Survival calculations related code.
All results are stored in  project_func_survival table. It works like a single table inheritance with discriminator column, column atype. Calculations are listed bellow and atype for each is mentioned.

SubPackage: CodeRemovalStepped (Deprecated)
===========================================
Calculations with termination event code removal. Using the idea of stepped approach. 

FuncCheckPointsSetttingsAll
---------------------------
Survival of function regarding deletion. Stepped approach.

**aType**: 0

FuncCheckPointsSetttingsWC
--------------------------
Survival of functions with calls (FanIn!=0) only regarding deletion. Stepped approach.

**aType**: 1

FuncCheckPointsSetttingsNC
--------------------------
Survival of functions without calls (FanIn=0) only regarding deletion. Stepped approach.

**aType**: 2

SubPackage: CodeRemovalNormalized (Deprecated)
==============================================
Calculations with termination event, but using the idea of normalized function lifetime.

FuncSurvivalNormalized
----------------------
Survival of functions regarding removal. Normalized lifetime approach

**aType**: 15

FuncNCNormalized
----------------
Survival of functions without calls (FanIn=0) only regarding removal. Normalized lifetime approach.

**aType**: 16

FuncWCNormalized
----------------
Survival of functions with calls (FanIn!=0) only regarding removal. Normalized lifetime approach.

**aType**: 17

Sub package: CodeRemoval
========================
Calculations with termination event code removal. Employing staggered entry KM.

FuncStaggeredEntry
------------------
Functions survival with staggered entry approach.

**aType**: 5

FuncNCStaggeredEntry
--------------------
Survival of functions without calls (FanIn=0) only. Staggered entry approach.

**aType**: 6

FuncWCStaggeredEntry
--------------------
Survival of functions with calls only (FanIn!=0). Staggered entry approach.

**aType**: 7

FMStaggeredEntry
----------------
Functions and methods survival with staggered entry approach.

**aType**: 12

SubPackage: CodeUsage
=====================

Calcuations with termination event usage. Employs staggered entry approach.

DeadCodeAll
-----------
Survival of functions and method regarding usage. Staggered entry approach

**aType**: 8

DeadCodeNoLibs
--------------
Survival of functions and method regarding usage excluding library code. Staggered entry approach.

**aType**: 9

DeadCodeFunc
------------
Survival of functions only regarding usage. Staggered entry approach.

**aType**: 10

DeadCodeMeth
------------
Survival of methods only regarding usage. Staggered entry approach.

**aType**: 11
