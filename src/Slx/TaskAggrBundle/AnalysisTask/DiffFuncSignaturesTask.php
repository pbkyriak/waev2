<?php

namespace Slx\TaskAggrBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of ProjectGenericDiffsTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DiffFuncSignaturesTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    
    protected $projectId;
    
    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        return true;
    }
    
    public function execute()
    {
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,0)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,1)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,2)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,3)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,4)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,5)", $this->projectId));
        $this->em->getConnection()->executeQuery(sprintf("call updateTagFuncSignatureDiffs(%s,6)", $this->projectId));
        return true;
    }

    public function getTaskName()
    {
        return 'Find function signature changes';
    }

    public function onFail()
    {
        return true;
    }
   
}
