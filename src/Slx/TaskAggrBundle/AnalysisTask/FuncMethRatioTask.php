<?php

namespace Slx\TaskAggrBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use PDO;
use Slx\GitMinerBundle\Entity\Project;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;

/**
 * Description of FuncMethPopulationTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncMethRatioTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    protected $projectId;

    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        return true;
    }

    public function execute()
    {
        $out = $this->calculus($this->getData());
        $fn = $this->getOutFilename('funcmeth.csv');
        //$this->writeData($out, $fn);
        $this->persist($out);
        return true;
    }

    private function calculus($data) {
        foreach($data as $k => $row) {
            $data[$k]['mratio'] = round($data[$k]['mcnt']/$data[$k]['tcnt'],2);
            $data[$k]['fratio'] = 1-$data[$k]['mratio'];
        }
        return $data;
    }
    
    private function persist($out ) {
        $this->em->getConnection()->delete('project_fm_ratio', array('project_id'=>$this->projectId));
        array_shift($out);  // drop first version, so it matches the rest of usage survival results
        $i=1;
        foreach($out as $row) {
            $r = array(
                'project_id'=>$this->projectId,
                'idx'=>$i, 
                'fratio'=> $row['fratio'],
                'mratio'=> $row['mratio'],
                );
            $this->em->getConnection()->insert('project_fm_ratio', $r);
            $i++;
        }
    }
    
    private function writeData($out, $fn) {
        $i=1;
        array_shift($out);  // drop first version, so it matches the rest of usage survival results
        $handle = fopen($fn, "w");
        foreach($out as $row) {
            $row['tag'] = $i;
            $r = array('id'=>$i, 'mratio'=> $row['mratio'], 'fratio'=> $row['fratio']);
            fputcsv($handle, $r, "\t");
            $i++;
        }
        fclose($handle);
    }
    
    private function getOutFilename($fn) {
        return sprintf("%s/%s/%s",
            realpath(__DIR__ . '/../../../../../web/uploads/projects'),
            $this->projectId,
            $fn
            );
    }
    
    private function getLibNamesFilter() {
        $out = '';
        $filters = array();
        $names = explode("\n",str_replace("\r","",$this->project->getLibraries()));
        print_R($names);
        foreach($names as $name) {
            $filters[] = sprintf("pf.fname like '%s'", $name);
        }
        $out = implode(' or ', $filters);
        return $out;
    }
    
    private function getData()
    {
        $filter = $this->getLibNamesFilter();
        if( !$filter ) {
            return array();
        }
        printf("%s\n", $filter);
        $sql = "
                select a.tag, count(*) as tcnt, sum(if(a.ctype='method',1,0)) as mcnt,sum(if(a.ctype='function',1,0)) as fcnt
               from (
               select distinct pt.id as tag_id, pt.tag, pcc.cname, pcc.ctype
                               from project_code_construct as pcc
                               left join project_tag as pt on (pcc.project_tag_id=pt.id)
                               left join project_file as pf on (pcc.project_file_id=pf.id)
                               where pt.project_id=:pid
                               and pcc.ctype in ('function', 'method')
                               and not (%libfilter% )
               ) a
               group by a.tag
               order by a.tag_id
        ";
        $sql = str_replace('%libfilter%', $filter, $sql);
        printf("%s\n", $sql);
        $params = array('pid'=>$this->projectId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTaskName()
    {
        return 'Function and Method population';
    }

    public function onFail()
    {
        return true;
    }    
}
