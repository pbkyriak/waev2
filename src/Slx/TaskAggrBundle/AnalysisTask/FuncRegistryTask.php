<?php

namespace Slx\TaskAggrBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of FuncRegistryTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class FuncRegistryTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    
    protected $projectId;
    
    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        return true;
    }
    
    public function execute()
    {
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->updateFuncRegistry();
        $this->updateTrend();
        $this->updateFMRegistry();
        return true;
    }

    private function updateFuncRegistry() {
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:FuncRegistry a WHERE a.project=:pid ")
            ->setParameter('pid', $this->projectId)
            ->execute();
        $sql = sprintf(
                "insert into func_registry (project_id, fname, toa_id, toe_id, nb_calls, age, call_trend )  (
                select %s as project_id, func, 
                min(project_tag_id) as toa_id, 
                waev_get_next_tag(%s, max(project_tag_id)) as toe_id, 
                sum(nb_calls) as nb_calls, count(project_tag_id) as age, 0 as trend
                from project_tag_func_call_aggr 
                where project_tag_id in ( select id from project_tag where project_id=%s)
                group by func )",
                $this->projectId,
                $this->projectId,
                $this->projectId);

        $this->em->getConnection()->executeQuery($sql);
        return true;
    }

    private function buildSectionSubQuery() {
        $out = "'system'";
        $testFilter = $this->project->getTestNamesFilter('pf.fname');
        $libFilter = $this->project->getLibNamesFilter('pf.fname');
        if( $testFilter  ) {
            $out = sprintf("if( %s, 'test', %s)", $testFilter, $out);
        }
        if( $libFilter ) {
            $out = sprintf("if( %s, 'lib', %s)", $libFilter, $out);
        }

        return $out;
    }
    
    private function updateFMRegistry() {
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:FMRegistry a WHERE a.project=:pid ")
            ->setParameter('pid', $this->projectId)
            ->execute();

        $sql = sprintf(
                "insert into fm_registry (project_id, ftype, fname, toa_id, toe_id, tod_id, age, section )  (
                    select 
                        %s as project_id, 
                        pcc.ctype, 
                        pcc.cname, 
                        min(pcc.project_tag_id) as toa_id, 
                        waev_get_next_tag(%s, max(pcc.project_tag_id)) as toe_id, 
                        (select min(p.project_tag_id) from project_code_construct p left join project_tag pt on (p.project_tag_id=pt.id) where pt.project_id=%s and p.cname=pcc.cname and p.ctype=pcc.ctype and p.dead=1) as tod_id,
                        count(pcc.project_tag_id) as age,
                        pf.section as section
                    from 
                        project_code_construct pcc
                        left join project_file as pf on (pf.id=pcc.project_file_id)
                    where 
                        pcc.project_tag_id in ( select id from project_tag where project_id=%s)
                        and (pcc.ctype='method' or pcc.ctype='function')
                    group by pcc.cname, pcc.ctype
                    )",
                $this->projectId,
                $this->projectId,
                $this->projectId,
                
                $this->projectId);
        //$this->buildSectionSubQuery(),
        printf("%s\n", $sql);
        $this->em->getConnection()->executeQuery($sql);
        return true;
    }

    public function getTaskName()
    {
        return 'Update function registry';
    }

    public function onFail()
    {
        return true;
    }
    
    private function updateTrend() {
        //printf("updateTrend\n");
        $funcs = $this->getDeletedFuncs();
        $sql = "UPDATE func_registry SET call_trend=waev_get_funcCallTrend(:pid, :fname) WHERE id=:id";
        foreach($funcs as $func) {
            //printf("%s %s %s\n", $func['id'], $func['fname'], $func['toe_id']);
            $this->em->getConnection()->executeQuery($sql, array('pid'=>$this->projectId, 'fname'=>$func['fname'], 'id'=>$func['id']));
        }
        $this->em->clear();
    }
    
    private function getDeletedFuncs() {
        $sql = "select id, fname, toe_id from func_registry where project_id=:pid and not isnull(toe_id) and nb_calls!=0";
        $q = $this->em->getConnection()->prepare($sql);
        $q->bindParam('pid', $this->projectId);
        $q->execute();
        return $q->fetchAll();
    }
}
