<?php

namespace Slx\TaskAggrBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;
use PDO;
use Slx\GitMinerBundle\DataWriter\DataWriterFileUtils;

/**
 * Description of LibUsageTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class LibUsageTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    protected $projectId;

    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
        return true;
    }

    public function execute()
    {
        $out = $this->calculus($this->getData());
        $fn = DataWriterFileUtils::getInstance()->addProjectPath($this->projectId,'libusage.csv');
        //$this->writeData($out, $fn);
        $this->persist($out);
        return true;
    }

    private function calculus($data) {
        foreach($data as $k => $row) {
            printf("id=%s lib=%s sys=%s\n",$data[$k]['project_tag_id'], $data[$k]['ulibBlock'],$data[$k]['sysBlock']);
            $data[$k]['ratio'] = round($data[$k]['ulibBlock']/$data[$k]['sysBlock'],2);
        }
        return $data;
    }
    
    private function persist($out ) {
        $this->em->getConnection()->delete('project_lib_usage', array('project_id'=>$this->projectId));
        array_shift($out);  // drop first version, so it matches the rest of usage survival results
        $i=1;
        foreach($out as $row) {
            $r = array('project_id'=>$this->projectId,'idx'=>$i, 'ratio'=> $row['ratio']);
            $this->em->getConnection()->insert('project_lib_usage', $r);
            $i++;
        }
    }
    
    private function writeData($out, $fn) {
        $i=1;
        array_shift($out);  // drop first version, so it matches the rest of usage survival results
        $handle = fopen($fn, "w");
        foreach($out as $row) {
            $row['tag'] = $i;
            $r = array('id'=>$i, 'ratio'=> $row['ratio']);
            fputcsv($handle, $r, "\t");
            $i++;
        }
        fclose($handle);
    }
    
    private function getData()
    {
        $filter = $this->project->getLibNamesFilter('pf.fname');
        if( !$filter ) {
            return array();
        }
        printf("%s\n", $filter);
        /*
        $sql = "
            select 
                pcc.project_tag_id, 
                sum(if(
                not ( %libfilter% )
                ,1,0)) as sysBlock,
                sum(if(
                ( %libfilter% ) and dead=0
                ,1,0)) as ulibBlock
            from 
                project_code_construct as pcc
                left join project_tag as pt on (pcc.project_tag_id=pt.id)
                left join project_file as pf on (pcc.project_file_id=pf.id)
            where pt.project_id=:pid and pcc.ctype in ('function', 'method')
            group by pcc.project_tag_id
        ";
        $sql = str_replace('%libfilter%', $filter, $sql);
        printf("%s\n", $sql);
        */
        $sql = "
                select 
                        pcc.project_tag_id, 
                        sum(if(
                        ( pf.section='system' )
                        ,1,0)) as sysBlock,
                        sum(if(
                        ( pf.section='lib' ) and dead=0
                        ,1,0)) as ulibBlock
                from 
                        project_code_construct as pcc
                        left join project_tag as pt on (pcc.project_tag_id=pt.id)
                        left join project_file as pf on (pcc.project_file_id=pf.id)
                where pt.project_id=:pid and pcc.ctype in ('function', 'method')
                group by pcc.project_tag_id";
        $params = array('pid'=>$this->projectId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTaskName()
    {
        return 'Third party libraries usage';
    }

    public function onFail()
    {
        return true;
    }

}
