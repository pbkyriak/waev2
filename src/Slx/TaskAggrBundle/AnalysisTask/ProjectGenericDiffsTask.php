<?php

namespace Slx\TaskAggrBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of ProjectGenericDiffsTask
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectGenericDiffsTask extends AbstractTask
{
    /** @var Project */
    protected $project;
    
    protected $projectId;
    
    final public function configure($options)
    {
        $this->projectId = $options['project_id'];
        return true;
    }
    
    public function execute()
    {
        $this->em->getConnection()->executeQuery(sprintf("call updateTagDiffs(%s)", $this->projectId));
        return true;
    }

    public function getTaskName()
    {
        return 'Update project_tag_diff';
    }

    public function onFail()
    {
        return true;
    }
   
}
