<?php

namespace Slx\TaskAggrBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Slx\TaskCoreBundle\DependencyInjection\Compiler\TaskFactoryBuilderPass;

class SlxTaskAggrBundle extends Bundle
{
    
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new TaskFactoryBuilderPass());
    }

}
