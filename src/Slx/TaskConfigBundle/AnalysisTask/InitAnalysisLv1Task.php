<?php

namespace Slx\TaskConfigBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\AnalysisTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of InitAnalysisLv1
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class InitAnalysisLv1Task extends AbstractTask
{
    /** @var Project */
    private $project;
    
    public function configure($options)
    {
        if (!is_array($options)) {
            return false;
        }
        if ( !isset($options['project_id'])) {
            return false;
        }
        $projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$this->project) {   // no project entity for that id, abort mission!
            $this->isCriticalFail = true;
            return false;
        }
        return true;
    }
    
    public function execute()
    {
        $tagCnt = count($this->project->getProjectTags());
        if ($tagCnt==0) {  // no tags, abort mission!
            $this->isCriticalFail=true;
            return false;
        }
        
        foreach ($this->project->getProjectTags() as $tag) {
            $this->addTagTasks($this->project, $tag);
        }

        $this->addProjectTask($this->project);
        return true;
    }

    public function onFail()
    {
        return true;
    }

    private function addProjectTask($project) {
        $task99 = new AnalysisTask();
        $task99->setName('InitAnalysisLv2')
            ->setClass('InitAnalysisLv2')
            ->setStatus(0)
            ->setState(0)
            ->setParams(serialize(array('project_id'=>$project->getId())));
        $project->addAnalysisTask($task99);

        $this->em->persist($project);
        $this->em->flush();
    }
    
    private function addTagTasks($project, $tag)
    {
        $q = $this->em->createQuery("SELECT b FROM SlxTaskCoreBundle:ProjectAnalysisTaskTag b JOIN b.analysis a WHERE a.project=:pid AND a.active=:act ORDER BY b.position")
            ->setParameter('pid', $project->getId())
            ->setParameter('act', true);
            
        $res = $q->getResult();
//printf("count=%s\n", $res->count);->setParameter('atype', 'tag');
        foreach($res as $at) {
            $task1 = new AnalysisTask();
            $task1->setName($at->getAlias())
                ->setClass($at->getAlias())
                ->setStatus(0)
                ->setState(0)
                ->setParams(serialize(array('project_id'=>$project->getId(), 'tag_id'=>$tag->getId())));
            $project->addAnalysisTask($task1);
        }
        $this->em->persist($project);
        $this->em->flush();
    }
    
    public function getTaskName()
    {
        return 'InitAnalysis Level 1';
    }
}
