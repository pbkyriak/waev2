<?php

namespace Slx\TaskConfigBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\AnalysisTask;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of InitAnalysisLv2
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class InitAnalysisLv2Task extends AbstractTask
{
    /** @var Project */
    private $project;
    
    public function configure($options)
    {
        if (!is_array($options)) {
            return false;
        }
        if ( !isset($options['project_id'])) {
            return false;
        }
        $projectId = $options['project_id'];
        $this->project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$this->project) {   // no project entity for that id, abort mission!
            $this->isCriticalFail = true;
            return false;
        }
        return true;
    }
    
    public function execute()
    {
        $this->addTasks($this->project);
        return true;
    }

    public function onFail()
    {
        return true;
    }

    private function addTasks($project) {
        $q = $this->em->createQuery("SELECT b FROM SlxTaskCoreBundle:ProjectAnalysisTaskProject b JOIN b.analysis a WHERE a.project=:pid AND a.active=:act ORDER BY b.position")
            ->setParameter('pid', $project->getId())
            ->setParameter('act', true);
        $res = $q->getResult();
        foreach($res as $at) {
            $task1 = new AnalysisTask();
            $task1->setName($at->getAlias())
                ->setClass($at->getAlias())
                ->setStatus(0)
                ->setState(0)
                ->setParams(serialize(array('project_id'=>$project->getId())));
            $project->addAnalysisTask($task1);
        }
        $this->em->persist($project);
        $this->em->flush();
    }

    public function getTaskName()
    {
        return 'InitAnalysis Level 2';
    }
}
