<?php

namespace Slx\TaskCoreBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTask;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * AbstractTagTask is the base for all tasks that handle a single tag.
 * Configuration takes the current tagId and loads its entity.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractTagTask extends AbstractTask
{
    /** @var ProjectTag */
    protected $tag;
    protected $tagId;
    /** @var \Slx\ParserBundle\Util\SourceCodeHelper */
    protected $scHelper=null;
    
    final public function configure($options)
    {
        $this->tagId = $options['tag_id'];
        $this->tag = $this->getTagById($options['tag_id']);
        if (!$this->tag) {
            return false;
        }
        if( $this->scHelper ) {
            $this->scHelper->setProjectTag($this->tag);
        }
        return true;
    }
    
    /**
     * Called from Dependency injection container to set sourceCodeHelper
     * setuped with mongoGridFs connection
     * 
     * @param type $scHelper
     */
    public function setSourceCodeHelper($scHelper) {
        $this->scHelper = $scHelper;
    }
    
    protected function setTagStatus($status) {
        $this->tag = $this->getTagById($this->tagId);
        $this->tag->setStatus($status);
        $this->em->persist($this->tag);
        $this->em->flush();
    }
    
    protected function dropTag($tag)
    {
        /*
        if ($tag) {
            try {
                $tag->dropFiles();
                $this->em->remove($tag);
                $this->em->flush();

                return true;
            } catch (\Exception $ex) {
                return false;
            }
        }
         * 
         */
        return true;
    }
}
