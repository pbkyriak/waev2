<?php

namespace Slx\TaskCoreBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\TaskInterface;
/**
 * This is the base implementation of TaskInterface. All tasks should extend this class.
 * Basic functionality is implemented, constractor gets EntityManager.
 * GetTagById is implemented here because TagTask and DiffTagTask use it.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
abstract class AbstractTask implements TaskInterface
{
    /** @var Doctrine\ORM\EntityManager */
    protected $em;
    protected $isCriticalFail=false;
    
    public function __construct(EntityManager $em)
    {
        $this->em=$em;
        $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
    }
    
    abstract public function configure($options);

    abstract public function execute();

    abstract public function onFail();
    
    abstract public function getTaskName();

    final public function isCriticalFail() {
        return $this->isCriticalFail;
    }

    protected function getTagById($tagId) {
        return $this->em
            ->getRepository('SlxGitMinerBundle:ProjectTag')
            ->find($tagId);
    }
    
    protected function log($msg) {
        return true;
    }
}
