<?php

namespace Slx\TaskCoreBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates instances of AnalysisTasks
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class AnalysisTaskFactory
{

    /** @var ContainerInterface */
    private $container;
    private $taskDefs;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->taskDefs = array();
    }

    public function addTaskDef($serviceId, $alias, $type, $position, $default)
    {
//        printf("%s %s %s %s %s<br />",$serviceId, $alias, $type, $position, $default);
        if (!isset($this->taskDefs[$type])) {
            $this->taskDefs[$type] = array();
        }
        $this->taskDefs[$type][$alias] = array('id' => $serviceId, 'pos' => $position, 'default'=>$default);
        $this->taskDefs[$type] = $this->sortDefs($this->taskDefs[$type]);
    }

    private function sortDefs($defs) {
        uasort($defs, 
            function($a, $b) {
                if( $a['pos']==$b['pos'] ) {
                    return 0;
                }
                return ($a['pos']<$b['pos'] ? -1 : 1 );
            }
        );
        return $defs;
    }
    
    public function getTaskDefinitions($type) {
        $out = null;
        if( isset($this->taskDefs[$type]) ) {
            $out = $this->taskDefs[$type];
        }
        return $out;
    }

    public function getIdFromAlias($alias) {
        $out = $alias;
        foreach ($this->taskDefs as $typeDefs) {
            if (isset($typeDefs[$alias])) {
                $out = $typeDefs[$alias]['id'];
            }
        }
        return $out;        
    }
    /**
     * Creates an object instance of AnalysisTask classes
     *
     * @param  string $className
     * @return \Slx\GitMinerBundle\AnalysisTask\AbstractAnalysisTask
     */
    public function getObject($alias)
    {
        $out = null; 
        foreach ($this->taskDefs as $typeDefs) {
            if (isset($typeDefs[$alias])) {
                $out = $this->container->get($typeDefs[$alias]['id']);
            }
        }
        return $out;
    }

    public function getTypeFromAlias($alias) {
        $out = null;
        foreach ($this->taskDefs as $type => $typeDefs) {
            if (isset($typeDefs[$alias])) {
                $out = $type;
            }
        }
        return $out;
    }
}
