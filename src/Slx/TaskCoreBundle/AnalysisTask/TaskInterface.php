<?php

namespace Slx\TaskCoreBundle\AnalysisTask;
/**
 * The idea is that all AnalysisTasks implements TaskInterface. 
 * Configure is executed with the options supplied for the task, if configuration is succesful returns true, otherwise returns false.
 * If configurations fails, the task is not executed and runner moves to the next task.
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
interface TaskInterface
{
    /**
     * 
     * @param array
     * @return boolean
     */
    public function configure($options);
    
    public function execute();
    
    public function onFail();
    
    public function getTaskName();
    
    public function isCriticalFail();
    
}
