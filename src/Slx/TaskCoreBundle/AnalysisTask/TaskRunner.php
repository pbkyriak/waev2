<?php

namespace Slx\TaskCoreBundle\AnalysisTask;

use Doctrine\ORM\EntityManager;
use Slx\TaskCoreBundle\AnalysisTask\TaskInterface;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\AnalysisTask as TaskEntity;
/**
 * Description of TaskRunner
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class TaskRunner
{

    /** @var Slx\GitMinerBundle\Entity\Project */
    private $project;
    private $projectId;
    
    /** @var Doctrine\ORM\EntityManager */
    private $em;

    private $factory;
    
    private $iTime;
    
    public function __construct(EntityManager $em, Project $project, $factory)
    {
        $this->em = $em;
        $this->project = $project;
        $this->projectId = $project->getId();
        $this->factory = $factory;
    }

    public function run()
    {
        $this->setProjectStatus(Project::STATUS_RUNNING);
        $status = Project::STATUS_FINSHED;

        while ($task = $this->getNextTask()) {
            if( $this->isProjectCanceled() ) {
                return;
            }
            $status = $this->processTask($task);

            if ($status == Project::STATUS_CRITICAL) {  // critical failure! abort run
                break;
            }
        }
        $this->setProjectStatus($status);
    }

    private function setProjectStatus($status) {
        $this->project->setStatus($status);
        $this->em->persist($this->project);
        $this->em->flush();        
    }
    
    private function setTaskStatus(TaskEntity $task, $status, $runTime=false) {
        if( $runTime ) {
            $task->setDuration(time() - $this->iTime);
        }
        $task->setStatus($status);
        $this->em->persist($task);
        $this->em->flush();        
    }
    
    private function processTask(TaskEntity $task) {
        $status = Project::STATUS_FINSHED;
        $this->logTaskToRun($task); 
        $taskId = $task->getId();
        $obj = $this->factory->getObject($task->getClass());
        if( $obj ) {
            if( $obj->configure(unserialize($task->getParams()))) {
                $status = $this->taskRun($task, $obj);
                if ($status != Project::STATUS_FINSHED) {
                    $this->taskOnFail($obj);
                }
            }           
        }
        // reload entities
        $this->project = $this->getProject();
        $task = $this->em->getRepository('SlxGitMinerBundle:AnalysisTask')->find($taskId);
        // 
        $this->setTaskStatus($task, $status, true);
        return $status;
    }
    
    private function taskRun(TaskEntity $entity, TaskInterface $task) {
        $status = Project::STATUS_FINSHED;
        $this->setTaskStatus($entity, Project::STATUS_RUNNING);
        try {
            // caution! some tasks to $em->clear() ! all entities are detached!
            if( !$task->execute() ) {
                $status=$task->isCriticalFail() ? Project::STATUS_CRITICAL : Project::STATUS_RECOVERABLE;
            }
        } catch (Exception $ex) {
            $status = Project::STATUS_CRITICAL; // unhandled exception might be a real code issue that have to be fixed.
        }
        return $status;
    }

    private function taskOnFail(TaskInterface $task) {
        try {
            $task->onFail();
        }
        catch (\Exception $ex) {
            $this->log('Error');
        }
        
    }

    /**
     * 
     * @return TaskEntity
     */
    private function getNextTask()
    {
        $out = null;
        $results = $this->em
            ->getRepository('SlxGitMinerBundle:AnalysisTask')
            ->createQueryBuilder('ta')
            ->where('ta.project=:project')
            ->andWhere('ta.status=0')
            ->setMaxResults(1)
            ->getQuery()
            ->setParameters(array('project' => $this->project))
            ->getResult();
        if ($results) {
            $out = $results[0];
        }

        return $out;
    }

    /**
     * check if cancel run is requested
     * @return boolean
     */
    private function isProjectCanceled() {
        $out = false;
        $this->em->refresh($this->project);
        if( $this->project->getStatus()== Project::STATUS_CANCELING ) {
            $this->setProjectStatus(Project::STATUS_FINSHED);
            $this->log("project run canceled by user");
            $out = true;
        }
        return $out;
    }

    private function logTaskToRun($task) {
        $this->iTime = time();
        $this->log(
            sprintf(
                "task to run name=%s class=%s",
                $task->getName(),
                $task->getClass()
            )
        );
    }
    private function log($msg) {
        printf("%s\n", $msg);
    }
    
    /**
     * might look resource consuming to fetch all the time the entity from the db
     * but for memory issues entitymanager->clear is called in some tasks, so 
     * we are loosing the project entity. Little time to get it back is not an
     * issue to task run times.
     */
    private function getProject() {
        return $this->em->getRepository('SlxGitMinerBundle:Project')->find($this->projectId);
    }
}
