<?php

namespace Slx\TaskCoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;
use JMS\JobQueueBundle\Entity\Job;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of ProjectControlCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class ProjectControlCommand extends ContainerAwareCommand
{

    /**
     *
     * @var EntityManager
     */
    private $em;
    
    protected function configure()
    {
        $this
            ->setName('waev:project:control')
            ->setDescription("Controller of projects")
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $this->resumeIncompleteJobs();
        $this->fireupPendingProjects();
    }
    
    private function resumeIncompleteJobs() {
        $projects = $this->getProjectsWithIncompleteJob();
        printf("incomplete projects = %s\n", count($projects));
        foreach($projects as $project) {
            $this->em->getRepository('SlxGitMinerBundle:Project')->resumeProject($project);
            //$this->resumeProject($project);
        }
    }
    
    private function resumeProject($project) {
        // set last non finished task to pending
        $tasks = $this->em->getRepository('SlxGitMinerBundle:AnalysisTask')->getLastProjectTasksInRunningState($project);
        foreach($tasks as $task) {
            $task->setStatus(0);
            $this->em->persist($task);
        }
        // set project to pending, so projectControl will start a new job for it
        $project->setStatus(Project::STATUS_PENDING);
        $this->em->persist($project);
        $this->em->flush();

    }
    
    private function getProjectsWithIncompleteJob() {
        $out = array();
        $projects = $this->em->getRepository('SlxGitMinerBundle:Project')
            ->findBy(array('status'=>Project::STATUS_RUNNING));
        foreach($projects as $project) {
            $jobId = $project->getJobId();
            if( $jobId ) {
                $job = $this->em->getRepository('JMSJobQueueBundle:Job')->find($jobId);
                if( $job ) {
                    if( $job->isIncomplete() ) {
                        $out[] = $project;
                    }
                }
            }
        }
        return $out;
    }
    
    private function fireupPendingProjects() {
        $projects = $this->getPendingProjects();
        foreach($projects as $project) {
            $job = new Job('waev:run:project', array($project->getId()));
            $this->em->persist($job);
            $this->em->flush();
            $project->setJobId($job->getId());
            $this->em->persist($project);
        }
        $this->em->flush();        
    }
    
    private function getPendingProjects() {
        $projects = $this->em->getRepository('SlxGitMinerBundle:Project')
            ->createQueryBuilder('p')
            ->where('p.status=1')
            ->getQuery()
            ->getResult();
        return $projects;
    }
    
}

