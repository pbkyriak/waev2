<?php

namespace Slx\TaskCoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use Doctrine\ORM\EntityManager;

use Slx\GitMinerBundle\Entity\Project;
use Slx\TaskCoreBundle\AnalysisTask\TaskRunner;

/**
 * RunPorjectCommand runs project's tasks using AnalysisTaskRunner
 * This is added as Job by the project jobs controller command
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class RunProjectCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('waev:run:project')
            ->setDescription("Runs project's analysis tasks")
            ->addArgument('projectId', InputArgument::REQUIRED,
                'Project Id to run?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $projectId = $input->getArgument('projectId');
        $em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $project = $em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
        if (!$project) {
            throw new Exception(spinrtf('Project with id %s not found!',
                $projectId));
        }
        if( $project->getStatus()== Project::STATUS_CANCELING ) {
            $project->setStatus(Project::STATUS_FINSHED);
            $em->persist($project);
            $em->flush();
        }
        else {
            try {
                $runner = new TaskRunner($em, $project, $this->getContainer()->get('waev.task.factory'));
                $runner->run();
            } catch(\Exception $e) {
                printf("%s:Exception: %s\n", __CLASS__, $e->getMessage());
                file_put_contents('/var/www/waev/err.txt', sprintf("Trace: \n",$e->getTraceAsString()));
                die(1);
            }
        }
    }
    
}

