<?php
namespace Slx\TaskCoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of TaskRunTagCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class TaskRunProjectCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('waev:taskrun:project')
            ->setDescription("Execute an analysis task on a project")
            ->addArgument('pid',
                InputArgument::REQUIRED,
                'project Id?')
            ->addArgument('task',
                InputArgument::REQUIRED,
                'task name?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $pId = $input->getArgument('pid');
        $taskName = $input->getArgument('task');
        $project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($pId);
        if( !$project ) {
            $output->writeln(sprintf("Project with id=%s not found", $pId));
            return 1;
        }
        $factory = $this->getContainer()->get('waev.task.factory');
        $task = $factory->getObject($taskName);
        if( !$task ) {
            $output->writeln(sprintf("Task with name=%s not found", $taskName));
            return 2;
        }
        $options = array(
            'project_id' => $project->getId()
        );
        $task->configure($options);
        $output->writeln(sprintf("Executing task %s on project %s(%s)",$task->getTaskName(), $project->getPname(), $project->getId()));
        $task->execute();
        $output->writeln(sprintf("execution time: %s sec", time()-$t1));
    }
    
}
