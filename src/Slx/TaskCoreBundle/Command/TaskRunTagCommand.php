<?php
namespace Slx\TaskCoreBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Description of TaskRunTagCommand
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class TaskRunTagCommand extends ContainerAwareCommand
{
    /**
     *
     * @var EntityManager
     */
    private $em;

    protected function configure()
    {
        $this
            ->setName('waev:taskrun:tag')
            ->setDescription("Execute an analysis task on a tag")
            ->addArgument('tagid',
                InputArgument::REQUIRED,
                'tag Id?')
            ->addArgument('task',
                InputArgument::REQUIRED,
                'task name?')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $t1 = time();
        $this->em = $this->getContainer()->get('doctrine')->getEntityManager('default');
        $tagId = $input->getArgument('tagid');
        $taskName = $input->getArgument('task');
        $tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        if( !$tag ) {
            $output->writeln(sprintf("Tag with id=%s not found", $tagId));
            return 1;
        }
        $factory = $this->getContainer()->get('waev.task.factory');
        $task = $factory->getObject($taskName);
        if( !$task ) {
            $output->writeln(sprintf("Task with name=%s not found", $task));
            return 2;
        }
        $options = array(
            'project_id' => $tag->getProject()->getId(),
            'tag_id' => $tag->getId(),
        );
        $task->configure($options);
        $output->writeln(sprintf("Executing task %s on tag %s(%s)",$task->getTaskName(), $tag->getTag(), $tag->getId()));
        $task->execute();
        $output->writeln(sprintf("execution time: %s sec", time()-$t1));
    }
    
}
