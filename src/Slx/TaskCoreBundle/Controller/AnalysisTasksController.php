<?php
namespace Slx\TaskCoreBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
use Slx\TaskCoreBundle\Form\ProjectAnalysisType;
use Slx\TaskCoreBundle\Entity\ProjectAnalysis;
use Slx\GitMinerBundle\Entity\Project;

/**
 * Description of AnalysisTasksController
 *
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Projects", route="project")
 * @CurrentMenuItem("project")
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 12 Αυγ 2014
 */

class AnalysisTasksController extends Controller
{
    
    private function getProjectEntity($id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxGitMinerBundle:Project')->find($id);        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Project entity.');
        }
        $this->get("apy_breadcrumb_trail")->add($entity->getPname(), 'project_show', array('id'=>$entity->getId()));
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analyseis', 'analysis_tasks', array('pid'=>$entity->getId()));
        return $entity;
    }

    /**
     * lists records
     * 
     */
    public function indexAction($pid) {
        $project = $this->getProjectEntity($pid);
        
        $request = $this->get('request');
        $dql = "SELECT a FROM SlxTaskCoreBundle:ProjectAnalysis a WHERE a.project=:pid";
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery($dql);
        $query->setParameter('pid', $pid);
        

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1) /* page number */,
            $request->query->get('limit', 15)
        );


        return $this->render('SlxTaskCoreBundle:AnalysisTasks:index.html.twig',
                array(
                'pagination' => $pagination,
                'project' => $project,
        ));

    }
    
    /**
     * Shows records
     * 
     * @param integer $pid
     * @param integer $id
     */
    public function showAction($pid, $id) {
        $em = $this->getDoctrine()->getManager();
        $factory = $this->get('waev.task.factory');
        $entity = $em->getRepository('SlxTaskCoreBundle:ProjectAnalysis')->find($id);
        $project = $this->getProjectEntity($pid);
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analysis');
        $request = $this->get('request');
        return $this->render('SlxTaskCoreBundle:AnalysisTasks:show.html.twig',
                array(
                'entity' => $entity,
                'project' => $project,
                'factory' => $factory,
        ));

    }
       
    /**
     * new record action
     * 
     * @param integer $pid
     */
    
    public function newAction($pid) {
        
        $project = $this->getProjectEntity($pid);
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analysis');
        $entity = new ProjectAnalysis();
        $form = $this->createAddForm($entity);
        return $this->render('SlxTaskCoreBundle:AnalysisTasks:new.html.twig',
                array(
                'entity' => $entity,
                'project' => $project,
                'form' => $form->createView(),
        ));
    }
    
    /**
     * create record action (new action posts to this action)
     * 
     * @param integer $pid
     */
    public function createAction($pid) {
        $request = $this->get('request');
        $project = $this->getProjectEntity($pid);
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analysis');
        $entity = new ProjectAnalysis();
        $form = $this->createAddForm($entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setProject($project);
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('analysis_tasks_new', array('pid' => $pid)));
            else
                return $this->redirect(
                    $this->generateUrl('analysis_tasks_show',
                        array('pid' => $pid, 'id'=>$entity->getId())
                    ));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return $this->render('SlxTaskCoreBundle:AnalysisTasks:new.html.twig',
                array(
                'entity' => $entity,
                'project' => $project,
                'form' => $form->createView(),
        ));
    }
    
    /**
     * creates and setups Analysis form object
     * 
     */
    private function createAddForm($entity) {
        /* @var $factory \Slx\TaskCoreBundle\AnalysisTask\AnalysisTaskFactory */
        $factory = $this->get('waev.task.factory');
        $tagDefs = $factory->getTaskDefinitions('tag');
        $prjDefs = $factory->getTaskDefinitions('project');
        $fopts = array(
            'tagDefs'=>$tagDefs, 
            'prjDefs'=>$prjDefs, 
            'em'=>$this->getDoctrine()->getManager(),
            'analysisId' => $entity->getId(),
            );
        
        $form = $this->createForm(new ProjectAnalysisType(), $entity, $fopts);
        return $form;
    }
    
    /**
     * Edit action
     * 
     * @param integer $pid
     * @param integer $id
     */
    public function editAction($pid, $id) {
        $request = $this->get('request');
        $project = $this->getProjectEntity($pid);
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analysis');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxTaskCoreBundle:ProjectAnalysis')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectAnalysis entity.');
        }
        
        $form = $this->createAddForm($entity);

        return $this->render('SlxTaskCoreBundle:AnalysisTasks:edit.html.twig',
                array(
                'entity' => $entity,
                'project' => $project,
                'form' => $form->createView(),
        ));

    }
    
    /**
     * Update action (edit posts to this action)
     * @param integer $pid
     * @param integer $id
     */
    public function updateAction($pid, $id) {
        $request = $this->get('request');
        $project = $this->getProjectEntity($pid);
        $this->get("apy_breadcrumb_trail")->add('taskcore.analysis.analysis');
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxTaskCoreBundle:ProjectAnalysis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ColorRecipie entity.');
        }

        $form = $this->createAddForm($entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info', 'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked()) {
                return $this->redirect($this->generateUrl('analysis_tasks_new', array('pid' => $pid)));
            }
            else {
                return $this->redirect($this->generateUrl('analysis_tasks_show', array('pid' => $pid, 'id'=>$entity->getId()) ));
            }
        } else {
            $this->get('session')->getFlashBag()->add('notice', 'gsprod.general.record_errors');
        }


        return $this->render(
            'SlxGsprodBundle:ColorRecipie:edit.html.twig',
            array(
                'entity' => $entity,
                'project' => $project,
                'edit_form' => $form->createView(),
            ));
    }
    
    /**
     * delete record action
     * 
     * @param integer $pid
     * @param integer $id
     */
    public function deleteAction($pid, $id) {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxTaskCoreBundle:ProjectAnalysis')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProjectAnalysis entity.');
        }

        if( $entity->getActive() ) {
            $this->get('session')->getFlashBag()->add('notice', 'taskcore.analysis.cannot_delete_active');
        }
        else {
            $em->remove($entity);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('analysis_tasks', array('pid' => $pid)));
    }
    
    public function makeActiveAction($pid, $id) {
        
        $em = $this->getDoctrine()->getManager();
        
        $project = $this->getProjectEntity($pid);
        $analysis = $em->getRepository("SlxTaskCoreBundle:ProjectAnalysis")->find($id);
        if (!$analysis) {
            throw $this->createNotFoundException('Unable to find ProjectAnalysis entity.');
        }
        if( $project->isInNutralState() ) {
            $em->getConnection()->update('project_analysis', array('active'=>false), array('project_id'=>$pid));
            $analysis->setActive(true);
            $em->persist($analysis);
            $em->flush();
        }
        return $this->redirect($this->generateUrl('analysis_tasks', array('pid' => $pid)));
    }
}
