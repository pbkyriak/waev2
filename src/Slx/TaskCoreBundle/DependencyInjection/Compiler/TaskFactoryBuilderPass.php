<?php

namespace Slx\TaskCoreBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Description of TaskFactoryBuilderPass
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 9/8/14
 */
class TaskFactoryBuilderPass implements CompilerPassInterface
{

    public function process(ContainerBuilder $container)
    {
        if( !$container->hasDefinition('waev.task.factory') ) {
            return;
        }
        $defintion = $container->getDefinition('waev.task.factory');
        
        $taggedServiceIds = $container->findTaggedServiceIds('waev.analysis.task');
        foreach($taggedServiceIds as $serviceId => $tags) {
            foreach($tags as $tagAttributes) {
                if( isset($tagAttributes['alias']) && $tagAttributes['type']) {
                    $args = array(
                        $serviceId, 
                        $tagAttributes['alias'], 
                        $tagAttributes['type'], 
                        ( isset($tagAttributes['position']) ? $tagAttributes['position'] : 100 ),
                        ( isset($tagAttributes['default']) ? $tagAttributes['default'] : true )
                    );
                    $defintion->addMethodCall('addTaskDef', $args);
                }
            }
        }
    }

}
