<?php

namespace Slx\TaskCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectAnalysis
 */
class ProjectAnalysis
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var boolean
     */
    private $active=false;

    /**
     * @var string
     */
    private $comments;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $analysisTasksTag;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $analysisTasksProject;

    /**
     * @var \Slx\GitMinerBundle\Entity\Project
     */
    private $project;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->analysisTasksTag = new \Doctrine\Common\Collections\ArrayCollection();
        $this->analysisTasksProject = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ProjectAnalysis
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return ProjectAnalysis
     */
    public function setActive($active)
    {
        $this->active = $active;
    
        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return ProjectAnalysis
     */
    public function setComments($comments)
    {
        $this->comments = $comments;
    
        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add analysisTasks
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysisTasks $analysisTasks
     * @return ProjectAnalysis
     */
    public function addAnalysisTasksTag(\Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskTag $analysisTasks)
    {
        $analysisTasks->setAnalysis($this);
        $this->analysisTasksTag[] = $analysisTasks;
    
        return $this;
    }

    /**
     * Remove analysisTasks
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysisTasks $analysisTasks
     */
    public function removeAnalysisTasksTag(\Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskTag $analysisTasks)
    {
        $analysisTasks->setAnalysis(null);
        $this->analysisTasksTag->removeElement($analysisTasks);
    }

    /**
     * Get analysisTasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnalysisTasksTag()
    {
        return $this->analysisTasksTag;
    }

    /**
     * Add analysisTasks
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysisTasks $analysisTasks
     * @return ProjectAnalysis
     */
    public function addAnalysisTasksProject(\Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskProject $analysisTasks)
    {
        $analysisTasks->setAnalysis($this);
        $this->analysisTasksProject[] = $analysisTasks;
    
        return $this;
    }

    /**
     * Remove analysisTasks
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysisTasks $analysisTasks
     */
    public function removeAnalysisTasksProject(\Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskProject $analysisTasks)
    {
        $analysisTasks->setAnalysis(null);
        $this->analysisTasksProject->removeElement($analysisTasks);
    }

    /**
     * Get analysisTasks
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnalysisTasksProject()
    {
        return $this->analysisTasksProject;
    }
    /**
     * Set project
     *
     * @param \Slx\GitMinerBundle\Entity\Project $project
     * @return ProjectAnalysis
     */
    public function setProject(\Slx\GitMinerBundle\Entity\Project $project = null)
    {
        $this->project = $project;
    
        return $this;
    }

    /**
     * Get project
     *
     * @return \Slx\TaskCoreBundle\Entity\Project 
     */
    public function getProject()
    {
        return $this->project;
    }
}
