<?php

namespace Slx\TaskCoreBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProjectAnalysisTask
 */
abstract class ProjectAnalysisTask
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $alias;

    /**
     * @var integer
     */
    private $position;

    /**
     *
     * @var string
     */
    private $atype;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alias
     *
     * @param string $alias
     * @return ProjectAnalysisTask
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    
        return $this;
    }

    /**
     * Get alias
     *
     * @return string 
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return ProjectAnalysisTask
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return integer 
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    public function getAType() {
        return $this->atype;
    }
    
    public function setAType($at) {
        $this->atype = $at;
        return $this;
    }
}
