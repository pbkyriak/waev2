<?php

namespace Slx\TaskCoreBundle\Entity;

/**
 * Description of ProjectAnalysisTaskTag
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 13 Αυγ 2014
 */
class ProjectAnalysisTaskTag extends ProjectAnalysisTask
{
    /**
     * @var \Slx\TaskCoreBundle\Entity\ProjectAnalysis
     */
    private $analysis;

    /**
     * Set analysis
     *
     * @param \Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis
     * @return ProjectAnalysisTask
     */
    public function setAnalysis(\Slx\TaskCoreBundle\Entity\ProjectAnalysis $analysis = null)
    {
        $this->analysis = $analysis;
    
        return $this;
    }

    /**
     * Get analysis
     *
     * @return \Slx\TaskCoreBundle\Entity\ProjectAnalysis 
     */
    public function getAnalysis()
    {
        return $this->analysis;
    }

}
