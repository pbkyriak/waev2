<?php

namespace Slx\TaskCoreBundle\Form\DataTransformer;

use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Doctrine\Common\Persistence\ObjectManager;
use Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskTag;
use Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskProject;
/**
 * Description of AnalysisTaskToStringTransformer
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 13 Αυγ 2014
 */
class AnalysisTaskToStringTransformer implements DataTransformerInterface
{

    /**
     * @var ObjectManager
     */
    private $om;
    private $taskDefs;
    private $analysisId;
    private $atype;
    /**
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om, $taskDefs, $analysisId, $atype)
    {
        $this->om = $om;
        $this->taskDefs = $taskDefs;
        $this->analysisId = $analysisId;
        $this->atype = $atype;
    }

    /**
     * Transforms an object to a string.
     *
     * @param  Article|null $task
     * @return string
     */
    public function transform($tasks)
    {
        if (null === $tasks) {
            $out = array();
        }
        $out = array();
        foreach ($tasks as $task) {
            if (method_exists($task, 'getAlias')) {
                $out[] = $task->getAlias();
            }
        }
        return $out;
    }

    /**
     * Transforms a string to an object.
     *
     * @param  string $alias
     *
     * @return Article|null
     *
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($aliases)
    {
        if (!$aliases) {
            return array();
        }

        $out = array();
        foreach ($aliases as $alias) {
            $task = $this->getRepository()
                ->findOneBy(array('analysis'=>$this->analysisId, 'alias' => $alias))
            ;

            if (null === $task) {
                if (isset($this->taskDefs[$alias])) {
                    $task = $this->getEntity();
                    $task->setAlias($alias);
                    $task->setPosition($this->taskDefs[$alias]['pos']);
                } else {
                    throw new TransformationFailedException(sprintf(
                        'A task with alias "%s" does not exist!', $alias
                    ));
                }
            }
            $out[] = $task;
        }

        return $out;
    }

    private function getRepository() {
        if( $this->atype=='tag' ) {
            return $this->om->getRepository('SlxTaskCoreBundle:ProjectAnalysisTaskTag');
        }
        else {
            return $this->om->getRepository('SlxTaskCoreBundle:ProjectAnalysisTaskProject');
        }
    }
    
    private function getEntity() {
        if( $this->atype=='tag' ) {
            return new ProjectAnalysisTaskTag();
        }
        else {
            return new ProjectAnalysisTaskProject();
        }        
    }
}
