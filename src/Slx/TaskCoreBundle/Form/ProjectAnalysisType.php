<?php

namespace Slx\TaskCoreBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Slx\TaskCoreBundle\Form\DataTransformer\AnalysisTaskToStringTransformer;
/**
 * Description of ProjectAnalysisType
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 12 Αυγ 2014
 */
class ProjectAnalysisType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tagChoices = array();
        foreach($options['tagDefs'] as $tid => $ta) {
            $tagChoices[$tid]  = $ta['id'];
        }
        $prjChoices = array();
        foreach($options['prjDefs'] as $tid => $ta) {
            $prjChoices[$tid]  = $ta['id'];
        }
        $transformerTag = new AnalysisTaskToStringTransformer($options['em'], $options['tagDefs'], $options['analysisId'], 'tag');
        $transformerPrj = new AnalysisTaskToStringTransformer($options['em'], $options['prjDefs'], $options['analysisId'], 'project');
        $builder
            ->add('title', null, array('label'=>'taskcore.analysis.title'))
            ->add('comments', null, array('label'=>'taskcore.analysis.comments', 'required'=>false))
            ->add(
                $builder->create(
                    'analysisTasksTag',
                    'choice',
                    array(
                        'label'=>'taskcore.analysis.tag_tasks',
                        'expanded' => true,
                        'multiple' => true,
                        'choices'=>$tagChoices,
                        'required' => false,
                    )
                )
                ->addModelTransformer($transformerTag)
            )
            ->add(
                $builder->create(
                    'analysisTasksProject',
                    'choice',
                    array(
                        'label'=>'taskcore.analysis.prj_tasks',
                        'expanded' => true,
                        'multiple' => true,
                        'choices'=>$prjChoices,
                        'required' => false,
                    )
                )
                ->addModelTransformer($transformerPrj)
            )
            ->add('save', 'submit', array('label'=>'gsprod.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\TaskCoreBundle\Entity\ProjectAnalysis',
        ));
        $resolver
            ->setRequired(array(
                'tagDefs', 'prjDefs', 'em', 'analysisId'
            ))
            ->setAllowedTypes(array(
                'em' => 'Doctrine\Common\Persistence\ObjectManager',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'slx_taskcorebundle_project_analysis';
    }    
}
