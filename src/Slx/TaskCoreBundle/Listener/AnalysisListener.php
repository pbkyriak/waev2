<?php
namespace Slx\TaskCoreBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Slx\TaskCoreBundle\AnalysisTask\AnalysisTaskFactory;
use Slx\TaskCoreBundle\Entity\ProjectAnalysis;
use Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskTag;
use Slx\TaskCoreBundle\Entity\ProjectAnalysisTaskProject;
/**
 * Description of AnalysisListener
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 * @since 10 Αυγ 2014
 */
class AnalysisListener
{
    
    /** @var Slx\TaskCoreBundle\AnalysisTask\AnalysisTaskFactory $factory */
    private $factory;
    
    public function __construct(AnalysisTaskFactory $factory)
    {
        $this->factory = $factory;
    }
    
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if( $entity instanceof \Slx\GitMinerBundle\Entity\Project ) {
            if( !$this->hasProjectAnalysis($args)) {
                $this->createAnalysis($args);
            }
        }

    }

    private function hasProjectAnalysis(LifecycleEventArgs $args) {
        $out = false;
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $res = $em->createQuery("SELECT count(a) FROM SlxTaskCoreBundle:ProjectAnalysis a WHERE a.project=:pid")
            ->setParameter("pid", $entity->getId())
            ->getSingleScalarResult();
        if( $res ) {
            $out = true;
        }
        //$q = $em->createQuery()->
    }
    
    private function createAnalysis(LifecycleEventArgs $args) {
        $project = $args->getEntity();
        $em = $args->getEntityManager();
        $pa = new ProjectAnalysis();
        $pa->setProject($project)
            ->setTitle('Default analysis')
            ->setComments('')
            ->setActive(true);
        $this->addTagTasks($pa, $em);
        $this->addProjectTasks($pa, $em);
        $em->persist($pa);
        $em->flush();
    }
    
    private function addTagTasks(ProjectAnalysis $pa, $em) {
        $taskDefs = $this->factory->getTaskDefinitions('tag');
        foreach($taskDefs as $alias => $td) {
            if( $td['default'] ) {
                $at = new ProjectAnalysisTaskTag();
                $at->setAlias($alias)
                    ->setPosition($td['pos'])
                    ->setAnalysis($pa);
                $pa->addAnalysisTasksTag($at);
                $em->persist($at);
            }
        }
        return $pa;
    }
    
    private function addProjectTasks(ProjectAnalysis $pa, $em) {
        $taskDefs = $this->factory->getTaskDefinitions('project');
        foreach($taskDefs as $alias => $td) {
            if( $td['default'] ) {
                $at = new ProjectAnalysisTaskProject();
                $at->setAlias($alias)
                    ->setPosition($td['pos'])
                    ->setAnalysis($pa);
                $pa->addAnalysisTasksProject($at);
                $em->persist($at);
            }
        }
        return $pa;
    }
}
