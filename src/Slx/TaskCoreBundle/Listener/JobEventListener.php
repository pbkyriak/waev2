<?php

namespace Slx\TaskCoreBundle\Listener;

use JMS\JobQueueBundle\Event\StateChangeEvent;
use JMS\JobQueueBundle\Entity\Job;
use Doctrine\ORM\EntityManager;
use Slx\GitMinerBundle\Entity\Project;
/**
 * Description of JobEventListener
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class JobEventListener
{

    /**
     *
     * @var Doctrine\ORM\EntityManager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function onJobStateChange(StateChangeEvent $event)
    {
        $state = $event->getNewState();
        $jobId = $event->getJob()->getId();
        if( $event->getJob()->getCommand()=='waev:run:project' && in_array($state, array(Job::STATE_CANCELED, Job::STATE_FAILED, Job::STATE_TERMINATED)) ) {
            $projectId = (int)str_replace(array('[',']'),'', $event->getJob()->getArgs());
            $project = $this->em->getRepository('SlxGitMinerBundle:Project')->find($projectId);
            if( $project ) {
                $project->setStatus(Project::STATUS_CRITICAL);
                $this->em->persist($project);
                $this->em->flush();
            }
        }

    }

}

