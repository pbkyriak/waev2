<?php
namespace Slx\TaskTagBundle\Analyser;

/**
 * Description of FileSectionGuesser
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileSectionGuesser {

    private $libRegExps;
    private $testRegExps;
    private $exclString;
    private $sourceRoot;
    
    public function config($libNames, $testNames, $exclString, $sourceRoot) {
        $this->libRegExps = $this->namesToRegExp($libNames);
        $this->testRegExps = $this->namesToRegExp($testNames);
        $this->exclString = trim($exclString);
        $this->sourceRoot = $sourceRoot;
        return $this;
    }
    
    private function namesToRegExp($names) {
        $regexps = array();
        if( !empty($names) ) {
            $namesAr = explode("\n", str_replace("\r", "", $names));
            foreach($namesAr as $name) {
                $regexps[] = '/^'.str_replace(array('%', '/'), array('\S*', '\/'), $name).'/';
            }
        }
        return $regexps;
    }
    
    private function isNameMatching($regExps, $fname) {
        $out = false;
        if( $regExps ) {
            foreach($regExps as $regExp) {
                if(preg_match($regExp, $fname)) {
                    $out = true;
                    break;
                }
            }
        }
        return $out;
    }
    
    private function fileContainsString($fname, $testString) {
        $out = false;
        if( empty($testString) ) {
            return $out;
        }
        $contents = file_get_contents($this->sourceRoot.$fname);
        if( strpos($contents, $testString) ) {
            $out = true;
        }
        return $out;
    }
    
    public function guessSection($fname) {
        
        $out = 'system';
        if( $this->isNameMatching($this->testRegExps, $fname) ) {
            $out = 'test';
        }
        else {
            if( $this->isNameMatching($this->libRegExps, $fname) && !$this->fileContainsString($fname, $this->exclString) ) {
                $out = 'lib';
            }
        }
        //printf("fname=%s section=%s\n", $fname, $out);
        return $out;
    }
} 
