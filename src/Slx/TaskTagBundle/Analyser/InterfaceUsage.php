<?php

namespace Slx\TaskTagBundle\Analyser;

use Doctrine\ORM\EntityManager;
/**
 * Description of InterfaceUsage
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class InterfaceUsage {
    /** @var Doctrine\ORM\EntityManager */
    private $em;
    private $counts;
    private $platform;
    public function __construct($em) {
        $this->em = $em;
        $counts = array();
        $this->platform = array(
            '\\Traversable',
            '\\Iterator',
            '\\IteratorAggregate',
            '\\Throwable',
            '\\ArrayAccess',
            '\\Serializable',
            '\\Countable',
            '\\OuterIterator',
            '\\RecursiveIterator',
            '\\SeekableIterator',
            '\\SplObserver',
            '\\SplSubject',
        );
    }
    
    public function countTag($tagId) {
        $sql = "select distinct pc.modifiers, if(pc.namespace!='', concat('\\\\',pc.namespace,'\\\\',pc.fname),concat('\\\\',pc.fname)) as fqn, pc.extends, pci.fname as interface
                from project_class pc 
                left join project_class_interface as pci on (pci.project_class_id=pc.id)
                where pc.project_tag_id=:tid
                order by fqn
                ";
        $params = array('tid'=>$tagId);
        $stmt = $this->em
            ->getConnection()
            ->executeQuery($sql, $params);
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $interfaces = $this->collectInterfaces($data);
        foreach($interfaces as $interface) {
            if( !in_array($interface, $this->platform)) {
                $this->counts[$interface] = array('impl'=>0, 'sub'=>0);
                $this->countInterfaceImplementations($data,$interface);
            }
        }
        $totals = array(
            'intf' => 0,
            'impl' => 0,
            'sub'   => 0,
        );
        if($this->counts) {
            foreach($this->counts as $key => $row) {
                $totals['intf']++;
                $totals['impl'] += $row['impl'];
                $totals['sub'] += $row['sub'];
            }
        }
        printf("%s\t%s\t%s\t%s\n",$tagId, $totals['intf'], $totals['impl'], $totals['sub']);
    }
    
    private function collectInterfaces($data) {
        $out = array();
        foreach($data as $row) {
            if($row['interface']) {
                $out[] = $row['interface'];
            }
        }
        $out = array_unique($out);
        return $out;
    }
    
    private function countInterfaceImplementations($data, $interface) {
        $direct = array_filter($data, function($var) use ($interface){ return ($var['interface']==$interface) ? true : false;});
        $this->counts[$interface]['impl'] = count($direct);
        foreach($direct as $row) {
            $this->countSubClasses($interface, $data, $row['fqn']);
        }
    }
    
    private function countSubClasses($interface, $data, $fqn) {
        $cnt = 0;
        foreach($data as $row) {
            if( $row['extends']==$fqn) {
                $cnt++;
                $this->countSubClasses($interface, $data, $row['fqn']);
            }
        }
        $this->counts[$interface]['sub'] += $cnt;
    }
}
