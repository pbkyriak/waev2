<?php

namespace Slx\TaskTagBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Removes from filenames the local path
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DropSourceTask extends AbstractTagTask
{

    public function getTaskName()
    {
        return 'Drop source code, global code and ast folder';
    }
    public function execute()
    {

        try {
            $this->dropSourceCode($this->tag);
            $this->dropGlobalSourceCode($this->tag);
            $this->dropAST($this->tag);
        } catch (\Exception $ex) {
            // some logging here
            $this->isCriticalFail = false;

            return false;
        }

        return true;
    }

    public function onFail()
    {
        return true;
    }

    private function dropSourceCode(ProjectTag $tag) {
        $command = sprintf("rm -rf %s", $tag->getSourceCodePath());
        exec($command);
    }

    private function dropGlobalSourceCode(ProjectTag $tag) {
        $command = sprintf("rm -rf %s", $tag->getGlobalCodeFolder());
        exec($command);
    }

    private function dropAST(ProjectTag $tag) {
        $command = sprintf("rm -rf %s/ast", $tag->getTagWorkingFolder());
        exec($command);
    }

}
