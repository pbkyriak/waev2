<?php
namespace Slx\TaskTagBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;

/**
 * This is a dummy task. Does nothing at all.
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class DummyTask extends AbstractTagTask
{

    public function getTaskName()
    {
        return 'A dummy task';
    }
    
    public function execute()
    {
        printf("running dummy task\n" );
        return true;
    }

    public function onFail()
    {
        printf("dummy task onFail\n");

        return true;
    }
}
