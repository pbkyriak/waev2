<?php

namespace Slx\TaskTagBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\ReflectionBundle\Analyser\SourceCodeHelper;
use Slx\TaskTagBundle\Analyser\FileSectionGuesser;

/**
 * Description of FileFinderTask
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileCollectorTask extends AbstractTagTask {
    
    private $fileCnt;
    private $fsGuesser;
    protected $scHelper=null;
    
    public function setSourceCodeHelper($scHelper) {
        $this->scHelper = $scHelper;
    }

    
    public function execute() {
        $mem1 = (memory_get_usage() / 1024);
        try {
            $this->em->getConnection()->getConfiguration()->setSQLLogger(null);
            //$this->em->getConnection()->query("SET autocommit=0;"); 
            $this->cleanExistingData($this->tag->getId());
            $this->scHelper->setProjectTag($this->tag);
            $this->setupFileSectionGuesser();
            $files = $this->scHelper->getTagSourceFiles();
            foreach ($files as $file) {
                $this->processFile($file);
            }
            $files = null;
            $this->setTagStatus(ProjectTag::STATUS_FC_DONE);
            //$this->em->getConnection()->query("COMMIT;");
        } catch (\Exception $ex) {
            printf("Error: %s\n", $ex->getMessage());
            //$this->em->getConnection()->query("ROLLBACK;");
            $this->isCriticalFail = false;
            return false;
        }
        printf("Files collected: %s\n", $this->fileCnt);
        $this->em->getConnection()->query("SET autocommit=1;"); 
        gc_collect_cycles();
        time_nanosleep(0, 10000000);

        $mem2 = (memory_get_usage() / 1024);
        echo "Memory usage: " . $mem1 . ", " . $mem2 . PHP_EOL;
        return true;

    }

    public function onFail()
    {
        return true;
    }

    public function getTaskName()
    {
        return 'Collect file names';
    }

    private function cleanExistingData($tagId) {
        // cleanup prev data import for this tag
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectFile a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $this->tagId)
            ->execute();
    }
    
    private function setupFileSectionGuesser() {
        $this->fsGuesser = new FileSectionGuesser();
        $this->fsGuesser->config(
                $this->tag->getProject()->getLibraries(), 
                $this->tag->getProject()->getTests(), 
                $this->tag->getProject()->getLibExclString(), 
                $this->tag->getSourceCodePath()
                );
    }
    
    private function processFile($file) {
        $comm = $this->tag->getSourceCodePath();
        $sfile = str_replace($comm, '', $file);
        $data = array(
            'project_tag_id' => $this->tagId,
            'fname' => $sfile,
            'sha1_hash' => sha1_file($file),
            'dir_depth' => substr_count($sfile,'/'),
            'cloc'=>0, 
            'eloc'=>0, 
            'lloc'=>0, 
            'loc'=>0, 
            'ncloc'=>0,
            'llocGlobal'=>0,
            'ccnGlobal' => 0,
            'ccn2Global' => 0,
            'clocGlobal' => 0,
            'parser' => 'good',
            'section' => $this->fsGuesser->guessSection($sfile),
        );
        $this->em->getConnection()->insert('project_file', $data);
        $fileId = $this->em->getConnection()->lastInsertId();
        $this->fileCnt++;
    }
}
