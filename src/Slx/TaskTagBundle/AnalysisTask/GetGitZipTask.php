<?php

namespace Slx\TaskTagBundle\AnalysisTask;

use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
use Slx\GitMinerBundle\Entity\ProjectTag;

/**
 * Downloads zipped source code for a tag. If something goes wrong it drops the tag. Non critical to abort run
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class GetGitZipTask extends AbstractTagTask
{

    public function execute()
    {
        if ( $this->tag->downloadZipBall() ) {
            $this->setTagStatus(ProjectTag::STATUS_SRC_DOWNLOADED);
        } else {
            // some logging here
            $this->isCriticalFail = false;
            return false;
        }
        return true;
    }

    public function onFail()
    {
        return $this->dropTag($this->tag);
    }
    
    public function getTaskName()
    {
        return 'Get Zip from Github';
    }
}
