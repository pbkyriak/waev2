<?php

namespace Slx\TaskTagBundle\AnalysisTask;

use Slx\GitMinerBundle\Entity\ProjectTag;
use Slx\TaskCoreBundle\AnalysisTask\AbstractTagTask;
/**
 * Removes from filenames the local path
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class MakeDirTreeTask extends AbstractTagTask
{

    public function execute()
    {
        try {
            $this->cleanExistingData($this->tag->getId());
            $this->makeDirectoryTree($this->tag->getId());
            $this->setTagStatus(ProjectTag::STATUS_DIRTREE_DONE);
        } catch (\Exception $ex) {
            $this->isCriticalFail = false;
            return false;
        }
        return true;
    }

    public function onFail()
    {
        return true;
    }

    /**
     * cleanup prev data import for this tag
     * @param int $tagId
     */
    private function cleanExistingData($tagId) {
        $this->em->createQuery("DELETE FROM SlxGitMinerBundle:ProjectDir a WHERE a.projectTag=:tagid ")
            ->setParameter('tagid', $tagId)
            ->execute();
        $this->em->flush();        
    }
    
    /**
     * 
     * @param int $tagId
     */
    private function makeDirectoryTree($tagId)
    {
        $tree = array();
        $tag = $this->em->getRepository('SlxGitMinerBundle:ProjectTag')->find($tagId);
        $files = $tag->getProjectFiles();
        foreach ($files as $file) {
            $dirs = $this->getDirs($file->getFname());
            foreach ($dirs as $dir) {
                if (!in_array($dir, $tree)) {
                    $tree[] = $dir;
                }
            }
        }
        $this->saveTree($tag, $tree);
    }

    private function getDirs($fname)
    {
        $out = array();
        $dd = explode('/', dirname($fname));
        $d = array_shift($dd);
        if ($d) {
            $out[] = $d;
        }
        foreach ($dd as $t) {
            $d = $d . '/' . $t;
            $out[] = $d;
        }

        return $out;
    }

    private function saveTree($tag, $tree)
    {
        foreach ($tree as $dir) {
            $data = array(
                'project_tag_id' => $tag->getId(),
                'fname' => $dir,
                'dir_depth' => substr_count($dir, '/'),
            );
            $this->em->getConnection()->insert('project_dir', $data);
        }
    }

    public function getTaskName()
    {
        return 'Make dir tree';
    }
}
