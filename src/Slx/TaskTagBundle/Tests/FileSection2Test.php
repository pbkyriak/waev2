<?php
namespace Slx\TaskTagBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\TaskTagBundle\Analyser\FileSectionGuesser;
use Symfony\Component\Finder\Finder;
/**
 * Description of FileSectionTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileSection2Test extends WebTestCase {
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }
    
    public function testFile1() {
        $testNames = <<<EOT
%/tests/%
/tests/%
EOT;
        $libNames  = <<<EOT
/libraries/%
EOT;
        $exclString = "Open Source Matters";
        $codePath = '/home/panos/joomla-cms-3.0.0/';
        $fg = new FileSectionGuesser();
        $fg->config($libNames, $testNames, $exclString,$codePath);
        $files = $this->getTagSourceFiles();
        foreach($files as $fname) {
            $fname = str_replace($codePath, '', $fname);
            printf("%s | %s\n", $fname, $fg->guessSection($fname));
        }
    }
    
    public function getTagSourceFiles() {
        $codePath = '/home/panos/joomla-cms-3.0.0/libraries/joomla/client';
        $exts = explode(',','php');
        if( !$exts ) {
            $exts = array('php');
        }
        $finder = new Finder();
        $finder->in($codePath);
        foreach($exts as $ext) {
            $finder->name(sprintf('*.%s', $ext));
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRealpath();
        }

        return $files;
    }

    
    public function testExlude() {
        $tests = <<<EOT
%/tests/%
/tests/%
EOT;
        printf("%s\n", $this->getExcludes($tests));
    }
    
    private function getExcludes($tests) {
        //$tests = $this->tag->getProject()->getTests();
        $namesAr = explode("\n", str_replace("\r", "", $tests));
        $out = '';
        foreach($namesAr as $name) {
            $name = preg_replace(array('/^\//', '/\/%$/', '/^%\//'),'', $name);
            $out .= ' --exclude '.$name;
        }
        return $out;
    }

}
