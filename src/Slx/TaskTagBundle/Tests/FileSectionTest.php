<?php
namespace Slx\TaskTagBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Slx\TaskTagBundle\Analyser\FileSectionGuesser;
use Symfony\Component\Finder\Finder;
/**
 * Description of FileSectionTest
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 */
class FileSectionTest extends WebTestCase {
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;
    
    public function setUp()
    {
        static::$kernel = static::createKernel();
        static::$kernel->boot();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }
    
    public function testFile1() {
        $testNames = <<<EOT
%/tests/%
/tests/%
EOT;
        $libNames  = <<<EOT
/lib/%
EOT;
        $exclString = "file is part of Moodle";
        $codePath = '/home/panos/moodle/moodle-2.5.9';
        $fg = new FileSectionGuesser();
        $fg->config($libNames, $testNames, $exclString,$codePath);
        $files = $this->getTagSourceFiles();
        foreach($files as $fname) {
            $fname = str_replace($codePath, '', $fname);
            printf("%s | %s\n", $fname, $fg->guessSection($fname));
        }
    }
    
    public function getTagSourceFiles() {
        $codePath = '/home/panos/moodle/moodle-2.5.9/lib';
        $exts = explode(',','php');
        if( !$exts ) {
            $exts = array('php');
        }
        $finder = new Finder();
        $finder->in($codePath);
        foreach($exts as $ext) {
            $finder->name(sprintf('*.%s', $ext));
        }
        $files = array();
        foreach ($finder as $file) {
            $files[] = $file->getRealpath();
        }

        return $files;
    }

}
