<?php

namespace Slx\TypeAnalysisBundle\Analyser;

/**
 * Description of ContextVarsReturnsCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class ContextVarsReturnsCounter {
    
    private $contextVars;
    private $newMetrics = array();
    private $tagId, $metricGroupId;
    
    public function __construct($tagId, $metricGroupId) {
        $this->tagId = $tagId;
        $this->metricGroupId = $metricGroupId;
    }
    
    public function setVars($vars) {
        foreach($vars as $k => $v) {
            if( $v['type']=='Class' ) {
                $this->contextVars[$k] = $v;
            }
        }
    }
    
    public function getNewMetrics() {
        return $this->newMetrics;
    }
    
    private function loadTestData() {
        $this->contextVars = array(
            'c' => ['extends'=>'f', 'type'=>'Class', 'file'=>'f_cd', 'vars'=> ['aa'=>['color'=>'return', 'method'=>'maa'],'bb'=>['color'=>'return', 'method'=>'mbb']]],
            'b' => ['extends'=>'c', 'type'=>'Class', 'file'=>'f_ab', 'vars'=> ['aa'=>['color'=>'newobj', 'pattern'=>'OI:R:Expr_Variable'],'bb'=>['color'=>'newobj', 'pattern'=>'OI:R:Expr_Variable']]],
            'a' => ['extends'=>'b', 'type'=>'Class', 'file'=>'f_ab', 'vars'=> ['aa'=>['color'=>'return', 'method'=>'maa'],'bb'=>['color'=>'return', 'method'=>'mbb']]],
            'd' => ['extends'=>'',  'type'=>'Class', 'file'=>'f_cd', 'vars'=> ['aa'=>['color'=>'newobj', 'pattern'=>'OI:R:Expr_Variable'],'bb'=>['color'=>'newobj', 'pattern'=>'OI:R:Expr_Variable']]],
            'e' => ['extends'=>'d', 'type'=>'Class', 'file'=>'f_e', 'vars'=> ['aa'=>['color'=>'return', 'method'=>'maa'],'bb'=>['color'=>'return', 'method'=>'mbb']]],
        );        
    }
    
    public function count() {
        //$this->loadTestData();
        //print_R($this->contextVars);
        $visited = array();
        $a = array_keys($this->contextVars);
        $c = $this->getNonExtended($a);
        while(count($c)) {
            foreach($c as $c1) {
                $this->countForClass($c1);
                $visited[] = $c1;
            }
            $a = array_diff($a, $visited);
            $c = $this->getNonExtended($a);
        }
        // aggregate metrics per file
        $this->aggregateMetricsPerFile();
        printf("visited %s of %s\n", count($visited),count($this->contextVars));
    }

    private function getNonExtended($toTest) {
        $b = array();
        foreach($toTest as $cn ) {
            if( $this->contextVars[$cn]['extends'] ) {
                $b[] = $this->contextVars[$cn]['extends'];
            }
        }
        $out = array_diff($toTest, $b);
        return $out;
    }
    
    private function countForClass($inClass) {
        $testClass = $inClass;
        $testVars = $this->contextVars[$testClass]['vars'];
        while($testClass) {
            if( isset($this->contextVars[$this->contextVars[$testClass]['extends']]) ) {
                $this->matchVars($testVars, $this->contextVars[$this->contextVars[$testClass]['extends']]['vars']);
                $testClass = $this->contextVars[$testClass]['extends'];
            }
            else {
                $testClass = '';
            }
        }
        // metrics per class
        $mm = $this->collectMetrics($inClass, $testVars);
        
        if( count($mm) ) {
            $this->newMetrics = array_merge($this->newMetrics,$mm);
        }
    }
    
    private function matchVars(&$src, $trg) {
        foreach($src as $sVar => $sAttrs) {
            if( isset($trg[$sVar]) && $sAttrs['color']!='black' ) {
                if( $src[$sVar]['color']=='return' && $trg[$sVar]['color']=='newobj' ) {
                    $src[$sVar]['pattern']=$trg[$sVar]['pattern'];
                    $src[$sVar]['color'] = 'black';
                }
                elseif( $src[$sVar]['color']=='newobj' && $trg[$sVar]['color']=='return' ) {
                    $src[$sVar]['method']=$trg[$sVar]['method'];
                    $src[$sVar]['color'] = 'black';
                }     
            }
        }
    }
    
    private function collectMetrics($inClass, $testVars) {
        $out = array();
        foreach($testVars as $tVar => $tAttrs) {
            if( $tAttrs['color']=='black' ) {
                if( isset($out[$tAttrs['pattern']])) {
                    $out[$tAttrs['pattern']]['metric']++;
                }
                else {
                $out[$tAttrs['pattern']] = array(
                    'project_tag_id' => $this->tagId,
                    'ctype' => 'class',
                    'cname' => $inClass,
                    'metric_name' => $tAttrs['pattern'],
                    'metric' => 1,
                    'metric_group_id' => $this->metricGroupId,
                );
                }
            }
        }
        return array_values($out);
    }
    
    private function aggregateMetricsPerFile() {
        $out = array();
        foreach($this->newMetrics as $cMetric) {
            $tC = $cMetric['cname']; // class
            $tF = $this->contextVars[$tC]['file'];
            $tMn = $cMetric['metric_name'];
            $found=false;
            for($i=0; $i<count($out); $i++) {
                if( $out[$i]['cname']==$tF && $out[$i]['metric_name']==$tMn ) {
                    $out[$i]['metric'] += $cMetric['metric'];
                    $found = true;
                    break;
                }
            }
            if(!$found) {
                $out[] = array(
                    'project_tag_id' => $this->tagId,
                    'ctype' => 'file',
                    'cname' => $tF,
                    'metric_name' => $tMn,
                    'metric' => $cMetric['metric'],
                    'metric_group_id' => $this->metricGroupId,
                );
            }
        }
        $this->newMetrics = array_merge($this->newMetrics,$out);
    }
}

