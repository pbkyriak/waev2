<?php

namespace Slx\TypeAnalysisBundle\Analyser;

use Doctrine\ORM\EntityManager;

/**
 * Description of ContextVarsWriter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class ContextVarsWriter {
    
    /** @var Doctrine\ORM\EntityManager */
    protected $em;
    
    public function __construct($em) {
        $this->em = $em;
    }
    
    public function writeVars($varsInfo) {
        foreach($varsInfo as $classfqn => $vars) {
            printf("class %s\n", $classfqn);
        }
    }
}
