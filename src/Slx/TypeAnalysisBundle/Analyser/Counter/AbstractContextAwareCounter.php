<?php

namespace Slx\TypeAnalysisBundle\Analyser\Counter;

use Slx\TypeAnalysisBundle\CodeContext\AbstractCodeContextVisitor;
use \PHPParser_Node;
use Slx\MetricsBundle\Analyser\FileMetric;
/**
 * Description of AbstractContextAwareCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 27 Ιαν 2016
 */
abstract class AbstractContextAwareCounter extends AbstractCodeContextVisitor {
    
    protected $metricContext = null;
    protected $metrics;
    protected $namespace = '';
    protected $metricGroupId = 0;
    protected $updateMembersMembers = false;
    protected $alias = '';
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        $this->metrics = $fm;
        $this->metricContext = $this->metrics;  
        $this->configureVisitor($fm->getName(), $noEventOnContextStms);
    }
    
    public function setUpdateMembersMembers($v) {
        $this->updateMembersMembers = $v;
    }
    public function setAlias($v) {
        $this->alias = $v;
    }
    public function getMetrics() {
        return $this->metrics;
    }

    protected function cContextDown(\PHPParser_Node $node) {
        if($this->metricContext->getParent()) {
            $this->metricContext = $this->metricContext->getParent();
        }        
        $this->ccContextDown($node);
    }

    protected function cContextUp(\PHPParser_Node $node) {
        $ctype = strtolower($this->getContext()->getType());
        $cname = $this->getContext()->getFQName();
        $m = new FileMetric( $ctype, $cname);
        $this->metricContext = $this->metricContext->addMember($cname, $m);
        $this->ccContextUp($node);
    }

    public function setMetricGroupId($v) {
        $this->metricGroupId = $v;
        return $this;
    }
    
    public function getMetricGroupId() {
        return $this->metricGroupId;
    }
    
    protected function incMetric($metric, $v = 1, $updGlobal=true) {
        if( $this->metricContext->getParent() ) {
            $this->metricContext->incMetric($metric, $v, true,$this->alias);
        }
        else {
            $metric2 = $metric . ($updGlobal ? 'global' : '');
            $this->metricContext->incMetric($metric2, $v, true,$this->alias);
        }
    }
    
    protected function cEnterNode(PHPParser_Node $node) {
        $this->ccEnterNode($node);
    }
    
    protected function cLeaveNode(PHPParser_Node $node) {
        $this->ccLeaveNode($node);
    }

    abstract protected function ccEnterNode(PHPParser_Node $node);
    abstract protected function ccLeaveNode(PHPParser_Node $node);
    abstract protected function ccContextUp(PHPParser_Node $node);
    abstract protected function ccContextDown(PHPParser_Node $node);

}
