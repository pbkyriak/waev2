<?php

namespace Slx\TypeAnalysisBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;
use Slx\TypeAnalysisBundle\Analyser\NodeInfoExtractors;
use Slx\TypeAnalysisBundle\Analyser\SelfNames;
/**
 * Description of ObjNewRetCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class ObjNewRetCounter  extends AbstractContextAwareCounter {
    
    private $extractors;
    private $wantedNewObjPatterns;
    private $pattenPrefix = 'OI:R';

    private $handle;
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm);
        $this->extractors = new NodeInfoExtractors();
        
        $this->wantedNewObjPatterns = array(
            ParserNodeTypes::NodeExprArrayDimFetch,
            ParserNodeTypes::NodeExprStaticPropertyFetch,
            ParserNodeTypes::NodeExprPropertyFetch,
            ParserNodeTypes::NodeExprVariable,
        );
        array_walk($this->wantedNewObjPatterns, function(&$item) { $item = $this->pattenPrefix.':'.$item;});
        $this->handle = fopen("/var/www/waev/web/uploads/project-data/new-ret-meth-fd.txt", "a");
        $this->metricGroupId = 7;
        
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter: %s line:%s\n", $node->getType(), $node->getLine());
        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprNew:      
                $this->visitExprNew($node);
                break;
            case ParserNodeTypes::NodeStmtReturn:
                $this->visitStmtReturn($node);
                break;
        }
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
    }

    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

    /**
     * 
     * @param \PHPParser_Node $node
     */
    private function visitExprNew($node) {
        $ln = $this->lastNode->getLastNode();
        $newObjPattern = $this->getNewObjPattern($this->pattenPrefix, $node->class);
        if( in_array($newObjPattern, $this->wantedNewObjPatterns)) {    // it is one of the wanted new object patters
            // check if the instance is assigned to a variable
            if( $ln ) { // there is a last node
                $nodeType = $this->lastNode->mergeSimilar($ln->getType());
                if( $nodeType==ParserNodeTypes::NodeExprAssign ) {  // it is assigned to a variable
                    $varName = $this->extractors->extractVarName($ln->var);
                    if(!SelfNames::isSelfName($varName)) { // local variable
                        if($this->checkContextNewVariable($this->getContext(), $varName, ['pattern'=>$newObjPattern])) { // the variable was returned
                            $this->logPatternMethod($this->getContext()->getFQName());
                            $this->incMetric($newObjPattern, 1, false);
                            $this->getContext()->setVariableAttr($varName, 'color', 'black'); // any value but newobj and return
                        }
                    }
                    else {  // property
                        $propName = $this->extractors->extractPropertyName($ln->var);
                        if( $propName ) {
                            if($this->getContext()->getParent()) {
                                if( $this->checkContextNewVariable($this->getContext()->getParent(), $propName, ['pattern'=>$newObjPattern]) ) {
                                    $varrAttrs = $this->getContext()->getParent()->getVariable($propName);
                                    $this->logPatternMethod($newObjPattern, $varrAttrs['method']);
                                    $this->incMetric($newObjPattern, 1, false);
                                    $this->getContext()->getParent()->setVariableAttr($propName, 'color', 'black'); // any value but newobj and return
                                }
                            }
                        }
                    }
                }                
            }
        }
    }
    
    private function getNewObjPattern($key,$node) {
        $var = $this->extractors->getMethodNameType($node);
        //return str_replace('PHPParser_Node_','',sprintf("%s:%s",$key,$var));
        return sprintf("%s:%s",$key,$var);
    }
    
    /**
     * If the variable exists in the context and has color=return means that 
     * sometime a return statement for that variable was encountered
     * else add the variable to the context
     * Returns true if the variable was returned sometime, false in anyother case
     * 
     * @param type $context
     * @param string $varName
     * @param array $newVarAttrs    Any attributes to add to the new variable if added
     * @return boolean
     */
    private function checkContextNewVariable($context, $varName, $newVarAttrs = null) {
        $out = false;
        if( $context->hasVariable($varName) ) {
            $varAttrs = $context->getVariable($varName);
            if( $varAttrs['color']=='return' ) {    // a return for this variable already found 
                $out = true;
            }
        }
        else {
            $context->addVariable($varName,'newobj');
            if( is_array($newVarAttrs) ) {
                foreach($newVarAttrs as $k => $v) {
                    $context->setVariableAttr($varName, $k,$v);
                }
            }
        }
        return $out;
    }
        
    private function visitStmtReturn($node) { 
        if( $node->expr ) {
            // CHECK local variables
            if( $node->expr->getType()== ParserNodeTypes::NodeExprVariable ) {
                $this->visitStmtReturnVariable($node);
            }
            // check object properties
            elseif( $node->expr->getType()== ParserNodeTypes::NodeExprPropertyFetch ) {
                $this->visitStmtReturnOProperty($node);
            }
            // check current static properties
            elseif( $node->expr->getType()== ParserNodeTypes::NodeExprStaticPropertyFetch ) {
                $this->visitStmtReturnOProperty($node);
            }
        }        
    }
    
    /**
     * Local variable return
     * In local variables we dont check color 
     * @param type $node
     */
    private function visitStmtReturnVariable($node) {
        $varName = $this->extractors->extractVarName($node->expr);
        if( $this->getContext()->hasVariable($varName)) {
            $var = $this->getContext()->getVariable($varName);
            $this->incMetric($var['pattern'], 1, false);
            $this->getContext()->setVariableAttr($varName, 'color', 'black'); // any value but newobj and return
        }        
    } 
    
    /**
     * Property return
     * 
     * @param type $node
     */
    private function visitStmtReturnOProperty($node) {
        $propName = $this->extractors->extractPropertyName($node->expr);
        if( $propName && $this->getContext()->getParent()) {
            if( $this->getContext()->getParent()->hasVariable($propName) ) {
                $var = $this->getContext()->getParent()->getVariable($propName);
                if( $var['color']=='newobj' ) {
                    $this->incMetric($var['pattern'], 1, false);
                    $this->logPatternMethod($var['pattern'], $this->getContext()->getFQName());
                    $this->getContext()->getParent()->setVariableAttr($propName, 'color', 'black'); // any value but newobj and return
                }
            }
            else {  // add variable with color=return
                $this->getContext()->getParent()->addVariable($propName,'return');
                $this->getContext()->getParent()->setVariableAttr($propName, 'method', $this->getContext()->getFQName());
            }
        }        
    } 

    private function logPatternMethod($pattern, $method) {
        //printf("Return %s from : %s\n", $pattern, $method);
        if( $this->handle ) {
            fputcsv($this->handle, [$pattern, $method]);
        } 
    }
    
    /**
     * Returns only class properties info
     * 
     * @return array
     */
    public function getPendingProperties() {
        $out = array();
        $co = $this->getRootContext();
        /*
        $tmp = $this->dumpContextVars($co);
        if( $tmp ) {
            $out[$co->getFQName()] = array('vars'=>$tmp, 'type'=>$co->getType());
        }
         * 
         */
        foreach( $co->getMembers() as $co1) {
            if( $co1->getType()=='Class' ) {  // we want only classes
                $tmp = $this->dumpContextVars($co1);
                if( $tmp ) {
                    $out[$co1->getFQName()] = array('vars'=>$tmp, 'extends'=>$co1->getExtends(), 'type'=> $co1->getType(), 'file'=>$co1->getFilename());
                }
            }
        }
        return $out;
    }
    
    private function dumpContextVars($co) {
        $out = array();
        foreach( $co->getVariables() as $varName => $var ) {
            if( $var['color']!='black' ) {
                $out[$varName] = $var;
            }
        }        
        return $out;
    }
}
