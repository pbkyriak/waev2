<?php

namespace Slx\TypeAnalysisBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;
use Slx\TypeAnalysisBundle\Analyser\NodeInfoExtractors;
use Slx\TypeAnalysisBundle\Analyser\SelfNames;
/**
 * Description of ObjNewCastCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 14 Μαρ 2016
 */
class ObjNewUnserializeCounter extends AbstractContextAwareCounter {
    
    const METRIC_PREFIX = 'OI:UNS';
    const COLOR_UNSERIALIZE = 'unserialize';
    
    private $extractors;
    /**
     * Holds items of interest in an expression.
     * @var \SplStack 
     */
    private $exprStack;
    /**
     * If true we are in some assignment expression nodes.
     * Is set to true when entering an assignment expression
     * set to false when leaving the assignment node.
     * @var boolean
     */
    private $inExprAssign = false;
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm);
        $this->extractors = new NodeInfoExtractors();
        $this->metricGroupId = 3;
        $this->exprStack = new \SplStack();
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        switch( $node->getType() ) {
            case ParserNodeTypes::NodeExprAssign:
                $this->enterAssignExpr($node);
                break;
            case ParserNodeTypes::NodeExprMethodCall:
                $this->enterMethodCall($node);
                break;
        }
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
//        printf("enter: %s line:%s\n", $node->getType(), $node->getLine());
        switch( $node->getType() ) {
            case ParserNodeTypes::NodeExprAssign:
                $this->leaveAssignExpr($node);
                break;
            case ParserNodeTypes::NodeExprFuncCall:
                if( $node->name==self::COLOR_UNSERIALIZE ) {
                    $this->leaveFuncCall($node);
                }
                break;
        }
    }

    protected function ccContextDown(PHPParser_Node $node) {
        $this->exprStack = new \SplStack();
    }

    protected function ccContextUp(PHPParser_Node $node) {
        $this->exprStack = new \SplStack();
    }

    /**
     * An assignment expression started, init expr stack 
     * and raise flag that we are in assignment expression
     * 
     * @param PHPParser_Node $node
     */
    private function enterAssignExpr(PHPParser_Node $node) {
        //printf("enter assign\n");
        $this->exprStack = new \SplStack();
        $this->inExprAssign = true;
        //printf("start of assignment\n");        
    }
    
    /**
     * Leaving assignment expression, now we can check if in the expression
     * a call to unserialize function was made.
     * if unserialize result is set to a variable, add to context a variable with color=unserialize
     * @param PHPParser_Node $node
     */
    private function leaveAssignExpr(PHPParser_Node $node) {
        //printf("end of assign\n");
        if( !$this->exprStack->isEmpty() ) {
            $item = $this->exprStack->pop();
            $var = $this->extractors->extractVarName($node);
            if( SelfNames::isSelfName($var) ) { // its class property, check if already used
                $context = $this->getContext()->getParent();
                $var = $this->extractors->extractPropertyName($node->var);
                if( $context->hasVariable($var)) { 
                    $cVar = $context->getVariable($var);
                    if( $cVar['color']=='object' ) {
                        $this->incMetric(implode(':',$item['keys']), 1, false);
                        $context->setVariableAttr($var, 'color', 'black');
                    }
                }
                else {
                    $newVar = array(
                        'type' => self::COLOR_UNSERIALIZE,
                        'keys' => $item['keys'],
                    );
                    $context->addVariable($var, $newVar['type']);
                    $context->setVariableAttr($var, 'keys', $newVar['keys']);                                        
                }
            }
            else {
                $this->getContext()->addVariable($var, $item['type']);
                $this->getContext()->setVariableAttr($var, 'keys', $item['keys']);                    
            }
//printf("var=%s type=%s\n", $var, $item['type']);
        }
        $this->inExprAssign = false;        
    }
    
    /**
     * unserialize is called, so add it to the expression stack.
     * 
     * @param PHPParser_Node $node
     */
    private function leaveFuncCall(PHPParser_Node $node) {
        if( $this->exprStack ) {
            $keys= array();
            $keys['type']=self::METRIC_PREFIX;
            $keys['datasource'] = $this->getFuncArgumentTypeByIdx($node,0);
            $this->exprStack->push(['type'=>self::COLOR_UNSERIALIZE, 'keys'=>$keys]);
        }
    }
    /**
     * checks if an -> operator is applied to a variable 
     * whose value was the result of unserialize function
     * 
     * @param PHPParser_Node $node
     */
    private function enterMethodCall(PHPParser_Node $node) {
        $keys = $this->getMethodCallSubject($node, 'any', self::COLOR_UNSERIALIZE);
        if( $keys ) {   // 
            $this->incMetric(implode(':',$keys), 1, false);
        }
    }
    
    /**
     * 
     * @param PHPParser_Node $node
     * @param type $methodName
     * @param type $objType
     * @return array|null           if the subject is found returns array else null
     */
    private function getMethodCallSubject(PHPParser_Node $node, $methodName, $objType) {
        $keys = null;
        //printf("methodname=%s objtype=%s nodevartype=%s\n",$methodName, $objType,$node->var->getType());
        if( $node->var->getType()==ParserNodeTypes::NodeExprVariable || $node->var->getType()==ParserNodeTypes::NodeExprPropertyFetch ) {
            if( $obj=$this->getMethodCallContextVariable($node, $methodName, $objType) ) {
                $keys = $obj['keys'];
//                printf(" from variables\n");
            }   
        }
        else {  // get from exprStack
            if( !$this->exprStack->isEmpty() ) {
                $item = $this->exprStack->pop();
                if($item['type']==$objType) {
                    $keys = $item['keys'];
                }
                else {
                    $this->exprStack->push($item);  // put it back!
                }
//                printf(" from stack\n");
            }
        }
        return $keys;
    }
    
    /**
     * if calling method matches $methodName then check if the variable 
     * exists in context and has the same color return variable name else false.
     * 
     * @param PHPParser_Node $node  method call node
     * @param string $methodName    wanted method name or 'any' to match any method.
     * @param string $color
     * @return string               variable name if any or false
     */
    private function getMethodCallContextVariable(PHPParser_Node $node, $methodName, $color) {
        $out = false;
        $methodMatch = $methodName=='any' ? true : false;
        if( !$methodMatch ) {
            if($node->name==$methodName) {
                $methodMatch = true;
            }
        }
        if( $methodMatch ) {
            $obj = $this->extractors->extractVarName($node->var);
            if( SelfNames::isSelfName($obj) ) {
                $obj = $this->extractors->extractPropertyName($node->var);
                if($this->getContext()->getParent()) {  // some nusty coding might call $this-> but the class is not in the same file. Like cs-cart controllers
                    $context = $this->getContext()->getParent();
                }
                else {
                    $context = $this->getContext();
                }
            }
            else {
                $context = $this->getContext();
            }
            //$this->dumpContextVars($context);
            if( $context->hasVariable($obj)) {  // defined in context
                $vobj = $context->getVariable($obj);
                if( $vobj['color']==$color ) {
                    $out = $vobj;
                    $context->setVariableAttr($obj, 'color', 'black');
                }
            }
            else {  // add context variable
                $item = array(
                    'type' => 'object',
                    'keys' => ['datasource'=>''],
                );
                $context->addVariable($obj, $item['type']);
                $context->setVariableAttr($obj, 'keys', $item['keys']);
            }
            
        }
        return $out;
    }
    
    private function getFuncArgumentTypeByIdx($node, $argIdx) {
        $out = '';
        if( is_array($node->args)) {
            $idx=0;
            foreach($node->args as $farg) {
                if( $idx==$argIdx) {
                    $out = $this->extractors->getMethodNameType($farg->value);
                    break;
                }
                $idx++;
            }
        }     
        return $out;        
    }
    private function dumpContextVars($co) {
        $out = array();
        printf("CONTEXT VARIABLES --------------\n");
        foreach( $co->getVariables() as $varName => $var ) {
            printf("\tvar=%s color=%s\n", $varName,$var['color'] );
        }        
        printf("--------------------------------\n");
    }
}
