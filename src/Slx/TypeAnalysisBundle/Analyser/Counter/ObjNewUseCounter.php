<?php
namespace Slx\TypeAnalysisBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;

/**
 * Description of ObjNewUseCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 27 Ιαν 2016
 */
class ObjNewUseCounter  extends AbstractContextAwareCounter {
    protected $lastNode;
    private $selfNames;
    private $skipNodes;
    private $similar;
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm);
        $this->metricGroupId = 6;
        $this->lastNode = new \SplStack();
        
        $this->selfNames = array('this', 'self', 'parent', 'static');
        $this->skipNodes = array(
            'Expr_Ternary','Expr_SmallerOrEqual', 'Expr_Smaller', 'Expr_NotEqual','Expr_GreaterOrEqual','Expr_Greater','Expr_Equal', 
            'Expr_ErrorSuppress', 'Expr_Array', 'Expr_ArrayItem', 'Expr_Cast_String', 'Expr_Plus'
        );
        $this->similar = array();
        $this->similar['Expr_AssignRef'] = 'Expr_Assign';
        
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter: %s line:%s\n", $node->getType(), $node->getLine());
        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprNew:      
                $this->visitExprNew($node);
                break;
            case ParserNodeTypes::NodeStmtReturn:
                $this->visitStmtReturn($node);
                break;
        }
        //printf("push: %s line:%s\n", $node->getType(), $node->getLine());
        $this->lastNode->push($node);
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
        //printf("pop: %s line:%s\n", $node->getType(), $node->getLine());
        if(!$this->lastNode->isEmpty()) {
            $this->lastNode->pop();
        }
        else {
            printf("pop from empty stack at %s %s \n",$this->metricContext->getName(), $node->getLine());
        }
    }

    protected function ccContextDown(PHPParser_Node $node) {
        
    }

    protected function ccContextUp(PHPParser_Node $node) {
        
    }

    /**
     * 
     * @param \PHPParser_Node $node
     */
    private function visitExprNew($node) {
        $ln = $this->getLastNode();
        if( $ln ) {
            //printf("last node before new %s line: %s\n", $ln->getType(), $ln->getLine());
            $nodeType = $this->mergeSimilar($ln->getType());
            $key = sprintf("OI:U:%s", $nodeType);
            if( $nodeType=='Expr_Assign' ) {
                $varName = $this->extractVarName($ln->var);
                //printf("varname=%s\n", $varName);
                //printf("nodetype=%s\n", $ln->getType());
                if(!in_array($varName, $this->selfNames)) { // local variable
                    //$this->getContext()->addVariable($varName);
                    $this->checkContextNewVariable($this->getContext(), $varName);
                }
                else {  // property
                    //printf("prop checks\n");
                    $propName = $this->extractPropertyName($ln->var);
                    if( $propName ) {
                        if($this->getContext()->getParent()) {
                            $this->checkContextNewVariable($this->getContext()->getParent(), $propName);
                            //$this->getContext()->getParent()->addVariable($propName);
                            //printf("added property: %s\n", $propName);
                        }
                    }
                }
            }
            //printf("key: %s\n", $key);
            $this->incMetric($key, 1, false);
        }
        else {
            $key = "OI:U:ROOT";
            $this->incMetric($key, 1, false);
        }

    }
    
    private function checkContextNewVariable($context, $varName) {
        if( $context->hasVariable($varName) ) {
            $varAttrs = $context->getVariable($varName);
            if( $varAttrs['color']=='return' ) {    // a return for this variable already found 
                $key = "OI:U:Expr_Assign:Return";
                $this->incMetric($key, 1, false);
                $this->incMetric('OI:U:Expr_Assign', -1, false);    // subtract from Expr_Assign count so it will not be counted again here                
            }
        }
        else {
            $context->addVariable($varName,'newobj');
        }
    }
    
    /**
     * 
     * @param string $nodeType
     * @return string
     */
    private function mergeSimilar($nodeType) {
        $out = $nodeType;
        if( isset($this->similar[$nodeType]) ) {
            $out = $this->similar[$nodeType];
        }
        return $out;
    }
    
    private function visitStmtReturn($node) { 
        if( $node->expr ) {
            // CHECK local variables
            if( $node->expr->getType()=='Expr_Variable' ) {
                $this->visitStmtReturnVariable($node);
            }
            // check object properties
            elseif( $node->expr->getType()=='Expr_PropertyFetch' ) {
                $this->visitStmtReturnOProperty($node);
            }
            // check current static properties
            elseif( $node->expr->getType()=='Expr_StaticPropertyFetch' ) {
                $this->visitStmtReturnOProperty($node);
            }
        }        
    }
    
    private function visitStmtReturnVariable($node) {
        $varName = $this->extractVarName($node->expr);
        if( $this->getContext()->hasVariable($varName)) {
            $key = "OI:U:Expr_Assign:Return";
            $this->incMetric($key, 1, false);
            $this->incMetric('OI:U:Expr_Assign', -1, false);    // subtract from Expr_Assign count so it will not be counted again here
        }        
    } 
    
    
    private function visitStmtReturnOProperty($node) {
        $propName = $this->extractPropertyName($node->expr);
        if( $propName && $this->getContext()->getParent()) {
            if( $this->getContext()->getParent()->hasVariable($propName) ) {
                $var = $this->getContext()->getParent()->getVariable($propName);
                if( $var['color']=='newobj' ) {
                    $key = "OI:U:Expr_Assign:Return";
                    $this->incMetric($key, 1, false);
                    $this->incMetric('OI:U:Expr_Assign', -1, false);    // subtract from Expr_Assign count so it will not be counted again here
                    $this->getContext()->getParent()->setVariableAttr($propName, 'color', 'black'); // any value but newobj and return
                }
            }
            else {  // add variable with color=return
                $this->getContext()->getParent()->addVariable($propName,'return');
            }
        }        
    } 
    /*
    private function visitStmtReturnSProperty($node) {
        $propName = $this->extractPropertyName($node->expr);
        //printf("ret:prop: %s\n", $propName);
        if( $propName && $this->getContext()->getParent()) {
            if( $this->getContext()->getParent()->hasVariable($propName) ) {
                $key = "OI:U:Expr_Assign:Return";
                $this->incMetric($key, 1, false);
                $this->incMetric('OI:U:Expr_Assign', -1, false);    // subtract from Expr_Assign count so it will not be counted again here
            }
        }                        
    } 
    */
    /**
     * 
     * @param PHPParser_Node $node
     * @return string
     */
    private function extractVarName(\PHPParser_Node $node) {
        $out = false;
        switch($node->getType()) {
            case "Expr_Variable":
                $out = $node->name;
                break;
            default:
                if($node->var) {
                    $out = $this->extractVarName($node->var);
                }
                elseif($node->class) {
                    //printf("type=%s\n",$node->class->getType());
                    if($node->class->getType()=='Name') {
                        $out = implode('',$node->class->parts);
                    }
                    else {
                        //$out = $this->extractVarName($node->name);
                    }

                }
                break;
        }
        return $out;
    }

    /**
     * 
     * @param PHPParser_Node $node
     * @return string
     */
    private function extractPropertyName(\PHPParser_Node $node) {
        $out = false;
        if($node->getType()=="Expr_PropertyFetch") {
            if( is_scalar($node->var->name) ) {
                if( in_array($node->var->name, $this->selfNames) ) {
                    if( is_scalar($node->name) ) {
                        $out = $node->name;
                    }
                }
            }
        }
        elseif( $node->getType()=='Expr_StaticPropertyFetch') {
            //printf("type=%s\n",$node->getType());
            if($node->class->getType()=='Name') {
                $cl = implode('',$node->class->parts);
                if( in_array($cl, $this->selfNames) ) {
                    if( is_scalar($node->name) ) {
                        $out = $node->name;
                    }
                }
            }
        }
        if( !$out ) {
            if($node->var) {
                $out = $this->extractPropertyName($node->var);
            }                
        }
        return $out;
    }
    
    /**
     * 
     * @return \PHPParser_Node
     */
    private function getLastNode() {
        $out = null;
        $this->lastNode->rewind();
        if(!$this->lastNode->current()) {
            return null;
        }
        while( $this->isSkipNodeToGetLast($this->lastNode->current()->getType())) {
            $this->lastNode->next();
            if( !$this->lastNode->current() ) {
                break;
            }
        }
        if( $this->lastNode->current() ) {
            $out = $this->lastNode->current();
        }
        return $out;
    }
    
    /**
     * 
     * @param string $nodeType
     * @return boolean
     */
    private function isSkipNodeToGetLast($nodeType) {
        $out = false;
        if(in_array($nodeType, $this->skipNodes)) {
            $out = true;
        }
        elseif(strpos($nodeType, 'Expr_Boolean')===0 ) {
            $out = true;
        }
        return $out;
    }
}
