<?php

namespace Slx\TypeAnalysisBundle\Analyser\Counter;

use \PHPParser_Node;
use Slx\TypeAnalysisBundle\Analyser\Counter\AbstractContextAwareCounter;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\MetricsBundle\Analyser\FileMetric;
use Slx\TypeAnalysisBundle\Analyser\NodeInfoExtractors;
use Slx\TypeAnalysisBundle\Analyser\SelfNames;
/**
 * Description of ReflectionInvokeCounter
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 21 Φεβ 2016
 */
class ReflectionInvokeCounter  extends AbstractContextAwareCounter {
    
    const METRIC_PREFIX_STATIC = 'MC:RES';
    const METRIC_PREFIX_OBJECT = 'MC:REO';
    
    private $extractors;
    private $exprStack;
    private $inExprAssign = false;
    
    public function configure(FileMetric $fm, $noEventOnContextStms=true) {
        parent::configure($fm);
        $this->extractors = new NodeInfoExtractors();
        $this->metricGroupId = 2;
        $this->exprStack = new \SplStack();
    }

    protected function ccEnterNode(PHPParser_Node $node) {
        //printf("enter: %s line:%s\n", $node->getType(), $node->getLine());
        if( $node->getType()=='Stmt_Foreach') {
            if( $node->expr && $node->expr->getType()==ParserNodeTypes::NodeExprVariable) {
                $varname =  $this->extractors->extractVarName($node->expr); //->name;
//                printf("expr varname=%s \n", $varname);
                if( $this->getContext()->hasVariable($varname)) {
                    if( $node->valueVar && $node->valueVar->getType()==ParserNodeTypes::NodeExprVariable) {
                        $valuevarname = $node->valueVar->name;
                        $item = $this->getContext()->getVariable($varname);
                        //print_R($item);
                        $this->getContext()->addVariable($valuevarname, $item['color']);
                        $this->getContext()->setVariableAttr($valuevarname, 'keys', $item['keys']);    
                    }
                }
            }
        }
        if( $node->getType()==ParserNodeTypes::NodeExprAssign ) {
            //printf("enter assign\n");
            $this->exprStack = new \SplStack();
            $this->inExprAssign = true;
            //printf("start of assignment\n");
        }
    }

    protected function ccLeaveNode(PHPParser_Node $node) {
        //printf("Leave: %s line:%s\n", $node->getType(), $node->getLine());
        if( $node->getType()==ParserNodeTypes::NodeExprAssign ) {
            //printf("end of assign\n");
            if( !$this->exprStack->isEmpty() ) {
                $item = $this->exprStack->pop();
                $var = $this->extractors->extractVarName($node);
                $this->getContext()->addVariable($var, $item['type']);
                $this->getContext()->setVariableAttr($var, 'keys', $item['keys']);    
            }
            $this->inExprAssign = false;
        }
        if( $node->getType()=='Expr_New' ) {
            if( ($item=$this->visitNewNode($node)) ) {
                if( $this->exprStack ) {
                    $this->exprStack->push(['type'=>$item['type'], 'keys'=>$item['keys']]);
                }
            }
        }
        if( $node->getType()=='Expr_MethodCall' ) {
            $this->visitMethodCall($node);
        }
    }

    protected function ccContextDown(PHPParser_Node $node) {
        $this->exprStack = new \SplStack();
    }

    protected function ccContextUp(PHPParser_Node $node) {
        $this->exprStack = new \SplStack();
    }

    private function visitNewNode($node) {
        $out = false;
        if( $this->isNewClass($node,'ReflectionClass') ) {
            $keys = ['type'=>self::METRIC_PREFIX_STATIC, 'class'=>'', 'method'=>''];
            if( is_array($node->args)) {
                $keys['class'] = $this->getFuncArgumentTypeByIdx($node,0);
                $farg = reset($node->args);
                if( $farg->value->getType()=='Expr_FuncCall' && $farg->value->name=='get_class') {
                    $keys['type']=self::METRIC_PREFIX_OBJECT;
                    $keys['class'] = $this->getFuncArgumentTypeByIdx($farg->value,0);
                }
            }
            $out = ['type'=>'ReflectionClass', 'keys'=>$keys];
        }  
        elseif( $this->isNewClass($node,'ReflectionMethod') ) {
            $keys = ['type'=>self::METRIC_PREFIX_STATIC, 'class'=>'', 'method'=>''];
            if( is_array($node->args)) {
                $keys['class'] = $this->getFuncArgumentTypeByIdx($node,0);
                $farg = reset($node->args);
                if( $farg->value->getType()=='Expr_FuncCall' && $farg->value->name=='get_class') {
                    $keys['type']=self::METRIC_PREFIX_OBJECT;
                    $keys['class'] = $this->getFuncArgumentTypeByIdx($farg->value,0);
                }
                $keys['method'] = $this->getFuncArgumentTypeByIdx($node,1);
            }
            $out = ['type'=>'ReflectionMethod', 'keys'=>$keys];
        }
        return $out;
    }
    
    private function isNewClass($node, $className) {
        $out = false;
        $newClass = '';
        if( $node->class->getType()=='Name') {
            $newClass = implode("/",$node->class->parts);
        }
        elseif( $node->class->getType()=='Name_FullyQualified') {
            $newClass = implode("/",$node->class->parts);
        }
        if( $newClass==$className) {
            $out = true;
        }        
        return $out;
    }
    
    private function getFuncArgumentTypeByIdx($node, $argIdx) {
        $out = '';
        if( is_array($node->args)) {
            $idx=0;
            foreach($node->args as $farg) {
                if( $idx==$argIdx) {
                    $out = $this->extractors->getMethodNameType($farg->value);
                    break;
                }
                $idx++;
            }
        }        
        return $out;        
    }
    
    private function getMethodCallContextVariable($node, $methodName, $color) {
        $out = false;
        if($node->name==$methodName) {
            $obj = $this->extractors->extractVarName($node->var);
            if( $this->getContext()->hasVariable($obj)) {
                $vobj = $this->getContext()->getVariable($obj);
                if( $vobj['color']==$color ) {
                    $out = $obj;
                }
            }
        }
        return $out;
    }
    
    private function visitMethodCall($node) {
        
        if( in_array($node->name,['getMethod', 'invoke','getMethods']) ) {
            $keys = null;
            $type = null;
            if( $keys = $this->getMethodCallSubject($node, 'getMethod', 'ReflectionClass') ) {
                $farg = $this->getFuncArgumentTypeByIdx($node,0);
                $keys['method'] = $farg;
                $type = 'ReflectionMethod';
            }
            elseif( $keys = $this->getMethodCallSubject($node, 'getMethods', 'ReflectionClass') ) {
                $type = 'ReflectionMethod';
                $keys['method'] = ParserNodeTypes::NodeExprVariable;    // a default value, chosen without a reason
            }
            elseif( $keys = $this->getMethodCallSubject($node, 'invoke', 'ReflectionMethod') ) {
                $farg = $this->getFuncArgumentTypeByIdx($node,0);
                if( $farg=='Expr_ConstFetch' ) {
                    $keys['type']=self::METRIC_PREFIX_STATIC;
                }
                else {
                    $keys['type']=self::METRIC_PREFIX_OBJECT;
                    $keys['class'] = $farg;
                }
                $type = 'mixed';
                $this->incMetric(implode(':',$keys), 1, false);
            }
            if( $keys ) {
                if( $this->exprStack ) {
                    $mitem = ['type'=>$type, 'keys'=>$keys];
                    $this->exprStack->push($mitem);
                }
            }
        }
    }
    
    private function getMethodCallSubject($node, $methodName, $objType) {
        $keys = null;
//        printf("methodname=%s objtype=%s nodevartype=%s\n",$methodName, $objType,$node->var->getType());
        if( $node->var->getType()==ParserNodeTypes::NodeExprVariable ) {
            if( $obj=$this->getMethodCallContextVariable($node, $methodName, $objType) ) {
                $vobj = $this->getContext()->getVariable($obj);
                $keys = $vobj['keys'];
//                printf(" from variables\n");
            }            
        }
        else {  // get from exprStack
            if( !$this->exprStack->isEmpty() ) {
                $item = $this->exprStack->pop();
                if($item['type']==$objType) {
                    $keys = $item['keys'];
                }
                else {
                    $this->exprStack->push($item);  // put it back!
                }
//                printf(" from stack\n");
            }
        }
        return $keys;
    }
}
