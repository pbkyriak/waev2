<?php

namespace Slx\TypeAnalysisBundle\Analyser;

use Slx\ReflectionBundle\Reflection\Traverser;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextGlobal;

/**
 * Description of DoAnalysis
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class DoAnalysis {

    /** @var Doctrine\ORM\EntityManager */
    private $em;

    /** @var \Slx\ParserBundle\Util\SourceCodeHelper */
    private $scHelper;
/*
    public function __construct($em, $scHelper) {
        $this->em = $em;
        $this->scHelper = $scHelper;
    }
*/
    private function getFileAst($filee) {
        $file = $filee['fname'];
        try {
            $p = new \PHPParser_Parser(new \PHPParser_Lexer());
            return $p->parse(file_get_contents($file));
        } catch (\PHPParser_Error $e) {
            printf("file: %s\n\tParse Error: %s\n", $file, $e->getMessage());
            return false;
        } catch (\Exception $e) {
            printf("file: %s\n\tParse Error: %s\n", $file, $e->getMessage());
            return false;
        }
    }

    public function exec($file) {
        $ast = $this->getFileAst($file);
        if( $ast ) {
            //var_dump($ast);
            $l1 = new SampleVisitor($file['fname'], false);
            $l1->configure();
            $context = $l1->getRootContext();
            $traverser = new Traverser();
            $traverser->addVisitor($l1);
            $traverser->traverseAst($ast);
            $ast = null;
            printf("after %s\n", $context->getType());
            $context->free();
        }
    }
    
}
