<?php

namespace Slx\TypeAnalysisBundle\Analyser;

use Slx\ReflectionBundle\Reflection\ParserNodeTypes;

/**
 * Description of LastNodeStack
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class LastNodeStack extends \SplStack {

    private $skipNodes = array(
            'Expr_Ternary','Expr_SmallerOrEqual', 'Expr_Smaller', 'Expr_NotEqual','Expr_GreaterOrEqual','Expr_Greater','Expr_Equal', 
            'Expr_ErrorSuppress', 'Expr_Array', 'Expr_ArrayItem', 'Expr_Cast_String', 'Expr_Plus'
        );
    
    private $similar;
    
    public function __construct() {
        $this->similar = array();
        $this->similar[ParserNodeTypes::NodeExprAssignRef] = ParserNodeTypes::NodeExprAssign;
    }


    /**
     * 
     * @return \PHPParser_Node
     */
    public function getLastNode() {
        $out = null;
        $this->rewind();
        if(!$this->current()) {
            return null;
        }
        while( $this->isSkipNodeToGetLast($this->current()->getType())) {
            $this->next();
            if( !$this->current() ) {
                break;
            }
        }
        if( $this->current() ) {
            $out = $this->current();
        }
        return $out;
    }
    
    /**
     * 
     * @param string $nodeType
     * @return boolean
     */
    private function isSkipNodeToGetLast($nodeType) {
        $out = false;
        if(in_array($nodeType, $this->skipNodes)) {
            $out = true;
        }
        elseif(strpos($nodeType, 'Expr_Boolean')===0 ) {
            $out = true;
        }
        return $out;
    }

    /**
     * 
     * @param string $nodeType
     * @return string
     */
    public function mergeSimilar($nodeType) {
        $out = $nodeType;
        if( isset($this->similar[$nodeType]) ) {
            $out = $this->similar[$nodeType];
        }
        return $out;
    }

}
