<?php

namespace Slx\TypeAnalysisBundle\Analyser;

use Slx\ReflectionBundle\Reflection\ParserNodeTypes;

/**
 * Description of NodeInfoExtractors
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class NodeInfoExtractors {

    /**
     * If node is object means that is not a method name, but it might be a variable
     * if node is expr variable we want to know its type
     * else return that type
     * 
     * @param PHPParser_Node|string $nameNode
     * @return string
     */
    public function getMethodNameType($nameNode) {
        $out = 'METHOD';
        if(is_object($nameNode) ) {
            $nameType = $nameNode->getType();
            if( $nameType==ParserNodeTypes::NodeExprVariable ) {
                $nameNameType = is_object($nameNode->name) ? $nameType.'|'.$nameNode->name->getType() : $nameType;
                $out = $nameNameType;
            }
            else {
                $out = $nameType;
            }
        }
        return $out;
    }
    
    /**
     * 
     * @param PHPParser_Node $node
     * @return string
     */
    public function extractVarName(\PHPParser_Node $node) {
        $out = false;
        switch($node->getType()) {
            case "Expr_Variable":
                if( is_scalar($node->name) ) {
                    $out = $node->name;
                }
                else {
                    $out = $this->extractVarName($node->name);
                }
                break;
            default:
                if($node->var) {
                    $out = $this->extractVarName($node->var);
                }
                elseif($node->class) {
                    if($node->class->getType()=='Name') {
                        $out = implode('',$node->class->parts);
                    }
                }
                break;
        }
        return $out;
    }

    /**
     * 
     * @param PHPParser_Node $node
     * @return string
     */
    public function extractPropertyName(\PHPParser_Node $node) {
        $out = false;
        if($node->getType()=="Expr_PropertyFetch") {
            if( is_scalar($node->var->name) ) {
                if( in_array($node->var->name, SelfNames::$selfNames) ) {
                    if( is_scalar($node->name) ) {
                        $out = $node->name;
                    }
                }
            }
        }
        elseif( $node->getType()=='Expr_StaticPropertyFetch') {
            if($node->class->getType()=='Name') {
                $cl = implode('',$node->class->parts);
                if( in_array($cl, SelfNames::$selfNames) ) {
                    if( is_scalar($node->name) ) {
                        $out = $node->name;
                    }
                }
            }
        }
        if( !$out ) {
            if($node->var) {
                $out = $this->extractPropertyName($node->var);
            }                
        }
        return $out;
    }
}
