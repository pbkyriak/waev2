<?php

namespace Slx\TypeAnalysisBundle\Analyser;

use Slx\TypeAnalysisBundle\CodeContext\AbstractCodeContextVisitor;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
/**
 * Description of SampleVisitor
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class SampleVisitor extends AbstractCodeContextVisitor {
    
    private $lastNode;
    private $selfNames;
    
    protected function configure() {
        $this->lastNode = new \SplStack();
        $this->selfNames = array('this', 'self', 'parent', 'static');
    }
       
    protected function cContextDown(\PHPParser_Node $node) {
        
    }

    protected function cContextUp(\PHPParser_Node $node) {
        
    }
    
    protected function cEnterNode(\PHPParser_Node $node) {

        switch ($node->getType()) {
            case ParserNodeTypes::NodeExprNew:      
                $this->visitExprNew($node);
                break;
            case ParserNodeTypes::NodeStmtReturn:
                $this->visitStmtReturn($node);
                break;
        }
        //printf("push: %s line:%s\n", $node->getType(), $node->getLine());
        $this->lastNode->push($node);
    }

    protected function cLeaveNode(\PHPParser_Node $node) {
        //printf("pop: %s line:%s\n", $node->getType(), $node->getLine());
        $this->lastNode->pop();
    }
    
    private function visitExprNew($node) {
        $ln = $this->getLastNode();
        if( $ln ) {
            printf("last node before new %s line: %s\n", $ln->getType(), $ln->getLine());
            $key = sprintf("OI:U:%s", $ln->getType());
            if( $ln->getType()=='Expr_Assign') {
                $varName = $this->extractVarName($ln->var);
                if(!in_array($varName, $this->selfNames)) {
                    $this->getContext()->addVariable($varName);
                }
            }
            printf("key: %s\n", $key);
        }
    }
    
    private function visitStmtReturn($node) { 
        if( $node->expr ) {
            if( $node->expr->getType()=='Expr_Variable' ) {
                $varName = $this->extractVarName($node->expr);
                if( $this->getContext()->hasVariable($varName)) {
                    $key = "OI:U:Expr_Assign:Return";
                    printf("key: %s\n", $key);
                }
            }
        }        
    }
    
    private function extractVarName(\PHPParser_Node $node) {
        $out = false;
        switch($node->getType()) {
            case "Expr_Variable":
                $out = $node->name;
                break;
            default:
                $out = $this->extractVarName($node->var);
                break;
        }
        
        return $out;
    }

    private function getLastNode() {
        $out = null;
        $this->lastNode->rewind();
        if(!$this->lastNode->current()) {
            return null;
        }
        while($this->lastNode->current()->getType()=='Expr_Ternary') {
            $this->lastNode->next();
            if( !$this->lastNode->current() ) {
                break;
            }
        }
        if( $this->lastNode->current() ) {
            $out = $this->lastNode->current();
        }
        return $out;
    }
}
