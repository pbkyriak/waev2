<?php

namespace Slx\TypeAnalysisBundle\Analyser;

/**
 * Description of SelfNames
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 7 Φεβ 2016
 */
class SelfNames {

    static $selfNames = array('this', 'self', 'parent', 'static');
    
    public static function isSelfName($name) {
        return in_array($name, SelfNames::$selfNames);
    }
}
