<?php

namespace Slx\TypeAnalysisBundle\CodeContext;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;
/**
 * Description of AbstractCodeContext
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class AbstractCodeContext implements CodeContextInterface {

    protected $fqname;
    protected $filename;
    protected $members;
    protected $parent;
    protected $namespace;
    static $current;
    protected $variables;
    
    public function __construct($filename) {
        $this->filename = $filename;
        $this->members = array();
        $this->parent = null;
        $this->variables = array();
    }
    
    public function getType() {
        $c = get_class($this);
        return substr(substr($c, strrpos($c,'\\')+1),11);
    }
    
    public function getFilename() {
        return $this->filename;
    }
    
    public function setFilename($v) {
        $this->filename = $v;
    }
    
    public function getNamespace() {
        return $this->namespace;
    }
    
    public function setNamespace($v) {
        $this->namespace = $v;
    }
    
    public function getFQName() {
        return $this->namespace .ReflectionConsts::NS_DELIMETER.$this->fqname;
    }
    
    public function setFQName($v) {
        $this->fqname = $v;
    }
    
    public function setParent($parent) {
        $this->parent = $parent;
    }
    
    /**
     * 
     * @return \Slx\TypeAnalysisBundle\Analyser\CodeContextInterface
     */
    public function getParent() {
        return $this->parent;
    }
    
    public function addMember(CodeContextInterface $member) {
        $member->setParent($this);
        $member->setNamespace($this->namespace);
        $this->members[] = $member;
        static::$current = $member;
        return $member;
    }
    
    public function leaveCurrent() {
        static::$current = $this->getParent();
        return static::$current;
    }
    
    public function getMembers() {
        return $this->members;
    }
    
    public function addVariable($varName, $color) {
        $this->variables[$varName] = array('color'=>$color);
    }
    
    public function hasVariable($varName) {
        return isset($this->variables[(string)$varName]);
    }

    public function getVariable($varName) {
        $out = false;
        if( isset($this->variables[$varName]) ) {
            $out = $this->variables[$varName];
        }
        return $out;
    }
    public function setVariableAttr($varName, $attr, $attrVal) {
        if( isset($this->variables[$varName]) ) {
            $this->variables[$varName][$attr] = $attrVal;
        }
    }
    
    public function getVariables() {
        return $this->variables;
    }
    
    public function free() {
        foreach($this->members as $k => $v) {
            $v->free();
            $this->members[$k]=null;
        }
        $this->parent = null;
        $this->variables = null;
    }
}
