<?php

namespace Slx\TypeAnalysisBundle\CodeContext;

use \PHPParser_NodeVisitor;
use \PHPParser_Node;
use Slx\ReflectionBundle\Reflection\ParserNodeTypes;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextGlobal    as ContextGlobal;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextInterface as ContextInterface;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextClass     as ContextClass;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextTrait     as ContextTrait;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextFunction  as ContextFunction;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextMethod    as ContextMethod;
use Slx\TypeAnalysisBundle\CodeContext\Context\CodeContextClosure   as ContextClosure;
use Slx\TypeAnalysisBundle\Analyser\LastNodeStack;
/**
 * Description of CodeContextVisitor
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
abstract class AbstractCodeContextVisitor implements PHPParser_NodeVisitor {

    /** @var Slx\TypeAnalysisBundle\CodeContext\CodeContextInterface */
    private $context;
    /** @var Slx\TypeAnalysisBundle\CodeContext\CodeContextInterface */
    private $rootContext;
    private $noEventOnContextStms = false;
    protected $lastNode;
    
    public function configureVisitor($context, $noEventOnContextStms) {
        if( is_string($context) ) {
            $this->context = new ContextGlobal($context);
        }
        else {
            $this->context = $context;
        }
        $this->rootContext = $this->context;
        $this->noEventOnContextStms = $noEventOnContextStms; 
        $this->lastNode = new LastNodeStack();
    }
    
    /**
     * 
     * @return ContextGlobal
     */
    final public function getRootContext() {
        return $this->rootContext;
    }
    
    /**
     * 
     * @return Slx\TypeAnalysisBundle\CodeContext\CodeContextInterface
     */
    final public function getContext() {
        return $this->context;
    }
    
    final public function afterTraverse(array $nodes) {}

    final public function beforeTraverse(array $nodes) {}

    final public function enterNode(PHPParser_Node $node) {
        $nodeType = $node->getType();
        $isContextStmt = false;
        switch ($nodeType) {
            case ParserNodeTypes::NodeStmtNamespace:
                if( is_array($node->name->parts) ) {
                    $ns = ReflectionConsts::NS_DELIMETER . implode(ReflectionConsts::NS_DELIMETER, $node->name->parts);
                    $this->context->setNamespace($ns);
                }
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtInterface:
                $c = new ContextInterface($this->context->getFilename());
                $c->setFQName($node->name);
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtClass:
                $c = new ContextClass($this->context->getFilename());
                $c->setFQName($node->name);
                if($node->extends) {
                    $c->setExtends($node->extends);
                }
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtTrait:
                $c = new ContextTrait($this->context->getFilename());
                $c->setFQName($node->name);
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtFunction:
                $c = new ContextFunction($this->context->getFilename());
                $c->setFQName($node->name);
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtClassMethod:
                $c = new ContextMethod($this->context->getFilename());
                $c->setFQName($node->name);
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeExprClosure:
                $c = new ContextClosure($this->context->getFilename());
                $c->setFQName($node->name);
                $this->contextUp($c, $node);
                $isContextStmt=true;
                break;
        }
        if( !($this->noEventOnContextStms && $isContextStmt) ) {
            $this->cEnterNode($node);
        }
        if(!$isContextStmt) {
            $this->lastNode->push($node);
        }
    }

    final public function leaveNode(PHPParser_Node $node) {
        $nodeType = $node->getType();
        $isContextStmt = false;
        switch ($nodeType) {
            case ParserNodeTypes::NodeStmtInterface:
            case ParserNodeTypes::NodeStmtClass:
            case ParserNodeTypes::NodeStmtTrait:
            case ParserNodeTypes::NodeStmtFunction:
            case ParserNodeTypes::NodeStmtClassMethod:
            case ParserNodeTypes::NodeExprClosure:
                $this->contextDown($node);
                $isContextStmt=true;
                break;
            case ParserNodeTypes::NodeStmtNamespace:
                $isContextStmt=true;
                break;
        }
        if( !($this->noEventOnContextStms && $isContextStmt) ) {
            $this->cLeaveNode($node);
        }
        if(!$isContextStmt) {
            if(!$this->lastNode->isEmpty()) {
                $this->lastNode->pop();
            }
        }
    }

    /**
     * New level, entered a new block
     * 
     * @param type $c
     * @param type $node
     */
    private function contextUp($c, $node) {
        $this->context = $this->context->addMember($c);
        $this->cContextUp($node);
    }

    /**
     * left a block, eg got out of a method
     * 
     * @param type $node
     */
    private function contextDown($node) {
        $this->context = $this->context->leaveCurrent();
        $this->cContextDown($node);
    }

    abstract protected function cEnterNode(PHPParser_Node $node);
    abstract protected function cLeaveNode(PHPParser_Node $node);
    abstract protected function cContextUp(PHPParser_Node $node);
    abstract protected function cContextDown(PHPParser_Node $node);
}
