<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Slx\TypeAnalysisBundle\CodeContext;

/**
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
interface CodeContextInterface {
    public function getType();
    public function getFQName();
    public function setFQName($v);
    public function getParent();
    public function setParent($v);
    public function getFilename();
    public function setFilename($v);
    public function getNamespace();
    public function setNamespace($v);
    public function addMember(CodeContextInterface $member);
    public function leaveCurrent();
    public function getMembers();
    public function addVariable($varName, $color);
    public function hasVariable($varName);
    public function getVariable($varName);
    public function setVariableAttr($varName, $attr, $attrVal);
}
