<?php

namespace Slx\TypeAnalysisBundle\CodeContext\Context;

use Slx\TypeAnalysisBundle\CodeContext\AbstractCodeContext;
use Slx\Reflection2Bundle\Reflection\ReflectionConsts;
/**
 * Description of CodeContextClass
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class CodeContextClass extends AbstractCodeContext {
    private $extends;
    
    public function setExtends($v, $toFQN=true) {
        if( $toFQN ) {
            $this->extends = $this->namespace .ReflectionConsts::NS_DELIMETER.$v;
        }
        else {
            $this->extends = $v;
        }
    }
    
    public function getExtends() {
        return $this->extends;
    }
}
