<?php

namespace Slx\TypeAnalysisBundle\CodeContext\Context;

use Slx\TypeAnalysisBundle\CodeContext\AbstractCodeContext;
/**
 * Description of CodeContextMethod
 *
 * @author Panos Kyriakakis <panos@salix.gr>
 * @since 9 Ιαν 2016
 */
class CodeContextMethod extends AbstractCodeContext {
    
    public function getFQName() {
        return $this->getParent()->getFQName() .'::'.$this->fqname;
    }

}
