<?php

namespace Slx\TypeAnalysisBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContextVar
 */
class ContextVar
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $project_tag_id;

    /**
     * @var string
     */
    private $class_name;

    /**
     * @var integer
     */
    private $project_class_id;

    /**
     * @var integer
     */
    private $extends_class_id;

    /**
     * @var string
     */
    private $var_name;

    /**
     * @var string
     */
    private $color;

    /**
     * @var string
     */
    private $method;

    /**
     * @var string
     */
    private $pattern;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set project_tag_id
     *
     * @param integer $projectTagId
     * @return ContextVar
     */
    public function setProjectTagId($projectTagId)
    {
        $this->project_tag_id = $projectTagId;
    
        return $this;
    }

    /**
     * Get project_tag_id
     *
     * @return integer 
     */
    public function getProjectTagId()
    {
        return $this->project_tag_id;
    }

    /**
     * Set class_name
     *
     * @param string $className
     * @return ContextVar
     */
    public function setClassName($className)
    {
        $this->class_name = $className;
    
        return $this;
    }

    /**
     * Get class_name
     *
     * @return string 
     */
    public function getClassName()
    {
        return $this->class_name;
    }

    /**
     * Set project_class_id
     *
     * @param integer $projectClassId
     * @return ContextVar
     */
    public function setProjectClassId($projectClassId)
    {
        $this->project_class_id = $projectClassId;
    
        return $this;
    }

    /**
     * Get project_class_id
     *
     * @return integer 
     */
    public function getProjectClassId()
    {
        return $this->project_class_id;
    }

    /**
     * Set extends_class_id
     *
     * @param integer $extendsClassId
     * @return ContextVar
     */
    public function setExtendsClassId($extendsClassId)
    {
        $this->extends_class_id = $extendsClassId;
    
        return $this;
    }

    /**
     * Get extends_class_id
     *
     * @return integer 
     */
    public function getExtendsClassId()
    {
        return $this->extends_class_id;
    }

    /**
     * Set var_name
     *
     * @param string $varName
     * @return ContextVar
     */
    public function setVarName($varName)
    {
        $this->var_name = $varName;
    
        return $this;
    }

    /**
     * Get var_name
     *
     * @return string 
     */
    public function getVarName()
    {
        return $this->var_name;
    }

    /**
     * Set color
     *
     * @param string $color
     * @return ContextVar
     */
    public function setColor($color)
    {
        $this->color = $color;
    
        return $this;
    }

    /**
     * Get color
     *
     * @return string 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set method
     *
     * @param string $method
     * @return ContextVar
     */
    public function setMethod($method)
    {
        $this->method = $method;
    
        return $this;
    }

    /**
     * Get method
     *
     * @return string 
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return ContextVar
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    
        return $this;
    }

    /**
     * Get pattern
     *
     * @return string 
     */
    public function getPattern()
    {
        return $this->pattern;
    }
}
