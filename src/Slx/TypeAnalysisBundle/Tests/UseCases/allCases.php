<?php
namespace Slx\Tests\AllCases;


interface Bar {
    public function methodA();
    
    public function methodB();
}
trait FooBar {
    public function methodAB() {}
}

class Foo {

    public function methodA() {}
    
    public function methodB() {
        $a = function() {};
        $a = new Object();
    }
}

function funA() {}