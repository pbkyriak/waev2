<?php
function create() {
    $this->a = new Object();
    $a = new Object();              // var = a
    $a[1] = new Object();           // var = a
    $a->depent = new Object();      // var = a
    $a->dep->depent = new Object(); // var = a
    $b->dep()->depent = new Object(); // var = a
    $this->dep->depent = new Object(); // var = a
    return $b;
}

function confuseme() {
    return $a;
}
//if ($this->EOF) return true ? (1>2 ? new PEAR_Error('EOF',-1) : false ): false;
//(new SplQueue())->push(1);

/*
 * $a->set(new ObjecT());
$a[1] = new Object();
$a->prop = new Object();
$$a = new Object();
$a->set(new ObjecT());
 * 
 */