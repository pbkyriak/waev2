<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Slx\UserBundle\Entity\Role;
use Slx\UserBundle\Form\RoleType;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
/**
 * Role controller.
 *
 * @Route("/roles")
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Χρήστες", route="user")
 * @Breadcrumb("Roles")
 * @CurrentMenuItem("roles")
 */
class RoleController extends Controller
{

    /**
     * Lists all Role entities.
     *
     * @Route("/", name="roles")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        if ($request->get('out') == 'csv') {
            $entities = $query->getResult();
            $response = $this->render('SlxUserBundle:Role:index.csv.twig',
                array(
                'entities' => $entities
            ));
            $response->headers->set('Content-Type', 'text/csv');
            $date = new \DateTime();
            $response->headers->set('Content-Disposition',
                'attachment; filename="' . $date->format("U") . '-machines.csv"');
            return $response;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxUserBundle:Role a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'gsprod.role.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new Role entity.
     *
     * @Route("/", name="roles_create")
     * @Method("POST")
     * @Template("SlxUserBundle:Role:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Role();
        $form = $this->createForm(new RoleType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('roles_new'));
            else
                return $this->redirect($this->generateUrl('roles_show',
                            array('id' => $entity->getId())));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Role entity.
     *
     * @Route("/new", name="roles_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Role();
        $form = $this->createForm(new RoleType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Role entity.
     *
     * @Route("/{id}", name="roles_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Role entity.
     *
     * @Route("/{id}/edit", name="roles_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $editForm = $this->createForm(new RoleType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Edits an existing Role entity.
     *
     * @Route("/{id}/update", name="roles_update")
     * @Method({"PUT", "POST"})
     * @Template("SlxUserBundle:Role:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new RoleType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('roles_new'));
            else
                return $this->redirect($this->generateUrl('roles_show',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Role entity.
     *
     * @Route("/{id}", name="roles_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SlxUserBundle:Role')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Role entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('roles'));
    }

    /**
     * Creates a form to delete a Role entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
                ->add('id', 'hidden')
                ->getForm()
        ;
    }

}
