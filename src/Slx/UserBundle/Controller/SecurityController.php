<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{

    public function loginAction(Request $request)
    {
        /*
        echo 'user1';
        $factory = $this->get('security.encoder_factory');
        $user = new \Slx\UserBundle\Entity\User();
        $user->setUsername('panos');
        $user->setEmail('panos@salix.gr');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword('panos', $user->getSalt());
        $user->setPassword($password);
        $em = $this->getDoctrine()->getManager();
        $em->persist($user);
        $em->flush();
        */
        $session = $request->getSession();
        if( $request->attributes->has(SecurityContext::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR
                );
        } else {
            $error = $session->get(SecurityContext::AUTHENTICATION_ERROR);
            $session->remove(SecurityContext::AUTHENTICATION_ERROR);
        }
        
        return $this->render(
            'SlxUserBundle:Security:login.html.twig',
            array(
                'last_username' => $session->get(SecurityContext::LAST_USERNAME),
                'error' => $error,
            )
        );
    }

    public function securityCheckAction()
    {
        // The security layer will intercept this request
        die('Wrong security configuration!');
    }

    public function logoutAction()
    {
        // The security layer will intercept this request
    }
}