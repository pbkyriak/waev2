<?php

namespace Slx\UserBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Slx\UserBundle\Entity\User;
use Slx\UserBundle\Form\UserType;
use Slx\UserBundle\Form\UserNewType;
use Slx\UserBundle\Security\User\SlxUser;
use APY\BreadcrumbTrailBundle\Annotation\Breadcrumb;
use Slx\MetronicBundle\Annotation\CurrentMenuItem;
/**
 * User controller.
 *
 * @Route("/user")
 * @Breadcrumb("Home", route="metronic_admin_homepage")
 * @Breadcrumb("Χρήστες", route="user")
 * @Breadcrumb("Users", route="user")
 * @CurrentMenuItem("user")
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     * @Route("/", name="user")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $form = $this->getFilterForm();
        $query = $this->makeListQuery($form);
        $request = $this->get('request');
        if ($request->get('out') == 'csv') {
            $entities = $query->getResult();
            $response = $this->render('SlxUserBundle:User:index.csv.twig',
                array(
                'entities' => $entities
            ));
            $response->headers->set('Content-Type', 'text/csv');
            $date = new \DateTime();
            $response->headers->set('Content-Disposition',
                'attachment; filename="' . $date->format("U") . '-machines.csv"');
            return $response;
        }
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $this->get('request')->query->get('page', 1) /* page number */,
            $this->get('request')->query->get('limit', 15)
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * Create list query
     */
    private function makeListQuery($form)
    {
        $em = $this->getDoctrine()->getManager();
        $dql = "SELECT a FROM SlxUserBundle:User a";
        $form->handleRequest($this->get('request'));
        $filterQryParams = array();
        $where = array();
        $this->get('request')->setLocale('el');
        if ($form->isValid()) {
            $filterData = $form->getData();
            if ($filterData['title']) {
                $where[] = " a.title like :atitle ";
                $filterQryParams['atitle'] = str_replace('*', '%',
                    $filterData['title']);
            }
        }
        if ($where) {
            $dql .= ' where ' . implode(' and ', $where);
        }

        $query = $em->createQuery($dql);
        $query->setParameters($filterQryParams);
        return $query;
    }

    /**
     * Creates filter form
     *
     */
    private function getFilterForm()
    {
        $filterData = array();
        $this->createFormBuilder();
        $form = $this->createFormBuilder($filterData,
                array('method' => 'GET', 'csrf_protection' => false))
            ->add(
                'title', 'text',
                array('required' => false, 'label' => 'gsprod.user.title')
            )
            ->getForm();
        return $form;
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("SlxUserBundle:User:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createForm(new UserNewType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $factory = $this->get('security.encoder_factory');
            $encoder = $factory->getEncoder(new SlxUser('', '', '', array()));
            $encodedPassword = $encoder->encodePassword($entity->getPassword(),
                $entity->getSalt());
            $entity->setPassword($encodedPassword);

            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($form->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('user_new'));
            else
                return $this->redirect($this->generateUrl('user_show',
                            array('id' => $entity->getId())));
        }
        else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/new", name="user_new")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Προσθήκη")
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createForm(new UserNewType(), $entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{id}", name="user_show")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Προβολή")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method("GET")
     * @Template()
     * @Breadcrumb("Μεταβολή")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserType(), $entity);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}/update", name="user_update")
     * @Method("POST")
     * @Template("SlxUserBundle:User:edit.html.twig")
     * @Breadcrumb("Μεταβολή")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createForm(new UserType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('info',
                'gsprod.general.record_saved');
            if ($editForm->get('saveAndAdd')->isClicked())
                return $this->redirect($this->generateUrl('user_new'));
            else
                return $this->redirect($this->generateUrl('user_show',
                            array('id' => $id)));
        } else {
            $this->get('session')->getFlashBag()->add('notice',
                'gsprod.general.record_errors');
        }


        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}/delete", name="user_delete")
     * @Method("GET")
     */
    public function deleteAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        if ($id == 1) {
            $this->get('session')->getFlashBag()->add('info',
                'users.user.cannot_delete_1');
            return $this->redirect($this->generateUrl('user_show',
                        array('id' => $id)));
        }
        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('user'));
    }

    /**
     * changes user password a User entity.
     *
     * @Route("/{id}/changePass", name="user_changePass")
     * @Method({"GET", "POST"})
     * @Template()
     * @Breadcrumb("Αλλαγή password")
     */
    public function changePassAction(Request $request, $id)
    {
        $returnToShow = false;
        if ($id) {
            $returnToShow = true;
        } else {
            $id = $this->getUser()->getId();
        }
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('SlxUserBundle:User')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('users.user.not_found');
        }

        $this->createFormBuilder();
        $form = $this->createFormBuilder()
            ->add(
                'password', 'text',
                array('required' => true, 'label' => 'users.user.password')
            )
            ->add('save', 'submit',
                array('label' => 'gsprod.general.save', 'attr' => array('class' => 'btn blue')))
            ->getForm();

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($this->get('request'));
            if ($form->isValid()) {
                $data = $form->getData();
                $factory = $this->get('security.encoder_factory');
                $encoder = $factory->getEncoder(new SlxUser('', '', '', array()));
                $encodedPassword = $encoder->encodePassword($data['password'],
                    $entity->getSalt());
                $entity->setPassword($encodedPassword);
                $em->persist($entity);
                $em->flush();
                $this->get('session')->getFlashBag()->add('info',
                    'users.user.password_changed');
                if ($returnToShow) {
                    return $this->redirect($this->generateUrl('user_show',
                                array('id' => $id)));
                } else {
                    return $this->redirect($this->generateUrl('slx_gsprod_homepage'));
                }
            }
        }

        return array(
            'form' => $form->createView(),
            'uid' => $returnToShow ? $id : 0,
        );
    }

}
