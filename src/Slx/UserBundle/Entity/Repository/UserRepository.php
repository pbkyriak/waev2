<?php

namespace Slx\UserBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Description of UserRepository
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class UserRepository  extends EntityRepository
{
    public function getUsernameList() {
        $users = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u')
            ->from('SlxUserBundle:User', 'u')
            ->getQuery()
            ->getResult();
        $out = array();
        foreach($users as $user ) {
            $out[$user->getId()] = $user->getUsername();
        }
        return $out;

    }
}
