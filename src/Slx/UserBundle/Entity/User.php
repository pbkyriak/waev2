<?php
namespace Slx\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Description of User
 * 
 * @ORM\Table(name="slx_user")
 * @ORM\Entity(repositoryClass="Slx\UserBundle\Entity\Repository\UserRepository")
 * 
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class User 
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=25, unique=true)
     */
    private $username;
    
    /**
     * @ORM\Column(type="string", length=32)
     */
    private $salt;
    
    /**
     * @ORM\Column(type="string", length=40)
     */
    private $password;
    
    /**
     * @ORM\Column(type="string", length=60)
     */
    private $email;
    
    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;
    
    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users")
     */
    private $roles;
    
    /**
     *
     * @ORM\Column(type="integer")
     */
    private $userType;
    
    public function __construct()
    {
        $this->isActive=true;
        $this->salt = md5(uniqid(null, true));
        $this->roles = new ArrayCollection();
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function getSalt()
    {
        return $this->salt;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function getPassword()
    {
        return $this->password;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function getUsername()
    {
        return $this->username;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function getRoles() {
        return $this->roles;
    }

    public function getRolesAr() {
        $roles = array();
        foreach( $this->roles as $role ) {
            $roles[] = $role->getRole();
        }
        if( $this->getUserType()==2 ) { // super admin
            $roles = array('ROLE_SUPER_ADMIN');
        }
        if( $this->getUserType()==1 ) { // admin
            if( !in_array('ROLE_ADMIN', $roles)) {
                $roles[] = 'ROLE_ADMIN';
            }
        }
        if( $this->getUserType()==0 ) { // user
            if( !in_array('ROLE_USER', $roles)) {
                $roles[] = 'ROLE_USER';
            }
        }

        return $roles;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
    
        return $this;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    
        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
    
        return $this;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    
        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean 
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Add roles
     *
     * @param \Slx\UserBundle\Entity\Role $roles
     * @return User
     */
    public function addRole(\Slx\UserBundle\Entity\Role $roles)
    {
        $this->roles[] = $roles;
    
        return $this;
    }

    /**
     * Remove roles
     *
     * @param \Slx\UserBundle\Entity\Role $roles
     */
    public function removeRole(\Slx\UserBundle\Entity\Role $roles)
    {
        $this->roles->removeElement($roles);
    }
    
    /**
     * 
     * @return integer
     */
    public function getUserType() {
        return $this->userType;
    }
    
    /**
     * 
     * @param integer $value
     * @return \Slx\UserBundle\Entity\User
     */
    public function setUserType($value) {
        $this->userType = $value;
        return $this;
    }
}