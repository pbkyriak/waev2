<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class RoleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('role')
            ->add('save', 'submit', array('label'=>'gsprod.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\Role'
        ));
    }

    public function getName()
    {
        return 'slx_userbundle_roletype';
    }
}
