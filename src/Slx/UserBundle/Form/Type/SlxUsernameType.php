<?php

namespace Slx\UserBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Description of SlxUserType
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxUsernameType extends AbstractType
{

    private $om;

    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $users = $this->om->getRepository('SlxUserBundle:User')->getUsernameList();
        $choices = array();
        foreach($users as $username) {
            $choices[$username] = $username;
        }
        $resolver->setDefaults(array(
            'choices' => $choices,
        ));
    }

    public function getParent()
    {
        return 'choice';
    }

    public function getName()
    {
        return 'slx_userbundle_username';
    }

}
