<?php

namespace Slx\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email')
            ->add('isActive', null, array('required'=>false))
            ->add('userType', 'choice', array('label'=>'users.user.userType', 'choices'=>array(0=>'Χρήστης', 1=>'Διαχειριστής', 2=>'Πάνος')))
            ->add('roles', null, array('label'=>'users.user.roles', 'multiple'=>true))
            ->add('save', 'submit', array('label'=>'gsprod.general.save','attr'=>array('class'=>'btn blue')))
            ->add('saveAndAdd', 'submit', array('label'=>'gsprod.general.save_and_add','attr'=>array('class'=>'btn blue')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Slx\UserBundle\Entity\User'
        ));
    }

    public function getName()
    {
        return 'slx_userbundle_usertype';
    }
}
