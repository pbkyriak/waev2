<?php

namespace Slx\UserBundle\Security\User;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;

/**
 * Description of SlxUser
 *
 * @author Panos Kyriakakis <panos at salix.gr>
 */
class SlxUser implements UserInterface, EquatableInterface, \Serializable, AdvancedUserInterface
{
    private $id;
    private $username;
    private $password;
    private $salt;
    private $roles;
    private $isActive;
    
    public function __construct($username, $password, $salt, array $roles)
    {
        $this->username = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->isActive = true;
    }

    public function setId($id) 
    {
        $this->id = $id;
        return $this;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function setIsActive($value) {
        $this->isActive=$value;
        return $this;
    }
    
    public function getIsActive() {
        return $this->isActive;
    }
    
    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function eraseCredentials()
    {
        
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof SlxUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * 
     * @inheritDoc
     */
    public function serialize() {
        return serialize(array($this->username));
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function unserialize($serialized) {
        list( 
            $this->username,
            ) = unserialize($serialized);
    }

    /**
     * 
     * @inheritDoc
     */
    public function isAccountNonExpired()
    {
        return true;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function isAccountNonLocked()
    {
        return true;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }
    
    /**
     * 
     * @inheritDoc
     */
    public function isEnabled()
    {
        return $this->getIsActive();
    }
    
}

