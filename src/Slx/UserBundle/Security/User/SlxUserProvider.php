<?php

namespace Slx\UserBundle\Security\User;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManager;

class SlxUserProvider implements UserProviderInterface
{

    private $em;
    
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function loadUserByUsername($username)
    {
        $userData = null;
        // make a call to your webservice here
        $q = $this->em
            ->createQueryBuilder()
            ->select('u, r')
            ->from('SlxUserBundle:User', 'u')
            ->leftJoin('u.roles', 'r')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery();
        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $userData = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin SlxUserBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        // pretend it returns an array on success, false if there is no user

        if ($userData) {
            $id = $userData->getId();
            $password = $userData->getPassword();
            $salt = $userData->getSalt();
            $roles = $userData->getRolesAr();
            $user = new SlxUser($username, $password, $salt, $roles);
            $user->setId($id);
            $user->setIsActive($userData->getIsActive());
            return $user;
        }

        throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.',
            $username));
    }

    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof SlxUser) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.',
                get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class)
    {
        return $class === 'Slx\UserBundle\Security\User\SlxUser';
    }

    public function findUsers() {
        return $this->em->getRepository('SlxUserBundle:User')->findAll();
    }
}

