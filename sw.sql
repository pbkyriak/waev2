select  a.project_tag_id, h, s from
(select pm.project_tag_id,  metric as h
from project_metric as pm
left join project_tag as pt on (pm.project_tag_id=pt.id)
where pt.project_id=:pid and pm.metric_group_id=202 and ctype='tag' and metric_name='SWH:MGI:2' order by project_tag_id) a
left join
(select pm.project_tag_id,  metric as s
from project_metric as pm
left join project_tag as pt on (pm.project_tag_id=pt.id)
where pt.project_id=:pid and pm.metric_group_id=202 and ctype='tag' and metric_name='SWS:MGI:2' order by project_tag_id) b
on a.project_tag_id=b.project_tag_id
